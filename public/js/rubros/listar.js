$(document).ready(function() {
    oTable = $('#rubros').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ rubros filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ rubros",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ rubros)",
        }
    });
});