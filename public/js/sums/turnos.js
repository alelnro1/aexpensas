$(document).ready(function() {
    $('#nuevo-turno').on('click', function (e) {
        e.preventDefault();

        var turno =  $('#nueva-fila-turno').html();

        $('#listado-turnos').append(turno);
    });

    $('body').on('click', '#eliminar-turno', function (e) {
        e.preventDefault();

        $(this).closest('div.turno').remove();
    });

    mostrarTurnos();

    $('#requiere_reserva').on('click', function () {
        mostrarTurnos();
    });

    function mostrarTurnos() {
        if ($('#requiere_reserva').is(':checked')) {
            $('#turnos-div, #costo').show();
        } else {
            $('#turnos-div, #costo').hide();
        }
    }
});