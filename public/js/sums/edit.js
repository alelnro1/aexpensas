
$(document).ready(function(){
    $('#archivo').bind('change', function() {
        if(window.File && window.FileReader && window.FileList && window.Blob){
            var sizee = this.files[0].size;
            if(sizee > 2097152){
                alert('El Archivo es mayor al tamaño soportado');
                $('#archivo').val("");
            }
        }
    });
});