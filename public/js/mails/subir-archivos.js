/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'subir-adjuntos';

    $('#files').html( $('input[name=urls_archivos]').val() );

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|docx|doc|txt|xls|pdf|xlsx|csv|ppt|pptx|rar|zip|)$/i,
        maxFileSize: 9999000,
        headers:
        {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        done: function (e, data) {
            console.log(data);
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');

        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                .append($('<span/>').text(file.name));
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
    }).on('fileuploaddone', function (e, data) {
        var file = data.result,
            urls_archivos = $('input[name=urls_archivos]');

        if (file.url) {
            var link = $('<a>')
                .attr('target', '_blank')
                .prop('href', file.url),
                urls_con_nuevo_archivo = "",
                separador = "||";

            if (urls_archivos.val() == "") {
                separador = "";
            }

            urls_con_nuevo_archivo = urls_archivos.val() + separador + file.url;

            // Escribo en el val de urls_archivos todas las urls que estaban mas la del archivo que acabo de subir
            urls_archivos.val(urls_con_nuevo_archivo);
        } else if (file.error) {
            var error = $('<span class="text-danger"/>').text(file.error);
        }
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});