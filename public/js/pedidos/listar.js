$(document).ready(function() {
    oTable = $('#pedidos, #otros_pedidos').DataTable({
        responsive: true,
        columnDefs: [
            { orderable: false, targets: -1 },
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
        order: [[0, 'ASC']],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ pedidos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ pedidos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ pedidos)",
        }
    });
});