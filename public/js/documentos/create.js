$(document).ready(function(){
$('#tipo').on('change', function () {

    var tipo_documento = $('#tipo option:selected').text().toLowerCase();

    if (tipo_documento == "documento") { // si es documento tiene que mostrar para cargar archivo
        $('#contenedor_archivo, #consorcios').show();
    } else {
        $('#contenedor_archivo, #consorcios').hide();
    }
});

});
