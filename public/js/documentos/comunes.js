$(document).ready(function(){
    $('#archivo').bind('change', function() {
        if(window.File && window.FileReader && window.FileList && window.Blob){
            var sizee = this.files[0].size;
            if(sizee > 2097152){
                alert('El Archivo es mayor al tamaño soportado');
                $('#archivo').val("");
            }
        }
    });

    $(".validar_form").submit( function(){
        var check = $("input[type='checkbox']:checked").length;

        if (check == "" && $('#tipo :selected').html() == "Documento") {
            $('.error1').text("Seleccione al menos un consorcio");
            return false;
        }
    });

});
