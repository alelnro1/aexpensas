$(document).ready(function() {
    oTable = $('#documentos').DataTable({
        iDisplayLength: 25,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ documentos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ documentos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ documentos)",
        }
    });
    oTable
        .order( [ 0, 'desc' ])
        .draw();
});