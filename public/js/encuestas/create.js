$(document).ready(function() {
    $('#nueva-respuesta').on('click', function (e) {
        e.preventDefault();

        var respuesta =  $('#nueva-fila-respuesta').html();

        $('#listado-respuestas').append(respuesta);
    });

    $('body').on('click', '#eliminar-respuesta', function (e) {
        e.preventDefault();

        $(this).closest('div.respuesta').remove();
    });
});