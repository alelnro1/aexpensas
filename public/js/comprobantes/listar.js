$(document).ready(function() {
    oTable = $('.comprobantes').DataTable({
        responsive: true,
        iDisplayLength: 25,
        columnDefs: [
            { orderable: false, targets: -1 },
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
        order: [[0, 'desc']],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ comprobantes filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ comprobantes",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ comprobantes)",
        }
    });
});