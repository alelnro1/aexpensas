$(document).ready(function(){
    $("#domicilio").geocomplete();

    $('#expensa_fija').on('change', function () {
        var $expensa_fija = $('#expensa_fija option:selected').val();
        if ($expensa_fija == 1) { // si es documento tiene que mostrar para cargar archivo
            $('#contenedor_monto_fijo').show();
        } else {
            $('#contenedor_monto_fijo').hide();
        }
    });

    $('#archivo').bind('change', function() {
        if(window.File && window.FileReader && window.FileList && window.Blob){
            var sizee = this.files[0].size;
            if(sizee > 2097152){
                alert('El Archivo es mayor al tamaño soportado');
                $('#archivo').val("");
            }
        }
    });

    var $expensa_fija = $('#expensa_fija option:selected').val();
    if ($expensa_fija == 1) { // si es documento tiene que mostrar para cargar archivo
        $('#contenedor_monto_fijo').show();
    }

    // Mask del CUIT
    $("#cuit").mask("99-99999999-9");
    $("#cbu").mask("9999999999999999999999");
});