function sumar() {
    var suma = 0;
    $('.valor').each(function () {
        if ($(this).val() !== "") {
            suma += parseFloat($(this).val());
        }
    });

    $('#valor_total').val(suma);
}
$(document).ready(function () {


    $('.datetimepicker1').datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd/mm/yyyy'
    });

    $(document)
        .on('keyup', '.valor', function(){sumar()})

        .on('click', '#agregar-campo', function (e) {
            e.preventDefault();
            campo = '<div class="detalle">' +
                '<div>' +
                '<label for="" class="control-label col-md-4"></label>' +
                '<div class="row campos_valores">' +
                '<div class="col-md-3">' +
                '<input  type="text" class="campo form-control" name="campo[]" autocomplete="off"  />' +
                '</div>' +
                '<div class="col-md-2">' +
                '<div>' +
                '<input  type="number" step="0.01"  class=" valor form-control valor" name="valor[]  />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2">' +
                '&nbsp;' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            $("#campos").append(campo);
        })
        .on('click', '#removeButton', function (e) {
            e.preventDefault();
            if ($(".detalle").last().data("no-borrar") != 'no borrar') {
                $(".detalle").last().remove();
                sumar();
            } else {
                alert('No se pueden borrar todos los campos')
            }

        });

    $("#formulario").submit(function (e) {
        var hay_importes_vacios = false;

        $(".campos_valores input").each(function (index, element) {

            if ($(this).val() == "") {
                hay_campos_vacios = true;
                return false;
            }
            else {
                hay_campos_vacios = false;
            }
            if ($('#fecha').val() == "") {
                hay_campos_vacios = true;
            }
        });
        if (hay_campos_vacios) {
            alert('No puede haber campos vacios');
            e.preventDefault();
            return false;
        }
        if (!hay_campos_vacios) {
            $('#boton-submit').prop('disabled', true);
        }

    });


});