$(document).ready(function() {
    oTable = $('.gastos').DataTable({
        responsive: true,
        iDisplayLength: 25,
        columnDefs: [
            { orderable: false, targets: -1 },
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
        order: [[0, 'ASC']],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ gastos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ gastos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ gastos)",
        }
    });
});


