$(document).ready(function() {
    oTable = $('#unidadesfuncionales').DataTable({
        responsive: true,
        iDisplayLength: 25,
        order: [[0, 'asc']],
        columnDefs: [
            { orderable: false, targets: -1 },
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ unidades funcionales filtradas",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ Unidades Funcionales",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ unidades funcionales)",
        }
    });
});