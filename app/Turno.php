<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $table = 'turnos_sum';

    protected $fillable = [
        'sum_id', 'hora_inicio', 'hora_fin'
    ];

    public function Sum()
    {
        return $this->belongsTo(Sum::class);
    }

    public function Reservas()
    {
        return $this->hasMany(Reserva::class);
    }
}
