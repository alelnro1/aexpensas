<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consorcio extends Model {

	use SoftDeletes;

	protected $table = 'consorcios';
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	protected $fillable = [
	    'nombre', 'domicilio', 'administrador_id', 'expensa_fija','interes_vto_2','interes_vto_1','dia_vto_1','dia_vto_2',
        'email_encargado', 'tel_encargado', 'cod_pago_electronico', 'suterh', 'firma', 'email', 'nota_expensas',
        'redondeo', 'redondeo_0', 'precioXuf', 'cochera_complementaria'
    ];

    public function Gastos()
	{
		return $this->hasMany(Gasto::class);
	}

	public function Novedades()
	{
		return $this->belongsToMany(Novedad::class, 'consorcio_novedad', 'consorcio_id', 'novedad_id');
	}

	public function Documentos()
	{
		return $this->belongsToMany(Documento::class, 'consorcio_documento', 'consorcio_id', 'documento_id');
	}

	public function UnidadesFuncionales()
	{
		return $this->hasMany(UnidadFuncional::class);
	}

	public function Sums()
	{
		return $this->hasMany(Sum::class);
	}

	public function Administrador()
	{
		return $this->belongsTo(Administrador::class);
	}

	public function Consorcio()
    {
        return $this->hasMany(Liquidacion::class);
    }

    public function getCantidadDeEspaciosComunes()
    {
        return $this->Sums()->count();
    }

}