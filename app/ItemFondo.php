<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemFondo extends Model {

	use SoftDeletes;

	protected $table = 'items_fondos';
	public $timestamps = true;

	protected $dates = ['deleted_at'];

	public function Fondo()
	{
		return $this->belongsTo(Fondo::class);
	}

}