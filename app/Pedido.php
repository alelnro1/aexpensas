<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Pedido extends Model {
    use SoftDeletes;

    protected $table = 'pedidos';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'titulo', 'descripcion', 'unidad_funcional_id', 'estado_id', 'archivo'
    ];

    public function EstadoPedido()
    {
        return $this->belongsTo(EstadoPedido::class, 'estado_id');
    }

    public function UnidadFuncional()
    {
        return $this->belongsTo(UnidadFuncional::class);
    }

    public function damePedidosDeUsuario()
    {
        $controller = new Controller();

        return
            $this->whereHas('UnidadFuncional', function ($query) use ($controller) {
                $query->where('consorcio_id', $controller->getConsorcio())
                    ->whereHas('Usuarios', function ($query) {
                        $query->where('usuario_id', Auth::user()->id);
                    });
            })
                ->with('EstadoPedido')
                ->get();
    }

    public function damePedidosQueNoSeanDeUF()
    {
        $controller = new Controller();

        return
            $this->whereHas('UnidadFuncional', function ($query) use ($controller) {
                $query->where('consorcio_id', $controller->getConsorcio())
                    ->whereDoesntHave('Usuarios', function ($query) {
                        $query->where('usuario_id', '=', Auth::user()->id);
                    });
            })
                ->with([
                    'UnidadFuncional' => function ($query) {
                        $query->select(['id', 'consorcio_id', 'codigo', 'piso', 'dpto']);
                    },
                    'EstadoPedido'
                ])
                ->get();
    }

}