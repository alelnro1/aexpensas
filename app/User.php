<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Rol;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $table = "users";

    public function Rol()
    {
        return $this->hasOne(Rol::class, 'id', 'rol_id');
    }

    public function UnidadesFuncionales()
    {
        return $this->belongsToMany(UnidadFuncional::class, 'unidad_funcional_usuario', 'usuario_id', 'unidad_funcional_id');
    }

    public function UnidadesFuncionalesDeConsorcio($consorcio_id)
    {
        return $this->UnidadesFuncionales()->where('consorcio_id', $consorcio_id)->get();

    }

    public function Administrador()
    {
        return $this->belongsTo(Administrador::class);
    }

    public function hasRole($roles)
    {
        // Si el rol es super admin => pasa siempre
        if ($this->esSuperAdmin()) {
            return true;
        }

        if (is_array($roles)) {
            foreach ($roles as $need_role) {
                if ($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else {
            return $this->checkIfUserHasRole($roles);
        }

        return false;
    }

    public function tieneUnidadFuncional($id)
    {
        $uf = UnidadFuncional::findOrFail($id);
        $user = User::where('id', Auth::user()->id)->select(['id'])->with('UnidadesFuncionales')->first();

        return $user->UnidadesFuncionales->contains($uf);
    }

    public function dameUsuariosAdmin()
    {
        $rol = new Rol();
        $rol_id = $rol->idRolByNommbre('admin');
        return $this->where('rol_id', $rol_id)->get();

    }

    /**
     * @param $need_role
     * @return bool
     */
    private function checkIfUserHasRole($need_role)
    {
        return (strtolower($need_role) == strtolower($this->rol->nombre)) ? true : false;
    }

    /**
     * Verifico que el usuario actual sea admin
     * @return bool
     */
    public function esAdmin()
    {
        return session('USUARIO_ACTUAL')->rol->nombre == 'admin';
    }

    /**
     * Verifico que el usuario actual sea admin
     * @return bool
     */
    public function esSuperAdmin()
    {
        return session('USUARIO_ACTUAL')->rol->nombre == 'super admin';
    }

    /**
     * Verifico que el usuario actual sea admin
     * @return bool
     */
    public function esPropietario()
    {
        return session('USUARIO_ACTUAL')->rol->nombre == 'propietario';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'password', 'rol_id', 'administrador_id'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
