<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GastoUnidadFuncional extends Model {
    protected $table = 'gastos_unidades_funcionales';

    public static function cantidadDeUFsConGasto($id)
    {
        return GastoUnidadFuncional::where('gasto_id', '=', $id)->count();
    }

    public function UnidadFuncional()
    {
        return $this->hasOne(UnidadFuncional::class, 'unidad_funcional_id');
    }

    public function Gasto()
    {
        return $this->hasOne(Gasto::class, 'gasto_id');
    }
}