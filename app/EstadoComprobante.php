<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoComprobante extends Model {

	protected $table = 'estados_comprobantes';

	public function Comprobantes()
	{
		return $this->hasMany(Comprobante::class);
	}

}