<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoGasto extends Model
{
    protected $table = 'tipo_gastos';

    protected $fillable = [
        'descripcion','administrador_id'
    ];

    public function Gastos()
    {
        return $this->hasMany(Gasto::class);
    }
    public function Administrador()
    {
        return $this->belongsTo(Administrador::class);
    }

    public static function getId($descripcion,$administrador_id)
    {
        $tipo_gasto = TipoGasto::where('descripcion','like',$descripcion)->where('administrador_id',$administrador_id)->first();

        $id = $tipo_gasto->id;

        return $id;
    }
}
