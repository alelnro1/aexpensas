<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    protected $table = 'rubros';
    public $timestamps = false;

    public function Gastos()
    {

        return $this->hasMany(Gasto::class, 'rubro_id');
    }

    public function RubroPadre()
    {
        return $this->hasOne(Rubro::class, 'id', 'rubro_id');
    }

    public function RubrosHijos()
    {
        return $this->hasMany(Rubro::class, 'rubro_id', 'id');
    }

    public function dameLosRubrosPadres($fecha)
    {
        $controller = new Controller();

        return
            Rubro::where('rubro_id', null)
                ->with(['RubrosHijos' => function ($query) use ($fecha, $controller) {
                    $query->with(['Gastos' => function ($query) use ($fecha, $controller) {
                        $query->where('consorcio_id', $controller->getConsorcio());
                        $query->with([
                            'Cuotas' => function ($query) use ($fecha) {
                                $query->whereDate('fecha', '<=', $fecha)
                                    ->whereNull('liquidacion_id');
                            },
                            'TipoExpensa' => function ($query) {
                                $query->where('descripcion', '<>', 'Cuota Extra');
                            }
                        ]);

                        $query->whereHas('Cuotas', function ($query) use ($fecha) {
                            $query->whereDate('fecha', '<=', $fecha)
                                ->whereNull('liquidacion_id');
                        });

                        $query->whereHas('TipoExpensa', function ($query) {
                            $query->where('descripcion', '<>', 'Cuota Extra');
                        });
                    }]);
                }])
                ->get();
    }

    public static function getId($descripcion)
    {
        return $rubro_id =  Rubro::where('descripcion','like',$descripcion)->get()->first()->id;
    }
}