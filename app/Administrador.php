<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Administrador extends Model {

	use SoftDeletes;

	protected $table = 'administradores';
	public $timestamps = false;
	protected $dates = ['deleted_at'];

	protected $fillable = [
		'nombre', 'archivo', 'password', 'domicilio', 'email', 'telefono', 'cuit',
        'inscripcion_rpa', 'situacion_fiscal'
	];

	public function Consorcios()
	{
		return $this->hasMany(Consorcio::class,"administrador_id");
	}

	public function Novedades()
	{
		return $this->hasMany(Novedad::class);
	}

	public function Usuarios()
	{
		return $this->hasMany(User::class, 'administrador_id');
	}

	public function Proveedores()
	{
		return $this->hasMany(Proveedor::class,"administrador_id" );
	}

	public function TipoGastos()
	{
		return $this->hasMany(TipoGasto::class);
	}
}