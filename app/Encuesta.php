<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Encuesta extends Model
{
    use SoftDeletes;

    protected $table = 'encuestas';

    protected $fillable = [
        'pregunta', 'consorcio_id', 'estado'
    ];

    public function Consorcio()
    {
        return $this->belongsTo(Consorcio::class);
    }

    public function Respuestas()
    {
        return $this->hasMany(RespuestaEncuesta::class);
    }

    public function RespuestasDeUf()
    {
        return $this->hasManyThrough(RespuestaUnidadFuncional::class, RespuestaEncuesta::class, 'encuesta_id', 'respuesta_id');
    }

    public function dameEncuestasActivas($id=null)
    {
        $controller = new Controller();

        return
            Encuesta::where('estado', '1')
                ->where('consorcio_id', $controller->getConsorcio())
                ->where('id', '<>', $id)
                ->get();
    }

    public function tieneVotos()
    {
        return $this->RespuestasDeUf()->count() > 0;
    }

    public function dameEncuestasNoActivas()
    {
        $controller = new Controller();

        return
            Encuesta::where('estado', '0')
                ->where('consorcio_id', $controller->getConsorcio())
                ->get();
    }
}
