<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'nombre'
    ];

    public function Users()
    {
        return $this->hasMany(User::class);
    }

    public function idRolByNommbre($nombre)
    {
        return $this->where('nombre',$nombre)->first()->id;
    }
}
