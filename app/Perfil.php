<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perfil extends Model {

	use SoftDeletes;

	protected $table = 'perfiles';
	public $timestamps = true;
	protected $dates = ['deleted_at'];

	public function Usuarios()
	{
		return $this->hasMany(Usuario::class);
	}

}