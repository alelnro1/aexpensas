<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleSueldo extends Model
{
    use SoftDeletes;
    protected $table = 'detalles_sueldos';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'gasto_id', 'valor', 'campo'
    ];

    public function Gasto()
    {
        return $this->belongsTo(Gasto::class, 'gasto_id');
    }
}
