<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liquidacion extends Model
{
    protected $table = "liquidaciones";

    protected $fillable = [
        'consorcio_id', 'nombre_periodo', 'fecha_inicio', 'fecha_fin',
        'saldo_estado_financiero', 'saldo_movimientos_bancarios', 'saldo_estado_patrimonial', 'pdf'
    ];

    public function Comprobantes()
    {
        return $this->hasMany(Comprobante::class);
    }

    public function EstadosFinancieros()
    {
        return $this->hasMany(EstadoFinanciero::class);
    }

    public function Consorcio()
    {
        return $this->belongsTo(Consorcio::class);
    }

    public function dameLiquidacionesParaReportes($fecha_desde, $fecha_hasta, $consorcio_id)
    {
        $fecha_desde = str_replace('/', '-', $fecha_desde);
        $fecha_hasta = str_replace('/', '-', $fecha_hasta);

        return
            $this->whereDate('fecha_fin', '>=', $fecha_desde)
                ->whereDate('fecha_fin', '<=', $fecha_hasta)
                ->where('consorcio_id', $consorcio_id)
                ->orderBy('id', 'ASC')
                ->take(12)
                ->get();
    }

    public function dameTotalExpensasTipoALiquidadas()
    {

    }

    public function dameUltimaLiquidacionDeConsorcio($id)
    {
        return
            $this->where('consorcio_id', $id)
                ->orderBy('id', 'DESC')
                ->select(['id'])
                ->take(1)
                ->first();
    }
}
