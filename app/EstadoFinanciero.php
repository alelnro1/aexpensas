<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoFinanciero extends Model
{
    protected $table = "estados_financieros";

    protected $fillable = [
        'categoria_financiera_id', 'subcategoria_financiera_id', 'nombre', 'monto', 'consorcio_id', 'liquidacion_id'
    ];

    public function setSubcategoriaFinancieraIdAttribute($value) {
        $this->attributes['subcategoria_financiera_id'] = $value ?: null;
    }


    public function CategoriaFinanciera(){
        return $this->belongsTo(CategoriaFinanciera::class);
    }
}
