<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Empleado extends Model
{
    use SoftDeletes;
    protected $table = 'empleados';

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'consorcio_id', 'nombre', 'descripcion', 'legajo', 'sector', 'cuil', 'cargo', 'categoria', 'archivo', 'fecha', 'domicilio', 'email', 'telefono'
    ];

    public function Consorcio()
    {
        return $this->belongsTo(Consorcio::class, 'consorcio_id');
    }

    public function Gastos()
    {
        return $this->hasMany(Gasto::class);
    }

    public function tenesGastos()
    {
        return $this->Gastos()->count() > 0;
    }

}
