<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoReserva extends Model {
	use SoftDeletes;

	protected $table = 'estados_reservas';
	public $timestamps = true;

	protected $dates = ['deleted_at'];

	public function Reservas()
	{
		return $this->hasMany(Reserva::class);
	}

}