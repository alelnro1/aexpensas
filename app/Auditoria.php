<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Auditoria extends Model
{
    protected $table = "auditoria";

    protected $fillable = [
        'unidad_funcional_id', 'accion', 'saldo_anterior', 'saldo_nuevo', 'interes_anterior', 'interes_nuevo',
        'termino_anterior', 'termino_nuevo', 'deuda_anterior', 'deuda_nuevo', 'gastos_particulares_anterior',
        'gastos_particulares_nuevo', 'futuros_anterior', 'futuros_nuevo',
        'comprobante_id', 'liquidacion_id', 'estado_anterior_id', 'estado_nuevo_id', 'comprobante_importe'
    ];

    public function Liquidacion()
    {
        return $this->belongsTo(Liquidacion::class);
    }

    public function Comprobante()
    {
        return $this->belongsTo(Comprobante::class);
    }

    public function UnidadFuncional()
    {
        return $this->belongsTo(UnidadFuncional::class);
    }

    /* Mutators */
    public function setUnidadFuncionalIdAttribute($value) {
        $this->attributes['unidad_funcional_id'] = $value ?: null;
    }

    public function setComprobanteIdAttribute($value) {
        $this->attributes['comprobante_id'] = $value ?: null;
    }

    public function setEstadoAnteriorIdAttribute($value) {
        $this->attributes['estado_anterior_id'] = $value ?: null;
    }

    public function setEstadoNuevoIdAttribute($value) {
        $this->attributes['estado_nuevo_id'] = $value ?: null;
    }

    public function setLiquidacionIdAttribute($value) {
        $this->attributes['liquidacion_id'] = $value ?: null;
    }

    public function scopeComprobanteAprobado()
    {
        return "Comprobante Aprobado";
    }

    public function scopeComprobantePendiente()
    {
        return "Comprobante Pendiente";
    }

    public function scopeComprobanteRechazado()
    {
        return "Comprobante Rechazado";
    }

    public function scopeAplicacionDeInteres()
    {
        return "Aplicación de Interés";
    }

    public function scopeExpensaGenerada()
    {
        return "Liquidación de Expensa ";
    }

    public function dameMovimientosDeUf($id)
    {
        return
            $this->where(function ($query) use ($id) {
                $query->where('accion', 'LIKE', '%Liquidación de Expensa%')
                    ->where('unidad_funcional_id', $id)
                    ->orWhere('accion', '=', 'Comprobante Aprobado');
            })
                ->whereHas('UnidadFuncional', function($query) use ($id) {
                    $query->where('id', $id);
                })
                ->with([
                    'Liquidacion' => function ($query) {
                        $query->select(['id', 'nombre_periodo', 'pdf']);
                    }
                ])
                ->get();
    }
}
