<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sum extends Model {
	use SoftDeletes;

	protected $table = 'sums';
	public $timestamps = true;

	protected $fillable = [
			'nombre', 'consorcio_id', 'costo'
	];

	protected $dates = ['deleted_at'];

	public function Reservas()
	{
		return $this->hasMany(Reserva::class);
	}

	public function Consorcio()
	{
		return $this->belongsTo(Consorcio::class);
	}

	public function Turnos()
    {
        return $this->hasMany(Turno::class);
    }

}