<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumento extends Model
{
    protected $table = "tipo_documento";
    public function Documentos()
    {
        return $this->hasMany(Documento::class, "tipo_documento_id");
    }

}


