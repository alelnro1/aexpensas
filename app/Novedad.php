<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Novedad extends Model {

	use SoftDeletes;

	protected $table = 'novedades';
	public $timestamps = false;
	protected $dates = ['deleted_at'];

	public function Consorcios()
	{
		return $this->belongsToMany(Consorcio::class, 'consorcios_novedades', 'novedad_id', 'consorcio_id');
	}

	public function Administrador()
	{
		return $this->belongsTo(Administrador::class);
	}

}