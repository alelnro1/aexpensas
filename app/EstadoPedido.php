<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPedido extends Model {

	use SoftDeletes;

	protected $table = 'estados_pedidos';
	public $timestamps = true;

	protected $dates = ['deleted_at'];

	public function Pedidos()
	{
		return $this->hasMany(Pedido::class);
	}

}