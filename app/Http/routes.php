<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

$perfil = function() {
    Route::get('/cambiar-clave-personal', 'PerfilController@cambiarClaveForm');
    Route::post('/cambiar-clave-personal', 'PerfilController@actualizarClave');

    Route::get('perfil', 'PerfilController@verPerfil');
    Route::get('/perfil/edit', 'PerfilController@editarPerfil');
    Route::patch('/perfil/update', 'PerfilController@actualizarPerfil');
};


Route::post('/usuarios/store', 'UsuariosController@store');

Route::get('/', 'FrontendController@index');
Route::get('/register', 'FrontendController@register');
Route::get('/register/{email}/{codigo}', 'FrontendController@registerConEmail');
Route::get('contacto', 'FrontendController@contacto');
Route::post('contacto', 'FrontendController@enviarContacto');

// Contacto del Footer
Route::post('contactar-admin', 'FrontendController@contactarAdmin');

Route::post('login', 'Auth\AuthController@login');

Route::auth();

// Tiene que haber entrado al panel de administracion
Route::group(['prefix' => 'admin'], function () {
    Route::auth();

});

// Tiene que estar logueado
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () use($perfil) {
    // Verifica por ajax cada 60 segundos que la sesion esté viva
    Route::get('check-session', 'Auth\AuthController@checkSession');

    Route::get('/actualizar-consorcio/{id}', 'Controller@setConsorcioDesdeVista');

    Route::get('/', 'HomeController@index');

    // Tiene que ser propietario
    Route::group(['middleware' => ['roles:propietario']], function() {
        Route::get('nueva-unidad-funcional', 'UnidadesFuncionalesController@formAsociarNuevaUF');
        Route::get('mis-unidades-funcionales', 'UnidadesFuncionalesController@misUnidadesFuncionales');
        Route::get('mis-movimientos', 'UnidadesFuncionalesController@misMovimientos');
        Route::post('asociar-nueva-uf', 'UnidadesFuncionalesController@generarNuevaAsociacionConUF');
        Route::delete('desasociar-unidad-funcional/{id}', 'UnidadesFuncionalesController@desasociarUf');
    });

    Route::resource('comprobantes', 'ComprobantesController');

    Route::get('comprobantes/{id}/aceptar', 'ComprobantesController@aceptar');
    Route::get('comprobantes/{id}/rechazar', 'ComprobantesController@rechazar');
    Route::get('comprobantes/{id}/pendiente', 'ComprobantesController@ponerPendiente');
    Route::resource('pedidos', 'PedidosController');

    Route::group(['middleware' => ['roles:super admin']], function(){

        // Roles
        Route::group(['prefix' => 'roles'], function(){
            Route::get('create', 'RolesController@nuevo');
            Route::post('create', 'RolesController@create');
        });

        // Administradores
        Route::resource('administradores', 'AdministradoresController');
        Route::resource('usuario-admin', 'UsuariosAdminController');
    });

    // Tiene que ser admin
    Route::group(['middleware' => ['roles:admin']], function() {
        Route::resource('proveedores', 'ProveedoresController');
        Route::resource('consorcios', 'ConsorciosController');
        Route::resource('unidades-funcionales', 'UnidadesFuncionalesController', ['except' => 'show']);
        Route::resource('tipos-de-gastos', 'TipoGastosController');
        Route::resource('encuestas', 'EncuestasController', ['except' => 'show']);
        Route::get('gastos/cargar-sueldo', 'GastosController@cargarSueldo');
        Route::post('gastos/store-sueldo', 'GastosController@storeSueldo');
        Route::post('gastos/update-sueldo/{id}', 'GastosController@updateSueldo');

        Route::resource('gastos', 'GastosController');

        Route::get('gastos/clonar/{gasto_id}', 'GastosController@clonar');

        Route::get('/listado_usuarios', 'UsuariosController@listado_usuarios_para_admin');
        Route::get('/listado_usuarios_segun_uf/{unidad_funcional_id}/{unidad_funcional}', 'UsuariosController@listado_usuarios_segun_uf');

        Route::get('liquidaciones/nueva', 'LiquidacionesController@armarGastos');

        // Descargar PDF de Liquidaciones
        Route::post('liquidaciones/generar-pdf', 'LiquidacionesController@descargar');

        // Cargamos las subcategorias del estado financiero
        Route::get('liquidaciones/cargar-subcategorias/{padre_id}', 'LiquidacionesController@cargarSubCategoriasFinancieras');

        // Vamos guardando los items temporales
        Route::post('liquidaciones/guardar-items-temporales', 'LiquidacionesController@guardarItemsTemporales');

        // Elimino un item temporal del estado financiero
        Route::post('liquidaciones/eliminar-item-estado-financiero', 'LiquidacionesController@eliminarItemTemporalEstadoFinanciero');

        // Actualizamos los estados financieros cuando carga alguno nuevo
        Route::get('liquidaciones/actualizar-estado-financiero', 'LiquidacionesController@actualizarVistaDeEstadoFinanciero');

        // Modificar la fecha para filtrar los datos de las liquidaciones
        Route::post('liquidaciones/aplicar-fecha-limite', 'LiquidacionesController@aplicarFechaLimite');

        // Confirmar expensa
        Route::post('liquidaciones/confirmar-expensa', 'LiquidacionesController@confirmarExpensa');

        // ADMINISTRADORES SOLO PARA PROBAR TEST TEST TEST SIENDO admin
        /*********************** TEST **********************************/
        Route::resource('administradores', 'AdministradoresController');

        Route::get('reservas/{id}/aceptar', 'ReservasController@aceptar');
        Route::get('reservas/{id}/rechazar', 'ReservasController@rechazar');

        Route::get('enviar-mail-asociacion/{id}', 'UnidadesFuncionalesController@enviarMailAsociativo');
        Route::post('enviar-mail-asociacion/{id}', 'UnidadesFuncionalesController@procesarEnvioMailAsociativo');
    });

    // Tiene que ser admin ó propietario
    Route::group(['middleware' => ['roles:propietario,admin']], function() use($perfil) {
        $perfil();

        Route::get('unidades-funcionales/{id}', 'UnidadesFuncionalesController@show');

        Route::get('liquidaciones', 'LiquidacionesController@index');

        Route::resource('mails', 'MailsController');

        // Subida de adjuntos para enviar email
        Route::post('mails/subir-adjuntos', 'MailsController@subirAdjuntos');

        Route::get('sums/ver-reservas', 'ReservasController@index');
        Route::resource('sums', 'SumsController');

        Route::get('mejora-continua', 'MejorasController@nueva');
        Route::post('mejora-continua', 'MejorasController@store');

        Route::get('sums/{id}/reservar', 'ReservasController@reservar');
        Route::post('sums/{id}/reservar', 'ReservasController@procesarReserva');

        Route::get('reservas/{id}', 'ReservasController@show');
        Route::get('reservas/{id}/edit', 'ReservasController@edit');
        Route::patch('reservas/{id}', 'ReservasController@update');


        Route::get('exportar/comprobantes-zip', 'ExportacionesController@exportarComprobantesZipForm');
        Route::post('exportar/comprobantes-zip', 'ExportacionesController@exportarComprobantesZip');

        Route::get('exportar/comprobantes', 'ExportacionesController@exportarComprobantes');
        Route::get('exportar/gastos', 'ExportacionesController@exportarGastos');

        Route::resource('documentos', 'DocumentosController');
        route::get('empleados/baja/{id}','EmpleadosController@baja');
        Route::resource('empleados', 'EmpleadosController');

        Route::get('reservas/{id}/eliminar', 'ReservasController@eliminar');

        Route::get('reportes', 'ReportesController@index');
        Route::post('reportes', 'ReportesController@generar');
        Route::get('reportes/exportar', 'ExportacionesController@exportarReporte');

        Route::get('encuestas', 'EncuestasController@index');
        Route::post('votar', 'EncuestasController@votar');
        Route::get('encuestas/{id}', 'EncuestasController@show');

        Route::get('movimientos/{id}', 'UnidadesFuncionalesController@movimientosDeUf');

        Route::get('gastos', 'GastosController@index');
        Route::get('gastos/{id}', 'GastosController@show');
    });
});//Route::group(['middleware' => 'web'], function() {
//});