<?php

namespace App\Http\Controllers;

use App\Comprobante;
use App\Gasto;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ExportacionesController extends ReportesController
{
    public function __construct()
    {
        parent::__construct();
    }

    /*********************************
     * Se muestra el form para elegir fecha desde y fecha hasta para exportar los comprobantes
     */
    public function exportarComprobantesZipForm ()
    {
        return view('comprobantes/exportar-zip');
    }

    /*************************
     * Se genera el ZIP con los comprobantes del usuario actual
     */
    public function exportarComprobantesZip(Request $request)
    {
        $comprobantes = new Comprobante();

        $fecha_desde = date("Y-m-d H:i:s", strtotime($request->fecha_desde));
        $fecha_hasta = date("Y-m-d H:i:s", strtotime($request->fecha_hasta));

        if (Auth::user()->esAdmin())
            $comprobantes = $comprobantes->obtenerComprobantesDeAdmin();
        else
            $comprobantes = $comprobantes->obtenerComprobantesDePropietario();

        $comprobantes = $comprobantes->get();

        // Filtramos de todos los comprobantes para que esté entre las dos fechas
        $comprobantes =
            $comprobantes->filter(function($comprobante) use ($fecha_desde, $fecha_hasta) {
                $fecha_comprobante = date("Y-m-d H:i:s", strtotime($comprobante->fecha_transaccion));

                if ($fecha_comprobante >= $fecha_desde && $fecha_comprobante <= $fecha_hasta) {
                    if ($comprobante->recibo != "")
                        return true;
                }
            });

        if ($comprobantes->count() > 0) {
            $recibos = [];

            foreach ($comprobantes as $comprobante) {
                array_push($recibos, $comprobante->recibo);
            }

            $nombre =
                'recibos/zips/recibos-del-' . date("d-m-Y", strtotime($fecha_desde)) .'-al-'.date("d-m-Y", strtotime($fecha_hasta)).'-'.mt_rand().'.zip';

            Zipper::make($nombre)->add($recibos);

            return redirect($this->base_url . 'exportar/comprobantes-zip')->with(
                'zip', $nombre
            );
        } else {
            return redirect('/admin/exportar/comprobantes-zip')->with('sin_recibos', 'No hay recibos');
        }
    }

    public function exportarComprobantes()
    {
        $comprobantes = new Comprobante();

        if (Auth::user()->esAdmin())
            $comprobantes = $comprobantes->obtenerComprobantesDeAdmin();
        else
            $comprobantes = $comprobantes->obtenerComprobantesDePropietario();

        $comprobantes = $comprobantes->get();

        Excel::create('Comprobantes', function($excel) use ($comprobantes) {
            $excel->sheet('New sheet', function($sheet)  use ($comprobantes) {
                $sheet->loadView('xls.comprobantes', ['comprobantes' => $comprobantes]);
            });
        })->download('csv');
    }

    public function exportarGastos()
    {
        $gastos = new Gasto();

        $gastos = $gastos->getCuotasGastos();

        Excel::create('Gastos', function($excel) use ($gastos) {
            $excel->sheet('New sheet', function($sheet)  use ($gastos) {
                $sheet->loadView('xls.gastos', ['gastos' => $gastos]);
            });
        })->download('csv');
    }

    public function exportarReporte()
    {
        $liquidaciones = session('CONSORCIO_' . $this->getConsorcio() . '_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id);
        $ingresos = session('CONSORCIO_' . $this->getConsorcio() . '_INGRESOS_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id);
        $cuotas = session('CONSORCIO_' . $this->getConsorcio() . 'CUOTAS_FILTRADOS_USUARIO_' . Auth::user()->id);
        $gastos = session('CONSORCIO_' . $this->getConsorcio() . '_EGRESOS_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id);

        // Inicializo la liquidacion anterior
        $liquidacion_anterior = null;

        foreach ($liquidaciones as $liquidacion) {
            // La primer liquidacion no tiene una liquidacion anterior
            if ($liquidacion_anterior != null) {
                $liquidacion->saldo_anterior = $liquidacion_anterior->saldo_estado_financiero;
            }

            if (!array_key_exists($liquidacion->nombre_periodo, $ingresos)) {
                $ingresos[$liquidacion->nombre_periodo] = 0;
            } else {
                // Hay ingresos calculados para la liquidacion actual
                $ingresos[$liquidacion->nombre_periodo] = $ingresos[$liquidacion->nombre_periodo];
            }

            $liquidacion->ingresos_totales = $ingresos[$liquidacion->nombre_periodo];

            $liquidacion->egresos_tipo_a = $this->getEgresosTipo('A', $cuotas, $liquidacion);
            $liquidacion->egresos_tipo_b = $this->getEgresosTipo('B', $cuotas, $liquidacion);
            $liquidacion->egresos_tipo_c = $this->getEgresosTipo('C', $cuotas, $liquidacion);
            $liquidacion->egresos_tipo_particular = $this->getEgresosTipo('Particular', $cuotas, $liquidacion);
            $liquidacion->egresos_cuota_extra_asignacion = $this->getEgresosTipo('Cuota Extra', $cuotas, $liquidacion);

            $liquidacion->total_egresos =
                $liquidacion->egresos_tipo_a + $liquidacion->egresos_tipo_b
                + $liquidacion->egresos_tipo_c + $liquidacion->egresos_tipo_particular + $liquidacion->egresos_cuota_extra_asignacion;

            $liquidacion_anterior = $liquidacion;
        }

        Excel::create('Reporte', function($excel) use ($liquidaciones) {
            $excel->sheet('New sheet', function($sheet)  use ($liquidaciones) {
                $sheet->loadView('xls.reporte', ['liquidaciones' => $liquidaciones]);
            });
        })->download('csv');
    }
}
