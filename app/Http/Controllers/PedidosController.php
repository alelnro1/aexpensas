<?php

namespace App\Http\Controllers;

use App\Consorcio;
use App\EstadoPedido;
use App\UnidadFuncional;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Pedido;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PedidosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consorcio_id = $this->getConsorcio();
        $consorcio = Consorcio::find($consorcio_id);
        $pedidos = $consorcio->Pedidos;
        //$estados_pedidos = EstadoPedido::all();

        if (Auth::user()->esPropietario()) {
            $pedido = new Pedido();

            // Obtengo los pedidos del usuario actual del consorcio actual
            $mis_pedidos = $pedido->damePedidosDeUsuario(Auth::user()->id);

            // Obtengo los pedidos del usuario que no es el actual del consorcio actual
            $pedidos_otras_ufs = $pedido->damePedidosQueNoSeanDeUF(Auth::user()->id);

        } else {
            $pedidos =
                Pedido::whereHas('UnidadFuncional', function ($query) {
                    $query->where('consorcio_id', '=', $this->getConsorcio());
                })
                    ->with('EstadoPedido')
                    ->get();

            $mis_pedidos = $pedidos_otras_ufs = [];
        }

        return view('pedidos.listar', [
            'mis_pedidos' => $mis_pedidos,
            'pedidos_otras_ufs' => $pedidos_otras_ufs,
            'pedidos' => $pedidos
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->esPropietario()) {
            $usuario = User::where('id', Auth::User()->id)->with([
                'UnidadesFuncionales' => function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());
                }
            ])
                ->whereHas(
                    'UnidadesFuncionales', function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());

                }
                )
                ->first();
            $unidades_funcionales = $usuario->UnidadesFuncionales;
        }
        else{
            $unidades_funcionales = UnidadFuncional::where('consorcio_id', $this->getConsorcio())->get();
        }

        return view('pedidos.create')->with('unidades_funcionales',$unidades_funcionales);
    }


    public function getEstadoSegunDescripcion($descripcion){
        $estado_pedido =  EstadoPedido::where('descripcion', $descripcion)->first();
        return $estado_pedido->id;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'titulo'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'unidad_funcional_id' => 'required',
            'archivo' => 'max:2000'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'pedidos/create')->withErrors($validator)->withInput();

        $estado_id = $this->getEstadoSegunDescripcion('Pendiente');
        $request['estado_id'] =$estado_id;
        $pedido = Pedido::create($request->all());

        // Le enviamos un email a todas las UF y a los administradores
        // Busco todos los administradores que deben recibir el email
        //$admins = $this->getAdministradoresDeConsorcio();

        // Obtengo los usuarios de esa unidad funcional
        $unidad_funcional = $this->getUsuariosDeUF($pedido->unidad_funcional_id);

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $pedido);

        if ($validator) {
            // Elimino el pedido recien creado
            $pedido->delete();
            return redirect($this->base_url . 'pedidos/create')->withErrors($validator)->withInput();
        }

        // Si la UF tiene usuarios => le envío un mail a cada uno
        if (count($unidad_funcional->Usuarios) > 0) {
            $users = $unidad_funcional->Usuarios;

            Mail::send('emails.pedidos.pedido-realizado-uf', ['pedido' => $pedido], function ($mail) use ($users, $pedido) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                foreach ($users as $user) {
                    $mail->to($user->email, $user->nombre)
                        ->subject('Nuevo Pedido Realizado');
                }
            });
        }

        $admin_email = $pedido->UnidadFuncional->Consorcio->email;

        if (!empty($admin_email)) {
            // Le envío un mail a todos los admins del consorcio, diciendo que se creó un pedido
            Mail::send('emails.pedidos.pedido-realizado-admin', ['pedido' => $pedido], function ($mail) use ($admin_email, $pedido) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                $mail->to($admin_email, $pedido->UnidadFuncional->Consorcio->Administrador->nombre)
                    ->subject('Nuevo Pedido Realizado');
            });
        }

        return redirect($this->base_url . 'pedidos/')->with('pedido_creado', 'Pedido: "'. $request->titulo .'" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        $pedido->load([
            'EstadoPedido',
            'UnidadFuncional' => function ($query) {
                $query->select(['id', 'codigo', 'piso', 'dpto']);
            }
        ]);

        return view('pedidos.show')->with('pedido', $pedido);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = Pedido::findOrFail($id);
        $estados_pedidos = EstadoPedido::all();
        if(Auth::user()->esPropietario()){
            $usuario = User::where('id', Auth::User()->id)->with([
                'UnidadesFuncionales' => function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());
                }
            ])
                ->whereHas(
                    'UnidadesFuncionales', function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());

                }
                )
                ->first();
            $unidades_funcionales = $usuario->UnidadesFuncionales;
        }
        else{
            $unidades_funcionales = UnidadFuncional::where('consorcio_id', $this->getConsorcio())->get();
        }

        return view('pedidos.edit',array('pedido' => $pedido,'estados_pedidos' => $estados_pedidos,'unidades_funcionales' => $unidades_funcionales));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'titulo'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'unidad_funcional_id' => 'required',
            'archivo' => 'max:2000'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'pedidos/create')->withErrors($validator)->withInput();

        $pedido = Pedido::findOrFail($id);
        $estado_anterior = $pedido->estado;

        if($request['estado_descripcion'] == "") {
            $estado_id = $this->getEstadoSegunDescripcion('Pendiente');
        } else {
            $estado_id = $this->getEstadoSegunDescripcion($request->estado_descripcion);
        }

        $request->request->add(['estado_id' => $estado_id]);

        $pedido->update($request->except(['_token', 'archivo']));

        // Si cambia el estado => envío email a los usuarios de la UF
        if ($estado_anterior != $estado_id) {
            $unidad_funcional = $this->getUsuariosDeUF($pedido->unidad_funcional_id);

            // Si la UF tiene usuarios => le envío un mail a cada uno
            if (count($unidad_funcional->Usuarios) > 0) {
                $users = $unidad_funcional->Usuarios;

                Mail::send('emails.pedidos.cambio-de-estado-uf', ['pedido' => $pedido], function ($mail) use ($users, $pedido) {
                    $mail->from('no-reply@aexpensas.com', 'AExpensas');

                    foreach ($users as $user) {
                        $mail->to($user->email, $user->nombre)
                            ->subject('Pedido Actualizado');
                    }
                });
            }
        }

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $pedido);

        if ($validator) {
            return redirect($this->base_url . 'pedidos/' . $pedido->id . '/edit')->withErrors($validator)->withInput();
        }


        return redirect($this->base_url . 'pedidos/')->with('pedido_actualizado', 'Pedido: "'.$pedido->titulo.'" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = Pedido::findOrFail($id);

        $pedido->delete();

        return redirect( $this->base_url . 'pedidos/')->with('pedido_eliminado', 'Pedido: "'. $pedido->titulo .'" eliminado');
    }

    private function validarPedido($request)
    {
        $this->validate($request, [
            'titulo'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'unidad_funcional_id' => 'required|',
        ]);
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $cliente
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, Pedido $pedido)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $pedido->archivo = $archivo['url'];
                $pedido->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }


    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

}