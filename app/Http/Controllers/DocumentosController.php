<?php

namespace App\Http\Controllers;

use App\Administrador;
use App\Consorcio;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Documento;
use Illuminate\Support\Facades\Auth;
use App\TipoDocumento;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class DocumentosController extends Controller
{
    use SoftDeletes;
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentos = Consorcio::find(parent::getConsorcio())->Documentos()->orderBy('created_at', 'DESC')->get();

        $tipos = TipoDocumento::all();

        if (Auth::user()->esPropietario()) {
            return view('documentos.listar-uf',array('documentos' => $documentos,'tipos' => $tipos ));
        } else if (Auth::user()->esAdmin()) {
            return view('documentos.listar-admin',array('documentos' => $documentos,'tipos' => $tipos ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user = User::find(Auth::user()->id);
        $consorcios = Administrador::find($user->administrador_id)->Consorcios()->get();
        $tipos = TipoDocumento::all();
        return view('documentos.create',array('consorcios' => $consorcios,'tipos' => $tipos ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarDocumento($request);

        $documento = Documento::create($request->all());

        if($request->archivo != ""){
            $archivo = $this->subirArchivo($request);
            $documento->archivo = $archivo;
            $documento->save();
        }

        if ($documento->Tipo->descripcion == "Novedad") {
            // Si el documento está activo enviar mail
            if ($documento->estado == 1) {
                $this->enviarMailInformativo($documento);
            }
        } else {
            if ($documento->estado == 1) {
                $this->enviarMailInformativoDocumento($documento);
            }
        }

        $request->request->add([
            'consorcios_elegidos' => [$this->getConsorcio()]
        ]);

        $this->crearRelacionesDocumentoConConosorcio($request->consorcios_elegidos, $documento);

        return redirect('/admin/documentos/')->with('documento_creado', 'Documento: "' . $request->nombre . '" creado');
    }

    public function crearRelacionesDocumentoConConosorcio($consorcios_elegidos, $documento){
        if($consorcios_elegidos != ""){
            foreach($consorcios_elegidos as $consorcios_elegido){
                $documento->Consorcios()->attach($consorcios_elegido);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $documento = Documento::findOrFail($id);
        $consorcios_asociados = $documento->Consorcios()->get();
        $tipos = TipoDocumento::all();
        return view('documentos.show', array('documento' => $documento, 'consorcios_asociados' => $consorcios_asociados,'tipos' => $tipos ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find(Auth::user()->id);
        $consorcios = Administrador::find($user->administrador_id)->Consorcios()->get();
        $documento = Documento::findOrFail($id);
        $consorcios_asociados = $documento->Consorcios()->get();
        $tipos = TipoDocumento::all();
        return view('documentos.edit',array('consorcios' => $consorcios, 'documento' => $documento, 'consorcios_asociados' => $consorcios_asociados,'tipos' => $tipos));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarDocumentoUpdate($request);
        $documento = Documento::findOrFail($id);
        $documento->load('Tipo');

        $estado_anterior = $documento->estado;

        $documento->update($request->all());


        if ($documento->Tipo->descripcion == "Documento") {
            if($request->archivo != ""){
                $archivo = $this->subirArchivo($request);
                $documento->archivo = $archivo;
            }

            $documento->Consorcios()->detach();

            $this->crearRelacionesDocumentoConConosorcio($request->consorcios_elegidos, $documento);

            $documento->save();
        } else {
            // Si el documento está activo enviar mail
            if ($estado_anterior == 0 && $request->estado == 1) {
                $this->enviarMailInformativo($documento);
            }
        }

        return redirect('/admin/documentos/')->with('documento_actualizado', 'Documento: "' . $documento->nombre . '" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $documento = Documento::findOrFail($id);

        $documento->delete();

        return redirect('/admin/documentos/')->with('documento_eliminado', 'Documento: "' . $documento->nombre . '" eliminado');
    }

    private function validarDocumento($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',

        ]);
        if($request->tipo == 1){
            $this->validate($request, [
                'archivo' => 'required',
            ]);
            }
    }

    private function validarDocumentoUpdate($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
        ]);
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

    private function enviarMailInformativo($novedad)
    {
        $ufs = $this->getUsuariosDeConsorcio();

        Mail::send('emails.nueva-novedad', ['novedad' => $novedad], function ($mail) use ($ufs) {
            $mail->from('no-reply@aexpensas.com', 'AExpensas');

            foreach ($ufs as $uf) {
                $mail->bcc($uf->email, $uf->nombre)
                    ->subject('AExpensas. Novedad de Consorcio');
            }
        });
    }

    private function enviarMailInformativoDocumento($documento)
    {
        $ufs = $this->getUsuariosDeConsorcio();

        Mail::send('emails.nuevo-documento', ['documento' => $documento], function ($mail) use ($ufs) {
            $mail->from('no-reply@aexpensas.com', 'AExpensas');

            foreach ($ufs as $uf) {
                $mail->bcc($uf->email, $uf->nombre)
                    ->subject('AExpensas. Documento de Consorcio');
            }
        });
    }

}