<?php

namespace App\Http\Controllers;

use App\Administrador;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PerfilController extends Controller
{
    /**
     * Mostrar el perfil del usuario actual
     */
    public function verPerfil()
    {
        $usuario = Auth::user();
        $usuario->load('UnidadesFuncionales');
        $usuario->load(['administrador' => function($query) {
            $query->with(['consorcios']);
        }]);

        // Si es admin le acorto el domicilio de los consorcios para que aparezca "más lindo"
        if (Auth::user()->esAdmin()) {
            $usuario = $this->acortarDomicilio($usuario);
        }

        return view('auth.perfil', array('usuario' => $usuario));
    }

    private function acortarDomicilio($usuario)
    {
        // Recorro los consorcios del administrador
        foreach ($usuario->administrador->consorcios as $consorcio) {
            // Acorto el domicilio
            $consorcio->domicilio = $this->acortarString($consorcio->domicilio, 30);
        }

        return $usuario;
    }

    /**
     * Editar el perfil actual
     */
    public function editarPerfil()
    {
        $usuario = Auth::user();
        return view('auth.editar-perfil', array('usuario' => $usuario));
    }

    /**
     * Actualizar el perfil con los campos ingresados (y foto si la hay)
     */
    public function actualizarPerfil(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => 'required|email|max:255',
            'telefono' => 'max:60',
            'archivo'  => 'file|max:1000|mimes:jpg,jpeg,png',
        ]);

        $validator->after(function($validator) use ($request) {
            // Obtengo todos los usuarios con mail igual al ingresado
            $usuarios_con_mail_igual  = User::where('email', $request->email)->select(['id'])->get();

            // Si algun id es distinto al del usuario actual => el email ya existe
            foreach ($usuarios_con_mail_igual as $usuario_con_mail_igual) {
                if ($usuario_con_mail_igual->id != Auth::user()->id) {
                    $validator->errors()->add('email', 'El email elegido pertenece a otro usuario');
                }
            }
        });

        if ($validator->fails()) {
            return redirect('admin/perfil/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $usuario = Auth::user();

        // Actualizar el usuario
        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->email    = $request->email;
        $usuario->telefono = $request->telefono;

        $usuario->save();

        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $usuario->archivo = $archivo['url'];
                $usuario->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return redirect('admin/perfil/edit')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        return redirect('admin/perfil')->with('perfil_actualizado', 'Su perfil se actualizó');
    }

    /**
     * Mostrar el formulario para cambiar la contraseña
     */
    public function cambiarClaveForm()
    {
        return view('auth.cambiar-clave');
    }

    /**
     * Actualizar la contraseña
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function actualizarClave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password'  => 'required',
            'password'      => 'required|min:6|confirmed',
        ]);

        // Busco el email y contraseña del usuario actual
        $usuario = User::where('id', '=', Auth::user()->id)->first(['id', 'email', 'password']);

        // Valido que la contraseña vieja sea la actual
        $validator->after(function($validator) use ($request, $usuario) {
            if (!Hash::check($request->old_password, $usuario->password)){
                $validator->errors()->add('old_password', 'Contraseña actual incorrecta');
            }
        });

        if ($validator->fails()) {
            return redirect('admin/cambiar-clave-personal')
                ->withErrors($validator)
                ->withInput();
        }

        // Todo funcionó => cambio la contraseña
        $nueva_pass = Hash::make($request->password);

        $usuario->password = $nueva_pass;

        $usuario->save();

        // Ciero la sesion
        Auth::logout();

        return redirect( $this->base_url . 'login');
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->getSize()) {
                if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                    $url = $directorio_destino . $nombre_archivo;
                    $error = false;
                } else {
                    $url = false;
                    $error = "No se pudo mover el archivo";
                }
            } else {
                $url = false;
                $error = "Archivo demasiado grande";
            }
        } else {
            $url = false;
            //$error = $request->archivo->getErrorMessage();
            $error = "El archivo debe ser menor a 2MB";
        }

        return array('url' => $url, 'err' => $error);
    }
}
