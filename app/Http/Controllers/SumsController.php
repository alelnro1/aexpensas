<?php

namespace App\Http\Controllers;

use App\Consorcio;
use App\Turno;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Sum;
use Illuminate\Support\Facades\Validator;

class SumsController extends Controller
{
    use SoftDeletes;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sums = Consorcio::find(parent::getConsorcio())->Sums()->get();
        return view('sums.listar')->with('sums', $sums);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('sums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required|max:100',
            'descripcion'   => 'max:500',
            'archivo'       => 'max:1000|mimes:jpg,jpeg,png,gif',
            'costo'         => 'numeric'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'sums/create')->withErrors($validator)->withInput();

        // Pruebo si tiene que poner turnos
        if (isset($request->requiere_reserva) && $request->requiere_reserva == "on") {
            $validator = Validator::make($request->all(), [
                'horas_inicio.*'  => 'required',
                'horas_fin.*'     => 'required'
            ]);

            if ($validator->fails())
                return redirect($this->base_url . 'sums/create')->withErrors($validator)->withInput();
        }

        $request['consorcio_id'] = parent::getConsorcio();

        $sum = Sum::create($request->except('_token'));
        $sum->descripcion = $request->descripcion;

        if($request->archivo != ""){
            $archivo = $this->subirArchivo($request);
            $sum->archivo = $archivo;
        }

        $sum->save();

        $this->relacionarTurnosConSum($sum, $request);

        return redirect('/admin/sums/')->with('sum_creado', 'Espacio Común "'. $request->nombre .'" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sum = Sum::findOrFail($id);
        $sum->load('Reservas');

        return view('sums.show')->with('sum', $sum);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sum = Sum::findOrFail($id);
        return view('sums.edit')->with('sum', $sum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required|max:100',
            'descripcion'   => 'max:500',
            'archivo'       => 'max:1000|mimes:jpg,jpeg,png,gif',
            'costo'         => 'numeric'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'sums/' . $id . '/edit')->withErrors($validator)->withInput();

        // Pruebo si tiene que poner turnos
        if (isset($request->requiere_reserva) && $request->requiere_reserva == "on") {
            $validator = Validator::make($request->all(), [
                'horas_inicio.*'  => 'required',
                'horas_fin.*'     => 'required'
            ]);

            if ($validator->fails())
                return redirect($this->base_url . 'sums/' . $id . '/edit')->withErrors($validator)->withInput();
        }

        $sum = Sum::findOrFail($id);
        $sum->load('Turnos');

        $sum->nombre = $request->nombre;
        $sum->descripcion = $request->descripcion;
        $sum->costo = $request->costo;

        if($request->archivo != ""){
            $archivo = $this->subirArchivo($request);
            $sum->archivo = $archivo;
        }

        $sum->save();

        $sum->Turnos()->whereDoesntHave('Reservas', function ($query) use ($sum) {
            $query->where('sum_id', $sum->id);
        })->delete();

        $this->relacionarTurnosConSum($sum, $request);

        return redirect('/admin/sums/')->with('sum_actualizado', 'Espacio Común "'. $sum->nombre .'" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sum = Sum::findOrFail($id);

        $sum->delete();

        return redirect('/admin/sums/')->with('sum_eliminado', 'Espacio Común: "'. $sum->nombre .'" eliminado');
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

    private function relacionarTurnosConSum($sum, $request)
    {
        // Le adjunto los turnos al sum
        $horas_inicio = $request->horas_inicio;
        $horas_fin = $request->horas_fin;

        for ($i = 0; $i < count($horas_inicio); $i++) {
            // Busco que no existan los turnos
            $turno =
                Turno::where('sum_id', $sum->id)
                    ->where('hora_inicio', $horas_inicio[$i])
                    ->where('hora_fin', $horas_fin[$i])
                    ->first();

            if (!$turno) {
                Turno::create([
                    'sum_id' => $sum->id,
                    'hora_inicio' => $horas_inicio[$i],
                    'hora_fin' => $horas_fin[$i]
                ]);
            }
        }
    }

}