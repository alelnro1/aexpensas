<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MejorasController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function nueva()
    {
        return view('mejoras.nueva');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email'        => 'required|email',
            'titulo'      => 'required|max:100',
            'descripcion' => 'required|max:500',
        ]);

        $email = $request->email;
        $titulo = $request->titulo;
        $descripcion = $request->descripcion;
        $user = Auth::user();

        Mail::send('emails.mejora-solicitada', ['email' => $email, 'titulo' => $titulo, 'descripcion' => $descripcion, 'user' => $user], function ($mail) {
            $mail
                ->from('no-reply@aexpensas.com', 'AExpensas')
                ->to('info@doublepoint.com.ar', 'Info')
                ->subject('Solicitud de Funcionalidad');
        });

        return redirect($this->base_url . 'mejora-continua')->with('mejora_solicitada', true);
    }
}
