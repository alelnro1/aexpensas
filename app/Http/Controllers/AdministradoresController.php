<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Administrador;
use Illuminate\Support\Facades\Validator;

class AdministradoresController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $administradores = Administrador::all();
        return view('administradores.listar')->with('administradores', $administradores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administradores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'archivo' => 'required|max:2500',
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'administradores/create')->withErrors($validator)->withInput();

        // Creo el comprobante
        $administrador = Administrador::create($request->except(['archivo']));

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $administrador);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'administradores/create')->withErrors($validator)->withInput();
        }

        return redirect($this->base_url . 'administradores')->with('administrador_creado', 'Administrador: "' . $request->nombre . '" creado');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $administrador = Administrador::findOrFail($id);
        return view('administradores.show')->with('administrador', $administrador);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $administrador = Administrador::findOrFail($id);
        return view('administradores.edit')->with('administrador', $administrador);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Busco el administrador
        $administrador = Administrador::findOrFail($id);

        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'administradores/' . $id . '/edit')->withErrors($validator)->withInput();

        // Actualizo el comprobante
        $administrador->update($request->except(['archivo', '_token']));

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $administrador);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'administradores/' . $id . '/edit')->withErrors($validator)->withInput();
        };

        return redirect($this->base_url . 'administradores/' . $administrador->id)->with('administrador_actualizado', 'Administrador "' . $administrador->nombre . '" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $administrador = Administrador::findOrFail($id);

        if(count($administrador->Consorcios) == 0){
            $administrador->delete();
            return redirect($this->base_url . 'administradores/')->with('administrador_eliminado', 'Eñ Administrador: "' . $administrador->nombre . '" NO fue eliminado');

        }
        return redirect($this->base_url . 'administradores/')->with('administrador_no_eliminado', 'Administrador: "' . $administrador->nombre . '" eliminado');

    }

    private function validarAdministrador($request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo' => 'required|max:1000|mimes:jpg,jpeg,png,gif',
            'fecha' => 'required|date',
            'password' => 'required|confirmed|min:6',
            'domicilio' => 'required',
            'email' => 'required|email|max:100',
            'telefono' => 'required'
        ]);
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $cliente
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $administrador)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $administrador->archivo = $archivo['url'];
                $administrador->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }


    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

}