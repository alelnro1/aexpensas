<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Rubro;

class RubrosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rubros = Rubro::all();
        return view('rubros.listar')->with('rubros', $rubros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rubros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarRubro($request);

        $archivo = $this->subirArchivo($request);

        $rubro = Rubro::create($request->all());

        $rubro->archivo = $archivo;
        $rubro->save();

        return redirect('/rubros/')->with('rubro_creado', 'Rubro: "' . $request->nombre . '" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rubro = Rubro::findOrFail($id);
        return view('rubros.show')->with('rubro', $rubro);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubro = Rubro::findOrFail($id);
        return view('rubros.edit')->with('rubro', $rubro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarRubro($request);

        $rubro = Rubro::findOrFail($id);

        $archivo = $this->subirArchivo($request);

        $rubro->update($request->all());

        $rubro->archivo = $archivo;
        $rubro->save();

        return redirect('/rubros/' . $rubro->id)->with('rubro_actualizado', 'Rubro: "'. $rubro .'" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rubro = Rubro::findOrFail($id);

        $rubro->delete();

        return redirect('/rubros/')->with('rubro_eliminado', 'Rubro: "' . $rubro->nombre . '" eliminado');
    }

    private function validarRubro($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo'     => 'required|max:1000|mimes:jpg,jpeg,png,gif',
            'fecha'       => 'required|date',
            'password'    => 'required|confirmed|min:6',
            'domicilio'   => 'required',
            'email'       => 'required|email|max:100',
            'telefono'    => 'required'
        ]);
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}