<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\UnidadFuncional;
use App\Consorcio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class UnidadesFuncionalesController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unidades_funcionales = Consorcio::find($this->getConsorcio())->UnidadesFuncionales()->get();

        $porcentajes = $this->getSumaPorcentajesAyB($unidades_funcionales);

        if (
            number_format($porcentajes['a'], 2) != number_format(100, 2) ||
            number_format($porcentajes['b'], 2) != number_format(100, 2)
        ) {
            Session::put('total_porcentaje_a_' . $this->getConsorcio(), $porcentajes['a']);
            Session::put('total_porcentaje_b_' . $this->getConsorcio(), $porcentajes['b']);
        } else {
            Session::forget('total_porcentaje_a_' . $this->getConsorcio());
            Session::forget('total_porcentaje_b_' . $this->getConsorcio());
        }

        return view('unidades_funcionales.listar', array('unidades_funcionales' => $unidades_funcionales));
    }

    public function misUnidadesFuncionales()
    {
        $unidades_funcionales = Auth::User()->UnidadesFuncionalesDeConsorcio($this->getConsorcio());
        return view('unidades_funcionales.listar', array('unidades_funcionales' => $unidades_funcionales));
    }

    public function desasociarUf($id)
    {
        Auth::user()->UnidadesFuncionales()->detach($id);
        if (count(Auth::User()->UnidadesFuncionalesDeConsorcio($this->getConsorcio())) > 0) {

            return redirect($this->base_url . 'mis-unidades-funcionales');
        } else {
            Session::forget('CONSORCIO');

            if (count(Auth::User()->UnidadesFuncionales()->get()) > 0) {
                // $this->setConsorcio(null);
                Auth::logout();

                return redirect('/admin');
            } else {
                $user = User::find(Auth::User()->id);

                $user->email = $user->email . rand(111111, 999999) . '_' . time();
                $user->save();
                //$this->setConsorcio(null);
                Auth::logout();

                $user->delete();

                return redirect('/');
            }

        }
    }

    /**
     * Se obtienen los movimientos del usuario actual
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function misMovimientos() {
        $movimientos =
            Auditoria::where(function ($query) {
                $query->where('accion', 'LIKE', '%Liquidación de Expensa%')
                    ->orWhere('accion', '=', 'Comprobante Aprobado');
            })
                ->whereHas('UnidadFuncional', function($query) {
                    $query->whereHas('Usuarios', function($query) {
                        $query->where('usuario_id', Auth::user()->id);
                    });
                })
                ->with([
                    'Liquidacion' => function ($query) {
                        $query->select(['id', 'nombre_periodo', 'pdf']);
                    }
                ])
                ->get();

        return view('movimientos.show', [
            'movimientos' => $movimientos
        ]);
    }

    /**
     * Se obtienen los movimientos de la unidad funcional actual
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function movimientosDeUf($uf)
    {
        $auditoria = new Auditoria();
        $movimientos = $auditoria->dameMovimientosDeUf($uf);

        return view('movimientos.show', [
            'movimientos' => $movimientos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades_funcionales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['consorcio_id'] = Consorcio::find(parent::getConsorcio())->id;
        $this->validarUnidadfuncional($request);
        $unidad_funcional = UnidadFuncional::create($request->all());
        $unidad_funcional->piso = $request->piso;
        $unidad_funcional->mtrs_cubiertos = $request->mtrs_cubiertos;
        $unidad_funcional->mtrs_descubiertos = $request->mtrs_descubiertos;
        $unidad_funcional->nombre = $request->nombre;
        $unidad_funcional->apellido = $request->apellido;
        $unidad_funcional->dpto = $request->dpto;
        $unidad_funcional->telefono = $request->telefono;
        $unidad_funcional->descripcion = $request->descripcion;
        $unidad_funcional->codigo_asociacion = $unidad_funcional->id . rand(111111, 999999) . time();
        $unidad_funcional->cochera = $request->cochera;
        $unidad_funcional->save();

        $this->actualizarSesionConSumaPorcentajes();

        return redirect($this->base_url . 'unidades-funcionales')->with('unidadfuncional_creado', 'Unidad funcional con código: ' . $request->codigo . ' creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unidad_funcional = UnidadFuncional::findOrFail($id);
        $movimientos =
            Auditoria::where('unidad_funcional_id', $id)
                ->where(function ($query) {
                    $query->where('accion', 'LIKE', '%Liquidación de Expensa%')
                        ->orWhere('accion', '=', 'Comprobante Aprobado');
                })
                ->with([
                    'Liquidacion' => function ($query) {
                        $query->select(['id', 'nombre_periodo', 'pdf']);
                    }
                ])
                ->get();

        return view('unidades_funcionales.show', [
            'unidad_funcional' => $unidad_funcional,
            'movimientos' => $movimientos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unidad_funcional = UnidadFuncional::findOrFail($id);
        return view('unidades_funcionales.edit')->with('unidad_funcional', $unidad_funcional);
    }

    public function formAsociarNuevaUF()
    {
        $unidades_funcionales =
            UnidadFuncional::where('consorcio_id', $this->getConsorcio())
                ->whereHas('Usuarios', function ($query) {
                    $query->where('usuario_id', '<>', Auth::user()->id);
                })
                ->get();

        return view('unidades_funcionales.asociar-nueva-uf', array('unidades_funcionales', $unidades_funcionales));
    }

    public function generarNuevaAsociacionConUF(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'codigo' => 'required|exists:unidades_funcionales,codigo_asociacion'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Busco la unidad funcional
        $unidad_funcional =
            UnidadFuncional::where('codigo_asociacion', $request->codigo)
                ->select(['id'])
                ->with('Usuarios')
                ->first();

        // Busco que no exista la relacion
        $validator->after(function ($validator) use ($request, $unidad_funcional) {
            if ($unidad_funcional->Usuarios->contains(Auth::user()->id)) {
                $validator->errors()->add('codigo', 'La Unidad Funcional ya está asociada');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Genero la relación
        Auth::user()->UnidadesFuncionales()->attach($unidad_funcional);

        return redirect()->back()->with(['uf_asociada' => 'La Unidad Funcional se asoció correctamente']);
    }

    public function enviarMailAsociativo($id)
    {
        $uf = UnidadFuncional::findOrFail($id);

        return view('unidades_funcionales.enviar-mail-asociacion', ['id' => $id, 'uf' => $uf]);
    }

    public function procesarEnvioMailAsociativo(Request $request, $id)
    {
        $uf = UnidadFuncional::findOrFail($id);
        $uf->load('Consorcio');

        $email = $request->email;

        Mail::send('emails.unidades-funcionales.email-asociativo', ['uf' => $uf, 'email' => $email], function ($mail) use ($email) {
            $mail->from('no-reply@aexpensas.com', 'AExpensas')
                ->to($email)
                ->subject('Cree su cuenta en AExpensas');
        });

        return redirect( $this->base_url . 'unidades-funcionales')->with('email_asociativo_enviado', 'Email enviado');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarUnidadfuncional($request, $id);

        $unidad_funcional = UnidadFuncional::findOrFail($id);
        $unidad_funcional->update($request->all());

        $unidad_funcional->piso = $request->piso;
        $unidad_funcional->mtrs_cubiertos = $request->mtrs_cubiertos;
        $unidad_funcional->mtrs_descubiertos = $request->mtrs_descubiertos;
        $unidad_funcional->nombre = $request->nombre;
        $unidad_funcional->apellido = $request->apellido;
        $unidad_funcional->telefono = $request->telefono;
        $unidad_funcional->dpto = $request->dpto;
        $unidad_funcional->descripcion = $request->descripcion;
        $unidad_funcional->cochera = $request->cochera;
        $unidad_funcional->save();

        $this->actualizarSesionConSumaPorcentajes();

        return redirect($this->base_url . 'unidades-funcionales')->with('unidadfuncional_actualizado', 'Unidad funcional con código: ' . $unidad_funcional->codigo . ', fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unidadfuncional = UnidadFuncional::findOrFail($id);

        $unidadfuncional->delete();

        $this->actualizarSesionConSumaPorcentajes();

        return redirect($this->base_url . 'unidades-funcionales')->with('unidadfuncional_eliminado', 'Unidad funcional con código ' . $unidadfuncional->codigo . ' eliminada');
    }

    private function validarUnidadfuncional($request, $id = null)
    {
        $this->validate($request, [
            'codigo' => 'required|numeric|unique:unidades_funcionales,codigo,' . $id . ',id,consorcio_id,' . $this->getConsorcio(),
            'piso' => 'max:10',
            'mtrs_descubiertos' => 'max:100',
            'mtrs_cubiertos' => 'max:100',
            'descripcion' => 'max:500',
            'porcentaje_a' => 'required|max:100',
            'porcentaje_b' => 'required|max:100',
            'nombre' => 'max:20',
            'apellido' => 'max:20',
            'telefono' => 'max:20',
        ]);
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

    private function actualizarSesionConSumaPorcentajes()
    {
        // Actualizo la sesion con los porcentajes
        $unidades_funcionales = Consorcio::find($this->getConsorcio())->UnidadesFuncionales()->get();
        $porcentajes = $this->getSumaPorcentajesAyB($unidades_funcionales);

        if ($porcentajes['a'] != 100 || $porcentajes['b'] != 100) {
            Session::put('total_porcentaje_a_' . $this->getConsorcio(), $porcentajes['a']);
            Session::put('total_porcentaje_b_' . $this->getConsorcio(), $porcentajes['b']);
        }
    }

}