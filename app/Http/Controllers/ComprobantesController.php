<?php

namespace App\Http\Controllers;

use App\Administrador;
use App\Classes\Comprobantes\ComprobanteControl;
use App\Classes\Comprobantes\AceptarComprobante;
use App\Classes\Comprobantes\PendienteComprobante;
use App\Classes\Comprobantes\RechazarComprobante;
use App\Consorcio;
use App\EstadoComprobante;
use App\MedioDePago;
use App\UnidadFuncional;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Comprobante;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class ComprobantesController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comprobantes = new Comprobante();

        if (Auth::user()->esAdmin())
            $comprobantes = $comprobantes->obtenerComprobantesDeAdmin();
        else
            $comprobantes = $comprobantes->obtenerComprobantesDePropietario();

        $comprobantes = $comprobantes->get();

        $comprobantes_agrupados = [];
        $totales = [];

        // Recorro para agrupar por mes
        foreach ($comprobantes as $comprobante) {
            $mes_anio = date("Y-m", strtotime($comprobante->fecha_transaccion));
            $comprobantes_agrupados[$mes_anio][] = $comprobante;

            // Solo sumo al total si no está rechazado
            if ($comprobante->Estado->descripcion != "Rechazado") {

                if (!isset($totales[$mes_anio])) { // Para entrar a la primer cuota del array
                    $totales[$mes_anio] = $comprobante->importe;
                } else {
                    $totales[$mes_anio] = $totales[$mes_anio] + $comprobante->importe;
                }
            }
        }

        ksort($comprobantes_agrupados);

        $comprobantes_agrupados = array_reverse($comprobantes_agrupados, true);

        return view('comprobantes.listar', [
            'comprobantes_agrupados' => $comprobantes_agrupados,
            'totales' => $totales
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades_funcionales = $this->getUnidadesFuncionales();
        $medios_de_pagos      = $this->getMediosDePagos();

        return view('comprobantes.create', array(
                'unidades_funcionales' => $unidades_funcionales,
                'medios_de_pagos'=> $medios_de_pagos)
        );
    }

    private function getMediosDePagos()
    {
        return MedioDePago::all();
    }

    /**
     * Get dinamico, de las unidades funcionales del consorcio actual, ya sea para propietario o para UF
     * @return mixed
     */
    private function getUnidadesFuncionales()
    {
        $consorcio_id = $this->getConsorcio();

        // Si el usuario es admin => obtengo TODAS las unidades funcionales del consorcio actual
        if (Auth::user()->esAdmin())
            $unidades_funcionales = $this->getUnidadesFuncionalesDeAdmin($consorcio_id);
        else
            $unidades_funcionales = $this->getUnidadesFuncionalesDePropietario($consorcio_id);

        return $unidades_funcionales;
    }

    /**
     * Get de las unidades funcionales del admin del consorcio actual
     * @param $consorcio_id
     * @return mixed
     */
    private function getUnidadesFuncionalesDeAdmin($consorcio_id)
    {
        return UnidadFuncional::where('consorcio_id', $consorcio_id)->orderBy('codigo', 'ASC')->get();
    }

    /** Get de las unidades funcionales del propietario del consorcio actual
     * @param $consorcio_id
     * @return mixed
     */
    private function getUnidadesFuncionalesDePropietario($consorcio_id)
    {
        $user_id = Auth::user()->id;

        return
            UnidadFuncional::where('consorcio_id', $consorcio_id)
                ->whereHas('usuarios', function($query) use ($user_id) {
                    $query->where('usuario_id', $user_id);
                })
                ->orderBy('codigo', 'ASC')
                ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            //'archivo'               => 'required|max:2500',
            'importe'               => 'required|numeric',
            'fecha_transaccion'     => 'required',
            'unidad_funcional_id'   => 'required',
            'medio_de_pago_id'      => 'required|not_in:0'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'comprobantes/create')->withErrors($validator)->withInput();

        // Creo el comprobante
        $comprobante = Comprobante::create($request->except(['archivo']));


        // Si es un admin => el estado es Aprobado de una. Sino Pendiente.
        if (Auth::user()->esAdmin()){
            $this->aceptar($comprobante->id);
        } else {
            $this->ponerPendiente($comprobante->id);
        }

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $comprobante);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'comprobantes/create')->withErrors($validator)->withInput();
        }

        // Busco el comprobante a adjuntar
        $adjunto =  $request->root() . '/' . $comprobante->archivo;

        // Busco todos los administradores que deben recibir el email
        //$admins = $this->getAdministradoresDeConsorcio();

        $admin_email = $comprobante->UnidadFuncional->Consorcio->email;

        if(!empty($admin_email)) {
            Mail::send('emails.pago-realizado', ['comprobante' => $comprobante], function ($mail) use ($admin_email, $comprobante, $adjunto) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas')
                    ->to($admin_email, $comprobante->UnidadFuncional->Consorcio->Administrador->nombre)
                    ->subject('Pago Declarado UF ' .
                        $comprobante->UnidadFuncional->codigo . '|' .
                        $comprobante->UnidadFuncional->piso . '-' .
                        $comprobante->UnidadFuncional->dpto);

                /*foreach ($admins as $admin) {
                    $mail->to($admin->email, $admin->nombre)
                        ->subject('Pago Declarado UF ' .
                            $comprobante->UnidadFuncional->codigo . '|' .
                            $comprobante->UnidadFuncional->piso . '-' .
                            $comprobante->UnidadFuncional->dpto );

                }*/

                if ($comprobante->archivo != null)
                    $mail->attach($adjunto);
            });
        }


        return redirect($this->base_url . 'comprobantes/')->with('comprobante_creado', 'Comprobante: "' . $request->nombre . '" creado');
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $cliente
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, Comprobante $comprobante)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $comprobante->archivo = $archivo['url'];
                $comprobante->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comprobante = Comprobante::findOrFail($id);
        $comprobante->load('MedioDePago');

        return view('comprobantes.show')->with('comprobante', $comprobante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comprobante = Comprobante::findOrFail($id);
        $unidades_funcionales = $this->getUnidadesFuncionales();
        $medios_de_pagos      = $this->getMediosDePagos();

        return view('comprobantes.edit', array(
            'comprobante' => $comprobante,
            'unidades_funcionales' => $unidades_funcionales,
            'medios_de_pagos' => $medios_de_pagos
        )   );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Busco el comprobante
        $comprobante = Comprobante::findOrFail($id);

        // Valido el input
        $validator = Validator::make($request->all(), [
            'fecha_transaccion'     => 'required'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'comprobantes/' . $id . '/edit')->withErrors($validator)->withInput();

        // Actualizo el comprobante
        $comprobante->update($request->except(['archivo', '_token']));

        // Si es un admin => el estado es Aprobado de una. Sino Pendiente.
        if (Auth::user()->esAdmin()){
            $this->aceptar($id);
        } else {
            $this->ponerPendiente($id);
        }

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $comprobante);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'comprobantes/' . $id . '/edit')->withErrors($validator)->withInput();
        }

        return redirect($this->base_url . 'comprobantes/' . $id)->with('comprobante_actualizado', 'Comprobante actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comprobante = Comprobante::findOrFail($id);

        $comprobante->delete();

        return redirect($this->base_url . 'comprobantes/')->with('comprobante_eliminado', 'Comprobante: "' . $comprobante->nombre . '" eliminado');
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

    public function aceptar($id)
    {
        // Busco el comprobante
        $comprobante = Comprobante::findOrFail($id);

        $aceptar_comprobante_obj = new AceptarComprobante( new ComprobanteControl($comprobante));
        $aceptar_comprobante_obj->cambiarEstado();

        $comprobante->load(['UnidadFuncional', 'MedioDePago']);

        // Ponerle el numero al comprobante
        $this->numerarComprobante($comprobante);

        /*$pdf = $this->crearPDFRecibo($comprobante);

        $comprobante->recibo = $pdf;*/
        $comprobante->save();

        // Envio mail de aprobacion a los admins del consorcio y a los usersde la UF

        // Busco todos los administradores que deben recibir el email
        //$admins = $this->getAdministradoresDeConsorcio();
        /*$uf_users = $this->getUsuariosDeUF($comprobante->UnidadFuncional->id);

        if (count($uf_users->Usuarios) > 0) {
            $users = $uf_users->Usuarios;

            Mail::send('emails.comprobantes.comprobante-aceptado-uf', ['comprobante' => $comprobante], function ($mail) use ($users, $comprobante, $pdf) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                foreach ($users as $user) {
                    $mail->bcc($user->email, $user->nombre)
                        ->subject('Comprobante Aceptado de UF ' .
                            $comprobante->UnidadFuncional->codigo . '|' .
                            $comprobante->UnidadFuncional->piso . '-' .
                            $comprobante->UnidadFuncional->dpto );
                }

                $mail->attach($pdf);
            });
        }

        $admin_email = $comprobante->UnidadFuncional->Consorcio->email;

        if (!empty($admin_email)) {
            Mail::send('emails.comprobantes.comprobante-aceptado-admin', ['comprobante' => $comprobante], function ($mail) use ($admin_email, $comprobante, $pdf) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                $mail->to($admin_email, $comprobante->UnidadFuncional->Consorcio->Administrador->nombre)
                    ->subject('Pago Aceptado UF ' .
                        $comprobante->UnidadFuncional->codigo . '|' .
                        $comprobante->UnidadFuncional->piso . '-' .
                        $comprobante->UnidadFuncional->dpto);

                $mail->attach($pdf);
            });
        }*/

        return response()->json(['msg' => 'El comprobante se aceptó']);
    }

    public function rechazar($id)
    {
        // Busco el comprobante
        $comprobante = Comprobante::findOrFail($id);

        $rechazar_comprobante_obj = new RechazarComprobante( new ComprobanteControl($comprobante));
        $rechazar_comprobante_obj->cambiarEstado();

        $uf_users = $this->getUsuariosDeUF($comprobante->UnidadFuncional->id);

        // Envio mail de que el comprobante se rechazó
        if (count($uf_users->Usuarios) > 0) {
            $users = $uf_users->Usuarios;

            Mail::send('emails.comprobantes.comprobante-rechazado-uf', ['comprobante' => $comprobante], function ($mail) use ($users, $comprobante) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                foreach ($users as $user) {
                    $mail->bcc($user->email, $user->nombre)
                        ->subject('Comprobante Rechazado de UF ' .
                            $comprobante->UnidadFuncional->codigo . '|' .
                            $comprobante->UnidadFuncional->piso . '-' .
                            $comprobante->UnidadFuncional->dpto );
                }
            });
        }


        return response()->json(['msg' => 'El comprobante se rechazó']);
    }

    public function ponerPendiente($id)
    {
        $comprobante = Comprobante::findOrFail($id);

        $pendiente_comprobante_obj = new PendienteComprobante( new ComprobanteControl($comprobante));
        $pendiente_comprobante_obj->cambiarEstado();

        $comprobante->recibo = "";
        $comprobante->save();

        return response()->json(['msg' => 'El comprobante se puso pendiente']);
    }

    private function crearPDFRecibo($comprobante)
    {
        $consorcio = Consorcio::where('id', $this->getConsorcio())->with('Administrador')->first();
        //$comprobante = Comprobante::where('id', '63')->with(['UnidadFuncional', 'MedioDePago'])->first();

        $pdf = PDF::loadView('pdf.comprobantes.recibo', [
            'consorcio' => $consorcio,
            'administrador' => $consorcio->administrador,
            'comprobante' => $comprobante
        ]);

        $nombre_pdf = 'recibos/recibo_' . $comprobante->id . '_' . $this->getConsorcio() . '_' . date("d-m-Y", time()) . '_' . date("H_i_s", time()) . '.pdf';

        $pdf
            ->setPaper('a4', 'portrait')
            ->save($nombre_pdf);

        return $nombre_pdf;
    }

    private function numerarComprobante($comprobante)
    {
        // Si el comprobante ya tiene un número asignado no hago nada (aprueba->pendiente->aprueba)
        if ($comprobante->numerador) return true;

        // Inicializo el nuevo numero
        $nuevo_numero = 0;

        // Obtengo el número del último comprobante aprobado
        $ultimo_comprobante =
            Comprobante::whereHas('UnidadFuncional', function ($query) {
                $query->where('consorcio_id', '=', $this->getConsorcio());
            })
                ->whereNotNull('numerador')
                ->orderBy('numerador')
                ->select('id', 'numerador')
                ->get()
                ->last();

        // Si hay un ultimo comprobante => le sumo uno y guardo
        if ($ultimo_comprobante) {
            $ultimo_numerador = $ultimo_comprobante->numerador;

            $nuevo_numero = $ultimo_numerador + 1;
        } else {
            // No hay ningun comprobante => devuelvo 1
            $nuevo_numero = 1;
        }

        // Actualizo el numerador del comprobante
        $comprobante->numerador = $nuevo_numero;
        $comprobante->save();

        return true;
    }
}