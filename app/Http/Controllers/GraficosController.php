<?php

namespace App\Http\Controllers;

use App\RespuestaEncuesta;
use App\RespuestaUnidadFuncional;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;

class GraficosController extends Controller
{
    public function armarGraficoResultadosDeEncuesta($encuesta)
    {
        $respuestas =
            RespuestaEncuesta::where('respuestas_encuesta.encuesta_id', $encuesta->id)
                ->leftJoin('encuestas_respuestas_unidad_funcional', 'encuestas_respuestas_unidad_funcional.respuesta_id', '=', 'respuestas_encuesta.id')
                ->select([
                    'respuestas_encuesta.respuesta',
                    DB::raw('count(encuestas_respuestas_unidad_funcional.created_at) as cantidad')
                ])
                ->groupBy('respuestas_encuesta.id')
                ->get();

        $lava = new Lavacharts();

        $grafico = $lava->DataTable();

        $grafico->addStringColumn('Tipo')
            ->addNumberColumn('Cantidad de Votos');

        $total_respuestas = 0;

        // Cuento cuanta gente votó
        foreach ($respuestas as $respuesta) {
            $total_respuestas = $total_respuestas + $respuesta->cantidad;
        }

        foreach ($respuestas as $respuesta) {
            if($total_respuestas){
            $grafico->addRow([
                $respuesta->respuesta, $respuesta->cantidad / $total_respuestas
            ]);
        }else {
            $grafico->addRow([
                $respuesta->respuesta, 0
            ]);        }
        }

        $lava->PieChart('CantidadDeVotos', $grafico, [
            'title' => 'Resultados Encuesta',
            'is3D' => true,
            'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
            ]
        ]);

        return $lava;
    }
}
