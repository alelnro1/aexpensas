<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\TipoGasto;
use App\Administrador;
use Illuminate\Support\Facades\Auth;
class TipoGastosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $TipoGastos  = Administrador::find(Auth::User()->administrador_id)->TipoGastos()->get();

        return view('TipoGastos.listar')->with('TipoGastos', $TipoGastos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('TipoGastos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarTipoGasto($request);
        $request["administrador_id"] =Auth::user()->administrador_id;


        $TipoGasto = TipoGasto::create($request->all());

        return redirect( $this->base_url . 'tipos-de-gastos')->with('TipoGasto_creado', 'Tipo de Gasto con descripción: "' . $request->descripcion . '" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $TipoGasto = TipoGasto::findOrFail($id);
        return view('TipoGastos.show')->with('TipoGasto', $TipoGasto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $TipoGasto = TipoGasto::findOrFail($id);
        return view('TipoGastos.edit')->with('TipoGasto', $TipoGasto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $TipoGasto = TipoGasto::findOrFail($id);


        $TipoGasto->update($request->all());

        return redirect( $this->base_url . 'tipos-de-gastos')->with('TipoGasto_actualizado', 'Tipo de Gasto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $TipoGasto = TipoGasto::findOrFail($id);

        $TipoGasto->delete();

        return redirect( $this->base_url . 'tipos-de-gastos')->with('TipoGasto_eliminado', 'Tipo de Gasto con Descripción:  "' . $TipoGasto->descripcion . '" eliminado');
    }

    private function validarTipoGasto($request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:500',
        ]);
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}