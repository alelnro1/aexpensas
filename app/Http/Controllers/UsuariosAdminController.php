<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Rol;
use App\Administrador;
use Illuminate\Support\Facades\Validator;

class UsuariosAdminController extends Controller
{
    public function index()
    {
        $usuarios = new User();

        $usuarios_admin = $usuarios->dameUsuariosAdmin();
        return view('usuarios.listar')->with('usuarios', $usuarios_admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $administradores = Administrador::all();
        return view('usuarios.create')->with('administradores', $administradores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Valido el input
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'administrador_id' => 'required',
        ]);
        if ($validator->fails())
            return redirect('usuarios/create')->withErrors($validator)->withInput();

        $rol = new Rol();
        $rol_id = $rol->idRolByNommbre('admin');
        // Creo el usuario
        $usuario = User::create([
            'nombre' => $request['name'],
            'email' => $request['email'],
            'administrador_id' => $request['administrador_id'],
            'password' => bcrypt($request['password']),
            'rol_id' => $rol_id,
        ]);

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $usuario);

        if ($validator) {
            if ($validator->fails())
                return redirect('usuarios/create')->withErrors($validator)->withInput();
        }

        return redirect('/admin/usuario-admin/')->with('usuario_creado', 'Usuario con nombre ' . $request->name . ' creado');
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $usuario
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $usuario)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $usuario->archivo = $archivo['url'];
                $usuario->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::findOrFail($id);
        return view('usuarios.show')->with('usuario', $usuario);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        $administradores = Administrador::all();
        return view('usuarios.edit')->with('usuario', $usuario)->with('administradores', $administradores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Busco al usuario
        $usuario = User::findOrFail($id);
        if ($request->password != "") {

            // Valido el input
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,' . $id . ',id',
                'password' => 'required|min:6|confirmed',
                'administrador_id' => 'required',

            ]);
            if ($validator->fails())
                return redirect('usuarios/' . $id . '/edit')->withErrors($validator)->withInput();

            // Actualizo el usuario
            $usuario->update([
                'nombre' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'administrador_id' => $request['administrador_id'],

            ]);


        } else {
            // Valido el input
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,' . $id . ',id',
                'administrador_id' => 'required',
            ]);
            if ($validator->fails())
                return redirect($this->base_url . 'usuario-admin/' . $id . '/edit')->withErrors($validator)->withInput();
            // Actualizo el usuario
            $usuario->update([
                'nombre' => $request['name'],
                'email' => $request['email'],
                'administrador_id' => $request['administrador_id'],
            ]);

        }

        return redirect($this->base_url . 'usuario-admin')->with('usuario_actualizado', 'Usuario actualizado');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        $usuario->delete();

        return redirect($this->base_url . 'usuario-admin/')->with('usuario_eliminado', 'Usuario con nombre ' . $usuario->nombre . ' eliminado');
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }


}


