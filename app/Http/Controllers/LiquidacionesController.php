<?php

namespace App\Http\Controllers;

use App\CategoriaFinanciera;
/*use App\Classes\Liquidaciones\ExpensasParticulares;
use App\Classes\Liquidaciones\ExpensasTipoA;
use App\Classes\Liquidaciones\ExpensasTipoB;
use App\Classes\Liquidaciones\ExpensasTipoC;
use App\Auditoria;
use App\UnidadFuncional;
use Barryvdh\DomPDF\Facade as PDF;
use Knp\Snappy\Pdf;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
*/
use App\Classes\Liquidaciones\ConfirmacionExpensa;
use App\Classes\Liquidaciones\Prorrateos;
use App\Comprobante;
use App\Consorcio;
use App\EstadoFinanciero;
use App\Gasto;
use App\GastoUnidadFuncional;
use App\Liquidacion;
use App\Rubro;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Http\Request;
use App\Classes\Liquidaciones\Saldos;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class LiquidacionesController extends Controller
{
    protected $saldos;
    protected $prorrateos;
    protected $confirmacion;

    public function __construct()
    {
        parent::__construct();

        $this->saldos = new Saldos();
        $this->prorrateos = new Prorrateos();
        $this->confirmacion = new ConfirmacionExpensa();
    }

    public function index()
    {
        $liquidaciones = Liquidacion::where('consorcio_id', $this->getConsorcio())->get();

        return view('liquidaciones.index', ['liquidaciones' => $liquidaciones]);
    }

    public function armarGastos()
    {
        // Obtengo la ultima liquidacion del consorcio actual
        $ultima_liquidacion = $this->getUltimaLiquidacion();

        // Obtengo la fecha de la variable sesion
        $fecha = session('LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $this->getConsorcio() . '_FECHA');

        if ($ultima_liquidacion) {
            $fecha_ult_liq = $ultima_liquidacion->fecha_fin;
        } else {
            $fecha_ult_liq = "";
        }

        // Si no existe la fecha no hago nada
        if (!$fecha) {
            return view('liquidaciones.nueva', array('error_falta_fecha' => true, 'fecha_ultima_liq' => $fecha_ult_liq));
        }

        // Obtengo los rubros padres
        $rubro = new Rubro();
        $rubros = $rubro->dameLosRubrosPadres($fecha);

        $proveedores = $this->getProveedores($rubros);

        $total_gastos = 0;

        // Calculo el total de los subrubros
        foreach ($rubros as $rubro) {
            foreach ($rubro->RubrosHijos as $subrubro) {
                $subrubro->total = $this->calcularTotalParaSubrubro($subrubro);
            }
        }



        // Calculo el total de los rubros (suma del total de subrubros)
        foreach ($rubros as $rubro) {
            $rubro->total = $this->calcularTotalParaRubro($rubro);

            // Este sería el total de todos los egresos, pagos del periodo, remuneraciones, etc
            $total_gastos = $total_gastos + $rubro->total;
        }

        session(['LIQ_TOTAL_GASTOS_' . $this->getConsorcio() => $total_gastos]);

        // Cargo la sumatoria de los porcentajes de las UF
        $unidades_funcionales = Consorcio::find($this->getConsorcio())->UnidadesFuncionales()->get();

        $porcentajes = $this->getSumaPorcentajesAyB($unidades_funcionales);

        if (
            number_format($porcentajes['a'], 2) != number_format(100, 2) ||
            number_format($porcentajes['b'], 2) != number_format(100, 2)
        ) {
            Session::put('total_porcentaje_a_' . $this->getConsorcio(), $porcentajes['a']);
            Session::put('total_porcentaje_b_' . $this->getConsorcio(), $porcentajes['b']);
        } else {
            Session::forget('total_porcentaje_a_' . $this->getConsorcio());
            Session::forget('total_porcentaje_b_' . $this->getConsorcio());
        }

        $prorrateos = $this->prorrateos->cargarProrrateosUnidadesFuncionales();

        // Cargo las categorias padres con los hijos
        $categorias_financieras = $this->getCategoriasFinancieras();

        // Obtengo todos los estados financieros, viejos y nuevos
        $estados_financieros = $this->getEstadosFinancieros();

        // Calculo los valores del fondo operativo para mostrar en el estado financiero
        $fondo_operativo = $this->getFondoOperativo();

        $saldo_adeudado_de_ufs = $this->prorrateos->getSaldoAdeudadoDeUFs();
        session(['LIQ_SALDO_ADEUDADO_UFS_' . $this->getConsorcio() => $saldo_adeudado_de_ufs]);

        if ($ultima_liquidacion) {
            // Obtengo el saldo anterior del estado financiero
            $saldo_anterior_estado_financiero = $ultima_liquidacion->saldo_estado_financiero;

            // Obtengo el saldo anterior de los movimientos bancarios
            $saldo_anterior_movimientos_bancarios = $ultima_liquidacion->saldo_movimientos_bancarios;

            // Obtengo el saldo anterior del estado patrimonial
            $saldo_anterior_estado_patrimonial = $ultima_liquidacion->saldo_estado_patrimonial;
        } else {
            $saldo_anterior_estado_financiero = $saldo_anterior_movimientos_bancarios = $saldo_anterior_estado_patrimonial = 0;
        }

        // Obtengo el saldo nuevo del estado financiero
        $saldo_estado_financiero = $this->saldos->getSaldoEstadoFinanciero($ultima_liquidacion, $fondo_operativo);

        // Obtengo el saldo de los movimientos bancarios
        $saldo_movimientos_bancarios = $this->saldos->getSaldoMovimientosBancarios($ultima_liquidacion, $estados_financieros);

        // Obtengo el saldo del estado patrimonial
        $saldo_estado_patrimonial = $this->saldos->getSaldoEstadoPatrimonial($ultima_liquidacion, $this->getConsorcio());

        // Obtengo todos los totales del prorrateo
        $total_prorrateos = $this->getTotalProrrateos($prorrateos);
        session(['LIQ_TOTAL_PRORRATEOS_' . $this->getConsorcio() => $total_prorrateos]);

        $consorcio = Consorcio::findOrFail($this->getConsorcio());

        // Cada vez que se carga la liquidacion, se guarda en la sesion los rubros y prorrateos para poder armar el PDF
        session(['LIQ_RUBROS_LOCAL_' . $this->getConsorcio() => $rubros]);
        session(['LIQ_PRORRATEOS_LOCAL_' . $this->getConsorcio() => $prorrateos]);
        session(['LIQ_ESTADOS_FINANCIEROS_LOCAL_' . $this->getConsorcio() => $estados_financieros]);
        session(['LIQ_SALDO_MOVIMIENTOS_BANCARIOS_' . $this->getConsorcio() => $saldo_movimientos_bancarios]);
        session(['LIQ_SALDO_ESTADO_FINANCIERO_' . $this->getConsorcio() => $saldo_estado_financiero]);
        session(['LIQ_SALDO_ESTADO_PATRIMONIAL_' . $this->getConsorcio() => $saldo_estado_patrimonial]);
        session(['LIQ_PROVEEDORES_' . $this->getConsorcio() => $proveedores]);
        session(['LIQ_PRORRATEO_' . $this->getConsorcio() => $prorrateos]);
        session(['CONSORCIO_TIENE_COCHERA' => $consorcio->cochera_complementaria]);


        return view('liquidaciones.nueva',
            array('rubros' => $rubros,
                'prorrateos' => $prorrateos,
                'categorias' => $categorias_financieras,
                'estados_financieros' => $estados_financieros,
                'fondo_operativo' => $fondo_operativo,
                'ultima_liquidacion' => $ultima_liquidacion,
                'fecha' => $fecha,

                'saldo_estado_financiero' => $saldo_estado_financiero,
                'saldo_anterior_estado_financiero' => $saldo_anterior_estado_financiero,

                'saldo_movimientos_bancarios' => $saldo_movimientos_bancarios,
                'saldo_anterior_movimientos_bancarios' => $saldo_anterior_movimientos_bancarios,

                'saldo_estado_patrimonial' => $saldo_estado_patrimonial,
                'saldo_anterior_estado_patrimonial' => $saldo_anterior_estado_patrimonial,

                'fecha_ultima_liq' => $fecha_ult_liq,
                'total_prorrateos' => $total_prorrateos
            )
        );
    }

    /**
     * Busco los estados financieros del consorcio
     * @return mixed
     */
    public function getEstadosFinancieros()
    {
        // Cargo los estados financieros que no tengan id liquidacion o sean de la ultima liquidación
        $estados_financieros =
            EstadoFinanciero::whereNull('liquidacion_id')
                ->with('CategoriaFinanciera')
                ->where('consorcio_id', $this->getConsorcio())
                ->get();

        $ultima_liq = session('LIQ_ULTIMA_LIQUIDACION_' . $this->getConsorcio());

        // Verifico que haya una ultima liquidacion
        if ($ultima_liq != null) {
            $ultima_liq_id = $ultima_liq->id;

            $estados_financieros_ultima_liq =
                EstadoFinanciero::where('consorcio_id', $this->getConsorcio())
                    ->with('CategoriaFinanciera')
                    ->where('liquidacion_id', $ultima_liq_id)
                    ->get();

            if ($estados_financieros_ultima_liq) {
                $estados_financieros = $estados_financieros->merge($estados_financieros_ultima_liq);
            }
        }

        return $estados_financieros;
    }

    /**
     * Busca las categorias padres con los hijos
     * @return mixed
     */
    public function getCategoriasFinancieras()
    {
        $categorias =
            CategoriaFinanciera::whereNull('padre_id')
                ->with([
                    'CategoriasHijas',
                    'EstadosFinancieros' => function($query) {
                        $query->where('consorcio_id', $this->getConsorcio());
                    }
                ])
                ->get();

        return $categorias;
    }

    public function actualizarVistaDeEstadoFinanciero()
    {
        $categorias = $this->getCategoriasFinancieras();
        $fondo_operativo = $this->getFondoOperativo();
        $estados_financieros = $this->getEstadosFinancieros();

        $ultima_liquidacion = session('LIQ_ULTIMA_LIQUIDACION_' . $this->getConsorcio());
        $saldo_estado_financiero = $this->saldos->getSaldoEstadoFinanciero($ultima_liquidacion, $fondo_operativo);

        $saldo_estado_patrimonial = $this->saldos->getSaldoEstadoPatrimonial($ultima_liquidacion, $this->getConsorcio());

        $saldo_movimientos_bancarios = $this->saldos->getSaldoMovimientosBancarios($ultima_liquidacion, $estados_financieros);
        session(['LIQ_SALDO_MOVIMIENTOS_BANCARIOS_' . $this->getConsorcio() => $saldo_movimientos_bancarios]);

        if ($ultima_liquidacion) {
            $saldo_anterior_movimientos_bancarios = $ultima_liquidacion->saldo_movimientos_bancarios;
            $saldo_anterior_estado_patrimonial = $ultima_liquidacion->saldo_estado_patrimonial;
        } else {
            $saldo_anterior_movimientos_bancarios = $saldo_anterior_estado_patrimonial = 0;
        }

        $listado_html = (string)view('liquidaciones.estado-financiero-verificar', [
            'categorias' => $categorias,
            'fondo_operativo' => $fondo_operativo,
            'ultima_liquidacion' => $ultima_liquidacion,

            'saldo_estado_financiero' => $saldo_estado_financiero,

            'saldo_estado_patrimonial' => $saldo_estado_patrimonial,
            'saldo_anterior_estado_patrimonial' => $saldo_anterior_estado_patrimonial,

            'saldo_movimientos_bancarios' => $saldo_movimientos_bancarios,
            'saldo_anterior_movimientos_bancarios' => $saldo_anterior_movimientos_bancarios,
        ]);

        echo json_encode(['listado' => $listado_html]);
    }

    /**
     * Se cargan las subcategorias cuando se elije una categoria, por ajax
     * @param $padre_id
     */
    public function cargarSubCategoriasFinancieras($padre_id)
    {
        // Busco los hijos de las subcategorias
        $subcategorias = CategoriaFinanciera::where('padre_id', $padre_id)->get();

        echo json_encode(array('subcategorias' => $subcategorias));
    }

    /**
     * Guardo temporalmente los items de los estados financieros
     * @param Request $request
     */
    public function guardarItemsTemporales(Request $request)
    {
        // Primero busco si el estado ya existe
        if (isset($request->temp_fila['item_id']) && $request->temp_fila['item_id'] != "") {
            // Existe => Busco el item
            $estado_financiero = EstadoFinanciero::where('id', $request->temp_fila['item_id'])->first();

            $estado_financiero->categoria_financiera_id = $request->temp_fila['categoria_financiera_id'];
            $estado_financiero->nombre = $request->temp_fila['nombre'];
            $estado_financiero->monto = $request->temp_fila['monto'];

            $estado_financiero->save();
        } else {
            $estado_financiero =
                EstadoFinanciero::create([
                    'categoria_financiera_id' => $request->temp_fila['categoria_financiera_id'],
                    'subcategoria_financiera_id' => $request->temp_fila['subcategoria_financiera_id'],
                    'nombre' => $request->temp_fila['nombre'],
                    'monto' => $request->temp_fila['monto'],
                    'consorcio_id' => $this->getConsorcio()
                ]);
        }

        echo json_encode(['estado_id' => $estado_financiero->id]);
    }

    /**
     * Se elimina un item de la pantalla de cargar estados financieros
     * @param Request $request
     */
    public function eliminarItemTemporalEstadoFinanciero(Request $request)
    {
        // Busco el registro en estados_financieros
        $item = EstadoFinanciero::where('id', $request->temp_fila['item_id'])->first();

        // Elimino el item
        $item->delete();

        // Devuelvo el resultado
        echo json_encode(['success' => true]);
    }

    /**
     * Se calcula el total de todos los prorrateos
     * @param $prorrateos
     */
    public function getTotalProrrateos($prorrateos)
    {
        // Inicializacion de los montos
        $total['porc_a'] = $total['porc_b'] = $total['exp_a'] = $total['exp_b'] = $total['saldo_inicial'] = $total['saldo_a_pagar'] = $total['cochera']
            = $total['monto_abonado'] = $total['saldo_ant'] = $total['deuda'] =$total['intereses'] = $total['monto_exp'] = $total['exp_part'] = $total['cuota_extra'] = 0;

        foreach ($prorrateos as $prorrateo) {
            $total['porc_a'] = $total['porc_a'] + $prorrateo->porcentaje_a;
            $total['porc_b'] = $total['porc_b'] + $prorrateo->porcentaje_b;
            $total['exp_a'] = $total['exp_a'] + $prorrateo->expensas_tipo_a;
            $total['exp_b'] = $total['exp_b'] + $prorrateo->expensas_tipo_b;
            $total['deuda'] = $total['deuda'] + $prorrateo->deuda - $prorrateo->futuros;
            $total['cuota_extra'] = $total['cuota_extra'] + $prorrateo->cuota_extra;
            $total['saldo_inicial'] = $total['saldo_inicial'] + $prorrateo->saldo_inicial;
            $total['monto_abonado'] = $total['monto_abonado'] + $prorrateo->monto_abonado;
            $total['saldo_ant'] = $total['saldo_ant'] + $this->truncate($prorrateo->saldo_anterior);
            $total['intereses'] = $total['intereses'] + $prorrateo->intereses;
            $total['monto_exp'] = $total['monto_exp'] + $prorrateo->monto_expensas;
            $total['exp_part'] = $total['exp_part'] + $prorrateo->expensas_tipo_particular;
            $total['cochera'] = $total['cochera'] + $prorrateo->cochera;
            $total['saldo_a_pagar'] = $total['saldo_a_pagar'] + $prorrateo->saldo_a_pagar;
        }

        $total['saldo_ant'] = $total['saldo_ant'] - 0.03;

        return $total;
    }

    private function truncate($number) {
        $first = $number * 1000;
        $esto = explode(".", $first)[0];
        $devuelvo = (float) intval($esto) / 1000;

        return $devuelvo;
    }

    private function calcularTotalParaSubrubro($subrubro)
    {
        $total_subrubro = 0;

        foreach ($subrubro->Gastos as $gasto) {
            foreach ($gasto->Cuotas as $cuota) {
                if ($gasto->TipoExpensa->descripcion == "Cochera" || $gasto->TipoExpensa->descripcion == "Particular") {
                    $total_subrubro = $total_subrubro + $cuota->importe * $cuota->dameCantidadDeUFsConEseGasto();
                    continue;
                } else {
                    $total_subrubro = $total_subrubro + $cuota->importe;
                }
            }
        }

        return $total_subrubro;
    }

    private function calcularTotalParaRubro($rubro)
    {
        $total_rubro = 0;

        foreach ($rubro->RubrosHijos as $subrubro) {
            $total_rubro = $total_rubro + $subrubro->total;
        }

        return $total_rubro;
    }

    /**
     * Se descarga el PDF de la liquidacion
     * @return Response
     */
    public function descargar(Request $request)
    {
        $nombre_periodo = $request->nombre_periodo;

        $pdf = $this->crearPDFDeLiquidacion($nombre_periodo);

        echo json_encode(['link' => $pdf]);
    }

    /**
     * Se genera el PDF, se guarda, y se devuelve el link
     */
    private function crearPDFDeLiquidacion($nombre_periodo)
    {
        $consorcio = Consorcio::where('id', $this->getConsorcio())->with('Administrador')->first();

        $rubros = session('LIQ_RUBROS_LOCAL_' . $this->getConsorcio());
        $prorrateos = session('LIQ_PRORRATEOS_LOCAL_' . $this->getConsorcio());
        $estados_financieros = session('LIQ_ESTADOS_FINANCIEROS_' . $this->getConsorcio());
        $proveedores = session('LIQ_PROVEEDORES_' . $this->getConsorcio());
        $ultima_liquidacion = session('LIQ_ULTIMA_LIQUIDACION_' . $this->getConsorcio());

        $saldo_estado_financiero = session('LIQ_SALDO_ESTADO_FINANCIERO_' . $this->getConsorcio());
        $saldo_estado_patrimonial = session('LIQ_SALDO_ESTADO_PATRIMONIAL_' . $this->getConsorcio());
        $saldo_movimientos_bancarios = session('LIQ_SALDO_MOVIMIENTOS_BANCARIOS_' . $this->getConsorcio());
        $total_gastos = session('LIQ_TOTAL_GASTOS_' . $this->getConsorcio());
        $total_prorrateos = session('LIQ_TOTAL_PRORRATEOS_' . $this->getConsorcio());

        if ($ultima_liquidacion) {
            $saldo_anterior_movimientos_bancarios = $ultima_liquidacion->saldo_movimientos_bancarios;
            $saldo_anterior_estado_patrimonial = $ultima_liquidacion->saldo_estado_patrimonial;
        } else {
            $saldo_anterior_movimientos_bancarios = $saldo_anterior_estado_patrimonial = 0;
        }

        $fondo_operativo = $this->getFondoOperativo();
        $categorias_financieras = $this->getCategoriasFinancieras();
        //$total_prorrateos = $this->getTotalProrrateos($prorrateos);

        $pdf = PDF::loadView('pdf.liquidacion', [
            'nombre_periodo' => $nombre_periodo,
            'consorcio' => $consorcio,
            'total_gastos' => $total_gastos,
            'total_prorrateos' => $total_prorrateos,
            'rubros' => $rubros,
            'prorrateos' => $prorrateos,
            'estados_financieros' => $estados_financieros,
            'ultima_liquidacion' => $ultima_liquidacion,
            'fondo_operativo' => $fondo_operativo,
            'categorias_financieras' => $categorias_financieras,
            'saldo_estado_financiero' => $saldo_estado_financiero,
            'saldo_estado_patrimonial' => $saldo_estado_patrimonial,
            'saldo_anterior_estado_patrimonial' => $saldo_anterior_estado_patrimonial,
            'saldo_movimientos_bancarios' => $saldo_movimientos_bancarios,
            'saldo_anterior_movimientos_bancarios' => $saldo_anterior_movimientos_bancarios,
            'proveedores' => $proveedores
        ]);

        $nombre_pdf = 'liquidaciones/liquidacion_' . $this->getConsorcio() . '_' . date("d-m-Y", time()) . '_' . date("H_i_s", time()) . '.pdf';

        $footer = view('pdf.footer');

        $pdf
            ->setPaper('a4', 'portrait')
            ->setOption('footer-html', $footer)
            ->save($nombre_pdf);

        return $nombre_pdf;
    }

    /**
     * Se confirma la expensa
     * @param Request $request
     */
    public function confirmarExpensa(Request $request)
    {
        // Obtener la fecha fin de la ultima liquidacion
        $ultima_liquidacion = session('LIQ_ULTIMA_LIQUIDACION_' . $this->getConsorcio());

        // Obtengo el fondo operativo
        //$fondo_operativo = $this->getFondoOperativo();

        // Obtengo los estados financieros
        $estados_financieros = $this->getEstadosFinancieros();

        // La fecha de inicio de la liquidacion actual será la fecha de fin de la ultima
        if ($ultima_liquidacion) {
            $fecha_inicio = $ultima_liquidacion->fecha_fin;
        } else {
            $fecha_inicio = date("Y-m-d H:i:s", strtotime("1 year ago"));
        }

        // Recibo el nombre del período
        $nombre = $request->nombre_periodo;

        session(['LIQ_NOMBRE_PERIODO_' . $this->getConsorcio() => $nombre]);

        // Recibo la fecha fin del periodo actual
        $fecha_fin = date("Y-m-d", strtotime(str_replace('/', '-', $request->fecha)));

        // Obtengo el saldo del estado financiero actual
        $saldo_estado_financiero = session('LIQ_SALDO_ESTADO_FINANCIERO_' . $this->getConsorcio());

        // Obtengo el saldo de los movimientos bancarios
        $saldo_movimientos_bancarios = session('LIQ_SALDO_MOVIMIENTOS_BANCARIOS_' . $this->getConsorcio());

        // Obtengo el saldo del estado patrimonial
        $saldo_estado_patrimonial = session('LIQ_SALDO_ESTADO_PATRIMONIAL_' . $this->getConsorcio());

        // Obtengo el PDF
        $pdf = $this->crearPDFDeLiquidacion($nombre);

        // Creo la liquidacion
        $liquidacion = Liquidacion::create([
            'consorcio_id' => $this->getConsorcio(),
            'nombre_periodo' => $nombre,
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'saldo_estado_financiero' => $saldo_estado_financiero,
            'saldo_movimientos_bancarios' => $saldo_movimientos_bancarios,
            'saldo_estado_patrimonial' => $saldo_estado_patrimonial,
            'pdf' => $pdf
        ]);

        // Mando a liquidar todos los gastos
        $this->confirmacion->liquidarGastos($liquidacion);

        // Mando a liquidar todos los comprobantes
        $this->confirmacion->liquidarComprobantes($liquidacion);

        // Mando a liquidar los estados financieros
        $this->confirmacion->liquidarEstadosFinancieros($liquidacion, $estados_financieros);

        // Paso los valores del prorrateo a las unidades funcionales y audito
        $this->prorrateos->pasarProrrateoAUFYAuditar($liquidacion);

        $users = $this->getUsuariosDeConsorcio();
        //$admins = $this->getAdministradoresDeConsorcio();

        $admin_email = session('CONSORCIO')->email;

        // Le envío un mail a todas las UF del consorcio
        Mail::send('emails.expensa-generada', ['liquidacion' => $liquidacion], function ($mail) use ($admin_email, $liquidacion, $users, $pdf) {
            $mail->from('no-reply@aexpensas.com', 'AExpensas')
                ->subject('Expensa Generada');

            foreach ($users as $user) {
                $mail->bcc($user->email, $user->nombre);
            }

            if (!empty($admin_email)) {
                $mail->bcc($admin_email, $liquidacion->Consorcio->Administrador->nombre);
            }

            // Mail para Mauro
            $mail->bcc('mauromazzara@gmail.com', 'Mauro Mazzara');

            $mail->attach($pdf);
        });
        //echo json_encode(['success' => true]);
    }

    /**
     * Se obtiene el fondo operativo con los calculos que corresponden para mostrar en el Estado Financiero
     * @return array
     */
    private function getFondoOperativo()
    {
        $comprobante = new Comprobante();
        $comprobantes = $comprobante->getComprobantesAprobadosSinLiquidacion();

        $gasto = new Gasto();
        $gastos = $gasto->getGastosSinLiquidacion();

        // Inicializo las variables de calculo
        $ingresos_terminos = $ingresos_adeudados = $ingresos_intereses = $ingresos_futuros = $ingresos_particulares = 0;
        $egresos_generales = $egresos_particulares = $egresos_asignacion_cuota_extra = 0;

        // Recorro los comprobantes y calculo los resultados para los ingresos
        foreach ($comprobantes as $comprobante) {
            $ingresos_terminos = $ingresos_terminos + $comprobante->termino;
            $ingresos_adeudados = $ingresos_adeudados + $comprobante->deuda;
            $ingresos_intereses = $ingresos_intereses + $comprobante->interes;
            $ingresos_particulares = $ingresos_particulares + $comprobante->gastos_particulares;
            $ingresos_futuros = $ingresos_futuros + $comprobante->futuros;
        }

        $egresos_cochera = 0;

        // Recorro los gastos y calculo los resultados para los egresos
        foreach ($gastos as $gasto) {
            if ($gasto->TipoExpensa->descripcion == "Cochera") {
                $cant_cocheras = GastoUnidadFuncional::cantidadDeCocherasConGasto($gasto->id);

                // Todas las cuotas para un gasto de tipo cochera tienen el mismo importe => tomo el primero
                $cuota_de_gasto = $gasto->Cuotas->first();

                $egresos_generales = $egresos_generales + $cuota_de_gasto->importe * $cant_cocheras;
            }

            // Recorro las cuotas no liquidadas que NO sean de tipo Cochera
            foreach ($gasto->Cuotas as $cuota) {
                if ($gasto->TipoExpensa->descripcion != "Cochera") {
                    switch ($gasto->TipoExpensa->descripcion) {
                        case "Particular":
                            // Expensa particular => sumo gastos particulares
                            $egresos_particulares = $egresos_particulares + $cuota->importe;

                            break;

                        case "Cuota Extra":
                            // Si la cuota extra es de asignacion, tengo que sumar
                            if ($gasto->tipo_cuota_extra == "1") {
                                $egresos_asignacion_cuota_extra = $egresos_asignacion_cuota_extra + $cuota->importe;
                            }

                            break;

                        default:
                            // Expensa general => sumo gastos generales
                            $egresos_generales = $egresos_generales + $cuota->importe;
                            break;
                    }
                }
            }
        }

        return [
            'ingresos_terminos' => $ingresos_terminos,
            'ingresos_adeudados' => $ingresos_adeudados,
            'ingresos_intereses' => $ingresos_intereses,
            'ingresos_particulares' => $ingresos_particulares,
            'ingresos_futuros' => $ingresos_futuros,
            'egresos_particulares' => $egresos_particulares,
            'egresos_generales' => $egresos_generales,
            'egresos_asignacion_cuota_extra' => $egresos_asignacion_cuota_extra
        ];
    }

    /**
     * Obtiene la ultima liquidacion del consorcio
     * @return mixed
     */
    private function getUltimaLiquidacion()
    {
        $ultima_liquidacion_id = Liquidacion::where('consorcio_id', $this->getConsorcio())->max('id');
        $ultima_liquidacion = Liquidacion::where('id', $ultima_liquidacion_id)->first();

        session(['LIQ_ULTIMA_LIQUIDACION_' . $this->getConsorcio() => $ultima_liquidacion]);

        return $ultima_liquidacion;
    }

    /**
     * Se recibe desde la vista la fecha limite para aplicar la liquidacion
     * @param Request $request
     */
    public function aplicarFechaLimite(Request $request)
    {
        // Cambio el / por el - porque mysql trabaja asi
        $request->fecha = str_replace('/', '-', $request->fecha);

        // Modifico la fecha para que matchee con el tipo de la base de datos. Tenemos xx/xx/xxx y en la BD hay: xx-xx-xxxx
        $fecha = date("Y-m-d", strtotime($request->fecha));

        // Genero la sesion unica por usuario para aplicar la liquidacion
        session(['LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $this->getConsorcio() . '_FECHA' => $fecha ]);

        echo json_encode(['success' => true]);
    }

    /**
     * Obtiene todos los proveedores a partir de los gastos
     * @param $rubros
     * @return array
     */
    private function getProveedores($rubros)
    {
        $proveedores = [];

        foreach ($rubros as $rubro) {
            foreach ($rubro->RubrosHijos as $rubro_hijo) {
                foreach ($rubro_hijo->Gastos as $gasto) {
                    if ($gasto->Proveedor) {
                        $proveedor = $gasto->Proveedor;

                        if (count($proveedores) >= 1) {
                            // Cada vez que se busca un gasto, el proveedor se pone inexistente para luego recorrer la lista
                            // de todos los proveedores y no duplicar
                            $existe = false;
                            foreach ($proveedores as $proveedor_existente) {
                                if ($proveedor_existente->id == $proveedor->id) {
                                    $existe = true;
                                    break;
                                }
                            }
                            // Verifico que el proveedor no exista
                            if (!$existe) {
                                array_push($proveedores, $proveedor);
                            }
                        } else {
                            array_push($proveedores, $proveedor);
                        }
                    }
                }
            }
        }

        return $proveedores;
    }
}