<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;

use App\Http\Requests;

class AuditoriaController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Guardo el nuevo movimiento para poder auditar
     * @param null $accion
     * @param null $saldo_anterior
     * @param null $saldo_nuevo
     * @param null $interes_anterior
     * @param null $interes_nuevo
     * @param null $comprobante_id
     * @param null $unidad_funcional_id
     * @param null $liquidacion_id
     * @return static
     */
    public function nuevoMovimiento (
        $accion=null,
        $saldo_anterior=null,
        $saldo_nuevo=null,
        $interes_anterior=null,
        $interes_nuevo=null,
        $termino_anterior=null,
        $termino_nuevo=null,
        $deuda_anterior=null,
        $deuda_nuevo=null,
        $gastos_particulares_anterior=null,
        $gastos_particulares_nuevo=null,
        $futuros_anterior=null,
        $futuros_nuevo=null,
        $estado_anterior_id=null,
        $estado_nuevo_id=null,
        $comprobante_id=null,
        $unidad_funcional_id=null,
        $liquidacion_id=null,
        $comprobante_importe=null
    ) {

        $movimiento =
            Auditoria::create([
                'unidad_funcional_id'             => $unidad_funcional_id,
                'accion'                          => $accion,
                'saldo_anterior'                  => $saldo_anterior,
                'saldo_nuevo'                     => $saldo_nuevo,
                'interes_anterior'                => $interes_anterior,
                'interes_nuevo'                   => $interes_nuevo,
                'termino_anterior'                => $termino_anterior,
                'termino_nuevo'                   => $termino_nuevo,
                'deuda_anterior'                  => $deuda_anterior,
                'deuda_nuevo'                     => $deuda_nuevo,
                'gastos_particulares_anterior'    => $gastos_particulares_anterior,
                'gastos_particulares_nuevo'       => $gastos_particulares_nuevo,
                'futuros_anterior'                => $futuros_anterior,
                'futuros_nuevo'                   => $futuros_nuevo,
                'estado_anterior_id'              => $estado_anterior_id,
                'estado_nuevo_id'                 => $estado_nuevo_id,
                'comprobante_id'                  => $comprobante_id,
                'liquidacion_id'                  => $liquidacion_id,
                'comprobante_importe'             => $comprobante_importe
            ]);

        return $movimiento;
    }
}
