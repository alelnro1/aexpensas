<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Consorcio;
use Illuminate\Support\Facades\Auth;
use App\Administrador;
use Illuminate\Support\Facades\Validator;


class ConsorciosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consorcios = Administrador::find(Auth::User()->administrador_id)->Consorcios()->get();
        return view('consorcios.listar')->with('consorcios', $consorcios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('consorcios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required|max:100',
            'archivo'       => 'max:1000|mimes:jpg,jpeg,png,gif',
            'firma'         => 'max:1000|mimes:jpg,jpeg,png,gif',
            'domicilio'     => 'required',
            'expensa_fija'  => 'required',
            'interes_vto_1' => 'required',
            'dia_vto_1'     => 'required',
            'cuit'          => 'max:100',
            'cbu'           => 'size:22',
            'email'         => 'required|email',
            'monto_fijo_a'  => 'required_if:expensa_fija,1',
            'monto_fijo_b'  => 'required_if:expensa_fija,1',
            'monto_fijo_c'  => 'required_if:expensa_fija,1',
            'precioXuf'     => 'numeric'

        ]);

        $validator->after(function ($validator) use ($request) {
            if ($request->expensa_fija == 1) {
                if ($request->monto_fijo_a == "" || $request->monto_fijo_b == "" || $request->monto_fijo_c == "") {
                    $validator->errors()->add('monto_fijo_a', 'Los montos fijos son obligatorios');
                }
            }
            if ($request->redondeo == "1" && $request->redondeo_0 == "1") {
                $validator->errors()->add('redondeo', 'Debe seleccionar un sólo tipo de redondeo');
            }
        });

        if ($validator->fails()) {
            return redirect($this->base_url . 'consorcios/create')->withErrors($validator)->withInput();
        }

        $request["administrador_id"] = Auth::user()->administrador_id;

        $consorcio = Consorcio::create($request->all());

        if ($request->archivo != "") {
            $archivo = $this->subirArchivo($request);
            $consorcio->archivo = $archivo['url'];
        }

        if ($request->firma != "") {
            $firma = $this->subirFirma($request);
            $consorcio->firma = $firma['url'];
        }


        if ($request['expensa_fija'] == 1) {
            $consorcio->monto_fijo_a = $request->monto_fijo_a;
            $consorcio->monto_fijo_b = $request->monto_fijo_b;
            $consorcio->monto_fijo_c = $request->monto_fijo_c;
        }
        $consorcio->redondeo = $request->redondeo;
        $consorcio->redondeo_0 = $request->redondeo_0;
        $consorcio->titular = $request->titular;
        $consorcio->banco = $request->banco;
        $consorcio->cbu = $request->cbu;
        $consorcio->cuit = $request->cuit;
        $consorcio->tipo = $request->tipo;
        $consorcio->numero_de_cuenta = $request->numero_de_cuenta;
        $consorcio->sucursal = $request->sucursal;
        $consorcio->domicilio = $request->domicilio;
        $consorcio->email = $request->email;
        $consorcio->nota_expensas = $request->nota_expensas;
        $consorcio->cochera_complementaria = $request->cochera_complementaria;
        $consorcio->tel_encargado = $request->tel_encargado;
        $consorcio->email_encargado = $request->email_encargado;

        if(Auth::user()->esSuperAdmin()){
            $consorcio->precioXuf = $request->precioXuf;
        }        
        $consorcio->save();

        return redirect('/admin/consorcios/')->with('consorcio_creado', 'Consorcio: "' . $request->nombre . '" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consorcio = Consorcio::findOrFail($id);
        return view('consorcios.show')->with('consorcio', $consorcio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consorcio = Consorcio::findOrFail($id);
        return view('consorcios.edit')->with('consorcio', $consorcio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:100',
            'archivo' => 'max:1000|mimes:jpg,jpeg,png,gif',
            'firma' => 'max:1000|mimes:jpg,jpeg,png,gif',
            'domicilio' => 'required',
            'expensa_fija' => 'required',
            //'interes_vto_2' => 'required',
            'interes_vto_1' => 'required',
            'dia_vto_1' => 'required',
            //'dia_vto_2'     => 'required',
            'cuit' => 'max:100',
            'cbu' => 'size:22',
            'email' => 'required|email',
            'monto_fijo_a' => 'required_if:expensa_fija,1',
            'monto_fijo_b' => 'required_if:expensa_fija,1',
            'monto_fijo_c' => 'required_if:expensa_fija,1',
            'precioXuf'     => 'numeric'

        ]);

        $validator->after(function ($validator) use ($request) {
            if ($request->expensa_fija == 1) {
                if ($request->monto_fijo_a == "" || $request->monto_fijo_b == "" || $request->monto_fijo_c == "") {
                    $validator->errors()->add('monto_fijo_a', 'Los montos fijos son obligatorios');
                }
            }
            if ($request->redondeo == "1" && $request->redondeo_0 == "1") {
                $validator->errors()->add('redondeo', 'Debe seleccionar un sólo tipo de redondeo');
            }
        });

        if ($validator->fails()) {
            return redirect($this->base_url . 'consorcios/' . $id . '/edit')->withErrors($validator)->withInput();
        }


        $consorcio = Consorcio::findOrFail($id);
        $consorcio->update($request->all());

        if ($request->archivo != "") {
            $archivo = $this->subirArchivo($request);
            $consorcio->archivo = $archivo['url'];
        }

        if ($request->firma != "") {
            $firma = $this->subirFirma($request);
            $consorcio->firma = $firma['url'];
        }

        if ($request['expensa_fija'] == 1) {
            $consorcio->monto_fijo_a = $request->monto_fijo_a;
            $consorcio->monto_fijo_b = $request->monto_fijo_b;
            $consorcio->monto_fijo_c = $request->monto_fijo_c;
        }
        $consorcio->redondeo = $request->redondeo;
        $consorcio->redondeo_0 = $request->redondeo_0;
        $consorcio->titular = $request->titular;
        $consorcio->banco = $request->banco;
        $consorcio->cbu = $request->cbu;
        $consorcio->cuit = $request->cuit;
        $consorcio->tipo = $request->tipo;
        $consorcio->numero_de_cuenta = $request->numero_de_cuenta;
        $consorcio->sucursal = $request->sucursal;
        $consorcio->email = $request->email;
        $consorcio->nota_expensas = $request->nota_expensas;
        $consorcio->cochera_complementaria = $request->cochera_complementaria;

        $consorcio->save();

        return redirect('/admin/consorcios/')->with('consorcio_actualizado', 'Consorcio: "' . $consorcio->nombre . '" actualizado');
    }

    /**
     * Elimino el consorcio si no hay UnidadesFuncionales asociadas
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $consorcio = Consorcio::findOrFail($id);

        if ($consorcio->UnidadesFuncionales->count() <= 0) {
            // Lo elimino
            $consorcio->delete();

            return redirect('/admin/consorcios/')->with('consorcio_eliminado', 'Consorcio: "' . $consorcio->nombre . '" eliminado');
        } else {
            return redirect('/admin/consorcios/')->with('consorcio_tiene_uf', 'El Consorcio: "' . $consorcio->nombre . '" no puede eliminarse porque tiene unidades funcionales relacionadas.');
        }
    }

    private function validarConsorcio($request)
    {
        /*if($request['expensa_fija'] == 1){
            $this->validate($request, [
                'monto_fijo_a'      => 'required',
                'monto_fijo_b'      => 'required',
                'monto_fijo_c'      => 'required',

            ]);
        }*/
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

    public function subirFirma(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->firma->getClientOriginalName();
        $extension = $request->firma->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->firma->isValid()) {
            if ($request->firma->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->firma->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

}