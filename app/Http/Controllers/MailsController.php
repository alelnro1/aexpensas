<?php

namespace App\Http\Controllers;

use App\Classes\Mails\HMail;
use App\UnidadFuncional;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mails = HMail::dameMisMailsEnviados();

        return view('hmail.listar')->with('mails', $mails);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades_funcionales = UnidadFuncional::where('consorcio_id', $this->getConsorcio())->get();

        return view('hmail.create', [
            'unidades_funcionales' => $unidades_funcionales
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'asunto'    => 'required|max:100',
            'cuerpo'    => 'required|max:500',
            'destinatarios' => 'required'
        ]);

        if ($validator->fails())
            return redirect('admin/mails/create')->withErrors($validator)->withInput();

        $request->request->add(['from_user_id' => Auth::user()->id]);

        if ($request->borrador == "1") {
            $enviado = false;
        } else {
            $enviado = true;
        }

        $request->request->add(['enviado' => $enviado]);

        $mail = HMail::create($request->all());

        $mail->vincularDestinatarios($request->destinatarios);

        $mail->vincularArchivos($request->urls_archivos);


        // Si se trató de guardar una foto para el local, validarla y subirla
        //$validator = $this->subirYGuardarArchivoSiHay($request, $validator, $mail);

        if ($validator) {
            if ($validator->fails())
                return redirect('mails/create')->withErrors($validator)->withInput();
        }

        return redirect('admin/mails/')->with('mail_creado', 'Mail con nombre ' . $request->nombre . ' creado');
    }

    public function subirAdjuntos(Request $request)
    {
        $directorio_destino = 'uploads/archivos/mails/';

        $adjuntos = $request->adjuntos;
        $nombre_original = $size = $url = null;

        foreach ($adjuntos as $adjunto) {
            $nombre_original    = $adjunto->getClientOriginalName();
            $extension          = $adjunto->getClientOriginalExtension();
            $size               = $adjunto->getClientSize();
            $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

            if ($adjunto->isValid()) {
                if ($adjunto->move($directorio_destino, $nombre_archivo)) {
                    $url = $directorio_destino . $nombre_archivo;

                    return json_encode([
                        'name' => $nombre_original,
                        'size' => $size,
                        'url' => $url,
                        'deleteUrl' => $url,
                        "deleteType" => "DELETE"
                    ]);
                } else {
                    return json_encode([
                        'name' => $nombre_original,
                        'size' => $size,
                        'error' => "No se pudo mover el archivo"
                    ]);
                }
            } else {
                return json_encode([
                    'name' => $nombre_original,
                    'size' => $size,
                    'error' => $adjunto->getErrorMessage()
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mailing = HMail::findOrFail($id);
        return view('hmail.show')->with('mailing', $mailing);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mailing = Mailing::findOrFail($id);
        return view('hmail.edit')->with('mailing', $mailing);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarMailing($request);

        $mailing = Mailing::findOrFail($id);

        $archivo = $this->subirArchivo($request);

        $mailing->update($request->all());

        $mailing->archivo = $archivo;
        $mailing->save();

        return redirect('/mailings/' . $mailing->id)->with('mailing_actualizado', 'Mailing actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailing = Mailing::findOrFail($id);

        $mailing->delete();

        return redirect('/mailings/')->with('mailing_eliminado', 'Mailing con nombre ' . $mailing->nombre . ' eliminado');
    }

    private function validarMailing($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo'     => 'required|max:1000|mimes:jpg,jpeg,png,gif',
            'fecha'       => 'required|date',
            'password'    => 'required|confirmed|min:6',
            'domicilio'   => 'required',
            'email'       => 'required|email|max:100',
            'telefono'    => 'required'
        ]);
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param ${{ABM-SINGULAR}}
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $mail)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $mail->archivo = $archivo['url'];
                $mail->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

}