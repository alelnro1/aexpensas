<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Rol;

class RolesController extends Controller
{
    public function nuevo(){
        return view('roles/nuevo');
    }

    public function create(Request $request){
        $this->validate($request, [
            'nombre' => 'required|unique:roles|max:100'
        ]);

        Rol::create([
            'nombre' => $request->nombre
        ]);

        return redirect('/roles');
    }
}
