<?php

namespace App\Http\Controllers;

use App\Consorcio;
use App\EstadoReserva;
use App\Sum;
use App\UnidadFuncional;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Reserva;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ReservasController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservas =
            Reserva::whereHas('Sum', function ($query) {
                $query->where('consorcio_id', $this->getConsorcio());
            })
                ->with([
                    'Sum' => function ($query) {
                        $query->select(['id', 'nombre']);
                    },
                    'UnidadFuncional' => function ($query) {
                        $query->select(['id', 'codigo', 'piso', 'dpto']);
                    },
                    'Turno' => function ($query) {
                        $query->select(['id', 'hora_inicio', 'hora_fin']);
                    },
                    'EstadoReserva' => function ($query) {
                        $query->select(['id', 'descripcion']);
                    }
                ])
                ->get();
        return view('reservas.listar')->with('reservas', $reservas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reservar($id)
    {
        // Busco el Sum
        $sum = Sum::findOrFail($id);
        $sum->load('Turnos');

        $unidades_funcionales = $this->getUnidadesFuncionales();

        return view('reservas.create', [
            'sum' => $sum,
            'unidades_funcionales' => $unidades_funcionales
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function procesarReserva($id, Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'descripcion'       => 'max:255',
            'fecha'             => 'required',
            'turno_id'             => 'required',
            'unidad_funcional_id'  => 'required'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'sums/' . $id . '/reservar')->withErrors($validator)->withInput();

        if (Auth::user()->esAdmin()) {
            // Buscar estado aceptado
            $estado_pendiente_id = EstadoReserva::where('descripcion', 'Aceptado')->select('id')->first()->id;
        } else {
            // Buscar estado pendiente
            $estado_pendiente_id = EstadoReserva::where('descripcion', 'Pendiente')->select('id')->first()->id;
        }

        $request->request->add([
            'sum_id' => $id,
            'estado_id' => $estado_pendiente_id
        ]);

        $reserva = Reserva::create($request->all());

        if (Auth::user()->esAdmin()) {
            // Hay que avisarle al usuario que se aceptó su reserva
            $this->enviarMailAUF($request->unidad_funcional_id, $reserva);
        } else {
            // Hay que avisarle al admin que se solicitó una reserva
            $this->enviarMailAAdmin($reserva);
        }

        return redirect($this->base_url . 'sums/')->with('reserva_creada', 'Reserva creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reserva = Reserva::findOrFail($id);

        return view('reservas.show')->with('reserva', $reserva);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reserva = Reserva::findOrFail($id);
        $reserva->load([
            'Turno'
        ]);

        $estados = EstadoReserva::all();

        $unidades_funcionales = $this->getUnidadesFuncionales();

        return view('reservas.edit', [
            'reserva' => $reserva,
            'unidades_funcionales' => $unidades_funcionales,
            'estados' => $estados
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'descripcion'       => 'max:255',
            'fecha'             => 'required',
            'turno_id'             => 'required',
            'unidad_funcional_id'  => 'required',
            'estado_id'     => 'required'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'reservas/' . $id . '/edit')->withErrors($validator)->withInput();

        $reserva = Reserva::findOrFail($id);

        $reserva->descripcion = $request->descripcion;
        $reserva->fecha = $request->fecha;
        $reserva->turno_id = $request->turno_id;
        $reserva->unidad_funcional_id = $request->unidad_funcional_id;
        $reserva->estado_id = $request->estado_id;

        $reserva->save();

        return redirect($this->base_url . 'reservas/' . $reserva->id)->with('reserva_actualizado', 'Reserva actualizada');
    }

    public function aceptar($id)
    {
        $reserva = Reserva::findOrFail($id);

        $estado_aceptado = EstadoReserva::where('descripcion', 'Aceptado')->select(['id'])->first()->id;

        $reserva->estado_id = $estado_aceptado;
        $reserva->save();

        $this->enviarMailAUF($reserva->unidad_funcional_id, $reserva);

        return back()->with('reserva_aceptada', 'Reserva aceptada');
    }

    public function rechazar($id)
    {
        $reserva = Reserva::findOrFail($id);

        $estado_aceptado = EstadoReserva::where('descripcion', 'Rechazado')->select(['id'])->first()->id;

        $reserva->estado_id = $estado_aceptado;
        $reserva->save();

        return back()->with('reserva_aceptada', 'Reserva rechazada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        $reserva = Reserva::findOrFail($id);
        $sum_id = $reserva->sum_id ;

        $reserva->delete();

        return redirect($this->base_url . 'sums/' . $sum_id)->with('reserva_eliminada', 'Reserva eliminada');
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

    private function getUnidadesFuncionales()
    {
        // Si es propietario traigo las UF de él. Sino traigo todas
        if(Auth::user()->esPropietario()) {
            $unidades_funcionales =
                UnidadFuncional::where('consorcio_id', $this->getConsorcio())
                    ->whereHas(
                        'Usuarios', function ($query) {
                        $query->where('usuario_id', Auth::user()->id);
                    })
                    ->get();
        } else if (Auth::user()->esAdmin()) {
            $unidades_funcionales =
                UnidadFuncional::where('consorcio_id', $this->getConsorcio())->get();
        }

        return $unidades_funcionales;
    }

    private function enviarMailAUF($unidad_funcional_id, $reserva)
    {
        $uf_users = $this->getUsuariosDeUF($unidad_funcional_id);

        if (count($uf_users->Usuarios) > 0) {
            $users = $uf_users->Usuarios;

            Mail::send('emails.reservas.reserva-aceptada-uf', ['reserva' => $reserva], function ($mail) use ($users, $reserva) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas');

                foreach ($users as $user) {
                    $mail->cc($user->email, $user->nombre)
                        ->subject('Reserva Aceptada de UF ' .
                            $reserva->UnidadFuncional->codigo . '|' .
                            $reserva->UnidadFuncional->piso . '-' .
                            $reserva->UnidadFuncional->dpto );
                }
            });
        }
    }

    private function enviarMailAAdmin($reserva)
    {
        // Si existe el mail en la sesion, envio. Sino busco del consorcio a ver si se actualizó y no esta en la sesion
        $admin_email = Consorcio::where('id', $this->getConsorcio())->select(['id', 'email'])->first()->email;

        if ($admin_email != "") {
            Mail::send('emails.reservas.reserva-solicitada-admin', ['reserva' => $reserva], function ($mail) use ($reserva, $admin_email) {
                $mail->from('no-reply@aexpensas.com', 'AExpensas')
                    ->to($admin_email, session('CONSORCIO')->Administrador->nombre)
                    ->subject('Reserva Solicitada de UF ' .
                        $reserva->UnidadFuncional->codigo . '|' .
                        $reserva->UnidadFuncional->piso . '-' .
                        $reserva->UnidadFuncional->dpto);
            });
        }
    }
}