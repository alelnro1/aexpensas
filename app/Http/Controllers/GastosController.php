<?php

namespace App\Http\Controllers;

use App\GastoUnidadFuncional;
use App\Rubro;
use App\TipoExpensa;
use App\TipoGasto;
use App\UnidadFuncional;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Gasto;
use App\Administrador;
use Illuminate\Support\Facades\Auth;
use App\Consorcio;
use App\Cuota;
use Illuminate\Support\Facades\DB;
use App\Empleado;
use App\DetalleSueldo;


class GastosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuota = new Cuota();
        $cuotas = $cuota->dameTodasLasCuotasParaListado();
        $cuotas_agrupadas = [];
        $totales = [];

        // Recorro para agrupar por mes
        foreach ($cuotas as $cuota) {
            $mes_anio = date("Y-m", strtotime($cuota->fecha));
            $cuotas_agrupadas[$mes_anio][] = $cuota;
            $descripcion = $cuota->Gasto->TipoExpensa->descripcion;

            // Si la cuota es Cochera => hay que ver cuantas cocheras hay para multiplicar
            if ($descripcion == "Cochera") {
                // Busco la cantidad de cocheras con ese id
                $cantidad_cocheras = GastoUnidadFuncional::cantidadDeUFsConGasto($cuota->Gasto->id);

                $cuota->importe = $cuota->importe_real * $cantidad_cocheras;
            } else if ($descripcion == "Particular") {
                $cantidad_particulares = GastoUnidadFuncional::cantidadDeUFsConGasto($cuota->Gasto->id);

                $cuota->importe = $cuota->importe_real * $cantidad_particulares;
            }

            if (!isset($totales[$mes_anio])) { // Para entrar a la primer cuota del array
                $totales[$mes_anio] = $cuota->importe;
            } else {
                $totales[$mes_anio] = $totales[$mes_anio] + $cuota->importe;
            }
        }

        ksort($cuotas_agrupadas);

        $cuotas_agrupadas = array_reverse($cuotas_agrupadas, true);

        $cuotas_chequeo = $cuotas;

        return view('gastos.listar', array('cuotas_agrupadas' => $cuotas_agrupadas, 'cuotas_chequeo' => $cuotas_chequeo, 'totales' => $totales));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $administrador = Administrador::where('id', Auth::User()->administrador_id)->first();
        $consorcio = Consorcio::where('id', $this->getConsorcio())->first();
        $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
        $rubros = Rubro::where('id', '<>', $rubro_id)->get();
        $tipos_expensas = TipoExpensa::all();
        $cantidad_cocheras = UnidadFuncional::cantidadUFsConCochera();

        $proveedores = $administrador->Proveedores;
        $tipos_gastos = $administrador->TipoGastos;
        $unidades_funcionales = $consorcio->UnidadesFuncionales;

        return view('gastos.create', [
            'tipos_gastos' => $tipos_gastos,
            'tipos_expensas' => $tipos_expensas,
            'rubros' => $rubros,
            'unidades_funcionales' => $unidades_funcionales,
            'proveedores' => $proveedores,
            'cantidad_cocheras' => $cantidad_cocheras
        ]);

    }

    public function cargarSueldo()
    {
        //
        $administrador = Administrador::where('id', Auth::User()->administrador_id)->first();
        $consorcio = Consorcio::where('id', $this->getConsorcio())->first();
        $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
        $tipos_expensa_id = TipoExpensa::getId('A');
        $tipos_gasto_id = TipoGasto::getId('Sueldos', $administrador->id);
        $empleados = Empleado::where('consorcio_id', $consorcio->id)->where('baja',null)->get();
        return view('gastos.cargar-sueldo', [
            'tipo_gasto_id' => $tipos_gasto_id,
            'tipo_expensa_id' => $tipos_expensa_id,
            'rubro_id' => $rubro_id,
            'empleados' => $empleados,
        ]);
    }

    public function clonar($gasto_id)
    {
        $gasto = Gasto::where('id', $gasto_id)->with([
            'DetallesSueldos',
            'TipoGasto',
            'TipoExpensa',
            'Rubro',
            'Empleado',
        ])->first();

        $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
        $proveedores = Administrador::find(Auth::User()->administrador_id)->Proveedores()->get();
        $administrador = Administrador::where('id', Auth::User()->administrador_id)->first();

        //Si el rubro NO es Detalle De Sueldos Y Cargas Sociales, entonces significa que no es un sueldo
        if ($rubro_id != $gasto->rubro_id) {
            $rubros = Rubro::where('id', '<>', $rubro_id)->get();
            $tipos_gastos = Administrador::find(Auth::User()->administrador_id)->TipoGastos()->get();
            $tipos_expensas = TipoExpensa::all();
            $unidades_funcionales = Consorcio::find(parent::getConsorcio())->UnidadesFuncionales()->get();
            $cantidad_cocheras = UnidadFuncional::cantidadUFsConCochera();
            return view('gastos.clonar', array('tipos_gastos' => $tipos_gastos, 'tipos_expensas' => $tipos_expensas, 'rubros' => $rubros, 'unidades_funcionales' => $unidades_funcionales, 'proveedores' => $proveedores, 'gasto' => $gasto, 'cantidad_cocheras' => $cantidad_cocheras));

        } else {
            $gasto->with('DetallesSueldos');
            $consorcio = Consorcio::where('id', $this->getConsorcio())->first();
            $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
            $tipos_expensa_id = TipoExpensa::getId('A');
            $tipos_gasto_id = TipoGasto::getId('Sueldos', $administrador->id);
            $empleados = Empleado::where('consorcio_id', $consorcio->id)->get();

            return view('gastos.clonar-sueldo', [
                'tipo_gasto_id' => $tipos_gasto_id,
                'tipo_expensa_id' => $tipos_expensa_id,
                'rubro_id' => $rubro_id,
                'empleados' => $empleados,
                'gasto' => $gasto
            ]);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarGasto($request);
        $request['consorcio_id'] = Consorcio::find(parent::getConsorcio())->id;
        $gasto = Gasto::create($request->all());
        $i = 0;
        $unidades_funcionales = $request->unidades_funcionales_seleccionadas;

        if ($request->archivo != "") {
            $archivo = $this->subirArchivo($request);
            $gasto->archivo = $archivo;
            $gasto->save();
        }

        if ($request->proveedor_id != "") {
            $gasto->proveedor_id = $request->proveedor_id;
            $gasto->save();
        }

        if ($request->fecha != "") {
            foreach ($request->fecha as $fecha) {
                // El monto total de la cuota
                $monto = $request->cuotas[$i];

                // Si el gasto es particular hay que dividir cada cuota por la cantidad de unidades funcionales seleccionadas
                // Si el gasto es cochera hay que dividir cada cuota por la cantidad de ufs que tienen cochera
                if ($request->tipo_expensa_id == '4') {
                    $monto = $monto / count($unidades_funcionales);
                } else if ($request->tipo_expensa_id == '6') {
                    $ufs_con_cocheras = UnidadFuncional::cantidadUFsConCochera();

                    if ($ufs_con_cocheras > 0) {
                        $monto = $monto / $ufs_con_cocheras;
                    }
                }

                $numero_de_cuota = $i + 1;
                $descripcion = $numero_de_cuota . ' de ' . count($request->cuotas);
                $this->crearCuota($gasto, $monto, $fecha, $descripcion);
                $i++;
            }
        }

        // Si el gasto es particular o de cochera, se debe crear la relacion
        if ($request->tipo_expensa_id == '4') {
            if ($request->unidades_funcionales_seleccionadas != "") {
                foreach ($request->unidades_funcionales_seleccionadas as $unidad_funcional) {
                    $this->crearRelacionesGastoConUnidadFuncional($unidad_funcional, $gasto);
                }
            }
        } else if ($request->tipo_expensa_id == '6') {
            $ufs_con_cocheras = UnidadFuncional::getUFsConCochera();

            foreach ($ufs_con_cocheras as $uf) {
                $this->crearRelacionesGastoConUnidadFuncional($uf, $gasto);
            }
        }

        return redirect('/admin/gastos/')->with('gasto_creado', 'Gasto: "' . $gasto->id . '" creado');
    }

    /**
     * storeSueldo a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeSueldo(Request $request)
    {

        $this->validarSueldo($request);
        $request['consorcio_id'] = Consorcio::find(parent::getConsorcio())->id;
        $gasto = Gasto::create($request->all());
        $i = 0;

        if ($request->archivo != "") {
            $archivo = $this->subirArchivo($request);
            $gasto->archivo = $archivo;
            $gasto->save();
        }


        if ($request->campo != "") {
            foreach ($request->campo as $key => $campo) {
                // El valor del campo
                $valor = $request->valor[$key];

                DetalleSueldo::create([
                    'campo' => $campo,
                    'valor' => $valor,
                    'gasto_id' => $gasto->id
                ]);
            }
            //ACA CREAR LA CUOTA

            $this->crearCuota($gasto, $request->cuota, $request->fecha, '1 de 1');
        }


        return redirect('/admin/gastos/')->with('gasto_creado', 'Gasto: "' . $gasto->id . '" creado');
    }

    /**
     * Se crea un registro por cada cuota y se dividide
     * @param $unidad_funcional
     * @param $gasto
     */
    public function crearRelacionesGastoConUnidadFuncional($unidad_funcional, $gasto)
    {
        $gasto->UnidadesFuncionales()->attach($unidad_funcional);
    }

    public function crearCuota($gasto, $monto, $fecha, $descripcion)
    {
        $fecha = str_replace("/", "-", $fecha);

        $cuota = Cuota::create();
        $cuota->importe = $monto;
        $cuota->importe_real = $monto;
        $cuota->fecha = date("Y/m/d", strtotime($fecha));
        $cuota->gasto_id = $gasto->id;
        $cuota->descripcion = $descripcion;
        $cuota->save();
    }

    public function crearDetalleSueldo($id, $campo, $valor)
    {

        $detalle = DetalleSueldo::create();

        $detalle->campo = $campo;

        $detalle->valor = $valor;

        $detalle->gasto_id = $id;

        $detalle->save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gasto = Gasto::findOrFail($id)
            ->with([
                'Cuotas',
                'TipoGasto',
                'TipoExpensa',
                'Rubro',
                'UnidadesFuncionales',
                'Proveedor',
                'Empleado',
                'DetallesSueldos'
            ])->where('id', $id)->first();

        $cantidad_cocheras = UnidadFuncional::cantidadUFsConCochera();

        $detalles_sueldo = DB::table('detalles_sueldos')->where('gasto_id', $id)->get();

        if ($gasto->rubro->id == Rubro::getId('Detalle De Sueldos Y Cargas Sociales') && $gasto->created_at > '2017-04-16 12:24:00') {
            return view('gastos.show-sueldo', array('gasto' => $gasto));
        }

        foreach ($gasto->Cuotas as $cuota) {
            if ($cuota->Gasto->TipoExpensa->id == 6) {
                $cuota->importe = $cuota->importe_real * $cantidad_cocheras;
            }  else if ($cuota->Gasto->TipoExpensa->descripcion == "Particular") {
                $cantidad_particulares = GastoUnidadFuncional::cantidadDeUFsConGasto($cuota->Gasto->id);

                $cuota->importe = $cuota->importe_real * $cantidad_particulares;
            }
        }

        return view('gastos.show', [
            'gasto' => $gasto,
            'cantidad_cocheras' => $cantidad_cocheras
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function cantidad_cuotas_liquidadas($id)
    {
        return $cantidad_de_cuotas_liquidadas = Cuota::where('gasto_id', $id)->where('liquidacion_id', '<>', null)->count();

    }

    public function edit($id)
    {
        $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
        $administrador = Administrador::where('id', Auth::User()->administrador_id)->first();
        $gasto = Gasto::findOrFail($id);

        if ($rubro_id != $gasto->rubro_id || $gasto->created_at < '2017-04-16 12:24:00') {
            $cantidad_de_cuotas_liquidadas = $this->cantidad_cuotas_liquidadas($id);
            $gasto = Gasto::findOrFail($id)
                ->with('Cuotas')
                ->with('TipoGasto')
                ->with('TipoExpensa')
                ->with('Rubro')
                ->with('UnidadesFuncionales')
                ->with('Proveedor')
                ->where('id', $id)->first();

            $proveedores = Administrador::find(Auth::User()->administrador_id)->Proveedores()->get();
            $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
            $rubros = Rubro::where('id', '<>', $rubro_id)->get();
            $tipos_gastos = Administrador::find(Auth::User()->administrador_id)->TipoGastos()->get();
            $tipos_expensas = TipoExpensa::all();
            $unidades_funcionales = Consorcio::find(parent::getConsorcio())->UnidadesFuncionales()->get();
            $cantidad_cocheras = UnidadFuncional::cantidadUFsConCochera();

            foreach ($gasto->Cuotas as $cuota) {
                if ($cuota->Gasto->TipoExpensa->id == 6) {
                    $cuota->importe = $cuota->importe_real * $cantidad_cocheras;

                    // Voy a truncar a dos decimales
                    $miles = $cuota->importe * 100;
                    $entero = intval(strval($miles));
                    $cuota->importe = $entero / 100;
                }  else if ($cuota->Gasto->TipoExpensa->descripcion == "Particular") {
                    $cantidad_particulares = GastoUnidadFuncional::cantidadDeUFsConGasto($cuota->Gasto->id);

                    $cuota->importe = $cuota->importe_real * $cantidad_particulares;

                    // Voy a truncar a dos decimales
                    $miles = $cuota->importe * 100;
                    $entero = intval(strval($miles));
                    $cuota->importe = $entero / 100;
                }
            }

            return view('gastos.edit', [
                'cantidad_de_cuotas_liquidadas' => $cantidad_de_cuotas_liquidadas,
                'gasto' => $gasto,
                'tipos_gastos' => $tipos_gastos,
                'tipos_expensas' => $tipos_expensas,
                'rubros' => $rubros,
                'unidades_funcionales' => $unidades_funcionales,
                'proveedores' => $proveedores,
                'cantidad_cocheras' => $cantidad_cocheras
            ]);


        } else {
            $gasto = Gasto::where('id', $id)->with([
                'DetallesSueldos',
                'TipoGasto',
                'TipoExpensa',
                'Rubro',
                'Empleado',
            ])->first();
            $gasto->with('DetallesSueldos');
            $consorcio = Consorcio::where('id', $this->getConsorcio())->first();
            $rubro_id = Rubro::getId('Detalle De Sueldos Y Cargas Sociales');
            $tipos_expensa_id = TipoExpensa::getId('A');
            $tipos_gasto_id = TipoGasto::getId('Sueldos', $administrador->id);
            $empleados = Empleado::where('consorcio_id', $consorcio->id)->get();

            return view('gastos.edit-sueldo', [
                'tipo_gasto_id' => $tipos_gasto_id,
                'tipo_expensa_id' => $tipos_expensa_id,
                'rubro_id' => $rubro_id,
                'empleados' => $empleados,
                'gasto' => $gasto
            ]);


        }
    }


    public function eliminar_cuotas_no_liquidadas($id)
    {
        return Cuota::where('gasto_id', $id)->where('liquidacion_id', null)->delete();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $ido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cantidad_de_cuotas_liquidadas = $this->cantidad_cuotas_liquidadas($id);

        $gasto = Gasto::findOrFail($id);
        if ($cantidad_de_cuotas_liquidadas > 0) {
            $request['rubro_id'] = $gasto->rubro_id;
            $request['tipo_expensa_id'] = $gasto->tipo_expensa_id;
            $request['tipo_gastos_id'] = $gasto->tipo_gastos_id;
            $request['proveedor_id'] = $gasto->proveedor_id;
            $request['descripcion'] = $gasto->descripcion;
        }
        $this->validarGasto($request);
        $gasto->update($request->all());


        $i = 0;
        if ($request->archivo != "") {
            $archivo = $this->subirArchivo($request);
            $gasto->archivo = $archivo;
            $gasto->save();
        }
        if ($request->proveedor_id != "") {
            $gasto->proveedor_id = $request->proveedor_id;
            $gasto->save();
        }
        $this->eliminar_cuotas_no_liquidadas($id);

        if ($request->fecha != "") {
            $unidades_funcionales = $request->unidades_funcionales_seleccionadas;

            foreach ($request->fecha as $fecha) {
                $monto = $request->cuotas[$i];

                // Si el gasto es particular hay que dividir cada cuota por la cantidad de unidades funcionales seleccionadas
                // Si el gasto es cochera hay que dividir cada cuota por la cantidad de ufs que tienen cochera
                if ($request->tipo_expensa_id == '4') {
                    $monto = $monto / count($unidades_funcionales);
                } else if ($request->tipo_expensa_id == '6') {
                    $ufs_con_cocheras = UnidadFuncional::cantidadUFsConCochera();

                    if ($ufs_con_cocheras > 0) {
                        $monto = $monto / $ufs_con_cocheras;
                    }
                }

                $numero_de_cuota = $i + 1;
                $descripcion = ($numero_de_cuota + $cantidad_de_cuotas_liquidadas) . ' de ' . (count($request->cuotas) + $cantidad_de_cuotas_liquidadas);
                $this->crearCuota($gasto, $monto, $fecha, $descripcion);
                $i++;
            }
        }
        if ($cantidad_de_cuotas_liquidadas == 0) {
            $gasto->UnidadesFuncionales()->detach();

            /*$tipo_expensa_objeto = TipoExpensa::find($request->tipo_expensa_id);
            if ($request->unidades_funcionales_seleccionadas != "" && $tipo_expensa_objeto->descripcion == 'Particular') {
                foreach ($request->unidades_funcionales_seleccionadas as $unidad_funcional) {
                    $this->crearRelacionesGastoConUnidadFuncional($unidad_funcional, $gasto);
                }
            }*/

            // Si el gasto es particular o de cochera, se debe crear la relacion
            if ($request->tipo_expensa_id == '4') {
                if ($request->unidades_funcionales_seleccionadas != "") {
                    foreach ($request->unidades_funcionales_seleccionadas as $unidad_funcional) {
                        $this->crearRelacionesGastoConUnidadFuncional($unidad_funcional, $gasto);
                    }
                }
            } else if ($request->tipo_expensa_id == '6') {
                $ufs_con_cocheras = UnidadFuncional::getUFsConCochera();

                foreach ($ufs_con_cocheras as $uf) {
                    $this->crearRelacionesGastoConUnidadFuncional($uf, $gasto);
                }
            }
        } else {
            return redirect($this->base_url . 'gastos')->with('gasto_actualizado', 'Gasto actualizado, recuerde que las unidades funcionales de este gasto no se actualizan ya que fue liquidado');
        }

        return redirect($this->base_url . 'gastos')->with('gasto_actualizado', 'Gasto "' . $gasto->id . '" actualizado');
    }

    public function updateSueldo(Request $request, $id)
    {
        $cantidad_de_cuotas_liquidadas = $this->cantidad_cuotas_liquidadas($id);

        $gasto = Gasto::findOrFail($id);
        if ($cantidad_de_cuotas_liquidadas > 0) {
            $request['descripcion'] = $gasto->descripcion;
        }
        $this->validarSueldoupdate($request);
        $gasto->update($request->all());


        $this->eliminar_cuotas_no_liquidadas($id);

        if ($request->fecha != "") {
            $monto_cuota = $request->cuota;
            $numero_de_cuota = 1;
            $descripcion = $numero_de_cuota . ' de 1';
            $this->crearCuota($gasto, $monto_cuota, $request->fecha, $descripcion);
        }
        //Eliminar todos los detalles y volver a cargar los que me llegaron
        DetalleSueldo::where('gasto_id',$id)->delete();
        if ($request->campo != "") {
            foreach ($request->campo as $key => $campo) {
                // El valor del campo
                $valor = $request->valor[$key];

                DetalleSueldo::create([
                    'campo' => $campo,
                    'valor' => $valor,
                    'gasto_id' => $gasto->id
                ]);
            }}
        return redirect($this->base_url . 'gastos')->with('gasto_actualizado', 'Gasto "' . $gasto->id . '" actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cantidad_de_cuotas_liquidadas = $this->cantidad_cuotas_liquidadas($id);
        $gasto = Gasto::findOrFail($id);
        if ($cantidad_de_cuotas_liquidadas > 0) {
            return redirect($this->base_url . 'gastos')->with('gasto_eliminado', 'El Gasto: "' . $gasto->id . '" no pudo ser eliminado ya que tiene' . $cantidad_de_cuotas_liquidadas);
        }
        DetalleSueldo::where('gasto_id', $id)->delete();

        $gasto->UnidadesFuncionales()->detach();
        $this->eliminar_cuotas_no_liquidadas($id);
        $gasto->delete();

        return redirect($this->base_url . 'gastos')->with('gasto_eliminado', 'Gasto: "' . $gasto->id . '" eliminado');
    }

    private function validarGasto($request)
    {
        $this->validate($request, [
            'rubro_id' => 'required',
            'tipo_expensa_id' => 'required',
            'tipo_gastos_id' => 'required',

        ]);
    }


    private function validarSueldo($request)
    {
        $this->validate($request, [
            'rubro_id' => 'required',
            'tipo_expensa_id' => 'required',
            'tipo_gastos_id' => 'required',
            'empleado_id' => 'required',
            'cuota' => 'required',
        ]);
    }

    private function validarSueldoupdate($request)
    {
        $this->validate($request, [
            'empleado_id' => 'required',
            'cuota' => 'required',
        ]);
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}