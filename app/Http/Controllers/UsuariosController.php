<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Rol;
use App\UnidadFuncional;
use App\Consorcio;

class UsuariosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('usuarios.listar')->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarUsuario($request);

        // Obtengo de la ruta (/register) o (/admin/register) si voy a crear un usuario
        $ruta = $request->route()->getUri();

        $ruta = explode('/', $ruta)[0];

        if ($ruta == "admin")
            $es_admin = true;
        else
            $es_admin = false;
        if (!$es_admin) {
            $rol_id = Rol::where('nombre', 'propietario')->get()[0]->id;


        }
        if ($es_admin) {
            $rol_id = Rol::where('nombre', 'admin')->get()[0]->id;
        }
        $request->request->add(['rol_id' => $rol_id]);

        $unidad_funcional = UnidadFuncional::where('codigo_asociacion', $request->codigo_asociacion)->get();
        if (count($unidad_funcional) > 0) {
            $request['password'] = bcrypt($request['password']);
            $usuario = User::create($request->all());
            $this->crearRelacionesUsuarioConUnidadFuncional($unidad_funcional[0]->id, $usuario);
            return redirect($this->base_url . 'login')->with([
                'registro_correcto' => 'Se ha registrado. Escriba sus datos para ingresar',
                'email_registrado' => $request->email
            ]);
        }
        return redirect('/register/')->with('unidad_rechazada', 'El código de unidad funcional ingresado es incorrecto, intentelo nuevamente');

    }


    public function listado_usuarios_para_admin()
    {
        // Listar usuarios con sus unidades funcionales
        $usuarios =
            User::with([
                'UnidadesFuncionales' => function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());
                }
            ])
                ->whereHas(
                    'UnidadesFuncionales', function ($query) {
                    $query->where('consorcio_id', $this->getConsorcio());
                }
                )
                ->get();

        return view('usuarios.listar')->with('usuarios', $usuarios);
    }

    public function listado_usuarios_segun_uf($unidad_funcional_id, $unidad_funcional)
    {
        // Listar usuarios con sus unidades funcionales
        $usuarios =
            User::with(
                'UnidadesFuncionales')
                ->whereHas(
                    'UnidadesFuncionales', function ($query) use ($unidad_funcional_id) {
                    $query->where('consorcio_id', $this->getConsorcio())->where('unidades_funcionales.id', $unidad_funcional_id);
                }
                )
                ->get();

        return view('usuarios.listar')->with('usuarios', $usuarios)->with('texto_unidad_funcional', 'de la Unidad Funcional con codigo: '. $unidad_funcional);

    }

    public function crearRelacionesUsuarioConUnidadFuncional($unidad_funcional, $usuario)
    {
        $usuario->UnidadesFuncionales()->attach($unidad_funcional);
    }

    private function validarUsuario($request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'codigo_asociacion' => 'required',
        ]);
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}