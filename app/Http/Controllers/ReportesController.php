<?php

namespace App\Http\Controllers;

use App\Comprobante;
use App\Consorcio;
use App\Cuota;
use App\Gasto;
use App\Liquidacion;
use App\Proveedor;
use App\Rubro;
use App\TipoExpensa;
use App\TipoGasto;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Khill\Lavacharts\Lavacharts;

class ReportesController extends Controller
{
    public function index()
    {
        $rubros = Rubro::all();
        $tipos_gastos = $this->obtenerTiposDeGastosAPartirDeGastosExistentes();
        $tipos_expensas = TipoExpensa::all();
        $proveedores = $this->obtenerProveedoresDeGastos();

        // Si el consorcio no tiene cochera, la quito para que no pueda filtrar
        $tipos_expensas =
            $tipos_expensas->filter(function($tipo_expensa) {
                if ($tipo_expensa->descripcion == "Cochera") {
                    $consorcio = Consorcio::where('id', '=', $this->getConsorcio())->first();

                    return $consorcio->cochera_complementaria;
                } else {
                    return true;
                }
            });

        return view('reportes.index', [
            'rubros' => $rubros,
            'tipos_gastos' => $tipos_gastos,
            'tipos_expensas' => $tipos_expensas,
            'proveedores' => $proveedores
        ]);
    }

    private function obtenerProveedoresDeGastos()
    {
        $gastos = new Gasto();
        $gastos_con_proveedores = $gastos->getGastosConProveedores();

        // Inicializo el array donde contendré los proveedores
        $proveedores = [];

        // Agrego los proveedores al array
        foreach ($gastos_con_proveedores as $gasto) {
            if ($gasto->Proveedor && !in_array($gasto->Proveedor, $proveedores)) {
                array_push($proveedores, $gasto->Proveedor);
            }
        }

        return $proveedores;
    }

    private function obtenerTiposDeGastosAPartirDeGastosExistentes()
    {
        $gastos = new Gasto();
        $gastos_con_proveedores = $gastos->getGastosConTiposDeGastos();

        // Inicializo el array donde contendré los proveedores
        $tipos_de_gastos = [];

        // Agrego los proveedores al array
        foreach ($gastos_con_proveedores as $gasto) {
            if ($gasto->TipoGasto && !in_array($gasto->TipoGasto, $tipos_de_gastos)) {
                array_push($tipos_de_gastos, $gasto->TipoGasto);
            }
        }

        return $tipos_de_gastos;
    }

    public function generar(Request $request)
    {
        // Obtengo los datos del form
        $fecha_desde = $request->fecha_desde;
        $fecha_hasta = $request->fecha_hasta;
        $rubros_elegidos = $request->rubros;
        $tipos_expensa_elegidos = $request->tipos_expensas;
        $tipos_gastos_elegidos = $request->tipos_gastos;
        $proveedores_elegidos = $request->proveedores;
        $ver_ingresos_elegido = $request->ver_ingresos;

        // Valido el input
        $validator = Validator::make($request->all(), [
            'fecha_desde'     => 'required',
            'fecha_hasta'     => 'required',
            'rubros'        => 'required',
            'tipos_expensas' => 'required',
            'tipos_gastos'      => 'required',
            'proveedores' => 'required'
        ]);

        $validator->after(function ($validator) use($fecha_desde, $fecha_hasta) {
            $fecha_desde = strtotime($fecha_desde);
            $fecha_hasta = strtotime($fecha_hasta);
            $diff = floor(($fecha_hasta - $fecha_desde) / (60 * 60 * 24));

            if ($diff > 366) {
                $validator->errors()->add('fecha_desde', 'Fechas mayores a 1 año');
            }
        });

        if ($validator->fails()) {
            return redirect($this->base_url . 'reportes')->withErrors($validator)->withInput()->with([
                'fecha_desde'    => $request->fecha_desde,
                'fecha_hasta'    => $request->fecha_hasta,
                'rubros_elegidos'    => $rubros_elegidos,
                'tipos_expensa_elegidos'    => $tipos_expensa_elegidos,
                'tipos_gastos_elegidos'  => $tipos_gastos_elegidos,
                'proveedores_elegidos'  => $proveedores_elegidos,
                'ver_ingresos_elegido' => $ver_ingresos_elegido
            ]);
        }

        // Paso las validaciones => Limpio todas las sesiones para el reporte
        $this->limpiarSesionesParaReporte();

        $liquidacion = new Liquidacion();

        // Busco las liquidaciones entre fechas
        $liquidaciones = $liquidacion->dameLiquidacionesParaReportes($fecha_desde, $fecha_hasta, $this->getConsorcio());
        session(['CONSORCIO_' . $this->getConsorcio() . '_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id => $liquidaciones ]);

        if (isset($ver_ingresos_elegido) && $ver_ingresos_elegido == "on") {
            $ingresos_calculados = $this->calcularIngresos($liquidaciones);
            session(['CONSORCIO_' . $this->getConsorcio() . '_INGRESOS_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id => $ingresos_calculados]);
        }

        // Calculo los egresos
        $egresos_calculados = $this->calcularEgresos($liquidaciones, $rubros_elegidos, $tipos_expensa_elegidos, $tipos_gastos_elegidos, $proveedores_elegidos);
        $cuotas = session('CONSORCIO_' . $this->getConsorcio() . 'CUOTAS_FILTRADOS_USUARIO_' . Auth::user()->id);

        // Inicializo los egresos e ingresos
        $egresos = $ingresos = [];

        // Recorro la liquidaciones, y si no existe en ingresos o egresos es porque no hay movimientos => los agrego con 0
        foreach ($liquidaciones as $liquidacion) {
            if (isset($ver_ingresos_elegido) && $ver_ingresos_elegido == "on") {
                // Veo si hay ingresos para esta liquidacion

                if (!array_key_exists($liquidacion->nombre_periodo, $ingresos_calculados)) {
                    $ingresos[$liquidacion->nombre_periodo] = 0;
                } else {
                    // Hay ingresos calculados para la liquidacion actual
                    $ingresos[$liquidacion->nombre_periodo] = $ingresos_calculados[$liquidacion->nombre_periodo];
                }
            }

            // Veo si hay egresos para esta liquidacion
            if (!array_key_exists($liquidacion->nombre_periodo, $egresos_calculados)){
                $egresos[$liquidacion->nombre_periodo] = 0;
            } else {
                // Calculo cada tipo de egreso y los sumo
                $liquidacion->egresos_tipo_a = $this->getEgresosTipo('A', $cuotas, $liquidacion);
                $liquidacion->egresos_tipo_b = $this->getEgresosTipo('B', $cuotas, $liquidacion);
                $liquidacion->egresos_tipo_c = $this->getEgresosTipo('C', $cuotas, $liquidacion);
                $liquidacion->egresos_tipo_particular = $this->getEgresosTipo('Particular', $cuotas, $liquidacion);
                $liquidacion->egresos_cuota_extra_asignacion = $this->getEgresosTipo('Cuota Extra', $cuotas, $liquidacion);

                // Hay egresos calculados para la liquidacion actual
                $egresos[$liquidacion->nombre_periodo] =
                    $liquidacion->egresos_tipo_a + $liquidacion->egresos_tipo_b
                    + $liquidacion->egresos_tipo_c + $liquidacion->egresos_tipo_particular + $liquidacion->egresos_cuota_extra_asignacion;

            }
        }


        $lava = new Lavacharts();

        $finances = $lava->DataTable();

        $finances->addStringColumn('Período');

        if (isset($ver_ingresos_elegido) && $ver_ingresos_elegido == "on") {
            $finances->addNumberColumn('Ingresos');
        }

        $finances->addNumberColumn('Egresos')
            ->setDateTimeFormat('Y');

        // Recorriendo las liquidaciones voy agregando para el gráfico
        foreach ($liquidaciones as $liquidacion) {
            if (isset($ver_ingresos_elegido) && $ver_ingresos_elegido == "on") {
                $finances->addRow([$liquidacion->nombre_periodo, $ingresos[$liquidacion->nombre_periodo], $egresos[$liquidacion->nombre_periodo]]);
            } else {
                $finances->addRow([$liquidacion->nombre_periodo, $egresos[$liquidacion->nombre_periodo]]);
            }
        }

        $lava->ColumnChart('Finances', $finances, [
            'title' => 'Reporte de Balance',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);


        // Obtengo los datos para reenviar al form
        $rubros = Rubro::all();
        $tipos_gastos = $this->obtenerTiposDeGastosAPartirDeGastosExistentes();
        $tipos_expensas = TipoExpensa::all();
        $proveedores = $this->obtenerProveedoresDeGastos();

        return view('reportes.index', [
            'lava' => $lava,
            'rubros' => $rubros,
            'tipos_gastos' => $tipos_gastos,
            'tipos_expensas' => $tipos_expensas,
            'proveedores' => $proveedores,
            'fecha_desde' => $request->fecha_desde,
            'fecha_hasta' => $request->fecha_hasta,
            'rubros_elegidos' => $rubros_elegidos,
            'tipos_expensa_elegidos' => $tipos_expensa_elegidos,
            'tipos_gastos_elegidos' => $tipos_gastos_elegidos,
            'proveedores_elegidos' => $proveedores_elegidos,
            'ver_ingresos_elegido' => $ver_ingresos_elegido
        ]);

    }

    private function calcularIngresos($liquidaciones)
    {
        $comprobante = new Comprobante();

        // Busco los comprobantes que tengan misma liquidacion
        $comprobantes = $comprobante->dameComprobantesDeLiquidaciones($liquidaciones, $this->getConsorcio());

        return $this->armarEstructuraParaReporteDeArray($comprobantes);
    }

    /**
     * @param $fecha_desde
     * @param $fecha_hasta
     * @param $rubros_elegidos
     * @param $tipos_expensa_elegidos
     * @param $tipos_gastos_elegidos
     */
    private function calcularEgresos($liquidaciones, $rubros_elegidos, $tipos_expensa_elegidos, $tipos_gastos_elegidos, $proveedores_elegidos)
    {
        $gasto = new Gasto();
        $cuota = new Cuota();

        // Busco los gastos filtrados
        $gastos = $gasto->dameGastosParaReportes($rubros_elegidos, $tipos_expensa_elegidos, $tipos_gastos_elegidos, $this->getConsorcio(), $proveedores_elegidos);

        // Busco las cuotas liquidadas de los gastos para calcular los totales
        $cuotas = $cuota->dameCuotasLiquidadasDeGastos($liquidaciones, $gastos);

        // Guardo la sesion con cuotas y gastos
        session(['CONSORCIO_' . $this->getConsorcio() . 'CUOTAS_FILTRADOS_USUARIO_' . Auth::user()->id => $cuotas]);

        return $this->armarEstructuraParaReporteDeArray($cuotas);
    }

    private function armarEstructuraParaReporteDeArray($array)
    {
        // Inicializo los totales por liquidacion
        $totales_por_liquidacion = [];

        // Genero las liquidaciones por nombre
        foreach ($array as $item) {
            // Obtengo el periodo
            $nombre_periodo = $item->Liquidacion->nombre_periodo;

            // Busco si existe el registro en los totales
            if ( !array_key_exists($nombre_periodo, $totales_por_liquidacion) ) {
                $totales_por_liquidacion[$nombre_periodo] = 0;
            }

            // Sumo el total al periodo
            $totales_por_liquidacion[$nombre_periodo] = $totales_por_liquidacion[$nombre_periodo] + $item->importe;
        }

        return $totales_por_liquidacion;
    }

    private function limpiarSesionesParaReporte()
    {
        session(['CONSORCIO_' . $this->getConsorcio() . 'LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id => null]);
        session(['CONSORCIO_' . $this->getConsorcio() . '_INGRESOS_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id => null]);
        session(['CONSORCIO_' . $this->getConsorcio() . 'CUOTAS_FILTRADOS_USUARIO_' . Auth::user()->id => null]);
        session(['CONSORCIO_' . $this->getConsorcio() . '_EGRESOS_LIQUIDACIONES_FILTRADAS_USUARIO_' . Auth::user()->id => null]);
    }

    protected function getEgresosTipo($tipo, $cuotas, $liquidacion)
    {
        // Si son cuotas extras, filtro solo las asignacion
        if ($tipo == "Cuota Extra") {
            $cuotas =
                $cuotas->filter(function ($cuota) use ($tipo, $liquidacion) {
                        return ($cuota->Gasto->tipo_cuota_extra === "1"); // 1= asignacion; 2= recaudacion
                });
        }


        $cuotas_de_tipo =
            $cuotas->filter(function ($cuota) use ($tipo, $liquidacion){
                return ($cuota->Gasto->TipoExpensa->descripcion == $tipo && $cuota->liquidacion_id == $liquidacion->id);
            });

        $total = 0;

        foreach ($cuotas_de_tipo as $cuota) {
            $total = $total + $cuota->importe;
        }

        return $total;
    }
}
