<?php

namespace App\Http\Controllers;

use App\Consorcio;
use App\Cuota;
use App\Documento;
use App\Encuesta;
use App\Gasto;
use App\Http\Requests;
use App\Liquidacion;
use App\RespuestaEncuesta;
use App\RespuestaUnidadFuncional;
use App\TipoGasto;
use App\UnidadFuncional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sum;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'logout']);
        if(!Auth::guest()){

            parent::__construct();
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $unidades_funcionales_deudoras = UnidadFuncional::where('deuda', '>', 0)->where('consorcio_id', $this->getConsorcio())->orderby('deuda', 'desc')->get();
        $unidadesFuncionales = UnidadFuncional::where('consorcio_id', $this->getConsorcio())->get();
        $sums = Sum::where('consorcio_id', $this->getConsorcio())->get();
        $liquidaciones = Liquidacion::where('consorcio_id', $this->getConsorcio())->orderBy('fecha_fin', 'DESC')->take(5)->get();
        $novedades =
            Documento::whereHas('Tipo', function ($query) {
                $query->where('descripcion', '=', 'Novedad');
            })
                ->whereHas('Consorcios', function($query) {
                    $query->where('consorcio_id', '=', $this->getConsorcio());
                })
                ->where('estado', '=', '1')
                ->orderBy('created_at', 'DESC')
                ->get();

        $grafico_totales_por_tipo = $this->armarGraficoDeGastosPorTipo();

        $encuesta = new Encuesta();
        $encuestas_activas = $encuesta->dameEncuestasActivas();

        // Voy guardando las unidades funcionales que pueden votar
        $unidades_funcionales_habilitadas = $this->getUnidadesFuncionalesHabilitadas($encuestas_activas);

        if (count($unidades_funcionales_habilitadas) <= 0 && count($encuestas_activas) > 0) {
            $grafico = new GraficosController();
            $resultados = $grafico->armarGraficoResultadosDeEncuesta($encuestas_activas->first());
        } else {
            $resultados = null;
        }

        return view('home')
            ->with([
                'grafico' => $resultados,
                'unidades_funcionales_habilitadas' => $unidades_funcionales_habilitadas,
                'encuestas_activas' => $encuestas_activas,
                'grafico_totales_por_tipo' => $grafico_totales_por_tipo,
                'unidades_funcionales_deudoras' => $unidades_funcionales_deudoras,
                'unidadesFuncionales' => $unidadesFuncionales,
                'sums' => $sums,
                'novedades' => $novedades,
                'liquidaciones' => $liquidaciones
            ]);
    }

    private function obtenerTotalesPorTipoDeGasto()
    {
        $liquidacion = new Liquidacion();
        $ultima_liquidacion = $liquidacion->dameUltimaLiquidacionDeConsorcio($this->getConsorcio());

        if ($ultima_liquidacion) {
            $ult_liq_id = $ultima_liquidacion->id;

            $totales_por_tipo_gasto =
                TipoGasto::where('administrador_id', Auth::user()->administrador_id)
                    ->leftJoin('gastos', function ($query) {
                        $query->on('tipo_gastos.id', '=', 'gastos.tipo_gastos_id');
                    })
                    ->leftJoin('cuotas_gastos', 'gastos.id', '=', 'cuotas_gastos.gasto_id')
                    ->where('gastos.consorcio_id', $this->getConsorcio())
                    ->where('cuotas_gastos.liquidacion_id', $ult_liq_id)
                    /*->whereHas('Gastos', function ($query) use ($ult_liq_id) {
                        $query->where('consorcio_id', $this->getConsorcio());

                        $query->whereHas('Cuotas', function ($query) use ($ult_liq_id) {
                            $query->where('liquidacion_id', $ult_liq_id);
                        });
                    })*/
                    ->select(['tipo_gastos.id', DB::raw('sum(importe) as total'), 'tipo_gastos.descripcion'])
                    ->groupBy('tipo_gastos.descripcion')
                    ->get();
        } else {
            $totales_por_tipo_gasto = null;
        }

        return $totales_por_tipo_gasto;
    }

    private function armarGraficoDeGastosPorTipo()
    {
        $totales_por_tipo_gasto = $this->obtenerTotalesPorTipoDeGasto();

        $lava = new Lavacharts();

        $grafico = $lava->DataTable();

        $grafico->addStringColumn('Tipo')
            ->addNumberColumn('Total');

        if ($totales_por_tipo_gasto) {

            foreach ($totales_por_tipo_gasto as $total_por_tipo) {
                $grafico->addRow([
                    $total_por_tipo->descripcion, (double)$total_por_tipo->total
                ]);
            }

            $lava->PieChart('TotalesPorTipo', $grafico, [
                'title' => 'Gastos de la Última Liquidación',
                'is3D' => true,
                'slices' => [
                    ['offset' => 0.2],
                    ['offset' => 0.25],
                    ['offset' => 0.3]
                ]
            ]);
        } else {
            $lava = null;
        }

        return $lava;
    }

    private function getUnidadesFuncionalesHabilitadas($encuestas_activas)
    {
        $unidades_funcionales_habilitadas = [];

        $unidades_funcionales_de_usuario =
            UnidadFuncional::where('consorcio_id', $this->getConsorcio())
                ->whereHas('Usuarios', function($query) use ($encuestas_activas) {
                    $query->where('usuario_id', Auth::user()->id);
                })
                ->orderBy('codigo', 'ASC')
                ->get();

        // Recorro las unidades funcionales para ver si votó
        foreach ($unidades_funcionales_de_usuario as $unidad_funcional) {
            foreach ($encuestas_activas as $encuestas_activa) {
                $uf_por_encuesta = RespuestaUnidadFuncional::where('unidad_funcional_id', $unidad_funcional->id)->where('encuesta_id', $encuestas_activa->id)->get();

                if (count($uf_por_encuesta) <= 0) {
                    array_push($unidades_funcionales_habilitadas, $unidad_funcional);
                }
            }
        }

        return $unidades_funcionales_habilitadas;
    }
}
