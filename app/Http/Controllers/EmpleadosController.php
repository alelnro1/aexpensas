<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use App\Empleado;

class EmpleadosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleado::where('consorcio_id', $this->getConsorcio())->get();
        return view('empleados.listar')->with('empleados', $empleados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:100',
            'cuil' => 'required|max:100',
            'legajo' => 'max:100',
            'cargo' => 'max:100',
            'categoria' => 'max:100',
            'sector' => 'max:100',
            'descripcion' => 'max:500',
            'archivo' => 'max:1000|mimes:jpg,jpeg,png,gif',
            'fecha' => 'required|date',
            'domicilio' => 'required',
            'email' => 'email|max:100',
            'telefono' => 'max:100'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'empleados/create')->withErrors($validator)->withInput();

        // Creo el empleado
        $request["consorcio_id"] = $this->getConsorcio();
        $empleado = Empleado::create($request->all());
        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $empleado);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'empleados/create')->withErrors($validator)->withInput();
        }

        return redirect($this->base_url . 'empleados/')->with('empleado_creado', 'Empleado con nombre ' . $request->nombre . ' creado');
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $empleado
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $empleado)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $empleado->archivo = $archivo['url'];
                $empleado->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado = Empleado::findOrFail($id);
        return view('empleados.show')->with('empleado', $empleado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = Empleado::findOrFail($id);
        return view('empleados.edit')->with('empleado', $empleado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:100',
            'cuil' => 'required|max:100',
            'cargo' => 'required|max:100',
            'categoria' => 'max:100',
            'sector' => 'max:100',
            'descripcion' => 'max:500',
            'archivo' => 'max:1000|mimes:jpg,jpeg,png,gif',
            'fecha' => 'date',
            'domicilio' => 'max:100',
            'email' => 'email|max:100',
            'telefono' => 'max:100'
        ]);

        if ($validator->fails())
            return redirect($this->base_url . 'empleados/' . $id . '/edit')->withErrors($validator)->withInput();

        // Busco el empleado
        $empleado = Empleado::findOrFail($id);

        // Actualizo el empleado
        $empleado->update($request->except(['_method', '_token']));

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $empleado);

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'empleados/create')->withErrors($validator)->withInput();
        }

        return redirect($this->base_url . 'empleados')->with('empleado_actualizado', 'Empleado actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleado::findOrFail($id);

        if ($empleado->tenesGastos()) {
            return redirect($this->base_url . 'empleados')->with('empleado_eliminado', 'El empleado no pudo ser eliminado ya que tiene gastos asociados');
        }
        $empleado = Empleado::findOrFail($id);

        $empleado->delete();

        return redirect($this->base_url . 'empleados/')->with('empleado_eliminado', 'Empleado eliminado');
    }

    public function baja($id)
    {
        $empleado = Empleado::findOrFail($id);
        if ($empleado->baja) {
            $empleado->baja = null;
            $empleado->save();

            return redirect($this->base_url . 'empleados/')->with('empleado_eliminado', 'Empleado con nombre ' . $empleado->nombre . ' se dió de alta');
        }
        $empleado->baja = date("Y-m-d H:i:s");
        $empleado->save();
        return redirect($this->base_url . 'empleados/')->with('empleado_eliminado', 'Empleado con nombre ' . $empleado->nombre . ' se dió de baja');

    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original = $request->archivo->getClientOriginalName();
        $extension = $request->archivo->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }


}
