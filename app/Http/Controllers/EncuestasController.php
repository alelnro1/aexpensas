<?php

namespace App\Http\Controllers;

use App\RespuestaEncuesta;
use App\RespuestaUnidadFuncional;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Encuesta;

class EncuestasController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encuesta = new Encuesta();
        $encuestas_activas = $encuesta->dameEncuestasActivas();
        $encuestas_no_activas = $encuesta->dameEncuestasNoActivas();

        return view('encuestas.listar')->with([
            'encuestas_activas' => $encuestas_activas,
            'encuestas_no_activas' => $encuestas_no_activas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('encuestas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'pregunta'      => 'required',
            'respuestas.*'  => 'required'
        ]);

        // Verifico que no haya una encuesta activa
        $validator->after(function($validator) use ($request){
            if (isset($request->estado) && $request->estado == "on") {
                $encuesta = new Encuesta();

                if (count($encuesta->dameEncuestasActivas()) > 0) {
                    $validator->errors()->add('pregunta', 'Ya existe una encuesta activa. No pueden haber dos.');
                }
            }
        });

        if ($validator->fails())
            return redirect($this->base_url . 'encuestas/create')->withErrors($validator)->withInput();

        $request->request->add(['consorcio_id' => $this->getConsorcio()]);
        
        // Creo la encuesta
        $encuesta = Encuesta::create($request->all());

        if ($request->estado == "on") {
            $encuesta->estado = true;
            $encuesta->save();
        }

        // Relaciono las respuestas con la encuesta recien creada
        $this->relacionarRespuestasConEncuesta($encuesta, $request);
        
        return redirect($this->base_url . 'encuestas/')->with('encuesta_creado', 'Encuesta creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $encuesta = Encuesta::findOrFail($id);
        $encuesta->load('Respuestas');

        $grafico = new GraficosController();
        $resultados = $grafico->armarGraficoResultadosDeEncuesta($encuesta);

        return view('encuestas.show')->with([
            'encuesta' => $encuesta,
            'grafico' => $resultados
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $encuesta = Encuesta::findOrFail($id);

        return view('encuestas.edit')->with('encuesta', $encuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'pregunta'      => 'required',
            'respuestas.*'  => 'required'
        ]);

        // Verifico que no haya una encuesta activa
        $validator->after(function($validator) use ($request, $id) {
            if (isset($request->estado) && $request->estado == "on") {
                $encuesta = new Encuesta();

                if (count($encuesta->dameEncuestasActivas($id)) > 0) {
                    $validator->errors()->add('pregunta', 'Ya existe una encuesta activa. No pueden haber dos.');
                }
            }
        });
        
        if ($validator->fails()) 
            return redirect($this->base_url . 'encuestas/' . $id .'/edit')->withErrors($validator)->withInput();

        // Busco el encuesta
        $encuesta = Encuesta::findOrFail($id);

        if ($request->estado == "on") {
            $request->request->add(['estado' => "1"]);
        } else {
            $request->request->add(['estado' => "0"]);
        }

        // TODO: Eliminamos las respuestas que no tengan votos
        if (!$encuesta->tieneVotos()) {
            /*$encuesta->Respuestas()->whereDoesntHave('Votos', function ($query) use ($sum) {
                $query->where('sum_id', $sum->id);
            })->delete();*/
            $encuesta->Respuestas()->delete();

            // Relaciono las respuestas con la encuesta recien creada
            $this->relacionarRespuestasConEncuesta($encuesta, $request);
        }

        // Actualizo el encuesta
        $encuesta->update($request->except(['_method', '_token']));

        if ($validator) {
            if ($validator->fails())
                return redirect($this->base_url . 'encuestas/' . $id . '/edit')->withErrors($validator)->withInput();
        }

        return redirect($this->base_url . 'encuestas')->with('encuesta_actualizado', 'Encuesta actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $encuesta = Encuesta::findOrFail($id);

        $encuesta->delete();

        return redirect($this->base_url . 'encuestas/')->with('encuesta_eliminado', 'Encuesta eliminada');
    }

    private function relacionarRespuestasConEncuesta($encuesta, $request)
    {
        // Le adjunto las respuestas a la encuesta
        $respuestas = $request->respuestas;

        foreach ($respuestas as $key => $value) {
            RespuestaEncuesta::create([
                'encuesta_id' => $encuesta->id,
                'respuesta' => $value
            ]);
        }
    }

    public function votar(Request $request)
    {
        $encuesta_id = $request->encuesta_id;
        $respuesta_id = $request->respuesta_id;
        $uf_id = $request->uf_id;
        $user = Auth::user()->id;

        $resp_uf = new RespuestaUnidadFuncional();

        if ($resp_uf->ufYaRespondioLaEncuesta($uf_id, $encuesta_id)) {
            $respuesta = "Usted ya ha votado en esta encuesta";
            $success = false;
        } else {
            RespuestaUnidadFuncional::create([
                'encuesta_id' => $encuesta_id,
                'respuesta_id' => $respuesta_id,
                'unidad_funcional_id' => $uf_id
            ]);

            $respuesta = "Gracias por votar en esta encuesta";
            $success = true;
        }

        return response()->json([
            'respuesta' => $respuesta,
            'success' => $success
        ]);
    }
}
