<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Proveedor;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Administrador;
class ProveedoresController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Administrador::find(Auth::User()->administrador_id)->Proveedores()->get();

        return view('proveedores.listar')->with('proveedores', $proveedores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validarProveedor($request);
        $request["administrador_id"] =Auth::user()->administrador_id;
        $proveedor = Proveedor::create($request->all());

        if($request->foto != ""){
            $foto = $this->subirfoto($request);
            $proveedor->foto = $foto;

        }

        $proveedor->descripcion = $request->descripcion;
        $proveedor->email = $request->email;
        $proveedor->celular = $request->celular;
        $proveedor->domicilio_fiscal = $request->domicilio_fiscal;
        $proveedor->domicilio_comercial = $request->domicilio_comercial;
        $proveedor->dni = $request->dni;
        $proveedor->cuit_cuil = $request->cuit_cuil;
        $proveedor->persona_contacto = $request->persona_contacto;
        $proveedor->pagina_web = $request->pagina_web;
        $proveedor->condicion_frente_al_iva = $request->condicion_frente_al_iva;

        $proveedor->save();
        return redirect('/admin/proveedores/')->with('proveedor_creado', 'Proveedor: "' . $request->nombre_razon_social . '" creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        return view('proveedores.show')->with('proveedor', $proveedor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        return view('proveedores.edit')->with('proveedor', $proveedor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarProveedor($request);

        $proveedor = Proveedor::findOrFail($id);

        $proveedor->update($request->all());
        if($request->foto != ""){
            $foto = $this->subirfoto($request);
            $proveedor->foto = $foto;

        }

        $proveedor->descripcion = $request->descripcion;
        $proveedor->email = $request->email;
        $proveedor->celular = $request->celular;
        $proveedor->domicilio_fiscal = $request->domicilio_fiscal;
        $proveedor->domicilio_comercial = $request->domicilio_comercial;
        $proveedor->dni = $request->dni;
        $proveedor->cuit_cuil = $request->cuit_cuil;
        $proveedor->persona_contacto = $request->persona_contacto;
        $proveedor->condicion_frente_al_iva = $request->condicion_frente_al_iva;
        $proveedor->pagina_web = $request->pagina_web;
        $proveedor->save();

        return redirect('/admin/proveedores/')->with('proveedor_actualizado', 'Proveedor: "' . $proveedor->nombre_razon_social . '" actualizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proveedor = Proveedor::findOrFail($id);

        $proveedor->delete();

        return redirect('/admin/proveedores/')->with('proveedor_eliminado', 'Proveedor "' . $proveedor->nombre_razon_social . '" eliminado');
    }

    private function validarProveedor($request)
    {
        $this->validate($request, [
            'nombre_razon_social'   => 'required|max:100',
            'foto'                  => 'max:1000|mimes:jpg,jpeg,png,gif',
            'email'                 => 'email|max:100',
        ]);
    }

        /**
     * Subir un foto
     * @param Request $request
     * @return JSON
     */
    public function subirfoto(Request $request)
    {
        $directorio_destino = 'uploads/fotos/';
        $nombre_original    = $request->foto->getClientOriginalName();
        $extension          = $request->foto->getClientOriginalExtension();
        $nombre_foto     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->foto->isValid()) {
            if ($request->foto->move($directorio_destino, $nombre_foto)) {
                $url = $directorio_destino . $nombre_foto;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el foto";
            }
        } else {
            $url = false;
            $error = $request->foto->getErrorMessage();
        }

        return $url;
    }

}