<?php

namespace App\Http\Controllers;

use App\Consorcio;
use App\UnidadFuncional;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;
use App\Administrador;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public $nombre_variable_consorcio_actual;
    public $base_url;

    public function __construct()
    {
        // Defino la url base. Ej: /admin/, /control-panel/
        $this->base_url = env('BASE_URL');

        // Seteo el nombre de la sesion que mantendra el consorcio actual
        $this->nombre_variable_consorcio_actual = 'CONSORCIO';

        // Obtengo el usuario actual
        $user = Auth::user();

        // Si hay alguien logueado => tiene que haber un consorcio elegido
        if ($user) {
            $this->obtenerConsorcioYCompartirConVistas($user);
        }
    }

    /**
     * Obtengo los consorcios y los comparto con las vistas.
     * Si es un admin => vera todos sus consorcios.
     * Si es una unidad funcional => vera un solo consorcio (o los que les correspondan)
     * @param $user
     */
    private function obtenerConsorcioYCompartirConVistas($user)
    {
        // Si es propietario, cargo las unidades funcionales y agarro el primer consorcio
        if ($user->esPropietario()) {
            $unidades_funcionales = $user->load(['UnidadesFuncionales' => function($query){
                $query
                    ->with(['consorcio'])
                    ->groupBy('consorcio_id');
            }]);

            // En realidad no son consorcios, son unidades funcionales para el propietario
            $unidades_funcionales = $user->UnidadesFuncionales;

            // Obtengo solo los consorcios
            $consorcios = [];

            foreach ($unidades_funcionales as $unidad_funcional) {
                $consorcio = $unidad_funcional->consorcio;
                array_push($consorcios, $consorcio);
            }

            $primer_consorcio_de_lista = $consorcios[0];
        } else {
            $consorcios = Administrador::find($user->administrador_id)->Consorcios()->get();
            $primer_consorcio_de_lista = $consorcios->first();
        }


        // Si acabo de iniciar sesion, o el consorcio NO estÃ¡ elegido, cargo el primer resultado de la coleccion de consorcios del usuario
        if (session($this->nombre_variable_consorcio_actual) == null || session($this->nombre_variable_consorcio_actual)->id == null || !$this->getConsorcio()) {
            // Busco el primer consorcio del usuario actual
            $this->setConsorcio($primer_consorcio_de_lista);
        }

        // Comparto con las vistas todos los consorcios (para el select global) y el consorcio actual (El seleccionado)
        view()->share('consorcios', $consorcios);
        view()->share($this->nombre_variable_consorcio_actual, $this->getConsorcio());
        view()->share($this->nombre_variable_consorcio_actual . '_NOMBRE', $this->getConsorcioNombre());
        view()->share('CANTIDAD_ESPACIOS_COMUNES', $this->getCantidadDeEspaciosComunes());

        // Comparto la url base con las vistas
        view()->share('base_url', $this->base_url);
    }

    public function getSumaPorcentajesAyB($unidades_funcionales)
    {
        // Hay que verificar que todos los porcentajes A y B sumen 100%
        $total_porcentaje = [];

        $total_porcentaje['a'] = $total_porcentaje['b'] = 0.00;

        foreach ($unidades_funcionales as $uf) {
            $total_porcentaje['a'] = $total_porcentaje['a'] + $uf->porcentaje_a;
            $total_porcentaje['b'] = $total_porcentaje['b'] + $uf->porcentaje_b;
        }

        return $total_porcentaje;
    }

    /**
     * Setter del consorcio a traves de una sesiÃ³n
     * @param $id
     */
    public function setConsorcio($consorcio)
    {
        if ($consorcio) {
            // Asigno el objeto consorcio a la sesion
            session([$this->nombre_variable_consorcio_actual => $consorcio]);
            echo json_encode(['valid' => true]);
        }
    }

    /**
     * Este metodo se llama cuando cambia de consorcio (o UF) desde la vista en layouts/app.blade
     * @param $consorcio_id
     */
    public function setConsorcioDesdeVista($consorcio_id)
    {
        // Busco el consorcio
        $consorcio = Consorcio::findOrFail($consorcio_id);

        $this->setConsorcio($consorcio);
    }

    /**
     * Getter del id o nombre del consorcio
     * @return mixed
     */
    public function getConsorcio(){
        $id = null;

        if (session($this->nombre_variable_consorcio_actual)) {
            $id = session($this->nombre_variable_consorcio_actual)->id;
        }

        return $id;
    }

    private function hola(){
        echo "hola";
    }

    public function getConsorcioNombre() {
        $nombre = null;

        if (session($this->nombre_variable_consorcio_actual)) {
            $nombre = session($this->nombre_variable_consorcio_actual)->nombre;
        }

        return $nombre;
    }

    /**
     * Acorta un $string a $len caracteres
     * @param $string El string a acortar
     * @param $len La longitud mÃ¡xima
     * @return string
     */
    protected function acortarString($string, $len) {
        $nuevo_string = $string;

        if (strlen($string) > $len) {
            $nuevo_string = substr($string, 0, $len);
            $nuevo_string .= "...";
        }

        return $nuevo_string;
    }

    protected function getAdministradoresDeConsorcio()
    {
        $administrador_id = Consorcio::where('id', $this->getConsorcio())->select(['administrador_id'])->first()->administrador_id;

        $admins = User::where('administrador_id', $administrador_id)->select(['id', 'email', 'nombre'])->get();

        return $admins;
    }

    protected function getUsuariosDeConsorcio()
    {
        $usuarios =
            User::whereHas('UnidadesFuncionales', function ($query) {
                $query->where('consorcio_id', '=', $this->getConsorcio());
            })->get();

        return $usuarios;
    }

    protected function getUsuariosDeUF($id)
    {
        return
            UnidadFuncional::where('id', $id)
            ->with('Usuarios')
            ->first();
    }

    protected function getCantidadDeEspaciosComunes()
    {
        $consorcio = Consorcio::where('id', $this->getConsorcio())->first();

        return $consorcio->getCantidadDeEspaciosComunes();
    }
}

