<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Rol;
use Illuminate\Support\Facades\Config;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/admin/login');
            }
        }

        // Seteo el rol actual del usuario logueado
        $usuario = Auth::user();
        $usuario->load(['rol' => function($query) {
            $query->select(['id', 'nombre']);
        }]);

        session(['USUARIO_ACTUAL' => $usuario]);
        /*$rol = Rol::find(Auth::user()->rol_id);
        Config::set('ACTUAL_ROL', $rol->nombre);*/

        return $next($request);
    }
}
