<?php namespace App\Http\Middleware;

use Closure;
class CheckRole{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role1=null, $role2=null, $role3=null)
    {
        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        /* Armo el array de roles que en PHP 5.6 no hace falta, se pone el parametro ... $roles */
        $argc = func_num_args();

        for ($i = 1; $i <= $argc; $i++) {
            $name = 'role'.$i;
            $roles[] = & $$name;
        }

        if($request->user()->hasRole($roles) || !$roles)
        {
            return $next($request);
        }
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'Usted no tiene autorizacion para ingresar a este area.'
            ]
        ], 401);
    }
}