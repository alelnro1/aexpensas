<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaFinanciera extends Model
{
    protected $table = "categorias_financieras";

    public function CategoriaPadre()
    {
        return $this->hasOne(CategoriaFinanciera::class, 'id', 'padre_id');
    }

    public function CategoriasHijas()
    {
        return $this->hasMany(CategoriaFinanciera::class, 'padre_id', 'id');
    }

    public function EstadosFinancieros()
    {
        return $this->hasMany(EstadoFinanciero::class, 'categoria_financiera_id');
    }
}
