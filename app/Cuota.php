<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cuota extends Model
{
    protected $table = 'cuotas_gastos';

    public function Gasto()
    {
        return $this->belongsTo(Gasto::class, 'gasto_id');
    }

    public function Liquidacion()
    {
        return $this->belongsTo(Liquidacion::class, 'liquidacion_id');
    }

    public function dameTodasLasCuotasParaListado()
    {
        $consorcio_id = session('CONSORCIO')->id;

        return
            $this
                ->with([
                    'Gasto' => function ($query) {
                        $query->with([
                            'TipoExpensa',
                            'TipoGasto',
                            'Rubro',
                            'Proveedor'
                        ]);
                    }
                ])
                ->whereHas('Gasto', function ($query) use ($consorcio_id) {
                    $query->where('consorcio_id', $consorcio_id);
                })
                ->get();
    }

    public function dameCuotasLiquidadasDeGastos($liquidaciones, $gastos)
    {
        // Inicializo el array de ids de gastos y el de ids de liquidaciones
        $ids_gastos = [];
        $ids_liquidaciones = [];

        // Agrego los ids de gastos al array
        foreach ($gastos as $gasto) {
            array_push($ids_gastos, $gasto->id);
        }

        foreach ($liquidaciones as $liquidacion) {
            array_push($ids_liquidaciones, $liquidacion->id);
        }

        // Busco las cuotas
        return
            $this
                ->with([
                    'Liquidacion' => function ($query) {
                        $query->select(['id', 'nombre_periodo']);
                    },
                    'Gasto' => function ($query) {
                        $query->with('TipoExpensa');
                    }
                ])
                ->whereIn('gasto_id', $ids_gastos)
                ->whereIn('liquidacion_id', $ids_liquidaciones)
                ->get();
    }

    public function dameCuotasDeLiquidacion($ultima_liquidacion)
    {
        return
            $this->where('liquidacion_id', $ultima_liquidacion->id)
                ->with('Gasto')
                ->get();
    }

    public function dameCantidadDeUFsConEseGasto()
    {
        // Obtengo el id del gasto para pedirle las cuotas y sumarlas
        $gasto_id = $this->gasto_id;

        return GastoUnidadFuncional::where('gasto_id', $gasto_id)->count();
    }
}
