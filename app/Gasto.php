<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Gasto extends Model
{

    use SoftDeletes;

    protected $table = 'gastos';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'tipo_expensa_id', 'rubro_id', 'tipo_gastos_id', 'tipo_cuota_extra', 'consorcio_id', 'descripcion', 'nro_factura', 'empleado_id'
    ];

    public function Consorcio()
    {
        return $this->belongsTo(Consorcio::class);
    }

    public function Proveedor()
    {
        return $this->belongsTo(Proveedor::class, 'proveedor_id');
    }
    public function Empleado()
    {
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }

    public function Rubro()
    {
        return $this->belongsTo(Rubro::class);
    }

    public function TipoExpensa()
    {
        return $this->belongsTo(TipoExpensa::class);
    }

    public function TipoGasto()
    {
        return $this->belongsTo(TipoGasto::class, 'tipo_gastos_id');
    }

    public function Cuotas()
    {
        return $this->hasMany(Cuota::class);
    }

    public function DetallesSueldos()
    {
        return $this->hasMany(DetalleSueldo::class, 'gasto_id');
    }

    public function UnidadesFuncionales()
    {
        return $this->belongsToMany(UnidadFuncional::class, 'gastos_unidades_funcionales', 'gasto_id', 'unidad_funcional_id');
    }

    public function getGastosSinLiquidacion()
    {
        $controller = new Controller();

        // Obtengo la fecha de la variable sesion
        $fecha = session('LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $controller->getConsorcio() . '_FECHA');

        return
            $this
                ->where('consorcio_id', session('CONSORCIO')->id)
                ->whereHas('Cuotas', function ($query) use ($fecha) {
                    $query->whereNull('liquidacion_id')
                        ->whereDate('fecha', '<=', $fecha);
                })
                ->with([
                    'TipoExpensa' => function ($query) {
                        $query->select(['id', 'descripcion']);
                    },
                    'Cuotas' => function($query) use ($fecha) {
                        $query->select(['id', 'gasto_id', 'importe'])
                            ->whereNull('liquidacion_id')
                            ->whereDate('fecha', '<=', $fecha);
                    }
                ])
                ->get();
    }

    public function getGastosConProveedores()
    {
        $controller = new Controller();

        return $this->where('consorcio_id', $controller->getConsorcio())
            ->with('Proveedor')
            ->whereHas('Cuotas', function($query) {
                $query->whereNotNull('liquidacion_id');
            })
            ->get();
    }


    public function getGastosConTiposDeGastos()
    {
        $controller = new Controller();

        return $this->where('consorcio_id', $controller->getConsorcio())
            ->with('TipoGasto')
            ->whereHas('Cuotas', function($query) {
                $query->whereNotNull('liquidacion_id');
            })
            ->get();
    }


    public function getCuotasGastos()
    {
        return
            $this
                ->where('consorcio_id', session('CONSORCIO')->id)
                ->with([
                    'Cuotas',
                    'TipoExpensa',
                    'TipoGasto',
                    'Rubro',
                    'Proveedor'
                ])
                ->get();
    }

    public function totalCuotas($id)
    {
        $gasto_con_cuotas = $this->with(['Cuotas'])->where('id', $id)->first();

        return count($gasto_con_cuotas->Cuotas);
    }

    /**
     * Recibo los ids de los combos para filtrar los gastos
     * @param $rubros_elegidos
     * @param $tipos_expensa_elegidos
     * @param $tipos_gastos_elegidos
     * @param $consorcio_id
     * @param $proveedores
     * @return mixed
     */
    public function dameGastosParaReportes($rubros_elegidos, $tipos_expensa_elegidos, $tipos_gastos_elegidos, $consorcio_id, $proveedores)
    {
        return $this->whereIn('rubro_id', $rubros_elegidos)
            ->whereIn('tipo_gastos_id', $tipos_gastos_elegidos)
            ->whereIn('tipo_expensa_id', $tipos_expensa_elegidos)
            ->whereIn('proveedor_id', $proveedores)
            ->where('consorcio_id', $consorcio_id)
            ->get();
    }

    public function dameGastosDeLiquidacion($id)
    {
        return
            $this->where('liquidacion_id', $id)
                ->with('Cuotas')
                ->get();
    }
}