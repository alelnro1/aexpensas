<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoExpensa extends Model
{
    protected $table = 'tipo_expensa';

    public function Gastos()
    {
        return $this->hasMany('Gastos');
    }

    public static function getId($descripcion)
    {
        return $tipoExpensa_id = TipoExpensa::where('descripcion', 'like', $descripcion)->get()->first()->id;
    }

}
