<?php

namespace App\Classes\Liquidaciones;

class ExpensasParticulares
{
    public $unidad_funcional;

    public function __construct($unidad_funcional)
    {
        $this->unidad_funcional = $unidad_funcional;
    }

    public function calcularExpensas()
    {
        // Obtengo la unidad funcional con gastos cuyas cuotas no hayan sido liquidadas
        $unidad_funcional =
            $this->unidad_funcional->load(['Gastos' => function($query) {
                $query->where('tipo_expensa_id', '4')
                    ->with([
                        'Cuotas' => function ($query) {
                            $query->whereNull('liquidacion_id');
                        }
                    ]);
            }]);

        // Le pido con lazy loading los gastos
        $gastos = $unidad_funcional->Gastos;

        // Inicializo el monto de la expensa particular
        $monto = 0;

        // Recorro los gastos de la UF y dentro de esos, recorro las cuotas
        foreach ($gastos as $gasto) {
            $cuotas = $gasto->Cuotas;

            foreach ($cuotas as $cuota) {
                $monto = $monto + $cuota->importe;
            }
        }

        return $monto;
    }
}