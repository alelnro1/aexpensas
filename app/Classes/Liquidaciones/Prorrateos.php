<?php

namespace App\Classes\Liquidaciones;

use App\Auditoria;
use App\Consorcio;
use App\Http\Controllers\AuditoriaController;
use App\Http\Controllers\Controller;
use App\Liquidacion;
use App\UnidadFuncional;
use Illuminate\Support\Facades\Auth;

class Prorrateos extends Liquidacion
{
    private $controller;

    public function __construct()
    {
        $this->controller = new Controller();
    }

    public function cargarProrrateosUnidadesFuncionales()
    {
        // Obtengo la fecha de la variable sesion
        $fecha = session('LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $this->controller->getConsorcio() . '_FECHA');

        // Busco el consorcio
        $consorcio = Consorcio::where('id', $this->controller->getConsorcio())->first();

        // Busco las unidades funcionales del consorcio
        $unidades_funcionales =
            UnidadFuncional::where('consorcio_id', $this->controller->getConsorcio())
                ->with([
                    'Usuarios' => function ($query) {
                        $query->select(['nombre', 'apellido']);
                    },
                    'Gastos' => function ($query) use ($fecha) {
                        //$query->select(['id']);
                        $query->with([
                            'Cuotas' => function ($query) use ($fecha) {
                                $query->whereDate('fecha', '<=', $fecha);
                            }
                        ]);
                    },
                    'Comprobantes' => function ($query) {
                        $query->whereHas('Estado', function ($query) {
                            $query->where('descripcion', 'Aprobado');
                            $query->whereNull('liquidacion_id');
                        });

                        $query->select(['id', 'unidad_funcional_id', 'importe']);
                    }
                ])
                ->get();

        // Guardo las unidades funcionales para no tener que cargarlas despues
        session(['LIQ_UNIDADES_FUNCIONALES_' . $this->controller->getConsorcio() => $unidades_funcionales]);

        // Inicializo la variable de resultado que contendrá los prorrateos de cada UF
        $prorrateos = array();

        // Creo los objetos para usar strategy y calcular las expensas de todos los tipos, ya sean fijas o variables
        $expensa_tipo_a = new ExpensasTipoA($consorcio);
        $expensa_tipo_b = new ExpensasTipoB($consorcio);
        $expensa_tipo_c = new ExpensasTipoC($consorcio);

        // Calculo los totales de cada tipo de expensa
        $total_expensa_tipo_a = $expensa_tipo_a->calcularExpensas();
        $total_expensa_tipo_b = $expensa_tipo_b->calcularExpensas();
        $total_expensa_tipo_c = $expensa_tipo_c->calcularExpensas();

        foreach ($unidades_funcionales as $key => $unidad_funcional) {
            // Si el consorcio tiene expensas fijas, se va a asignar los mismo a todas las UF. Sino calcula por el %
            if ($consorcio->expensas_fijas) {
                $expensa_uf_tipo_a = $total_expensa_tipo_a;
                $expensa_uf_tipo_b = $total_expensa_tipo_b;
                $expensa_uf_tipo_c = $total_expensa_tipo_c;
            } else {
                // El porcentaje que le corresponde pagar a la UF para cada tipo de expensa
                $porcentaje_a = $unidad_funcional->porcentaje_a;
                $porcentaje_b = $unidad_funcional->porcentaje_b;
                $porcentaje_c = $unidad_funcional->porcentaje_c;

                // La expensa que debe pagar real la expensa
                $expensa_uf_tipo_a = $total_expensa_tipo_a * ($porcentaje_a / 100);
                $expensa_uf_tipo_b = $total_expensa_tipo_b * ($porcentaje_b / 100);
                $expensa_uf_tipo_c = $total_expensa_tipo_c * ($porcentaje_c / 100);
            }

            // Calculo las expensas particulares de la UF
            $expensas_particulares = new ExpensasParticulares($unidad_funcional);
            $expensa_uf_tipo_particular = $expensas_particulares->calcularExpensas();

            $expensa_cochera_uf = $this->getExpensaCochera($unidad_funcional, $consorcio);

            // Calculo los fondos extras de la UF
            $fondo_extra = new CuotaExtra($unidad_funcional);
            $cuota_extra_uf = $fondo_extra->calcularExpensas() * ($unidad_funcional->porcentaje_a / 100);

            // Hago cálculos dinámicos
            $pagos_realizados = $this->calcularPagosDeUF($unidad_funcional->Comprobantes);
            $gastos_particulares = $unidad_funcional->gastos_particulares;
            $monto_expensas = $expensa_uf_tipo_a + $expensa_uf_tipo_b + $expensa_uf_tipo_c + $gastos_particulares + $cuota_extra_uf;
            $saldo_inicial = $unidad_funcional->saldo;
            $termino = $unidad_funcional->termino;
            $futuros = $unidad_funcional->futuros;
            $deuda = $unidad_funcional->deuda + $unidad_funcional->termino;
            $saldo_anterior = $unidad_funcional->saldo_anterior;//$gastos_particulares + $expensa_cochera_uf + $deuda - $futuros;

            if (($deuda - $futuros) > 0) {
                $intereses = ((($unidad_funcional->gastos_particulares + $unidad_funcional->deuda + $unidad_funcional->termino - $unidad_funcional->futuros) * $consorcio->interes_vto_1) / 30) * (30 - $consorcio->dia_vto_1)/100;
            } else {
                $intereses = 0;
            }

            $saldo_a_pagar = $monto_expensas + $saldo_anterior + $intereses - $pagos_realizados;

            // Si el consorcio tiene redondeo, agregarselo al saldo a pagar
            if ($consorcio->redondeo) {
                if ($saldo_a_pagar > 0) {
                    $saldo_a_pagar = floor($saldo_a_pagar) + ($unidad_funcional->codigo / 100);
                } else {
                    $saldo_a_pagar = floor($saldo_a_pagar) - ($unidad_funcional->codigo / 100);
                }
            } else if($consorcio->redondeo_0) {
                // Se activa el redondeo a 0
                $saldo_a_pagar = floor($saldo_a_pagar);
            }

            // En la posicion $key guardo la UF y en subposiciones guardo las expensas
            $prorrateos[$key] = $unidad_funcional;
            $prorrateos[$key]['expensas_tipo_a'] = $expensa_uf_tipo_a;
            $prorrateos[$key]['expensas_tipo_b'] = $expensa_uf_tipo_b;
            $prorrateos[$key]['expensas_tipo_c'] = $expensa_uf_tipo_c;
            $prorrateos[$key]['cuota_extra'] = $cuota_extra_uf;
            $prorrateos[$key]['expensas_tipo_particular'] = $expensa_uf_tipo_particular;
            $prorrateos[$key]['cochera'] = $expensa_cochera_uf;
            $prorrateos[$key]['monto_abonado'] = $pagos_realizados;
            $prorrateos[$key]['deuda'] = $deuda - $futuros;
            $prorrateos[$key]['saldo_anterior'] = $saldo_anterior;
            $prorrateos[$key]['intereses'] = $intereses;
            $prorrateos[$key]['monto_expensas'] = $monto_expensas;
            $prorrateos[$key]['pagos_realizados'] = $pagos_realizados;
            $prorrateos[$key]['saldo_a_pagar'] = $saldo_a_pagar;
        }

        return $prorrateos;
    }

    /*private function truncate($number) {
        $first = $number * 1000;
        $esto = explode(".", $first)[0];
        $devuelvo = (float) intval($esto) / 1000;
        return $devuelvo;
    }*/

    public function getSaldoAdeudadoDeUFs()
    {
        $ufs = session('LIQ_UNIDADES_FUNCIONALES_' . $this->controller->getConsorcio());

        $total_adeudado = 0;

        foreach ($ufs as $uf) {
            $total_adeudado = $total_adeudado + $uf->deuda;
        }

        return $total_adeudado;
    }

    /**
     * Calculo del monto pagado por la unidad funcional.
     * Toma los comprobantes que tengan estado Aprobado
     * @param $comprobantes
     * @return int
     */
    public function calcularPagosDeUF($comprobantes)
    {
        // Inicializo el monto
        $monto_pagado = 0;

        // Recorro los comprobantes que hayan sido aceptados
        foreach ($comprobantes as $comprobante) {
            $monto_pagado = $monto_pagado + $comprobante->importe;
        }

        return $monto_pagado;
    }

    /**
     * Paso el resultado de los prorrateos de la liquidacion a la tabla de unidades funcionales
     * Además, audito todos los cambios
     * @param $liquidacion
     */
    public function pasarProrrateoAUFYAuditar($liquidacion)
    {
        $prorrateos = session('LIQ_PRORRATEO_' . $this->controller->getConsorcio());
        $unidades_funcionales = session('LIQ_UNIDADES_FUNCIONALES_' . $this->controller->getConsorcio());
        $auditoria = new AuditoriaController();
        $consorcio = Consorcio::where('id', '=', $this->controller->getConsorcio())->first();

        foreach ($prorrateos as $prorrateo) {
            foreach ($unidades_funcionales as $unidad_funcional) {
                // Si no es el prorrateo de la unidad funcional, ir al siguiente
                if ($unidad_funcional->id != $prorrateo->id) continue;

                $unidad_funcional =
                    UnidadFuncional::where('id', $prorrateo->id)
                        ->select(['id', 'termino', 'deuda', 'interes', 'futuros', 'gastos_particulares'])
                        ->first();

                // Guardo variables auxiliares para poder auditar
                $deuda_anterior = $unidad_funcional->deuda;
                $termino_anterior = $unidad_funcional->termino;
                $gp_anterior = $unidad_funcional->gastos_particulares;
                $interes_anterior = $unidad_funcional->interes;
                $futuros_anterior = $unidad_funcional->futuros;
                $saldo_anterior = $unidad_funcional->saldo_anterior;//$deuda_anterior + $termino_anterior + $gp_anterior + $interes_anterior - $futuros_anterior;
                $expensa_cochera_uf = $this->getExpensaCochera($unidad_funcional, $consorcio, $liquidacion);

                // Acumular deuda
                //$unidad_funcional->deuda = $prorrateo->deuda + $prorrateo->termino + $prorrateo->intereses + $prorrateo->gastos_particulares;
                $unidad_funcional->deuda = $prorrateo->deuda + $prorrateo->intereses;

                if ($prorrateo->saldo_anterior > 0) {
                    // TODO: Agregar intereses
                    $unidad_funcional->termino = $prorrateo->monto_expensas;
                    $unidad_funcional->gastos_particulares = $prorrateo->expensas_tipo_particular;
                }

                if ($prorrateo->saldo_anterior <= 0 && $prorrateo->saldo_a_pagar > 0) {
                    $unidad_funcional->termino = $prorrateo->saldo_a_pagar;
                    $unidad_funcional->gastos_particulares = 0;
                    $unidad_funcional->interes = 0;
                    $unidad_funcional->deuda = 0;
                }

                if ($prorrateo->saldo_anterior < 0 && $prorrateo->saldo_a_pagar < 0) {
                    $unidad_funcional->futuros = abs($prorrateo->saldo_a_pagar);
                    $unidad_funcional->termino = 0;
                    $unidad_funcional->gastos_particulares = 0;
                    $unidad_funcional->interes = 0;
                    $unidad_funcional->deuda = 0;
                }

                $saldo_nuevo =
                    $unidad_funcional->deuda + $unidad_funcional->termino + $prorrateo->cochera +
                    $unidad_funcional->gastos_particulares +
                    $unidad_funcional->interes - $unidad_funcional->futuros;

                // Si tiene futuros y el saldo a pagar > 0, le voy a restar a termino y le quito los futuros
                if ($prorrateo->saldo_a_pagar > 0 && $unidad_funcional->futuros > 0) {
                    $unidad_funcional->termino = $unidad_funcional->termino - $unidad_funcional->futuros;
                    $unidad_funcional->futuros = 0;
                }

                $unidad_funcional->termino = $unidad_funcional->termino + $expensa_cochera_uf;

                // Guardo los cambios a la unidad funcional
                $unidad_funcional->save();

                // Audito la unidad funcional
                $auditoria->nuevoMovimiento(
                    Auditoria::ExpensaGenerada() . $liquidacion->nombre_periodo,
                    $saldo_anterior, // saldo_anterior
                    $saldo_nuevo, // saldo_nuevo
                    $interes_anterior,
                    $unidad_funcional->interes,
                    $termino_anterior,
                    $unidad_funcional->termino,
                    $deuda_anterior,
                    $unidad_funcional->deuda,
                    $gp_anterior,
                    $unidad_funcional->gastos_particulares,
                    $futuros_anterior,
                    $unidad_funcional->futuros,
                    null, // estado_anterior_id
                    null, // estado_nuevo_id
                    null, // comprobante_id
                    $unidad_funcional->id,
                    $liquidacion->id
                );
            }
        }
    }

    /**
     * Trae el monto de la expensa de la cochera.
     * Si la liquidacion es true => hay que buscar en la ultima expensa
     * @param $unidad_funcional
     * @param $consorcio
     * @param bool $ultima
     * @return int
     */
    private function getExpensaCochera($unidad_funcional, $consorcio, $liquidacion=false)
    {
        if ($consorcio->cochera_complementaria) {
            // Calculo las expensas de la cochera
            $expensas_cochera = new ExpensasCochera($unidad_funcional);
            $expensa_cochera_uf = $expensas_cochera->calcularExpensas($liquidacion);
        } else {
            $expensa_cochera_uf = 0;
        }

        return $expensa_cochera_uf;
    }
}