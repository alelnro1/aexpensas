<?php

namespace App\Classes\Liquidaciones;

use App\Consorcio;
use App\Cuota;
use App\TipoExpensa;
use Illuminate\Support\Facades\Auth;

abstract class CalcularExpensasAbstract
{
    public $consorcio;

    public function __construct($consorcio)
    {
        $this->consorcio = $consorcio;
    }

    public abstract function calcularExpensas();

    /**
     * Se obtiene del consorcio el monto fijo del tipo de expensa A, B y C
     * @param $tipo
     * @return int
     */
    public function getExpensasFijas($tipo)
    {
        $monto_fijo = 0;

        switch ($tipo) {
            case 'A':
                $monto_fijo = $this->consorcio->monto_fijo_a;
                break;
            case 'B':
                $monto_fijo = $this->consorcio->monto_fijo_b;
                break;
            case 'C':
                $monto_fijo = $this->consorcio->monto_fijo_c;
                break;
        }

        return $monto_fijo;
    }

    /**
     * Digo si un consorcio tiene expensas fijas. Si las tiene, cada UF deberá pagar lo mismo por cada tipo de expensa
     * @return bool
     */
    public function consorcioTieneExpensasFijas()
    {
        return $this->consorcio->expensas_fijas == true;
    }

    /**
     * Se calcula el total de las expensas variables de un consorcio.
     * Pueden ser de tipo A, B ó C.
     * @param $tipo_expensa
     * @return int
     */
    public function getExpensasVariables($tipo_expensa)
    {
        // Obtengo la fecha de la variable sesion
        $fecha = session('LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $this->consorcio->id . '_FECHA');

        // Obtengo el ID del tipo de expensa, que pueden ser A, B ó C
        $tipo_expensa_id = $this->getTipoExpensaIdPorDescripcion($tipo_expensa);

        // Obtengo el ID del consorcio en cuestión
        $consorcio_id    = $this->consorcio->id;

        // Obtengo todas las cuotas del consorcio del tipo de expensa solicitado que no hayan sido liquidadas y sean menor que la fecha de sesion
        $cuotas =
            Cuota::select(['id', 'importe', 'fecha'])
                ->whereNull('liquidacion_id')
                ->where('fecha', '<=', $fecha)
                ->whereHas('Gasto', function($query) use ($consorcio_id, $tipo_expensa_id) {
                    $query->where('consorcio_id', $consorcio_id);

                    $query->whereHas('TipoExpensa', function ($query) use ($consorcio_id, $tipo_expensa_id) {
                        $query->where('id', $tipo_expensa_id);
                    });
                })
                ->get();

        // Inicializo el monto de la expensa
        $monto = 0;

        // Calculo el monto de la expensa sumando todos los importes de las cuotas
        foreach ($cuotas as $cuota) {
            $monto = $monto + $cuota->importe;
        }

        return $monto;
    }

    /**
     * Obtengo el ID de un tipo de expensa filtrando por una descripcion
     * Puede ser A, B, C ó Particular
     * @param $descripcion
     * @return mixed
     */
    protected function getTipoExpensaIdPorDescripcion($descripcion)
    {
        // Busco el tipo de expensa, puede ser A, B, C, Particular
        $tipo_expensa = TipoExpensa::where('descripcion', $descripcion)->select(['id'])->first();

        return $tipo_expensa->id;
    }
}