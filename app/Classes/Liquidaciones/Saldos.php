<?php

namespace App\Classes\Liquidaciones;

use App\Liquidacion;

class Saldos extends Liquidacion
{
    /**
     * Obtengo el saldo actual del estado financiero
     * @param $ultima_liquidacion
     * @param $fondo_operativo
     * @return int
     */
    public function getSaldoEstadoFinanciero($ultima_liquidacion, $fondo_operativo)
    {
        // Inicializo el saldo
        $saldo = 0;

        if ($ultima_liquidacion) {
            $saldo = $ultima_liquidacion->saldo_estado_financiero;
        }

        $saldo =
            $saldo +
            ($fondo_operativo['ingresos_terminos'] + $fondo_operativo['ingresos_adeudados'] +
                $fondo_operativo['ingresos_particulares'] + $fondo_operativo['ingresos_intereses']
                + $fondo_operativo['ingresos_futuros'])
            -
            ($fondo_operativo['egresos_generales'] +
                $fondo_operativo['egresos_particulares'] +
                $fondo_operativo['egresos_asignacion_cuota_extra']);

        return $saldo;
    }

    /**
     * Obtengo el saldo del periodo actual de los movimientos bancarios
     * Saldo Actual = Saldo Anterior + Estados Financieros Actuales
     * @param $estados_financieros
     * @return int
     */
    public function getSaldoMovimientosBancarios($ultima_liquidacion, $estados_financieros)
    {
        // Inicializo el saldo, va a arrancar en el saldo de los movimientos bancarios de la ultima liquidacion
        $saldo = 0;

        if ($ultima_liquidacion != null) {
            $saldo = $ultima_liquidacion->saldo_movimientos_bancarios;
        }

        foreach ($estados_financieros as $estado_financiero) {
            // Agarro solo las liquidaciones que no tienen ID de liquidacion para sumar al saldo anterior
            //if ($estado_financiero->liquidacion_id != null) {
            // Agarro solo los estados financieros de la categoria Resumen de Movimientos Bancarios
            if ($estado_financiero->CategoriaFinanciera->nombre == "Resumen de Movimientos Bancarios") {
                $saldo = $saldo + $estado_financiero->monto;
            }
            //}
        }

        return $saldo;
    }

    /**
     * Obtengo el saldo del periodo actual del estado patrimonial
     * Saldo Actual = Saldo Anterior + Saldo Del Período
     * @param $estados_financieros
     * @return int
     */
    public function getSaldoEstadoPatrimonial($ultima_liquidacion, $consorcio_id)
    {
        // Inicializo el saldo, va a arrancar en el saldo de los movimientos bancarios de la ultima liquidacion
        $saldo = 0;

        /*if ($ultima_liquidacion != null) {
            $saldo = $ultima_liquidacion->saldo_estado_patrimonial;
        }*/

        $total_prorrateos = session('LIQ_TOTAL_PRORRATEOS_' . $consorcio_id);
        $total_cuota_extra = $total_prorrateos['cuota_extra'];

        return $total_prorrateos['saldo_a_pagar'] + $total_prorrateos['cochera'];
            /*session('LIQ_TOTAL_GASTOS_' . $consorcio_id) +
            session('LIQ_SALDO_ADEUDADO_UFS_' . $consorcio_id) +
            $total_cuota_extra;*/
    }
}