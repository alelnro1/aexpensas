<?php

namespace App\Classes\Liquidaciones;

use App\Comprobante;
use App\Gasto;
use App\Liquidacion;

class ConfirmacionExpensa extends Liquidacion
{
    /**
     * Se liquidan las cuotas de los gastos que se incluyan en el período
     */
    public function liquidarGastos($liquidacion)
    {
        $gasto = new Gasto();

        // Obtengo los gastos del consorcio sin liquidar del período
        $gastos = $gasto->getGastosSinLiquidacion();

        foreach ($gastos as $gasto) {
            foreach ($gasto->Cuotas as $cuota) {
                $cuota->liquidacion_id = $liquidacion->id;
                $cuota->save();
            }
        }
    }

    /**
     * Se liquidan todos los comprobantes aprobados sin un id_liquidacion
     * @param $liquidacion
     */
    public function liquidarComprobantes($liquidacion)
    {
        $comprobante = new Comprobante();
        $comprobantes = $comprobante->getComprobantesAprobadosSinLiquidacion();

        foreach ($comprobantes as $comprobante) {
            $comprobante->liquidacion_id = $liquidacion->id;
            $comprobante->save();
        }
    }

    /**
     * Se liquidan todos los estados financieros
     */
    public function liquidarEstadosFinancieros($liquidacion, $estados_financieros)
    {
        foreach ($estados_financieros as $estado_financiero) {
            $estado_financiero->liquidacion_id = $liquidacion->id;
            $estado_financiero->save();
        }
    }

}