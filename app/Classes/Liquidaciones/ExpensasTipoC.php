<?php

namespace App\Classes\Liquidaciones;

class ExpensasTipoC extends CalcularExpensasAbstract
{
    public function __construct($consorcio)
    {
        parent::__construct($consorcio);
    }

    public function calcularExpensas()
    {
        // Inicializo el monto
        $monto_expensas = null;

        if ($this->consorcioTieneExpensasFijas()) {
            $monto_expensas = $this->getExpensasFijas('C');
        } else {
            $monto_expensas = $this->getExpensasVariables('C');
        }

        return $monto_expensas;
    }
}