<?php

namespace App\Classes\Liquidaciones;

use App\Gasto;

class CuotaExtra
{
    public $unidad_funcional;

    public function __construct($unidad_funcional)
    {
        $this->unidad_funcional = $unidad_funcional;
    }

    public function calcularExpensas()
    {
        $gastos =
            Gasto::whereHas('TipoExpensa', function ($query) {
                $query->where('descripcion', '=', 'Cuota Extra')
                    ->where('tipo_cuota_extra', '<>', '1');
            })
                ->whereHas('Cuotas', function ($query) {
                    $query->whereNull('liquidacion_id');
                })
                ->whereHas('Consorcio', function ($query) {
                    $query->where('id', $this->unidad_funcional->consorcio_id);
                })
                ->with([
                    'Cuotas' => function ($query) {
                        $query->whereNull('liquidacion_id');
                    },
                    'TipoExpensa'
                ])
                ->get();

        // Inicializo el monto de la expensa particular
        $monto = 0;

        // Recorro los gastos de la UF y dentro de esos, recorro las cuotas
        foreach ($gastos as $gasto) {
            $cuotas = $gasto->Cuotas;

            foreach ($cuotas as $cuota) {
                $monto = $monto + $cuota->importe;
            }
        }

        return $monto;
    }
}