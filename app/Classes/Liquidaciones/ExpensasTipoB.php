<?php

namespace App\Classes\Liquidaciones;

class ExpensasTipoB extends CalcularExpensasAbstract
{
    public function __construct($consorcio)
    {
        parent::__construct($consorcio);
    }

    public function calcularExpensas()
    {
        // Inicializo el monto
        $monto_expensas = null;

        if ($this->consorcioTieneExpensasFijas()) {
            $monto_expensas = $this->getExpensasFijas('B');
        } else {
            $monto_expensas = $this->getExpensasVariables('B');
        }

        return $monto_expensas;
    }
}