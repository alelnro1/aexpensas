<?php

namespace App\Classes\Liquidaciones;

use App\Cuota;
use App\Gasto;

class ExpensasTipoA extends CalcularExpensasAbstract
{
    public function __construct($consorcio)
    {
        parent::__construct($consorcio);
    }

    public function calcularExpensas()
    {
        // Inicializo el monto
        $monto_expensas = null;

        if ($this->consorcioTieneExpensasFijas()) {
            $monto_expensas = $this->getExpensasFijas('A');
        } else {
            $monto_expensas = $this->getExpensasVariables('A');
        }

        return $monto_expensas;
    }
}