<?php
namespace App\Classes\Comprobantes;

use App\Auditoria;
use App\Comprobante;
use App\Consorcio;
use App\EstadoComprobante;
use App\Http\Controllers\AuditoriaController;

class ComprobanteControl extends Comprobante
{
    public $comprobante;
    public $saldo_uf_aux;
    public $interes_uf_aux;
    public $deuda_uf_aux;
    public $futuros_uf_aux;
    public $gastos_part_uf_aux;
    public $termino_uf_aux;

    // Guardo en variables auxiliares para actualizar, datos del comprobantes
    public $importe_comprobante_aux;
    public $importe_comprobante_real;
    public $interes_comprobante_aux;
    public $deuda_comprobante_aux;
    public $futuros_comprobante_aux;
    public $gastos_part_comprobante_aux;
    public $termino_comprobante_aux;
    private $auditoria;
    private $ultimo_comprobante; // Ultimo comprobante de la tabla auditoria con el mismo id que vamos a tocar

    public function __construct(Comprobante $comprobante)
    {
        parent::__construct();

        $this->comprobante = $comprobante;

        // Guardo en variables auxiliares para actualizar, datos de la UF
        $this->interes_uf_aux = $comprobante->UnidadFuncional->interes;
        $this->deuda_uf_aux = $comprobante->UnidadFuncional->deuda;
        $this->futuros_uf_aux = $comprobante->UnidadFuncional->futuros;
        $this->gastos_part_uf_aux = $comprobante->UnidadFuncional->gastos_particulares;
        $this->termino_uf_aux = $comprobante->UnidadFuncional->termino;
        $this->saldo_uf_aux =
            $comprobante->UnidadFuncional->termino
            + $comprobante->UnidadFuncional->gastos_particulares
            + $comprobante->UnidadFuncional->deuda
            - $comprobante->UnidadFuncional->futuros;

        // Guardo en variables auxiliares para actualizar, datos del comprobantes
        $this->importe_comprobante_aux = $comprobante->importe;
        $this->interes_comprobante_aux = $comprobante->interes;
        $this->deuda_comprobante_aux = $comprobante->deuda;
        $this->futuros_comprobante_aux = $comprobante->futuros;
        $this->gastos_part_comprobante_aux = $comprobante->gastos_particulares;
        $this->termino_comprobante_aux = $comprobante->termino;
        $this->importe_comprobante_real = $comprobante->importe;

        $this->ultimo_comprobante = $this->ultimoComprobante();
        $this->auditoria = new AuditoriaController();
    }

    /**
     * Busco el ultimo comprobante con estado igual en la tabla auditoria
     * @return mixed
     */
    public function ultimoComprobante()
    {
        $comprobante =
            Auditoria::where('comprobante_id', $this->comprobante->id)
                ->orderBy('created_at')
                ->get();

        $comprobante = $comprobante->last();

        return $comprobante;
    }

    public function aceptar()
    {
        // Busco el estado
        $estado_aprobado = $this->buscarEstadoIdSegunDescripcion('Aprobado');

        // Antes de actualizar busco el comprobante auditado
        $comprobante_auditado = Auditoria::where('comprobante_id', $this->comprobante->id)->first();

        if ($comprobante_auditado) {
            // Verifico si cambió el importe del comprobante (o sea si se actualiza al aceptar, por el admin)
            if ($comprobante_auditado->comprobante_importe != $this->comprobante->importe) { // Paga menos o más
                /*
                    saldo anterior = 500
                    comprobante importe = 100
                    saldo nuevo = 400
                    --------------------------
                    saldo anterior = 500
                    comprobante importe = 50 || 200
                    saldo nuevo = saldo anterior - comprobante importe = 500 - 50 = 450 || 500 - 200 = 300
                */
                $comprobante_auditado->saldo_nuevo = $comprobante_auditado->saldo_anterior - $this->comprobante->importe;
                $comprobante_auditado->deuda_nuevo = $comprobante_auditado->deuda_anterior - $this->comprobante->importe;
                $comprobante_auditado->comprobante_importe = $this->comprobante->importe;

                $this->analizarDeudaUF($comprobante_auditado->comprobante_importe);
            }

            $comprobante_auditado->accion = Auditoria::comprobanteAprobado();
            $comprobante_auditado->estado_anterior_id = $comprobante_auditado->estado_nuevo_id;
            $comprobante_auditado->estado_nuevo_id = $estado_aprobado->id;
            $comprobante_auditado->UnidadFuncional->deuda = $comprobante_auditado->deuda_nuevo;
            $comprobante_auditado->UnidadFuncional->termino = $comprobante_auditado->termino_nuevo;
            $comprobante_auditado->UnidadFuncional->gastos_particulares = $comprobante_auditado->gastos_particulares_nuevo;
            $comprobante_auditado->UnidadFuncional->futuros = $comprobante_auditado->futuros_nuevo;
            $comprobante_auditado->UnidadFuncional->save();
            $comprobante_auditado->save();
        }

        $this->actualizar($estado_aprobado->id);

        // Si se actualiza, no hay que crear un registro nuevo en la auditoria
        if (!$comprobante_auditado) {
            if ($this->comprobante->UnidadFuncional->interes > 0) {
                if ($this->comprobante->importe >= $this->comprobante->UnidadFuncional->interes) {
                    /*
                     * C.I = UF.I
                     * C.S = C.S – UF.I
                        UF.I = 0

                     * */

                    $this->comprobante->interes = $this->comprobante->UnidadFuncional->interes;
                    $importe = $this->comprobante->importe - $this->comprobante->UnidadFuncional->interes;
                    $this->comprobante->UnidadFuncional->interes = 0;

                    $this->guardarCalculos();

                    $this->analizarDeudaUF($importe);
                } else {
                    /*
                     * UF.I = UF.I – C.S
                        C.I = C.S
                     */
                    $this->comprobante->UnidadFuncional->interes = $this->comprobante->UnidadFuncional->interes - $this->comprobante->importe;
                    $this->comprobante->interes = $this->comprobante->importe;

                    $this->guardarCalculos();
                }
            } else {
                $this->analizarDeudaUF($this->comprobante->importe);
            }

            // Audito que se cambio el saldo y el interes
            $this->auditoria->nuevoMovimiento(
                Auditoria::comprobanteAprobado(),
                $this->saldo_uf_aux,
                $this->saldo_uf_aux - $this->comprobante->importe,
                $this->interes_aux,
                $this->comprobante->UnidadFuncional->interes,
                $this->termino_uf_aux,
                $this->comprobante->UnidadFuncional->termino,
                $this->deuda_uf_aux,
                $this->comprobante->UnidadFuncional->deuda,
                $this->gastos_part_uf_aux,
                $this->comprobante->UnidadFuncional->gastos_particulares,
                $this->futuros_uf_aux,
                $this->comprobante->UnidadFuncional->futuros,
                $this->comprobante->estado_id,
                $estado_aprobado->id,
                $this->comprobante->id,
                $this->comprobante->UnidadFuncional->id,
                $this->comprobante->liquidacion_id,
                $this->comprobante->importe
            );
        }
    }

    public function rechazar()
    {
        // Busco el estado
        $estado_rechazado = $this->buscarEstadoIdSegunDescripcion('Rechazado');

        // Antes de actualizar busco el comprobante auditado
        $comprobante_auditado = Auditoria::where('comprobante_id', $this->comprobante->id)->first();

        if ($comprobante_auditado) {
            $comprobante_auditado->accion = Auditoria::comprobanteRechazado();
            $comprobante_auditado->UnidadFuncional->deuda = $comprobante_auditado->deuda_anterior;
            $comprobante_auditado->estado_anterior_id = $comprobante_auditado->estado_nuevo_id;
            $comprobante_auditado->estado_nuevo_id = $estado_rechazado->id;
            $comprobante_auditado->save();
        }

        $this->actualizar($estado_rechazado->id);
    }

    private function restarSaldo()
    {
        if ($this->ultimo_comprobante) {
            $this->saldo_uf_aux = $this->ultimo_comprobante->saldo_anterior;
        } else {
            $this->saldo_uf_aux = $this->saldo_uf_aux - $this->comprobante->importe;
        }
    }

    private function sumarSaldo()
    {
        if ($this->ultimo_comprobante) {
            $this->saldo_uf_aux = $this->ultimo_comprobante->saldo_anterior;
        } else {
            $this->saldo_uf_aux = $this->saldo_uf + $this->comprobante->importe;
        }
    }

    private function restarInteres()
    {
        if ($this->ultimo_comprobante) {
            $this->interes_uf_aux = $this->ultimo_comprobante->interes_anterior;
        } else {
            $this->interes_uf_aux = $this->interes_uf_aux - $this->calcularYVerificarInteres();
        }
    }

    private function sumarInteres()
    {
        if ($this->ultimo_comprobante) {
            $this->interes_uf_aux = $this->ultimo_comprobante->interes_anterior;
        } else {
            $this->interes_uf_aux = $this->interes_uf_aux + $this->calcularYVerificarInteres();
        }
    }

    public function ponerPendiente()
    {
        // Busco el estado
        $estado_pendiente = $this->buscarEstadoIdSegunDescripcion('Pendiente');
        $estado_aprobado = $this->buscarEstadoIdSegunDescripcion('Aprobado');

        // Si el estado era aprobado y pasa a pendiente => se suma saldo y resto interes
        if ($this->comprobante->estado_id == $estado_aprobado->id) {
            $this->restarInteres();

            $this->sumarSaldo();
        }

        // Antes de actualizar busco el comprobante auditado
        $comprobante_auditado = Auditoria::where('comprobante_id', $this->comprobante->id)->first();

        // Si no hay un comprobante auditado => lo creo
        if (!$comprobante_auditado) {
            if ($this->comprobante->UnidadFuncional->saldo != $this->saldo_uf) {
                // Audito que se cambio el saldo y el interes
                $this->auditoria->nuevoMovimiento(
                    Auditoria::comprobantePendiente(),
                    $this->saldo_uf_aux,
                    $this->saldo_uf_aux - $this->comprobante->importe,
                    $this->interes_uf_aux,
                    $this->comprobante->UnidadFuncional->interes,
                    $this->termino_uf_aux,
                    $this->comprobante->UnidadFuncional->termino,
                    $this->deuda_uf_aux,
                    $this->comprobante->UnidadFuncional->deuda - $this->comprobante->importe,
                    $this->gastos_part_uf_aux,
                    $this->comprobante->UnidadFuncional->gastos_particulares,
                    $this->futuros_uf_aux,
                    $this->comprobante->UnidadFuncional->futuros,
                    $this->comprobante->estado_id,
                    $estado_pendiente->id,
                    $this->comprobante->id,
                    $this->comprobante->UnidadFuncional->id,
                    null,
                    $this->comprobante->importe
                );
            }
        } else {
            // Encontré un comprobante => lo actualizo
            if ($comprobante_auditado->comprobante_importe != $this->comprobante->importe) {
                // Se cambió el importe => lo actualizo
                $comprobante_auditado->comprobante_importe = $this->comprobante->importe;

                /*
                 * Saldo anterior = 500
                 * importe = 100
                 * saldo nuevo = 400
                 * ------------------------
                 * saldo anterior = 500
                 * importe = 50
                 * saldo nuevo = 450
                 */
                $comprobante_auditado->saldo_nuevo = $comprobante_auditado->saldo_anterior - $this->comprobante->importe;
                $comprobante_auditado->deuda_nuevo = $comprobante_auditado->deuda_anterior - $this->comprobante->importe;
                $comprobante_auditado->comprobante_importe = $this->comprobante->importe;
            }

            $comprobante_auditado->accion = Auditoria::comprobantePendiente();
            $comprobante_auditado->UnidadFuncional->deuda = $comprobante_auditado->deuda_anterior;
            $comprobante_auditado->UnidadFuncional->termino = $comprobante_auditado->termino_anterior;
            $comprobante_auditado->UnidadFuncional->gastos_particulares = $comprobante_auditado->gastos_particulares_anterior;
            $comprobante_auditado->UnidadFuncional->futuros = $comprobante_auditado->futuros_anterior;
            $comprobante_auditado->estado_anterior_id = $comprobante_auditado->estado_nuevo_id;
            $comprobante_auditado->estado_nuevo_id = $estado_pendiente->id;
            $comprobante_auditado->UnidadFuncional->save();
            $comprobante_auditado->save();
        }

        $this->actualizar($estado_pendiente->id);
    }

    private function actualizar($estado_id)
    {
        // Cambio el estado
        $this->comprobante->estado_id = $estado_id;

        // Cambio el saldo de la unidad funcional
        /*$this->comprobante->UnidadFuncional->saldo = $this->saldo_uf_aux;

        // Cambio el interés de la unidad funcioanl
        $this->comprobante->UnidadFuncional->interes = $this->interes_uf_aux;*/

        // Como ya se guardaron los calculos a medida que se iban haciendo, guardo el importe real del comprobante
        $this->comprobante->importe = $this->importe_comprobante_real;

        // Guardo
        $this->comprobante->UnidadFuncional->save();
        $this->comprobante->save();
    }

    private function buscarEstadoIdSegunDescripcion($descripcion = null)
    {
        $estado_comprobante = EstadoComprobante::where('descripcion', 'LIKE', '%' . $descripcion . '%')->select(['id'])->first();

        return $estado_comprobante;
    }

    private function calcularYVerificarInteres()
    {
        $porcentaje_interes = $this->getPorcentajeInteres();

        return
            ($this->comprobante->UnidadFuncional->termino + $this->comprobante->UnidadFuncional->gastos_particulares)
            * ($porcentaje_interes / 100);
    }

    private function getPorcentajeInteres()
    {
        //$consorcio = Consorcio::where('id', $this->comprobante->UnidadFuncional->Consorcio)->get();
        $consorcio = $this->comprobante->UnidadFuncional->Consorcio;

        // Busco los dias de vencimientos del consorcio
        $dia_vto_1 = $consorcio->dia_vto_1;
        $dia_vto_2 = $consorcio->dia_vto_2;

        // Obtengo el dia de transaccion
        $dia_transaccion = date("d", strtotime($this->comprobante->fecha_transaccion));

        // Inicializo el interes
        $interes = 0;

        // Verifico la fecha de transaccion. Si pago después de fecha_vto_1 tomo %1, si pago despues de fecha_vto_2 tomo %2
        if ($dia_transaccion > $dia_vto_1 && $dia_transaccion < $dia_vto_2) {
            // Entra en el vencimiento 1 => Tomo el %1
            $interes = $consorcio->interes_vto_1;
        } else if ($dia_transaccion > $dia_vto_2) {
            // Entra en el vencimiento 2 => Tomo el 2%
            $interes = $consorcio->interes_vto_2;
        }

        return $interes;
    }

    private function analizarDeudaUF($importe)
    {
        if ($this->comprobante->UnidadFuncional->deuda > 0) {
            if ($importe >= $this->comprobante->UnidadFuncional->deuda) {
                /*$this->deuda_comprobante_aux = $this->comprobante->UnidadFuncional->deuda;
                $this->comprobante->UnidadFuncional->deuda = 0;
                $this->comprobante->importe = $this->comprobante->importe - $this->comprobante->UnidadFuncional->deuda;*/
                $this->comprobante->deuda = $this->comprobante->UnidadFuncional->deuda;
                $importe = $importe - $this->comprobante->UnidadFuncional->deuda;
                $this->comprobante->UnidadFuncional->deuda = 0;

                $this->guardarCalculos();

                $this->analizarGastosParticularesUF($importe);
            } else {
                $this->comprobante->UnidadFuncional->deuda = $this->comprobante->UnidadFuncional->deuda - $this->comprobante->importe;

                $this->comprobante->deuda = $this->comprobante->importe;

                $this->guardarCalculos();
            }
        } else {
            $this->analizarGastosParticularesUF($importe);
        }
    }

    private function analizarGastosParticularesUF($importe)
    {
        if ($this->comprobante->UnidadFuncional->gastos_particulares > 0) {
            if ($importe >= $this->comprobante->UnidadFuncional->gastos_particulares) {
                /*
                 * C.GP = UF. GP
                    UF.GP = 0
                    C.S = C.S – UF. GP
                 */
                $this->comprobante->gastos_particulares = $this->comprobante->UnidadFuncional->gastos_particulares;

                $importe = $importe - $this->comprobante->UnidadFuncional->gastos_particulares;

                $this->comprobante->UnidadFuncional->gastos_particulares = 0;

                $this->guardarCalculos();

                $this->analizarTerminoUF($importe);
            } else {
                /*
                 * UF.GP = UF.GP – C.S
                    C.GP = C.S
                 */
                $this->comprobante->UnidadFuncional->gastos_particulares = $this->comprobante->UnidadFuncional->gastos_particulares - $importe;

                $this->comprobante->gastos_particulares = $importe;

                $this->guardarCalculos();
            }
        } else {
            $this->analizarTerminoUF($importe);
        }
    }

    private function analizarTerminoUF($importe)
    {
        if ($this->comprobante->UnidadFuncional->termino > 0) {
            if ($importe > $this->comprobante->UnidadFuncional->termino) {
                /*
                 * C.T = UF. T
                    UF.T = 0
                    C.S = C.S – UF. T
                 */
                $this->comprobante->termino = $this->comprobante->UnidadFuncional->termino;

                $importe = $importe - $this->comprobante->UnidadFuncional->termino;

                $this->comprobante->UnidadFuncional->termino = 0;

                $this->guardarCalculos();

                $this->analizarImporteComprobante($importe);
            } else {
                /*
                 * UF.T = UF.T – C.S
                    C.T = C.S
                 */
                $this->comprobante->UnidadFuncional->termino = $this->comprobante->UnidadFuncional->termino - $importe;

                $this->comprobante->termino = $importe;

                $this->guardarCalculos();
            }
        } else {
            /*
             * UF.T = UF.T – C.S
                C.T = C.S
             */
            $this->comprobante->UnidadFuncional->futuros = $this->comprobante->UnidadFuncional->futuros + $importe;

            $this->comprobante->futuros = $importe;

            $this->guardarCalculos();
        }
    }

    private function analizarImporteComprobante($importe)
    {
        if ($importe > 0) {
            /*
             * UF.F = UF.F + CS
                C.F = C.S
                C.S = 0
             */
            $this->comprobante->UnidadFuncional->futuros = $this->comprobante->UnidadFuncional->futuros + $importe;

            $this->comprobante->futuros = $importe;

            $this->guardarCalculos();
        } else {
            // Actualizo el interes sobre el saldo actual
            $this->sumarInteres();

            // El saldo que se restará
            $this->restarSaldo();

            // La deuda que tiene
            //$this->sumarDeuda();
        }
    }

    private function guardarCalculos()
    {
        $this->comprobante->UnidadFuncional->save();

        //$this->comprobante->importe = $this->importe_comprobante_real;
        $this->comprobante->save();
    }
}