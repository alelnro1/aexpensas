<?php

namespace App\Classes\Comprobantes;

class RechazarComprobante implements ComprobanteCommand
{
    private $comprobante;

    public function __construct(ComprobanteControl $comprobante)
    {
        $this->comprobante = $comprobante;
    }

    public function cambiarEstado()
    {
        $this->comprobante->rechazar();
    }
}