<?php

namespace App\Classes\Comprobantes;


interface ComprobanteCommand
{
    public function cambiarEstado();
}