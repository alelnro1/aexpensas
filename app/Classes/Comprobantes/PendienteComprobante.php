<?php
/**
 * Created by PhpStorm.
 * User: ale
 * Date: 27/07/16
 * Time: 10:32
 */

namespace App\Classes\Comprobantes;


class PendienteComprobante implements ComprobanteCommand
{
    private $comprobante;

    public function __construct(ComprobanteControl $comprobante)
    {
        $this->comprobante = $comprobante;
    }

    public function cambiarEstado()
    {
        $this->comprobante->ponerPendiente();
    }
}