<?php

namespace App\Classes\Mails;

use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    protected $table = 'mails_adjuntos';

    protected $fillable = [
        'mail_id', 'link'
    ];

    public function Mail()
    {
        return $this->belongsTo(HMail::class);
    }
}