<?php

namespace App\Classes\Mails;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class HMail: Generic Mail (porque es un módulo genérico)
 * @package App\Classes\Mails
 */
class HMail extends Model {

    use SoftDeletes;

    protected $table = 'mails';

    protected $fillable = [
        'from_user_id', 'asunto', 'cuerpo', 'enviado'
    ];

    protected $dates = ['deleted_at'];

    public function Destinatarios()
    {
        return $this->belongsTo(Destinatario::class);
    }

    public function Adjuntos()
    {
        return $this->hasMany(Adjunto::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Se vinculan los destinatarios con el mail
     */
    public function vincularDestinatarios($destinatarios)
    {
        foreach ($destinatarios as $destinatario) {
            Destinatario::create([
                'mail_id' => $this->id,
                'destinatario_id' => $destinatario
            ]);
        }
    }

    public function vincularArchivos($urls_archivos)
    {
        $urls_archivos = explode('||', $urls_archivos);

        foreach ($urls_archivos as $url){
            // Creo el adjunto y lo vinculo
            Adjunto::create([
                'mail_id' => $this->id,
                'link' => $url
            ]);
        }

        return true;
    }

    public function scopeDameMisMailsEnviados()
    {
        return $this->where('from_user_id', Auth::user()->id)->get();
    }

}