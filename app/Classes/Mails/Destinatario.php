<?php

namespace App\Classes\Mails;

use App\UnidadFuncional;
use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    protected $table = 'mails_destinatarios';

    protected $fillable = [
        'mail_id', 'destinatario_id'
    ];

    public function Mail()
    {
        return $this->belongsTo(HMail::class);
    }

    public function UnidadFuncional()
    {
        return $this->belongsTo(UnidadFuncional::class);
    }
}