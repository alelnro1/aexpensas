<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadFuncional extends Model {

	use SoftDeletes;

	protected $table = 'unidades_funcionales';
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	protected $fillable = ['codigo', 'porcentaje_a', 'porcentaje_b', 'consorcio_id', 'cochera'];

    /** GETTERS **/
    public function getInteres()
    {
        return $this->interes;
    }

    public function getTermino()
    {
        return $this->termino;
    }

    public function getDeuda()
    {
        return $this->deuda;
    }

    public function getGastosParticulares()
    {
        return $this->gastos_particulares;
    }

    public function getFuturos()
    {
        return $this->futuros;
    }

    /** SETTERS **/
    public function setInteres($interes)
    {
        return $this->interes = $interes;
    }

    public function setTermino()
    {
        return $this->termino;
    }

    public function setDeuda()
    {
        return $this->deuda;
    }

    public function setGastosParticulares()
    {
        return $this->gastos_particulares;
    }

    public function setFuturos()
    {
        return $this->getFuturos;
    }

    /** RELACIONES **/
	public function Consorcio()
	{
		return $this->belongsTo(Consorcio::class);
	}

	public function Mails()
	{
		return $this->hasMany(Mailing::class);
	}

	public function Usuarios()
	{
		return $this->belongsToMany(User::class, 'unidad_funcional_usuario', 'unidad_funcional_id', 'usuario_id');
	}

	public function Reservas()
	{
		return $this->hasMany(Reserva::class);
	}

	public function Pedidos()
	{
		return $this->hasMany(Pedido::class);
	}

	public function Comprobantes()
	{
		return $this->hasMany(Comprobante::class);
	}
	public function Gastos()
	{
		return $this->belongsToMany(Gasto::class, 'gastos_unidades_funcionales', 'unidad_funcional_id', 'gasto_id');
	}

	public function GastosTipoA()
    {
        //return $this->Gastos()->where();
    }

	public function Consorcios()
	{
		return $this->belongsToMany(Consorcio::class, 'consorcio_documento', 'documento_id', 'consorcio_id');
	}

    public function RespuestasEncuestas()
    {
        return $this->hasMany(RespuestaUnidadFuncional::class);
    }

    public static function cantidadUFsConCochera()
    {
        $controller = new Controller();

        return UnidadFuncional::where('consorcio_id', $controller->getConsorcio())->where('cochera', true)->count();
    }

    public static function getUFsConCochera()
    {
        $controller = new Controller();

        return UnidadFuncional::where('consorcio_id', $controller->getConsorcio())->where('cochera', true)->get();
    }
}