<?php

namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Comprobante extends Model {

    use SoftDeletes;

    protected $table = 'comprobantes';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'importe', 'archivo', 'descripcion', 'fecha_transaccion', 'codigo_operacion',
        'unidad_funcional_id', 'estado_id', 'medio_de_pago_id', 'recibo', 'numerador'
    ];

    public function getComprobantesAprobadosSinLiquidacion()
    {
        $controller = new Controller();

        // Obtengo la fecha de la variable sesion
        $fecha = session('LIQUIDACION_USER_ID_' . Auth::user()->id . '_CONSORCIO_ID_' . $controller->getConsorcio() . '_FECHA');

        return
            $this
                ->whereNull('liquidacion_id')
                ->whereDate('fecha_transaccion', '<=', $fecha)
                ->whereHas(
                    'Estado', function ($query) {
                    $query->where('descripcion', 'Aprobado');
                }
                )->whereHas(
                    'UnidadFuncional', function ($query) {
                    $query->where('consorcio_id', session('CONSORCIO')->id);
                }
                )->get();
    }

    public function UnidadFuncional()
    {
        return $this->belongsTo(UnidadFuncional::class);
    }

    public function Estado()
    {
        return $this->belongsTo(EstadoComprobante::class);
    }

    public function MedioDePago()
    {
        return $this->belongsTo(MedioDePago::class);
    }

    public function Liquidacion()
    {
        return $this->belongsTo(Liquidacion::class);
    }

    public function obtenerComprobantesDeAdmin()
    {
        return
            Comprobante::with([
                'UnidadFuncional' => function($query) {
                    $query->select(['id', 'codigo', 'piso', 'dpto']);
                },
                'Estado'
                ])
                ->whereHas('UnidadFuncional', function($query){
                    $query->where('consorcio_id', session('CONSORCIO')->id);
                });
    }

    public function obtenerComprobantesDePropietario()
    {
        return
            Comprobante::with([
                'UnidadFuncional' => function($query) {
                    $query->select(['id', 'codigo', 'piso', 'dpto', 'nombre', 'apellido']);
                },
                'Estado'])
                ->whereHas('UnidadFuncional', function($query){
                    $query->where('consorcio_id', session('CONSORCIO')->id);

                    $query->whereHas('Usuarios', function($query){
                        $query->where('usuario_id', Auth::user()->id);
                    });
                });
    }

    public function dameComprobantesDeLiquidaciones($liquidaciones, $consorcio_id)
    {
        // Inicializo el array que tendrá los ids de las liquidaciones
        $ids_liquidaciones = [];

        // Agrego los ids de las liquidaciones al array
        foreach ($liquidaciones as $liquidacion) {
            array_push($ids_liquidaciones, $liquidacion->id);
        }

        return $this->with([
            'Liquidacion' => function ($query) {
                $query->select(['id', 'nombre_periodo']);
            }
        ])
            ->whereHas('UnidadFuncional', function($query) use ($consorcio_id) {
                $query->where('consorcio_id', $consorcio_id);
            })
            ->whereIn('liquidacion_id', $ids_liquidaciones)
            ->get();
    }

}