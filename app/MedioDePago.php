<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedioDePago extends Model {
    protected $table = 'medios_de_pagos';

    public function Comprobantes()
    {
        return $this->hasMany(Comprobante::class);
    }
}