<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedor extends Model {

	use SoftDeletes;

	protected $table = 'proveedores';
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	protected $fillable = ['nombre_razon_social','administrador_id','telefono'];


	public function Administrador()
	{
		return $this->belongsTo(Administrador::class, 'administrador_id');
	}

	public function Gastos()
	{
		return $this->hasMany(Gasto::class, 'proveedor_id');
	}

}