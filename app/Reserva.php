<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reserva extends Model {
	use SoftDeletes;

	protected $table = 'reservas';
	public $timestamps = true;

    protected $fillable = [
        'sum_id', 'descripcion', 'fecha', 'turno_id', 'unidad_funcional_id', 'estado_id'
    ];

	protected $dates = ['deleted_at'];

	public function UnidadFuncional()
	{
		return $this->belongsTo(UnidadFuncional::class);
	}

	public function Sum()
	{
		return $this->belongsTo(Sum::class);
	}

	public function EstadoReserva()
	{
		return $this->belongsTo(EstadoReserva::class, 'estado_id');
	}

	public function Turno()
    {
        return $this->belongsTo(Turno::class);
    }

}