<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model {

	use SoftDeletes;

	protected $table = 'documentos';
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	protected $fillable = ['nombre', 'descripcion', 'tipo_documento_id', 'estado'];
	public function Consorcios()
	{
		return $this->belongsToMany(Consorcio::class, 'consorcio_documento', 'documento_id', 'consorcio_id');
	}
	public function Tipo()
	{
		return $this->belongsTo(TipoDocumento::class, "tipo_documento_id");
	}


}