<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaUnidadFuncional extends Model
{
    protected $table = 'encuestas_respuestas_unidad_funcional';

    protected $fillable = [
        'encuesta_id', 'respuesta_id', 'unidad_funcional_id'
    ];

    public function Encuesta()
    {
        return $this->belongsTo(Encuesta::class);
    }

    public function Respuesta()
    {
        return $this->belongsTo(RespuestaEncuesta::class);
    }

    public function UnidadFuncional()
    {
        return $this->belongsTo(UnidadFuncional::class);
    }

    public function ufYaRespondioLaEncuesta($uf_id, $encuesta_id)
    {
        $uf =
            $this->where('unidad_funcional_id', $uf_id)
                ->where('encuesta_id', $encuesta_id)
                ->get();

        return count($uf);

    }
}
