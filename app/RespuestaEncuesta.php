<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaEncuesta extends Model
{
    protected $table = 'respuestas_encuesta';

    protected $fillable = [
        'encuesta_id', 'respuesta'
    ];

    public function Encuesta()
    {
        return $this->belongsTo(Encuesta::class);
    }

    public function RespuestasUF()
    {
        return $this->hasMany(RespuestaUnidadFuncional::class, 'respuesta_id');
    }
}
