@extends('layouts.app')

@section('site-name', 'Viendo Tipo de Gasto')

@section('content')
    <div class="panel-heading">
        Tipo de Gasto con Descripción <b><i>{{ $TipoGasto->descripcion }}</i></b>
    </div>

    <div class="panel-body">


        <table class="table table-striped task-table">

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $TipoGasto->descripcion }}</td>
            </tr>

        </table>

        <div>
             <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'tipos-de-gastos' . $TipoGasto->id . '/edit') }}">Editar</a>
        </div>

        <div>
            
        </div>
    </div>
@stop
