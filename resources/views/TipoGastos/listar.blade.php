@extends('layouts.app')

@section('site-name', 'Listando Tipos de Gasto')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Tipo Gastos

        <div style="float:right;">
            <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'tipos-de-gastos/create') }}">Nuevo Tipo de Gasto</a>
        </div>
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('TipoGasto_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('TipoGasto_eliminado') }}
                </div>
            @endif

            @if(Session::has('TipoGasto_creado'))
                <div class="alert alert-success">
                    {{ Session::get('TipoGasto_creado') }}
                </div>
            @endif

            @if(Session::has('TipoGasto_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('TipoGasto_actualizado') }}
                </div>
            @endif

            @if (count($TipoGastos) > 0)
                <table class="table table-striped task-table" id="TipoGastos" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($TipoGastos as $TipoGasto)
                        <tr>
                            <td>{{ $TipoGasto->descripcion }}</td>
                            <td>
                                <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'tipos-de-gastos/' . $TipoGasto['id']) }}"
                                   data-method="delete"
                                   data-token="{{ csrf_token() }}"
                                   data-confirm="Esta seguro que desea eliminar al Tipo de Gasto con descripción {{ $TipoGasto->descripcion }}?">
                                    Eliminar
                                </a>
                                |
                                <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'tipos-de-gastos/' . $TipoGasto->id . '/edit') }}">Editar</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay Tipos de Gasto
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/TipoGastos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/TipoGastos/delete-link.js') }}"></script>
@stop