@extends('layouts.app')

@section('site-name', 'Viendo a fondo')

@section('content')
    <div class="panel-heading">
        Fondo con nombre <b><i>{{ $fondo->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('fondo_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('fondo_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $fondo->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td>{{ $fondo->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $fondo->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($fondo->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $fondo->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $fondo->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $fondo->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $fondo->archivo }}" height="250" /></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $fondo->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/fondos/{{ $fondo->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
