@extends('layouts.app')

@section('site-name', 'Listando fondos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Fondos

        <div style="float:right;">
            <a href="/fondos/create">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('fondo_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('fondo_eliminado') }}
            </div>
        @endif

        @if(Session::has('fondo_creado'))
            <div class="alert alert-success">
                {{ Session::get('fondo_creado') }}
            </div>
        @endif

        @if (count($fondos) > 0)
            <table class="table table-striped task-table" id="fondos">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Estado</th>
                        <th>Fecha</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($fondos as $fondo)
                    <tr>
                        <td>{{ $fondo->nombre }}</td>
                        <td style="width:75%">{{ $fondo->descripcion }}</td>
                        <td>{{ $fondo->estado }}</td>
                        <td>{{ date('Y/m/d', strtotime($fondo->fecha)) }}</td>
                        <td>{{ $fondo->email }}</td>
                        <td>{{ $fondo->telefono }}</td>

                        <td>
                            <a href="/fondos/{{ $fondo['id'] }}">Ver</a>
                            |
                            <a href="/fondos/{{ $fondo['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a fondo con nombre {{ $fondo->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay fondos
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/fondos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/fondos/delete-link.js') }}"></script>
@stop