@extends('layouts.no-auth')

@section('content')
    <div class="panel-heading">Welcome</div>

    <div class="panel-body">
        Your Application's Landing Page.
    </div>
@endsection
