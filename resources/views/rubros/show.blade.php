@extends('layouts.app')

@section('site-name', 'Viendo a rubro')

@section('content')
    <div class="panel-heading">
        Rubro con nombre <b><i>{{ $rubro->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('rubro_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('rubro_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $rubro->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td>{{ $rubro->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $rubro->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($rubro->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $rubro->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $rubro->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $rubro->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $rubro->archivo }}" height="250" /></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $rubro->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/rubros/{{ $rubro->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
