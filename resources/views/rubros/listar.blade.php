@extends('layouts.app')

@section('site-name', 'Listando rubros')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Rubros

        <div style="float:right;">
            <a href="/rubros/create">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('rubro_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('rubro_eliminado') }}
            </div>
        @endif

        @if(Session::has('rubro_creado'))
            <div class="alert alert-success">
                {{ Session::get('rubro_creado') }}
            </div>
        @endif

        @if (count($rubros) > 0)
            <table class="table table-striped task-table" id="rubros">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Estado</th>
                        <th>Fecha</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($rubros as $rubro)
                    <tr>
                        <td>{{ $rubro->nombre }}</td>
                        <td style="width:75%">{{ $rubro->descripcion }}</td>
                        <td>{{ $rubro->estado }}</td>
                        <td>{{ date('Y/m/d', strtotime($rubro->fecha)) }}</td>
                        <td>{{ $rubro->email }}</td>
                        <td>{{ $rubro->telefono }}</td>

                        <td>
                            <a href="/rubros/{{ $rubro['id'] }}">Ver</a>
                            |
                            <a href="/rubros/{{ $rubro['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a rubro con nombre {{ $rubro->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay rubros
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/rubros/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/rubros/delete-link.js') }}"></script>
@stop