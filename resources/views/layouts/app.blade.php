&nbsp;<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AExpensas - Su compañero en el manejo de expensas</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
<!--link href="{{ asset('css/plugins/morris.css')}} " rel="stylesheet"-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <style>
        body {
            font-size: 13px;
            min-height: 100%;
        }
        #body-content {
            display: none;
        }
    </style>

    @yield('styles')
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">
                <img src="{{ asset('images/logo.jpg') }}" alt="Logo" width="140" style="margin-top: -12px;">
            </a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>--}}
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>{{ Auth::user()->nombre }}</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>{{ Auth::user()->nombre }}</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>{{ Auth::user()->nombre }}</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-building"></i>&nbsp;
                    <span id="consorcio-actual"><strong>{{ $CONSORCIO_NOMBRE }}</strong></span> &nbsp;
                    <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown" style="max-height: 350px; overflow-y: scroll;">
                    @foreach($consorcios as $consorcio)
                        <li>
                            <a href="#" data-consorcio-id="{{$consorcio->id}}"
                               @if ($consorcio->nombre == $CONSORCIO_NOMBRE)
                               style="background-color: #f0f0f0"
                               @endif
                               class="cambiar-consorcio">
                                {{$consorcio->nombre}}
                            </a>
                        </li>
                        <li class="divider"></li>
                    @endforeach
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-user"></i> {{ Auth::user()->nombre }} <b class="caret"></b></a>
                <ul class="dropdown-menu" style="width:210px;">
                    <li>
                        <a href="{{ url( $base_url . 'perfil') }}"><i class="fa fa-fw fa-user"></i> Perfil</a>
                    </li>
                    {{--<li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>--}}
                    <li>
                        <a href="{{ url( $base_url . 'perfil/edit') }}">
                            <i class="fa fa-fw fa-wrench"></i>&nbsp; Actualizar Perfil
                        </a>
                    </li>
                    <li>
                        <a href="{{ url( $base_url . 'cambiar-clave-personal') }}">
                            <i class="fa fa-fw fa-gear"></i>&nbsp; Cambiar Contraseña
                        </a>
                    </li>
                    @if(Auth::user()->esAdmin())
                        <li class="divider">
                        <li>
                            <a href="{{ url( $base_url . 'proveedores') }}">
                                <i class="fa fa-fw fa-truck"></i>
                                Proveedores
                            </a>
                        </li>
                        <li>
                            <a href="{{ url( $base_url . 'empleados') }}">
                                <i class="glyphicon glyphicon-pawn fa-fw" ></i>
                                Empleados
                            </a>
                        </li>
                        <li>
                            <a href="{{ url( $base_url . 'tipos-de-gastos') }}">
                                <i class="fa fa-fw fa-money"></i>
                                Tipo
                                de Gastos</a>
                        </li>
                    @endif
                    <li class="divider"></li>
                    <li>
                        <a href="{{ url( $base_url . 'mejora-continua') }}">
                            <i class="fa fa-fw fa-exclamation"></i>&nbsp; Mejora Continua
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/Manual de Usuario.pdf" target="_blank">
                            <i class="fa fa-book fa-fw"></i>&nbsp; Manual De Usuario
                        </a>

                    </li>
                    @if(Auth::user()->esAdmin())
                    <li>
                        <a href="/Manual Administrador.pdf" target="_blank">
                            <i class="fa fa-book fa-fw"></i>&nbsp; Manual Administrador
                        </a>

                    </li>
                    @endif
                    <li>
                        <a href="{{ url( $base_url . 'logout') }}"><i class="fa fa-fw fa-power-off"></i> Salir</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar
         Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
            @include('layouts.menu')
            <!--
                    <li>
                        <a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a>
                    </li>

                    <li>
                        <a href="forms.html"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>


                    <li>
                        <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li>
                        <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="blank-page.html"><i class="fa fa-fw fa-dashboard"></i> Blank Page</a>
                    </li>
                    <li>
                        <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                    </li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="">

        <div class="container-fluid">

            <div class="">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class=" col-xs-12" role="navigation">
            <div class="text-center">
                <p class="col-xs-12" style="color: white;">&copy; 2016. Powered by <a
                            href="http://www.doublepoint.com.ar" target="_blank">Double Point</a></p>
            </div>
        </nav>
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('js/jquery.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/bootstrap.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<!--script src="{{ asset('js/plugins/morris/morris.min.js')}}"></script>
<script src="{{ asset('js/plugins/morris/morris-data.js')}}"></script> -->


<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDHTkG8qa8Lev-pSGwhnhVS-cY7NU225g4"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

@yield('javascript')

<script type="text/javascript">
    $(document).ready(function () {
        $('.cambiar-consorcio').on('click', function () {
            var consorcio_id = $(this).data('consorcio-id');

            $.ajax({
                url: '/admin/actualizar-consorcio/' + consorcio_id,
                dataType: 'json',
                success: function (data) {
                    if (data.valid == true)
                        window.location.href = '/admin';
                }
            });
        });

        $('form').on('submit', function (e) {
            if (!$(this).hasClass("no-bloquear")) {
                $(this).find('button.btn-primary').prop('disabled', true);
            }

        });

        //  $('.body-loader').remove();
        //$('#body-content').slideDown(500);
    });

    $(document).ready(function() {
        setTimeout(function(){
            $('#body-content').slideDown(500);
            $('.body-loader').remove();
        },1000);
    });
</script>
</body>

</html>