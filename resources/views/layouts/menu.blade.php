@if(Auth::user()->esAdmin())
    @include('menus.admin')
@elseif (Auth::user()->esPropietario())
    @include('menus.propietario')
@else
    @include('menus.super-admin')
@endif