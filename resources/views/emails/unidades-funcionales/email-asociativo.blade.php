<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; color: #333;">
                        <h1>AExpensas. Regístrese</h1>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Estimado/a, <br><br>

                        Este email tiene como finalidad su registro en nuestro sistema de manejo de expensas <strong>AExpensas</strong>.
                        A continuación se le detallan los pasos para el mismo: <br><br>

                        1) Haga <a href="http://www.aexpensas.doublepoint.com.ar/register/{{ $email }}/{{ $uf->codigo_asociacion }}">click aquí</a> para comenzar el proceso.<br>
                        2) Ingrese su nombre, una contraseña deseada, repita la contraseña y escriba su teléfono.<br>
                        3) En el campo <i>Código Unidad Funcional</i> ingrese el siguiente código: <strong>{{ $uf->codigo_asociacion }}</strong><br>
                        4) Haga click en <strong>Registrar</strong>.<br><br>

                        Para ingresar al sistema, luego de registrarse, haga click en <strong>Ingresar</strong> e introduzca los datos que ingresó en el paso anterior.<br><br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.doublepoint.com.ar">AExpensas</a>
                    </td>
                </tr>
                <!--<tr>
                    <td align="left">
                        <a href="#"><img src="http://placehold.it/200x50/333&text=CTA+»" alt="CTA" style="display: block; border: 0;" /></a>
                    </td>
                </tr>-->
            </table>
        </td>
    </tr>
</table>



