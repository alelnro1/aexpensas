<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; color: #333;">
                        <h1>AExpensas. Documento de Consorcio</h1>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Estimado/a, <br><br>

                        Se le comunica que hay un nuevo documento titulado <strong>{{ $documento->nombre }}</strong>.<br>
                        <i>{{ $documento->descripcion }}</i>
                        <br><br>
                        Puede verlo también haciendo <a href="http://aexpensas.doublepoint.com.ar/admin/">click aquí</a>
                        <br><br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.doublepoint.com.ar">AExpensas</a>
                    </td>
                </tr>
                <!--<tr>
                    <td align="left">
                        <a href="#"><img src="http://placehold.it/200x50/333&text=CTA+»" alt="CTA" style="display: block; border: 0;" /></a>
                    </td>
                </tr>-->
            </table>
        </td>
    </tr>
</table>



