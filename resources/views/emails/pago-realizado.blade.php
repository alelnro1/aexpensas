<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; color: #333;">
                        <h1>Pago Declarado</h1>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Sr. Administrador, <br><br>

                        Se le comunica que la unidad funcional con código <strong>{{ $comprobante->UnidadFuncional->codigo }}</strong>,
                        piso <strong>{{ $comprobante->UnidadFuncional->piso }}</strong>,
                        dpto <strong>{{ $comprobante->UnidadFuncional->dpto }}</strong>
                        del propietario <strong>{{ $comprobante->UnidadFuncional->nombre }} {{ $comprobante->UnidadFuncional->apellido }}</strong>
                        ha realizado un pago con importe <strong>${{ $comprobante->importe }}</strong> el día {{ date('d/m/Y', strtotime($comprobante->created_at)) }}
                        a las {{ date('H:m', strtotime($comprobante->created_at)) }}<br>

                        Se le adjunta el comprobante de pago para que pueda verificar la operación.<br><br>

                        Para aprobar el pago, haga <a href="http://aexpensas.doublepoint.com.ar/admin/comprobantes/{{ $comprobante->id }}">click aquí</a><br><br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.doublepoint.com.ar">AExpensas</a>
                    </td>
                </tr>
                <!--<tr>
                    <td align="left">
                        <a href="#"><img src="http://placehold.it/200x50/333&text=CTA+»" alt="CTA" style="display: block; border: 0;" /></a>
                    </td>
                </tr>-->
            </table>
        </td>
    </tr>
</table>

        

