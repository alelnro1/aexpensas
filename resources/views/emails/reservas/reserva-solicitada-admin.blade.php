<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; color: #333;">
                        <h1>Nueva Solicitud de Reserva: {{ $reserva->Sum->descripcion }}</h1>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Sr. Administrador, <br><br>

                        Se le informa la Unidad Funcional con código <strong>{{ $reserva->UnidadFuncional->codigo }}</strong>,
                        piso <strong>{{ $reserva->UnidadFuncional->piso }}</strong>,
                        dpto <strong>{{ $reserva->UnidadFuncional->dpto }}</strong>
                        ha solicitado una reserva para <strong>{{ $reserva->Sum->descripcion }}</strong>
                        a el dia <strong>{{ date("d/m/Y", strtotime($reserva->created_at)) }}</strong>
                        a las <strong>{{ date("H:i", strtotime($reserva->created_at)) }}</strong>
                        ha sido <strong>aceptada</strong>. <br><br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.com">AExpensas</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
