<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; color: #333;">
                        <h1>Nuevo Pedido</h1>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Sr. Administrador, <br><br>

                        La Unidad Funcional con código <strong>{{ $pedido->UnidadFuncional->codigo }}</strong>,
                        piso <strong>{{ $pedido->UnidadFuncional->piso }}</strong>,
                        dpto <strong>{{ $pedido->UnidadFuncional->dpto }}</strong>
                        del propietario <strong>{{ $pedido->UnidadFuncional->nombre }} {{ $pedido->UnidadFuncional->apellido }}</strong>
                        ha realizado el siguiente pedido: <br>
                            <strong>{{ $pedido->titulo }}</strong>
                        a las {{ date('H:m', strtotime($pedido->created_at)) }}<br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.doublepoint.com.ar">AExpensas</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



