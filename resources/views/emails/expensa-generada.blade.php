<style>
    /* Shrink Wrap Layout Pattern CSS */
    @media only screen and (max-width: 599px) {
        td[class="hero"] img {
            width: 100%;
            height: auto !important;
        }
        td[class="pattern"] td{
            width: 100%;
        }
    }
</style>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600">
            <table cellpadding="0" cellspacing="0">
                <!--<tr>
                    <td class="hero">
                        <img src="http://placehold.it/600x200&text=Hero+Image" alt="" style="display: block; border: 0;" />
                    </td>
                </tr>-->
                <tr>
                    <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                        Estimado/a, <br><br>

                        Se le informa que se encuentra disponible la expensa <strong>{{ $liquidacion->nombre_periodo }}</strong>
                        del consorcio <strong>{{ $liquidacion->Consorcio->nombre }}</strong>.<br> 
                        Podrá verla en el adjunto de este mail
                        o haciendo <a href="http://aexpensas.doublepoint.com.ar/admin/liquidaciones">click aquí</a>.

                        {{--la Unidad Funcional con código <strong>{{ $pedido->UnidadFuncional->codigo }}</strong>,
                        piso <strong>{{ $pedido->UnidadFuncional->piso }}</strong>,
                        dpto <strong>{{ $pedido->UnidadFuncional->dpto }}</strong>
                        con el título:
                        <strong>{{ $pedido->titulo }}</strong><br>
                        a las {{ date('H:m', strtotime($pedido->created_at)) }}<br>--}}

                        <br><br>

                        Lo saluda atentamente el equipo de <a href="http://aexpensas.doublepoint.com.ar">AExpensas</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
