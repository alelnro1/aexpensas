@extends('layouts.app')

@section('site-name', 'Editar documento')

@section('content')
    <div class="panel-heading">Documento</div>

    <div class="panel-body">
        <form class="form-horizontal validar_form" method="POST" action="/admin/documentos/{{ $documento->id }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}

        <!-- Estado -->
            <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Estado</label>
                <div class="col-md-6">
                    <select class="form-control" name="estado">

                        @if($documento->estado == 1)
                            <option selected value="1">Activo</option>
                            <option  value="0">Inactivo</option>
                        @else
                            <option  value="1">Activo</option>
                            <option selected value="0">Inactivo</option>
                        @endif

                    </select>
                    @if ($errors->has('estado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Titulo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $documento->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ $documento->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


        @if ($documento->Tipo->descripcion == "Documento")
            <!-- Archivo -->
                <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                    <label for="archivo" class="control-label col-md-4">Archivo actualmente cargado</label>
                    <div class="col-md-6">
                        <tr>
                            <td><a href="{{ url($documento->archivo) }}" onclick="this.target='_blank'"><img src="{{ url("images/descargar_documento.png") }}" style="max-height: 25px;" alt="" ></a> | {{ $documento->nombre }}</td>
                        </tr>

                    </div>
                </div>
                <!-- Archivo -->
                <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                    <label for="archivo" class="control-label col-md-4">Archivo Nuevo</label>

                    <div class="col-md-6">
                        <input type="file" class="form-control" name="archivo" id="archivo">
                        <leyend>El archivo debe ser menor a 2MB</leyend>
                        @if ($errors->has('archivo'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('archivo') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('') ? ' has-error' : '' }}" id="consorcios">
                    <label for="relacion" class="control-label col-md-4">Consorcio</label>

                    <div class="col-md-6">

                        <table class="table table-striped task-table">
                            <tr>
                            @foreach($consorcios as $consorcio)

                                <tr>
                                    {{$checked = false}}
                                    @foreach($consorcios_asociados as $consorcio_asociado)
                                        @if($consorcio_asociado->id == $consorcio->id)
                                            <?php $checked = true ?>
                                        @endif
                                    @endforeach
                                    @if($checked == true)
                                        <td class=""><input checked type="checkbox" name=consorcios_elegidos[]" value="{{$consorcio->id}}"></td>
                                    @else
                                        <td class=""><input  type="checkbox" name=consorcios_elegidos[]" value="{{$consorcio->id}}"></td>
                                    @endif
                                    <td class="">{{$consorcio->nombre}}</td>
                                </tr>
                            @endforeach
                        </table>
                        <span style="color:red;"class="help-block">
                            <strong class="errors error1"></strong>
                        </span>
                    </div>
                </div>
            @endif



            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/documentos/edit.js') }}"></script>

    @if ($documento->Tipo->descripcion == "Documento")
        <script type="text/javascript" src="{{ asset('/js/documentos/comunes.js') }}"></script>
    @endif
@stop