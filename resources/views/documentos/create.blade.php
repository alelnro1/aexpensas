@extends('layouts.app')

@section('site-name', 'Nuevo documento')

@section('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Documento</div>

    <div class="panel-body">
        <form action="/admin/documentos" method="POST" class="form-horizontal validar_form"
              enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Tipo -->
            <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Tipo</label>

                <div class="col-md-6">
                    <select class="form-control" name="tipo_documento_id" id="tipo">
                        @foreach($tipos as $tipo)
                            <option value="{{ $tipo->id }}">{{$tipo->descripcion}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tipo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Estado</label>

                <div class="col-md-6">
                    <select class="form-control" name="estado">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>
                    @if ($errors->has('estado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3"
                              class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}" id="contenedor_archivo">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo" id="id=" archivo"">
                    <leyend>El archivo debe ser menor a 2MB</leyend>


                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Añadir relación -->
            <div class="form-group {{ $errors->has('') ? ' has-error' : '' }}" id="consorcios">
                <label for="relacion" class="control-label col-md-4">Consorcio</label>

                <div class="col-md-6">

                    <table class="table table-striped task-table">
                        <tr class="">
                        @foreach($consorcios as $consorcio)

                            <tr>
                                <td class=""><input type="checkbox" id="consorcios_elegidos"
                                                    name="consorcios_elegidos[]" value="{{$consorcio->id}}"></td>
                                <td class="">{{$consorcio->nombre}}</td>

                            </tr>
                        @endforeach
                    </table>
                    <span style="color:red;" class="help-block">
                            <strong class="errors error1"></strong>
                        </span>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>

    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/documentos/create.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/documentos/comunes.js') }}"></script>
@stop