@extends('layouts.app')

@section('site-name', 'Listando documentos')

@section('content')
    <div class="panel-heading">
        Documentos

        @if (Auth::user()->esAdmin())
            <div style="float:right;">
                <a class="btn btn-xs btn-default" href="/admin/documentos/create">Nuevo Documento</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @if(Session::has('documento_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('documento_eliminado') }}
            </div>
        @endif

        @if(Session::has('documento_creado'))
            <div class="alert alert-success">
                {{ Session::get('documento_creado') }}
            </div>
        @endif
        @if(Session::has('documento_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('documento_actualizado') }}
            </div>
        @endif

        @if (count($documentos) > 0)
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table">
                            @foreach ($documentos as $documento)
                                @if (($documento->estado == 0 || $documento->Tipo->descripcion == "Novedad")) @continue
                                @endif


                                <tr>
                                    <td>
                                        <strong>
                                            <a href="{{ url($base_url . 'documentos/' . $documento->id) }}">
                                                {{ $documento->nombre }}
                                            </a>

                                            <small>({{ date("d/m/Y", strtotime($documento->created_at)) }})</small>
                                        </strong>
                                        <br>

                                        @if(strlen( $documento->descripcion) > 50){{substr ($documento->descripcion, 0 , 50  )}}...
                                        @else {{$documento->descripcion}}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div>
                No hay documentos
            </div>
        @endif
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/documentos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/documentos/delete-link.js') }}"></script>
@stop