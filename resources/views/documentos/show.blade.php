@extends('layouts.app')

@section('site-name', 'Viendo a documento')

@section('content')
    <div class="panel-heading">
        Documento <b><i>{{ $documento->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('documento_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('documento_actualizado') }}
            </div>
        @endif

        <fieldset>
            <legend>{{ $documento->nombre }}</legend>

            <div>
                {{ $documento->descripcion }}
            </div>
        </fieldset>

        <table class="table table-striped task-table">
            @if(Auth::user()->esAdmin())
                <tr>
                    <td><strong>Consorcios</strong></td>
                    <td>
                        @foreach($consorcios_asociados as $consorcio_asociado)
                            {{$consorcio_asociado->nombre}} |
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td><strong>Estado</strong></td>
                    <td>
                        @if($documento->estado == 0)
                            Inactivo
                        @else
                            Activo
                        @endif
                    </td>
                </tr>
                @foreach($tipos as $tipo)
                    @if($tipo->id == $documento->tipo_documento_id)
                        @if($tipo->descripcion == "Documento")
                            <tr>
                                <td><strong>Archivo</strong></td>
                                <td><a href="{{ url($documento->archivo) }}" onclick="this.target='_blank'" ><img src="{{ url("images/descargar_documento.png") }}" style="max-height: 25px;" alt="" ></a></td>

                            </tr>
                        @endif
                    @endif
                @endforeach
            @endif
        </table>

        @if (Auth::user()->esAdmin())
            <td>
                <a class="btn btn-xs btn-default" href="/admin/documentos/{{ $documento->id }}/edit">Editar</a>
            </td>
        @endif

    </div>
@stop
