<li
        @if (Request::is('admin/documentos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'administradores') }}">Admins</a>
</li>
<li
        @if (Request::is('admin/usuario-admin*')) class="active" @endif>
    <a href="{{ url( $base_url . 'usuario-admin') }}">Usuarios</a>
</li>