<li
        @if (Request::is('admin/consorcios*')) class="active" @endif>
    <a href="{{ url( $base_url . 'consorcios') }}">
        <i class="fa fa-fw fa-building"></i>
        Consorcios
    </a>
</li>

<li
        @if (Request::is('admin/liquidaciones*')) class="active" @endif>
    <a href="{{ url( $base_url . 'liquidaciones') }}">
        <i class="fa fa-fw fa-fax" aria-hidden="true"></i>
        Liquidaciones
    </a>
</li>

<li
        @if (Request::is('admin/unidades-funcionales*')) class="active" @endif>
    <a href="{{ url( $base_url . 'unidades-funcionales') }}">
        <i class="fa fa-fw fa-fw fa-table"></i>
        Unidades Funcionales</a>
</li>

<li
        @if (Request::is('admin/comprobantes*')) class="active" @endif>
    <a href="{{ url( $base_url . 'comprobantes') }}">
        <i class="fa fa-fw fa-flag-checkered" aria-hidden="true"></i>
        Comprobantes</a>
</li>

<li
        @if (Request::is('admin/gastos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'gastos') }}">
        <i class="fa fa-fw fa-usd"></i>
        Gastos
    </a>
</li>

<li
        @if (Request::is('admin/sums*')) class="active" @endif>
    <a href="{{ url( $base_url . 'sums') }}">
        <i class="fa fa-fw fa-exchange"></i>
        Espacios
        Comunes</a>
</li>

<li
        @if (Request::is('admin/pedidos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'pedidos') }}">
        <i class="fa fa-fw fa-inbox" aria-hidden="true"></i>
        Pedidos
    </a>
</li>

<li
        @if (Request::is('admin/documentos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'documentos') }}">
        <i class="fa fa-fw fa-file"></i>
        Documentos
    </a>
</li>

<li
        @if (Request::is('admin/listado_usuarios*')) class="active" @endif>
    <a href="{{ url( $base_url . 'listado_usuarios') }}">
        <i class="fa fa-fw fa-users" aria-hidden="true"></i>
        Usuarios
    </a>
</li>

<li
        @if (Request::is('admin/reportes*')) class="active" @endif>
    <a href="{{ url( $base_url . 'reportes') }}">
        <i class="fa fa-fw fa-file" aria-hidden="true"></i>
        Reportes
    </a>
</li>

<li
        @if (Request::is('admin/encuestas*')) class="active" @endif>
    <a href="{{ url( $base_url . 'encuestas') }}">
        <i class="fa fa-fw fa-question" aria-hidden="true"></i>
        Encuestas
    </a>
</li>