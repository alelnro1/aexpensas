<li
        @if (Request::is('admin/liquidaciones*')) class="active" @endif>
    <a href="{{ url( $base_url . 'liquidaciones') }}">
        <i class="fa fa-fw fa-fax" aria-hidden="true"></i>
        Liquidaciones
    </a>
</li>


<li
        @if (Request::is('admin/nueva-unidad-funcional*')) class="active" @endif>
    <a href="{{ url( $base_url . 'mis-unidades-funcionales') }}">
        <i class="fa fa-fw fa-table"></i>
        Mis Unidades Funcionales
    </a>
</li>

<li
        @if (Request::is('admin/mis-movimientos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'mis-movimientos') }}">
        <i class="fa fa-fw fa-flag-checkered" aria-hidden="true"></i>
        Mis Movimientos
    </a>
</li>

<li
        @if (Request::is('admin/gastos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'gastos') }}">
        <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
        Gastos
    </a>
</li>

@if ($CANTIDAD_ESPACIOS_COMUNES > 0)
    <li
            @if (Request::is('admin/sums*')) class="active" @endif>
        <a href="{{ url( $base_url . 'sums') }}">
            <i class="fa fa-fw fa-exchange"></i>
            Espacios
            Comunes</a>
    </li>
@endif

<li
        @if (Request::is('admin/comprobantes*')) class="active" @endif>
    <a href="{{ url( $base_url . 'comprobantes') }}">
        <i class="fa fa-fw fa-flag-checkered" aria-hidden="true"></i>
        Comprobantes
    </a>
</li>

<li
        @if (Request::is('admin/pedidos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'pedidos') }}">
        <i class="fa fa-fw fa-inbox" aria-hidden="true"></i>
        Pedidos
    </a>
</li>

<li
        @if (Request::is('admin/documentos*')) class="active" @endif>
    <a href="{{ url( $base_url . 'documentos') }}">
        <i class="fa fa-fw fa-file" aria-hidden="true"></i>
        Documentos
    </a>
</li>

<li
        @if (Request::is('admin/reportes*')) class="active" @endif>
    <a href="{{ url( $base_url . 'reportes') }}">
        <i class="fa fa-fw fa-file" aria-hidden="true"></i>
        Reportes
    </a>
</li>

<li
        @if (Request::is('admin/encuestas*')) class="active" @endif>
    <a href="{{ url( $base_url . 'encuestas') }}">
        <i class="fa fa-fw fa-question" aria-hidden="true"></i>
        Encuestas
    </a>
</li>