@extends('layouts.app')

@section('site-name', 'Listando proveedores')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Proveedores

        <div style="float:right;">
            <a class="btn btn-xs btn-default" href="/admin/proveedores/create">Nuevo Proveedor</a>
        </div>
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('proveedor_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('proveedor_eliminado') }}
                </div>
            @endif

            @if(Session::has('proveedor_creado'))
                <div class="alert alert-success">
                    {{ Session::get('proveedor_creado') }}
                </div>
            @endif
            @if(Session::has('proveedor_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('proveedor_actualizado') }}
                </div>
            @endif

            @if (count($proveedores) > 0)
                <table class="table table-striped task-table" id="proveedores" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Nombre/Razón Social</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th>Persona Contacto</th>
                        <th></th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($proveedores as $proveedor)
                        <tr>
                            <td>
                                <div>
                                    @if(strlen( $proveedor->nombre_razon_social) > 15){{substr ($proveedor->nombre_razon_social, 0 , 15  )}}...
                                    @else {{$proveedor->nombre_razon_social}}
                                    @endif
                                </div>
                            </td>
                            <td >
                                <div>
                                    @if(strlen( $proveedor->email) > 15){{substr ($proveedor->email, 0 , 15  )}}...
                                    @else {{$proveedor->email}}
                                    @endif
                                </div>
                            </td>
                            <td>{{ $proveedor->telefono }}</td>
                            <td>
                                <div>
                                    @if(strlen( $proveedor->persona_contacto) > 15){{substr ($proveedor->persona_contacto, 0 , 15  )}}...
                                    @else {{$proveedor->persona_contacto}}
                                    @endif
                                </div>
                            </td>

                            <td>
                                <a class="btn btn-xs btn-default" href="/admin/proveedores/{{ $proveedor['id'] }}">Ver</a>
                                |
                                <a class="btn btn-xs btn-default" href="/admin/proveedores/{{ $proveedor['id'] }}"
                                   data-method="delete"
                                   data-token="{{ csrf_token() }}"
                                   data-confirm="Esta seguro que desea eliminar a consorcio con nombre {{ $proveedor->nombre }}?">
                                    Eliminar
                                </a>
                                |
                                <a class="btn btn-xs btn-default" href="/admin/proveedores/{{ $proveedor->id }}/edit">Editar</a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay proveedores
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/proveedores/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proveedores/delete-link.js') }}"></script>
@stop