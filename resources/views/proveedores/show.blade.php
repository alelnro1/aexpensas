@extends('layouts.app')
@section('site-name', 'Viendo a proveedor')

@section('content')
    <div class="panel-heading">
        Proveedor<b><i>{{ $proveedor->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('proveedor_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('proveedor_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" >
            <tr>
                <td><strong>Nombre | Razón social</strong></td>
                <td>{{ $proveedor->nombre_razon_social }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td><div>{{ $proveedor->descripcion }}</div></td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $proveedor->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $proveedor->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Celular</strong></td>
                <td>{{ $proveedor->celular }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio Fiscal</strong></td>
                <td>{{ $proveedor->domicilio_fiscal }}</td>
            </tr>
            <tr>
                <td><strong>Domicilio Comercial</strong></td>
                <td>{{ $proveedor->domicilio_comercial }}</td>
            </tr>
            <tr>
                <td><strong>N° Documento</strong></td>
                <td>{{ $proveedor->dni }}</td>
            </tr>
            <tr>
                <td><strong>Cuit | Cuil</strong></td>
                <td>{{ $proveedor->cuit_cuil }}</td>
            </tr>
            <tr>
                <td><strong>Página Web</strong></td>
                <td>{{ $proveedor->pagina_web }}</td>
            </tr>

            @if ($proveedor->foto != "")
                <tr>
                    <td><strong>Foto</strong></td>
                    <td><img src="/{{ $proveedor->foto }}" height="250" /></td>
                </tr>
            @endif

            <tr>
                <td><strong>Persona Contacto</strong></td>
                <td>{{ $proveedor->persona_contacto }}</td>
            </tr>
            <tr>
                <td><strong> Condición Frente al IVA</strong></td>
                <td>{{ $proveedor->condicion_frente_al_iva }}</td>
            </tr>

        </table>

        <td>
            <a class="btn btn-xs btn-default" href="/admin/proveedores/{{ $proveedor->id }}/edit">Editar</a>
        </td>

    </div>
@stop
