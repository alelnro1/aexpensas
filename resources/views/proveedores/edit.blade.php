@extends('layouts.app')

@section('site-name', 'Editar proveedor')

@section('content')
    <div class="panel-heading">Proveedor</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/admin/proveedores/{{ $proveedor->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}


                    <!-- Nombre/Razon Social -->
            <div class="form-group{{ $errors->has('nombre_razon_social') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre | Razón social</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre_razon_social" value="{{ $proveedor->nombre_razon_social }}">

                    @if ($errors->has('nombre_razon_social'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre_razon_social') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ $proveedor->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ $proveedor->email }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono -->
            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ $proveedor->telefono }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Celular -->
            <div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Celular</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="celular" value="{{ $proveedor->celular }}">

                    @if ($errors->has('celular'))
                        <span class="help-block">
                            <strong>{{ $errors->first('celular') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Pagina web -->
            <div class="form-group{{ $errors->has('pagina_web') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Pagina Web</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="pagina_web" value="{{ $proveedor->pagina_web }}">

                    @if ($errors->has('pagina_web'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pagina_web') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Foto actiamente en el sistema -->
            <div class="form-group">
                <label for="foto" class="control-label col-md-4">Foto Actual</label>
                    <div class="col-md-6">
                        <img src="/{{ $proveedor->foto }}" height="100" />
                    </div>
            </div>

            <!-- Nueva Foto -->
            <div class="form-group {{ $errors->has('foto') ? ' has-error' : '' }}">
                <label for="foto" class="control-label col-md-4">Nueva Foto</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="foto" id="archivo">
                    <leyend>La imagen debe ser menor a 2MB</leyend>

                    @if ($errors->has('foto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('foto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Condicion frente al iva -->
            <div class="form-group{{ $errors->has('condicion_frente_al_iva') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Condición Frente al IVA</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="condicion_frente_al_iva" value="{{ $proveedor->condicion_frente_al_iva }}">

                    @if ($errors->has('condicion_frente_al_iva'))
                        <span class="help-block">
                            <strong>{{ $errors->first('condicion_frente_al_iva') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Persona Contacto -->
            <div class="form-group{{ $errors->has('persona_contacto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Persona Contacto</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="persona_contacto" value="{{ $proveedor->persona_contacto }}">

                    @if ($errors->has('persona_contacto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('persona_contacto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Domicilio Fiscal -->
            <div class="form-group{{ $errors->has('domicilio_fiscal') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Fiscal</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_fiscal" value="{{ $proveedor->domicilio_fiscal }}" id="domicilio_fiscal" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_fiscal') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <!-- Domicilio Comercial -->
            <div class="form-group{{ $errors->has('domicilio_comercial') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Comercial</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_comercial" value="{{ $proveedor->domicilio_comercial }}" id="domicilio_comercial" autocomplete="off">

                    @if ($errors->has('domicilio_comercial'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_comercial') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <!-- Dni -->
            <div class="form-group{{ $errors->has('dni') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Número de documento</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="dni" value="{{ $proveedor->dni }}">

                    @if ($errors->has('dni'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dni') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Cuil/ Cuit -->
            <div class="form-group{{ $errors->has('cuit_cuil') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cuil | Cuit </label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit_cuil" value="{{ $proveedor->cuit_cuil }}">

                    @if ($errors->has('cuit_cuil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit_cuil') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>

    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/proveedores/edit.js') }}"></script>
@stop