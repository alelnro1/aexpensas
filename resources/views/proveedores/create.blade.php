@extends('layouts.app')

@section('site-name', 'Nuevo proveedor')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Proveedor</div>


    <div class="panel-body">
        <form action="/admin/proveedores" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="administrador_id" value="">


            <!-- Nombre/Razon Social -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre | Razón social</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre_razon_social" value="{{ old('nombre_razon_social') }}">

                    @if ($errors->has('nombre_razon_social'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre_razon_social') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono -->
            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ old('telefono') }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Celular -->
            <div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Celular</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="celular" value="{{ old('celular') }}">

                    @if ($errors->has('celular'))
                        <span class="help-block">
                            <strong>{{ $errors->first('celular') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Pagina Web -->
            <div class="form-group{{ $errors->has('pagina_web') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Página Web</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="pagina_web" value="{{ old('pagina_web') }}">

                    @if ($errors->has('pagina_web'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pagina_web') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Foto -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Foto</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="foto" id="archivo">
                    <leyend>La imagen debe ser menor a 2MB</leyend>

                    @if ($errors->has('foto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('foto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Condicion frente al iva -->
            <div class="form-group{{ $errors->has('condicion_frente_al_iva') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Condición Frente al IVA</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="condicion_frente_al_iva" value="{{ old('condicion_frente_al_iva') }}">

                    @if ($errors->has('condicion_frente_al_iva'))
                        <span class="help-block">
                            <strong>{{ $errors->first('condicion_frente_al_iva') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Persona Contacto -->
            <div class="form-group{{ $errors->has('persona_contacto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Persona Contacto</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="persona_contacto" value="{{ old('persona_contacto') }}">

                    @if ($errors->has('persona_contacto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('persona_contacto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Domicilio Fiscal -->
            <div class="form-group{{ $errors->has('domicilio_fiscal') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Fiscal</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_fiscal" value="{{ old('domicilio_fiscal') }}" id="domicilio_fiscal" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_fiscal') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <!-- Domicilio Comercial -->
            <div class="form-group{{ $errors->has('domicilio_comercial') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Comercial</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_comercial" value="{{ old('domicilio_comercial') }}" id="domicilio_comercial" autocomplete="off">

                    @if ($errors->has('domicilio_comercial'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_comercial') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <!-- Dni -->
            <div class="form-group{{ $errors->has('dni') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Número de documento</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="dni" value="{{ old('dni') }}">

                    @if ($errors->has('dni'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dni') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Cuil/ Cuit -->
            <div class="form-group{{ $errors->has('cuit_cuil') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cuil | Cuit </label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit_cuil" value="{{ old('cuit_cuil') }}">

                    @if ($errors->has('cuit_cuil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit_cuil') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>


    </div>
@stop

@section('javascript')

    <script type="text/javascript" src="{{ asset('/js/proveedores/create.js') }}"></script>
@stop