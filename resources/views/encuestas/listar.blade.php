@extends('layouts.app')

@section('site-name', 'Listando encuestas')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Encuestas

        @if (!Auth::user()->esPropietario())
            <div style="float:right;">
                <a href="{{ url($base_url . 'encuestas/create') }}" class="btn btn-block btn-default btn-xs">Nueva Encuesta</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('encuesta_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('encuesta_eliminado') }}
                </div>
            @endif

            @if(Session::has('encuesta_creado'))
                <div class="alert alert-success">
                    {{ Session::get('encuesta_creado') }}
                </div>
            @endif

            @if(Session::has('encuesta_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('encuesta_actualizado') }}
                </div>
            @endif

            <fieldset>
                <legend>Encuestas Activas</legend>

                @if (count($encuestas_activas) > 0)
                    <table class="table table-bordered table-hover" width="100%">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>Pregunta</th>
                            <th>Fecha Creación</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($encuestas_activas as $encuesta)
                            <tr>
                                <td>{{ $encuesta->pregunta }}</td>
                                <td>{{ date('d/m/Y H:i', strtotime($encuesta->created_at)) }}</td>

                                <td>
                                    <a href="{{ url( $base_url . 'encuestas/' . $encuesta['id']) }}" class="btn btn-default btn-xs">Ver</a>

                                    @if(!Auth::user()->esPropietario())
                                        <a href="{{ url( $base_url . 'encuestas/' . $encuesta['id'] . '/edit') }}" class="btn btn-info btn-xs">Editar</a>
                                    @endif
                                    {{--<a href="/admin/encuestas/{{ $encuesta['id'] }}" class="btn btn-danger btn-xs"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Está seguro que desea eliminar la encuesta?">
                                        Eliminar
                                    </a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No hay encuestas activas
                @endif
                <br><br>
            </fieldset>


            <fieldset>
                <legend>Encuestas No Activas</legend>

                @if (count($encuestas_no_activas) > 0)
                    <table class="table table-bordered table-hover" id="encuestas" style="width:100%">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>Pregunta</th>
                            <th>Fecha Creación</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($encuestas_no_activas as $encuesta)
                            <tr>
                                <td>{{ $encuesta->pregunta }}</td>
                                <td>{{ date('d/m/Y H:i', strtotime($encuesta->created_at)) }}</td>

                                <td>
                                    <a href="{{ url( $base_url . 'encuestas/' . $encuesta['id']) }}" class="btn btn-default btn-xs">Ver</a>

                                    @if (!Auth::user()->esPropietario())
                                        <a href="{{ url( $base_url . 'encuestas/' . $encuesta['id'] . '/edit') }}" class="btn btn-info btn-xs">Editar</a>

                                        <a href="/admin/encuestas/{{ $encuesta['id'] }}" class="btn btn-danger btn-xs"
                                           data-method="delete"
                                           data-token="{{ csrf_token() }}"
                                           data-confirm="Está seguro que desea eliminar la encuesta?">
                                            Eliminar
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No hay encuestas activas
                @endif
            </fieldset>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/encuestas/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/encuestas/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
