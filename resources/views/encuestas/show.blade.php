@extends('layouts.app')

@section('site-name', 'Viendo a encuesta')

@section('content')
    <div class="panel-heading">
        Encuesta creada el {{ date("d/m/Y", strtotime($encuesta->created_at)) }}
        a las {{ date("H:i", strtotime($encuesta->created_at)) }}
    </div>

    <div class="panel-body">
        @if(isset($encuesta->archivo) && $encuesta->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $encuesta->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <div class="row">
            <div class="col-xs-8">
                <h3>{{ $encuesta->pregunta }}</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <div>
                    <span><u>Posibles Respuestas</u></span>
                    <br>
                </div>

                @foreach ($encuesta->Respuestas as $key => $respuesta)
                    {{ $key+1 }}) {{ $respuesta->respuesta }}
                    <br>
                @endforeach

            </div>
        </div>

        @if ($encuesta->tieneVotos())

            <div class="row">
                <div class="col-xs-8">

                    <h5>
                        <u>Resultados</u>
                    </h5>

                    <div id="grafico-cantidad-votos"></div>

                    {!! $grafico->render('PieChart', 'CantidadDeVotos', 'grafico-cantidad-votos', ['width' => '100%', 'height' => '100%']) !!}
                </div>
            </div>
        @endif


        <hr>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        @if(!Auth::user()->esPropietario())
            <div class="col-xs-6">
                <a href="{{ url( $base_url . 'encuestas/' . $encuesta->id . '/edit') }}" class="btn btn-default btn-primary" style="float:right; color: white;">
                    <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
                </a>
            </div>
        @endif
    </div>
@stop
