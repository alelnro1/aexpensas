@extends('layouts.app')

@section('site-name', 'Editar encuesta')

@section('styles')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="panel-heading">Editar Encuesta</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url($base_url . 'encuestas/' . $encuesta->id) }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}

        <!-- Nombre -->
            <div class="form-group{{ $errors->has('pregunta') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label">Pregunta</label>

                <div class="col-md-7">
                    <input type="text" class="form-control" name="pregunta" value="{{ $encuesta->pregunta }}">

                    @if ($errors->has('pregunta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pregunta') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group">
                <label class="col-md-3 control-label">Activo</label>

                <div class="col-md-7">
                    <input @if ($encuesta->estado == "1") checked @endif data-toggle="toggle" type="checkbox" name="estado" data-on="Sí" data-off="No">
                </div>
            </div>

            <div class="form-group{{ $errors->has('respuestas') ? ' has-error' : '' }}" id="respuestas-div">
                <label for="" class="control-label col-xs-3">Respuestas</label>

                <div class="row col-xs-8">
                    <div class="col-xs-7" id="listado-respuestas">
                        @foreach ($encuesta->Respuestas as $respuesta)
                            <div class="col-xs-12 respuesta">
                                <br>
                                <div class="col-xs-9">
                                    <input type="text" name="respuestas[]" class="form-control" value="{{ $respuesta->respuesta }}" placeholder="Escriba la respuesta disponible">
                                </div>

                                <div class="col-xs-1">
                                    <a href="#" class="btn btn-danger" id="eliminar-respuesta">-</a>
                                </div>
                            </div>

                        @endforeach
                    </div>

                    <div class="col-xs-1">
                        <a href="#" class="btn btn-success" id="nueva-respuesta">Agregar Respuesta</a>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/encuestas/create.js') }}"></script>
    <script type="x-template" id="nueva-fila-respuesta">
        <div class="col-xs-12 respuesta">
            <br>
            <div class="col-xs-9">
                <input type="text" name="respuestas[]" class="form-control" placeholder="Escriba la respuesta disponible">
            </div>

            <div class="col-xs-1">
                <a href="#" class="btn btn-danger" id="eliminar-respuesta">-</a>
            </div>
        </div>
    </script>
@stop
