@extends('layouts.app')

@section('site-name', 'Nueva encuesta')

@section('styles')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="panel-heading">Nueva</div>

    <div class="panel-body">
        @if($errors->any())
            <div class="alert alert-danger">
                Hay errores en la encuesta. Verifique que hayan respuestas y los campos sean correctos.
            </div>
        @endif

        <form action="{{ url($base_url . 'encuestas') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Pregunta -->
            <div class="form-group{{ $errors->has('pregunta') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label">Pregunta</label>

                <div class="col-md-7">
                    <input type="text" class="form-control" name="pregunta" value="{{ old('pregunta') }}"  placeholder="Escriba la pregunta a encuestar">

                    @if ($errors->has('pregunta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pregunta') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group">
                <label class="col-md-3 control-label">Activo</label>

                <div class="col-md-7">
                    <input @if (old('estado') == "on") checked @endif data-toggle="toggle" type="checkbox" name="estado" data-on="Sí" data-off="No">
                </div>
            </div>

            <!-- Respuestas -->
            <div class="form-group{{ $errors->has('respuestas') ? ' has-error' : '' }}" id="respuestas-div">
                <label for="" class="control-label col-xs-3">Respuestas</label>

                <div class="row col-xs-8">
                    <div class="col-xs-7" id="listado-respuestas">
                        <div class="col-xs-12 respuesta">
                            <div class="col-xs-9">
                                <input type="text" name="respuestas[]" class="form-control" placeholder="Escriba la respuesta disponible">
                            </div>

                            <div class="col-xs-1">
                                <a href="#" class="btn btn-danger" id="eliminar-respuesta">-</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-1">
                        <a href="#" class="btn btn-success" id="nueva-respuesta">Agregar Respuesta</a>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i>&nbsp;Crear Encuesta
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="x-template" id="nueva-fila-respuesta">
        <div class="col-xs-12 respuesta">
            <br>
            <div class="col-xs-9">
                <input type="text" name="respuestas[]" class="form-control" placeholder="Escriba la respuesta disponible">
            </div>

            <div class="col-xs-1">
                <a href="#" class="btn btn-danger" id="eliminar-respuesta">-</a>
            </div>
        </div>
    </script>
    <script type="text/javascript" src="{{ asset('/js/encuestas/create.js') }}"></script>
@stop
