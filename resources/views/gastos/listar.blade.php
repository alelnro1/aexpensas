@extends('layouts.app')

@section('site-name', 'Listando gastos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Gastos

        @if (!Auth::user()->esPropietario())
            <div style="float:right;">
                <a class="btn btn-xs btn-default" href="/admin/gastos/create">Nuevo Gasto</a>
                <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'gastos/cargar-sueldo')}}">Cargar Sueldo</a>
            </div>
        @endif
    </div>
    <div class="panel-body table-responsive">
        @include('loader')

        <div id="body-content">
            @if(Session::has('gasto_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('gasto_eliminado') }}
                </div>
            @endif

            @if(Session::has('gasto_creado'))
                <div class="alert alert-success">
                    {{ Session::get('gasto_creado') }}
                </div>
            @endif
            @if(Session::has('gasto_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('gasto_actualizado') }}
                </div>
            @endif

            @if (count($cuotas_agrupadas) > 0)
                <div class="accordion" style="margin-bottom: 10px;">
                    @foreach ($cuotas_agrupadas as $mes => $cuotas)
                        <h3>
                            <div>
                                <div style="width: 80px; float: left;">
                                    {{ $mes }}
                                </div>

                                <div style="width: 180px; float: right; text-align: right;">
                                    (${{ number_format($totales[$mes], 2) }})
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </h3>

                        <table class="table table-striped task-table table gastos" width="100%">
                            <!-- Table Headings -->
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Fecha</th>
                                <th>Cuota</th>
                                <th>Importe</th>
                                <th>Rubro</th>
                                <th>Descripción</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($cuotas as $cuota)
                                <tr>
                                    <td>{{ $cuota->Gasto->id }}</td>
                                    <td>{{ date('d/m/Y', strtotime($cuota->fecha)) }}</td>
                                    @if ($cuota->descripcion == "1 de 1")
                                        <td>-</td>
                                    @else
                                        <td>{{ $cuota->descripcion}}</td>
                                    @endif

                                    <td class="text-right">
                                        {{ '$ '. number_format($cuota->importe, 2) }}
                                    </td>
                                    <td>{{ $cuota->Gasto->Rubro->descripcion }}</td>
                                    <td>
                                        @if(strlen( $cuota->Gasto->descripcion) > 15)
                                            {{ substr($cuota->Gasto->descripcion, 0 , 15 ) }}...
                                        @else
                                            {{ $cuota->Gasto->descripcion }}
                                        @endif
                                    </td>

                                    <td>
                                        @if (!Auth::user()->esPropietario())
                                            <a class="btn btn-xs btn-default"
                                               href="{{ url( $base_url . 'gastos/' . $cuota->Gasto->id) }}">Ver</a>
                                            <a class="
                                                    @if($cuota->Gasto->created_at < '2017-04-16 12:24:00' && $cuota->Gasto->Rubro->descripcion == 'Detalle De Sueldos Y Cargas Sociales')
                                                    hidden
                                                @endif
                                                    btn btn-xs btn-default"
                                               href="{{ url( $base_url . 'gastos/clonar/' . $cuota->Gasto->id) }}">Clonar
                                            </a>
                                            <?php $se_puede_eliminar = true ?>
                                            @foreach($cuotas_chequeo as $cuota_chequeo)
                                                @if($cuota_chequeo->gasto_id == $cuota->gasto_id)
                                                    @if($cuota_chequeo->liquidacion_id != null)
                                                        <?php $se_puede_eliminar = false ?>

                                                    @endif
                                                @endif

                                            @endforeach
                                            @if ($se_puede_eliminar)
                                                |
                                                <a class="btn btn-xs btn-default"
                                                   href="/admin/gastos/{{ $cuota->gasto_id }}/edit">Editar</a>
                                                |
                                                <a class="btn btn-xs btn-default"
                                                   href="{{ url( $base_url . 'gastos/' . $cuota->gasto_id) }}"
                                                   data-method="delete"
                                                   data-token="{{ csrf_token() }}"
                                                   data-confirm="Esta seguro que desea eliminar al Gasto con Id: {{ $cuota->gasto_id }}?">
                                                    Eliminar
                                                </a>
                                            @else

                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endforeach
                </div>


                <a href="{{ url($base_url . 'exportar/gastos') }}">Exportar</a>
            @else
                No hay gastos
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/gastos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/gastos/delete-link.js') }}"></script>
    <script>
        $(function () {
            $(".accordion").accordion({
                heightStyle: "content",
                collapsible: true
            });
        });
    </script>
@stop
