@extends('layouts.app')

@section('site-name', 'Nuevo gasto')

@section('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
    <link href="{{ asset('css/quitar-flechas-imput.css') }}" rel="stylesheet">


@stop

@section('content')
    <div class="panel-heading">Cargar Sueldo</div>

    <div class="panel-body">
        <form action="/admin/gastos/store-sueldo" method="POST" class="form-horizontal no-bloquear" id="formulario"
              enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Fecha -->

            <!-- Rubro  -->
            <input hidden name="rubro_id" value="{{$rubro_id}}">
            <!-- Tipo Gasto  -->
            <input hidden name="tipo_gastos_id" value="{{$tipo_gasto_id}}">
            <!-- Tipo expensa  -->
            <input hidden name="tipo_expensa_id" value="{{$tipo_expensa_id}}">



            <div class="form-group{{ $errors->has('empleado_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Empleado</label>

                <div class="col-md-6">
                    <select class="form-control" name="empleado_id" id="empleado_id">
                        <option value="">Seleccionar...</option>
                    @foreach($empleados as $empleado)
                            <option value="{{ $empleado->id }}">{{$empleado->nombre . ' | ' . $empleado->legajo}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('empleado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('empleado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" class="form-control" rows="5">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                        <strong>{{ $errors->first('descripcion') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <legend>Detalle del sueldo</legend>


            <!-- Detalle Sueldo -->
            <div id="campos" class="form-group{{ $errors->has('campos_valores') ? ' has-error' : '' }}">
                <label for="" class="control-label col-md-4"></label>

                <div class="row campos_valores">
                    <div class="col-md-3">
                        <span>Descripción</span>
                        <br><br>
                        <input type="text" class="form-control" name="campo[]">
                    </div>
                    <div class="col-md-2">
                        <span>Monto</span>
                        <br><br>
                        <div>
                            <input id="" type="number" step="0.01" class="form-control valor" name="valor[]"/>
                        </div>
                    </div>
                    <div class="col-md-1">

                        <div>
                            <a href="#" class="btn btn-success" id="agregar-campo">+</a>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div>
                            <a href="#" class="btn btn-danger" id="removeButton">-</a>
                        </div>
                    </div>

                </div>
                <br>
            </div>
            <hr>
            <!-- cuotas -->
            <div class="form-group{{ $errors->has('cuotas') ? ' has-error' : '' }}">

                <label for="" class="control-label col-md-4">Fecha</label>
                <div class="row cuotas_fechas_importes">
                    <div class="col-md-3">
                        <div class="input-group date datetimepicker1">
                            <input type="text" class="form-control" id='fecha' name="fecha" value="{{ old('fecha') }}"
                                   autocomplete="off"/>
                            <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <input id="valor_total" type="number" step="0.01" class="form-control" name="cuota"
                                   value="" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div>
                            &nbsp;
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div>
                            &nbsp;
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" id="boton-submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>

    </div>
@stop

@section('javascript')
    <script src="/js/gastos/sueldos.js"></script>


@stop