@extends('layouts.app')

@section('site-name', 'Viendo a gasto')

@section('content')
    <div class="panel-heading">
        Gasto <strong>{{ $gasto->descripcion }}</strong>
    </div>

    <div class="panel-body table-responsive">

        <table class="table table-striped task-table" >
            <tr>
                <td><strong>Rubro</strong></td>
                <td>{{ $gasto->Rubro->descripcion }}</td>
            </tr>
            <tr>
                <td><strong>Tipo Gasto</strong></td>
                <td>{{ $gasto->TipoGasto->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Tipo Expensa</strong></td>
                <td>{{ $gasto->TipoExpensa->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Proveedor</strong></td>
                @if( $gasto->Proveedor != null)
                    <td>{{ $gasto->Proveedor->nombre_razon_social }}</td>
                @else
                    <td>Ninguno</td>

                @endif
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $gasto->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Nro de Factura</strong></td>
                <td>{{ $gasto->nro_facturas }}</td>
            </tr>

            <tr>
                <td><strong>Cuotas</strong></td>
                <td>
                    <div class="table-responsive">
                        <table class="table table-striped task-table"  >
                            <?php $i=1?>
                            <tr>

                                <th>N° Cuota</th>
                                <th>Fecha</th>
                                <th>Importe</th>
                            </tr>
                            @foreach($gasto->Cuotas as $cuota)
                                <tr>
                                    <td>
                                        {{$i . "°"}}
                                        <?php $i++ ;?>
                                    </td>
                                    <td>{{ date('d/m/Y', strtotime($cuota->fecha)) }}</td>
                                    <td>${{ number_format($cuota->importe, 2) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>
            @if($gasto->TipoExpensa->descripcion == "Particular")
                <tr>
                    <td><strong>Unidades Funcionales</strong></td>
                    <td>
                        <div class="table-responsive">
                            <table class="table table-striped task-table"  >
                                <?php $i=1?>
                                <tr>
                                    <th>Unidad Funcional</th>
                                    <th>Propietario</th>
                                    <th>Piso</th>
                                    <th>Dpto</th>
                                </tr>

                                @foreach($gasto->UnidadesFuncionales as $unidad_funcional)
                                    <tr>
                                        <td>{{$unidad_funcional->codigo}}</td>
                                        <td>{{$unidad_funcional->nombre . " ". $unidad_funcional->apellido}}</td>
                                        <td>{{$unidad_funcional->piso}}</td>
                                        <td>{{$unidad_funcional->dpto}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </td>
                </tr>
            @endif
            @if($gasto->archivo != "")
                <tr>
                    <td><strong>Archivo</strong></td>
                    <td><a href="{{ url($gasto->archivo) }}" onclick="this.target='_blank'" ><img src="{{ url("images/descargar_documento.png") }}" style="max-height: 25px;" alt="" ></a></td>
                </tr>
            @endif

        </table>

        <div>
        <!-- a class="btn btn-xs btn-default"  href="/admin/gastos/{{ $gasto->id }}/edit">Editar</ a> -->
        </div>

        <div>

        </div>
    </div>
@stop

