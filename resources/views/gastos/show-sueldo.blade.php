@extends('layouts.app')

@section('site-name', 'Viendo a gasto')

@section('content')
    <div class="panel-heading">
        Gasto <strong>{{ $gasto->descripcion }}</strong>
    </div>

    <div class="panel-body table-responsive">

        <table class="table table-striped task-table">
            <tr>
                <td><strong>Rubro</strong></td>
                <td>{{ $gasto->Rubro->descripcion }}</td>
            </tr>
            <tr>
                <td><strong>Tipo Gasto</strong></td>
                <td>{{ $gasto->TipoGasto->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Tipo Expensa</strong></td>
                <td>{{ $gasto->TipoExpensa->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Empleado</strong></td>
                <td>{{ $gasto->Empleado->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $gasto->descripcion }}</td>
            </tr>
            <tr>
                <td><strong>Detalle Sueldo</strong></td>
                <td>
                    <div class="table-responsive">
                        <table class="table table-striped task-table">
                            <?php $i = 1?>
                            <tr>

                                <th>Descripción</th>
                                <th>Importe</th>
                            </tr>
                            @foreach($gasto->DetallesSueldos as $detalle)
                                <tr>
                                    <td>{{ $detalle->campo }}</td>
                                    <td>${{ number_format($detalle->valor, 2) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>

            <tr>
                <td><strong>Total</strong></td>
                <td>
                    <div class="table-responsive">
                        <table class="table table-striped task-table">
                            <?php $i = 1?>
                            <tr>

                                <th>Fecha</th>
                                <th>Importe</th>
                            </tr>
                            @foreach($gasto->cuotas as $cuota)
                                <tr>
                                    <td>{{ date('d/m/Y', strtotime($cuota->fecha)) }}</td>
                                    <td>${{ number_format($cuota->importe, 2) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>

            @if($gasto->archivo != "")
                <tr>
                    <td><strong>Archivo</strong></td>
                    <td><a href="{{ url($gasto->archivo) }}" onclick="this.target='_blank'"><img
                                    src="{{ url("images/descargar_documento.png") }}" style="max-height: 25px;" alt=""></a>
                    </td>
                </tr>
            @endif

        </table>

        <div>
        <!-- a class="btn btn-xs btn-default"  href="/admin/gastos/{{ $gasto->id }}/edit">Editar</ a> -->
        </div>

        <div>

        </div>
    </div>
@stop

