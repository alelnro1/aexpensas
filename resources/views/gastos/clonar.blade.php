@extends('layouts.app')

@section('site-name', 'Editar gasto')

@section('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="panel-heading">Nuevo Gasto</div>
    <div class="panel-body">
        <form action="/admin/gastos" method="POST" class="form-horizontal no-bloquear" id="formulario"
              enctype="multipart/form-data">
        {!! csrf_field() !!}


        <!-- Rubro  -->
            <div class="form-group{{ $errors->has('rubro_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Rubro</label>

                <div class="col-md-6">
                    <select class="form-control"
                            name="rubro_id">
                        @foreach($rubros as $rubro)
                            @if($rubro->rubro_id != null)
                                <option value="{{$rubro->id}}"
                                        @if($rubro->id == $gasto->Rubro->id) selected @endif > {{$rubro->descripcion}}</option>
                            @endif
                        @endforeach
                    </select>
                    @if ($errors->has('rubro_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('rubro_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tipo Gasto  -->
            <div class="form-group{{ $errors->has('tipo_gastos_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Tipo Gasto</label>
                <div class="col-md-6">
                    <select class="form-control"
                            name="tipo_gastos_id">
                        @foreach($tipos_gastos as $tipo_gastos)
                            <option value="{{$tipo_gastos->id}}"
                                    @if($tipo_gastos->id == $gasto->TipoGasto->id) selected @endif>{{$tipo_gastos->descripcion}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tipo_gastos_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo_gastos_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tipo expensa  -->
            <div class="form-group{{ $errors->has('tipo_expensa_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Tipo Expensa</label>

                <div class="col-md-6">
                    <select class="form-control"
                            id="tipo_expensa" name="tipo_expensa_id">
                        @foreach($tipos_expensas as $tipo_expensa)
                            @if ($tipo_expensa->descripcion == "Cochera" && $cantidad_cocheras == 0) @continue @endif
                            <option value="{{$tipo_expensa->id}}"
                                    @if($tipo_expensa->id == $gasto->TipoExpensa->id ) selected @endif>{{$tipo_expensa->descripcion}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tipo_expensa_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo_expensa_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Nuevo Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control"
                           name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion"
                              class="form-control" rows="5">{{ $gasto->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                </span>
                    @endif
                </div>
            </div>

            <!-- nro de factura -->
            <div class="form-group {{ $errors->has('nro_factura') ? ' has-error' : '' }}">
                <label for="nro_factura" class="control-label col-md-4">Nro de Factura</label>

                <div class="col-md-6">
                    <input name="nro_factura" class="form-control" rows="5" value="{{ $gasto->nro_factura }}"></input>

                    @if ($errors->has('nro_factura'))
                        <span class="help-block">
                        <strong>{{ $errors->first('nro_factura') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <!-- proveedores  -->
            <div class="form-group{{ $errors->has('proveedor_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Proveedor</label>

                <div class="col-md-6">
                    <select class="form-control"
                            name="proveedor_id">

                        @foreach($proveedores as $proveedor)
                            <option value="{{$proveedor->id}}"
                                    @if($gasto->Proveedor != null) @if($gasto->Proveedor->id == $proveedor->id ) selected @endif @endif>{{$proveedor->nombre_razon_social}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('proveedor_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('proveedor_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- cuotas -->
            <div id="campos" class="form-group{{ $errors->has('cuotas') ? ' has-error' : '' }}">

                <label for="" class="control-label col-md-4">Cuota 1</label>
                <div class="row cuotas_fechas_importes">
                    <div class="col-md-3">
                        <div class="input-group date datetimepicker1">
                            <input type="text" class="form-control" name="fecha[]" value="{{ old('fecha') }}"
                                   autocomplete="off"/>
                            <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <input id="" type="number" step="0.01" class="form-control" name="cuotas[]"
                                   value="{{old('cuotas')}}"/>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div>
                            <a href="#" class="btn btn-success" onclick="AgregarCampos();">+</a>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div>
                            <a href="#" class="btn btn-danger" id="removeButton">-</a>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Seleccionar Unidades Funcionales -->

            <div hidden id="contenedor_de_select_unidades_funcionales"
                 class="form-group {{ $errors->has('unidades_funcionales_seleccionadas') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="unidades_funcionales_seleccionadas">Unidades
                    Funcionales</label>

                <div class="col-md-8">
                    <select name="unidades_funcionales_seleccionadas[]" id="seleccionar_unidad_funcional"
                            multiple="multiple">
                        @foreach ($unidades_funcionales as $unidad_funcional)
                            @foreach($gasto->UnidadesFuncionales as $unidad_funcional_elegida)@if($unidad_funcional->id == $unidad_funcional_elegida->id )
                                selected @endif @endforeach
                            <option @foreach($gasto->UnidadesFuncionales as $unidad_funcional_elegida) @if($unidad_funcional->id == $unidad_funcional_elegida->id ) selected
                                    @endif @endforeach
                                    value="{{ $unidad_funcional->id }}">{{ $unidad_funcional->codigo .' | '.$unidad_funcional->piso .'-'.$unidad_funcional->dpto.' | '.$unidad_funcional->nombre .' '.$unidad_funcional->apellido  }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('unidades_funcionales_seleccionadas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('unidades_funcionales_seleccionadas') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Importe -->

            <!-- Cuota.php Gastos (para agregar otra cuota)-->


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" id="boton-submit" class="btn btn-primary ">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>


    <script type="text/javascript" src="{{ asset('/js/gastos/edit.js') }}"></script>
    <script src="{{ asset('js/gastos/multiselect.js') }}"></script>
    <script>
        $('#seleccionar_unidad_funcional').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Buscar...',
            allSelectedText: 'Todos seleccionados...',
            numberDisplayed: 1,

            nonSelectedText: 'Seleccione una unidad funcional...',
            selectAllText: 'Seleccionar todos',
            maxHeight: 220
        });

        $('.datetimepicker1').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: 'dd/mm/yyyy'
        });


        if ($('#tipo_expensa option:selected').text() == 'Particular') {
            $('#contenedor_de_select_unidades_funcionales').show();

        }

        $('#tipo_expensa').on('change', function () {
            var opcion_seleccionada = $('#tipo_expensa option:selected').text()
            if (opcion_seleccionada == "Particular") {
                $('#contenedor_de_select_unidades_funcionales').slideDown();
            }
            else {
                $('#contenedor_de_select_unidades_funcionales').slideUp();
            }
        })
    </script>

    <script type="text/javascript">

        //lo que hace aparecer los campos de cuotas
        var nextinput = 0;
        var nextinput_lavel = 1;
        function AgregarCampos() {
            nextinput++;
            nextinput_lavel++;

            campo = '<div id="TextBoxDiv' + nextinput + '">' +
                '<br><label for="fecha" class="control-label col-md-4">Cuota ' + nextinput_lavel + '</label>' +
                '<div class="row cuotas_fechas_importes">' +
                '<div class="col-md-3">' +
                '<div class="input-group date datetimepicker1">' +
                '<input id="fecha[' + nextinput + ']" type="text" class="form-control" name="fecha[' + nextinput + ']" autocomplete="off"  />' +
                '<span class="input-group-addon">' +
                '<span class="glyphicon glyphicon-calendar"></span>' +
                '</span>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2">' +
                '<div>' +
                '<input id="cuotas[' + nextinput + ']" type="number" step="0.01"  class="form-control" name="cuotas[' + nextinput + ']  />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2">' +
                '&nbsp;' +
                '</div>' +
                '</div>' +
                '</div>';

            $("#campos").append(campo);
            $('.datetimepicker1').datepicker({
                todayHighlight: true,
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        }
        $("#removeButton").click(function () {
            var cuotas = contar_cuotas(),
                nextinput = cuotas[0] - 1 - $('#cantidad_de_cuotas_liquidadas').val();
            if (nextinput + $('#cantidad_de_cuotas_liquidadas').val() == 0) {
                alert("No es posible eliminar todas las cuotas");
                return false;
            }
            $("#TextBoxDiv" + nextinput).remove();
        });


        $("#formulario").submit(function (e) {
            var hay_cuotas_vacias = false;
            var fecha_anterior = null,
                fechas_validas = true;

            $(".cuotas_fechas_importes input").each(function (index, element) {
                fecha_de_comparacion = null;

                if ($(this).val() == "") {
                    hay_cuotas_vacias = true;
                    return false;
                }

                if ((index % 2) == 0) {
                    // Si no hay nada para comparar, creo el primer objeto
                    if (fecha_anterior == null) {
                        dia_anterior = $(this).val().split('/')[0];
                        mes_anterior = $(this).val().split('/')[1];
                        anio_anterior = $(this).val().split('/')[2];

                        fecha_anterior = new Date(anio_anterior, mes_anterior, dia_anterior);
                    }

                    // Obtengo la fecha del ultimo listado de fechas, para comparar con la anterior
                    dia_actual = $(this).val().split('/')[0];
                    mes_actual = $(this).val().split('/')[1];
                    anio_actual = $(this).val().split('/')[2];

                    fecha_actual = new Date(anio_actual, mes_actual, dia_actual);

                    if (fecha_anterior <= fecha_actual) {
                        console.log(fecha_anterior + " < " + fecha_actual);

                        fecha_anterior = fecha_actual;
                    } else {
                        console.log('invalido');
                        fechas_validas = false
                    }
                }
            });

            if (!hay_cuotas_vacias && fechas_validas) {
                $('#boton-submit').prop('disabled', true);
            }
            if (hay_cuotas_vacias) {
                alert('No puede haber fechas ni cuotas vacias');
                e.preventDefault();
                return false;
            }
            if (!fechas_validas) {
                alert('El orden de las cuotas ingresadas no es correcto');
                e.preventDefault();
            }

            if ($('input[name=archivo]').val() == "") {
                if (!confirm('No se adjunto documento desea continuar?')) {
                    e.preventDefault();
                    $('#boton-submit').prop('disabled', false);
                }
            }
        });


    </script>
@stop