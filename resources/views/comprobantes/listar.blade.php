@extends('layouts.app')

@section('site-name', 'Listando comprobantes')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Comprobantes

        <div style="float:right;">
            <a href="{{ url( $base_url . 'comprobantes/create') }}" class="btn btn-default btn-xs">Nuevo Comprobante</a>
        </div>
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">

            @if(Session::has('comprobante_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('comprobante_eliminado') }}
                </div>
            @endif

            @if(Session::has('comprobante_creado'))
                <div class="alert alert-success">
                    {{ Session::get('comprobante_creado') }}
                </div>
            @endif

            @if (count($comprobantes_agrupados) > 0)
                <div class="accordion" style="margin-bottom: 10px;">
                    @foreach ($comprobantes_agrupados as $mes => $comprobantes)
                        <h3>
                            <div>
                                <div style="width: 80px; float: left;">
                                    {{ $mes }}
                                </div>

                                <div style="width: 180px; float: right; text-align: right;">
                                    ${{ number_format($totales[$mes], 2) }}
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </h3>

                        <table class="table table-striped task-table comprobantes" width="100%">
                            <!-- Table Headings -->
                            <thead>
                            <tr>
                                <th>fecha_completa</th>
                                <th>UF</th>
                                <th>Importe</th>
                                <th>Estado</th>
                                <th>Fecha</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($comprobantes as $comprobante)
                                <tr>
                                    <td>{{ $comprobante->fecha_transaccion }}</td>
                                    <td class="text-right">
                                        {{ $comprobante->UnidadFuncional->codigo }}|{{ $comprobante->UnidadFuncional->piso }}-{{ $comprobante->UnidadFuncional->dpto }}
                                    </td>
                                    <td class="text-right">${{ number_format($comprobante->importe, 2) }}</td>
                                    <td>{{ $comprobante->Estado->descripcion }}</td>
                                    <td>{{ date('d/m/Y', strtotime($comprobante->fecha_transaccion)) }}</td>

                                    <td>
                                        <a href="{{ url( $base_url . 'comprobantes/' . $comprobante['id']) }}" class="btn btn-default btn-sm">Ver</a>
                                    </td>

                                    <td>
                                        @if ($comprobante->recibo != "")
                                            <a href="{{ url('/' . $comprobante->recibo) }}" class="btn btn-default btn-sm" download>Recibo</a>
                                        @endif
                                    </td>

                                    <td>
                                        @if ($comprobante->archivo != "")
                                            <a href="{{ url('/' . $comprobante->archivo) }}" class="btn btn-default btn-sm" download>Comprobante</a>
                                        @endif
                                    </td>

                                    <td class="right">
                                        @if (
                                                ($comprobante->Estado->descripcion == "Pendiente" && Auth::user()->esPropietario())
                                                ||
                                                (Auth::user()->esAdmin() && $comprobante->liquidacion_id == null)
                                                )
                                            <a href="{{ url( $base_url . 'comprobantes/' . $comprobante['id'] . '/edit') }}" class="btn btn-warning btn-sm">Editar</a>

                                            @if (
                                                !Auth::user()->esAdmin()
                                                && $comprobante->Estado->descripcion == "Pendiente" && Auth::user()->esPropietario()
                                            )
                                                <a href="{{ url( $base_url . 'comprobantes/' . $comprobante['id']) }}" class="btn btn-danger btn-sm"
                                                   data-method="delete"
                                                   data-token="{{ csrf_token() }}"
                                                   data-confirm="Esta seguro que desea eliminar a comprobante de la UF {{ $comprobante->UnidadFuncional->codigo }}?">
                                                    Eliminar
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    @endforeach
                </div>

                <a href="{{ url($base_url . 'exportar/comprobantes') }}">Exportar</a> |
                <a href="{{ url($base_url . 'exportar/comprobantes-zip') }}">Descargar ZIP</a>
            @else
                No hay comprobantes
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/comprobantes/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/comprobantes/delete-link.js') }}"></script>
    <script>
        $( function() {
            $( ".accordion" ).accordion({
                heightStyle: "content",
                collapsible: true
            });
        } );
    </script>
@stop