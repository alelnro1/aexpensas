@extends('layouts.app')

@section('site-name', 'Nuevo comprobante')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Comprobante</div>

    <div class="panel-body">
        <form action="{{ url( $base_url . 'comprobantes') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Importe -->
            <div class="form-group{{ $errors->has('importe') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Importe</label>

                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" name="importe" value="{{ old('importe') }}">
                    </div>

                    @if ($errors->has('importe'))
                        <span class="help-block">
                            <strong>{{ $errors->first('importe') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Codigo de Operación -->
            <div class="form-group{{ $errors->has('codigo_operacion') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Código de Operación</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="codigo_operacion" value="{{ old('codigo_operacion') }}">

                    @if ($errors->has('codigo_operacion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo_operacion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
                
            <!-- Medio de Pago -->
            <div class="form-group{{ $errors->has('medio_de_pago_id') ? ' has-error' : '' }}">
                <label for="medio_de_pago_id" class="control-label col-md-4">Medio de Pago</label>

                <div class="col-md-6">
                    <select name="medio_de_pago_id" class="form-control">
                        <option value="0">Seleccionar...</option>
                        @foreach($medios_de_pagos as $medio_de_pago)
                            <option value="{{ $medio_de_pago->id }}">{{ $medio_de_pago->descripcion }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- Fecha -->
            <div class="form-group{{ $errors->has('fecha_transaccion') ? ' has-error' : '' }}">
                  <label for="fecha_transaccion" class="control-label col-md-4">Fecha de Transacción</label>

                  <div class="col-md-6">
                      <div class="input-group">
                          <input type="text" class="form-control date" id="datetimepicker1" name="fecha_transaccion" value="{{ old('fecha_transaccion') }}" autocomplete="off" readonly />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>

                      @if ($errors->has('fecha_transaccion'))
                          <span class="help-block">
                            <strong>{{ $errors->first('fecha_transaccion') }}</strong>
                        </span>
                      @endif
                  </div>
            </div>

            <!-- Unidades Funcionales -->
            <div class="form-group{{ $errors->has('unidad_funcional_id') ? ' has-error' : '' }}">
                <label for="unidad_funcional_id" class="control-label col-md-4">Unidad Funcional</label>

                <div class="col-md-6">
                    <select name="unidad_funcional_id" class="form-control">
                        <option value="">Seleccionar...</option>

                        @foreach($unidades_funcionales as $unidad_funcional)
                            <option value="{{ $unidad_funcional->id }}" @if(old('unidad_funcional_id') == $unidad_funcional->id) selected @endif>
                                {{ $unidad_funcional->codigo }} |
                                {{ $unidad_funcional->piso }}°{{ $unidad_funcional->dpto }}
                                |
                                {{ $unidad_funcional->apellido }}
                                {{ $unidad_funcional->nombre }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('unidad_funcional_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('unidad_funcional_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Crear
                    </button>
                </div>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/comprobantes/create.js') }}"></script>
@stop