@extends('layouts.app')
@section('site-name', 'Viendo a proveedor')

@section('styles')
    <link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Exportar Comprobantes a un archivo ZIP
    </div>

    <div class="panel-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                Verifique que todos los campos estén completos y que la diferencia entre
                fechas sea como <strong>máximo</strong> de <strong>1 año</strong>
            </div>
        @endif

        <form action="{{ url($base_url . 'exportar/comprobantes-zip') }}" method="POST">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-lg-3  col-md-3 col-sm-3 ">
                    <label>Desde</label>
                    <div class="input-group date datetimepicker1">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        <input type="text" class="form-control" name="fecha_desde" id="fecha_desde"
                               @if (old('fecha_desde'))
                               value="{{ old('fecha_desde') }}"
                               @elseif (isset($fecha_desde))
                               value="{{ $fecha_desde }}"
                               @else
                               value="{{ date("Y/m/d", strtotime("-1 month")) }}"
                               @endif
                               autocomplete="off"/>
                    </div>
                </div>
                <div class="col-lg-3  col-md-3 col-sm-3 ">
                    <label>Hasta</label>
                    <div class="input-group date datetimepicker1">
                                <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        <input type="text" class="form-control" name="fecha_hasta" id="fecha_hasta"
                               @if (old('fecha_hasta'))
                               value="{{ old('fecha_hasta') }}"
                               @elseif (isset($fecha_hasta))
                               value="{{ $fecha_hasta }}"
                               @else
                               value="{{ date("Y/m/d", strtotime("now")) }}"
                               @endif
                               autocomplete="off"/>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-3 col-sm-3  col-md-3">
                    <label>&nbsp;</label>
                    <div>
                        <input type="submit" class="btn btn-default" value="Generar ZIP">
                    </div>
                </div>
            </div>

        </form>

        @if(Session::has('zip'))
            <div class="alert alert-success">
                Haga click <a href="{{ url('../../' . Session::get('zip')) }}" target="_blank">aquí</a> para descargar el archivo ZIP.
            </div>
        @endif

        @if(Session::has('sin_recibos'))
            <div class="alert alert-danger">
                <div>No hay recibos en el período indicado.</div>
            </div>
        @endif
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('js/gastos/multiselect.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#rubros, #tipos_expensas, #tipos_gastos, #proveedores').multiselect({
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Buscar...',
                allSelectedText: 'Todos seleccionados...',
                numberDisplayed: 0,

                nonSelectedText: 'Seleccione...',
                maxHeight: 220,
                nSelectedText: 'seleccionados'
            });

            $('#fecha_desde').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy/mm/dd'
            });

            $('#fecha_hasta').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy/mm/dd'
            });

            $('#form-reporte').slideDown(500);
        });
    </script>
@stop