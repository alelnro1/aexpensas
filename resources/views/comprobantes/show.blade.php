@extends('layouts.app')

@section('site-name', 'Viendo a comprobante')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <style>
        table {
            font-size: 11px !important;
        }
    </style>
@stop

@section('content')
    <div class="panel-heading">
        Comprobante</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('comprobante_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('comprobante_actualizado') }}
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="col-lg-6 col-xs-12">
                    <fieldset>
                        <legend>Datos Comprobante</legend>

                        <table class="table table-striped table-responsive">
                            <tr>
                                <td><strong>Unidad Funcional</strong></td>
                                <td>{{ $comprobante->UnidadFuncional->nombre }} {{ $comprobante->UnidadFuncional->apellido }}</td>
                            </tr>

                            <tr>
                                <td><strong>Descripción</strong></td>
                                <td>{{ $comprobante->descripcion }}</td>
                            </tr>

                            <tr>
                                <td><strong>Estado</strong></td>
                                <td>{{ $comprobante->estado->descripcion }}</td>
                            </tr>

                            <tr>
                                <td><strong>Fecha de Transacción</strong></td>
                                <td>{{ date('d/m/Y', strtotime($comprobante->fecha_transaccion)) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Medio de Pago</strong></td>
                                <td>{{ $comprobante->MedioDePago->descripcion }}</td>
                            </tr>

                            <tr>
                                <td><strong>Importe</strong></td>
                                <td>${{ number_format($comprobante->importe, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Archivo</strong></td>
                                <td><a href="{{ url('/' . $comprobante->archivo) }}" target="_blank">Ver...</a></td>
                            </tr>

                            <tr>
                                <td><strong>Fecha de Creación</strong></td>
                                <td>{{ $comprobante->created_at }}</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>

                <div class="col-lg-6 col-xs-12">
                    <fieldset>
                        <legend>Saldos</legend>

                        <table class="table table-striped table-responsive">
                            <tr>
                                <td><strong>Termino</strong></td>
                                <td>${{ number_format($comprobante->termino, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Deuda</strong></td>
                                <td>${{ number_format($comprobante->deuda, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Interes</strong></td>
                                <td>${{ number_format($comprobante->interes, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Gastos Particulares</strong></td>
                                <td>${{ number_format($comprobante->gastos_particulares, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Futuros</strong></td>
                                <td>${{ number_format($comprobante->futuros, 2) }}</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>

        @if(!$comprobante->liquidacion_id)
            @if($comprobante->estado->descripcion == "Pendiente")
                @if(Auth::user()->esPropietario())
                    <div>
                        <a href="{{ url( $base_url . 'comprobantes/' . $comprobante->id . '/edit') }}" class="btn btn-default btn-sm">Editar</a>
                    </div>
                @endif
            @endif

            @if(Auth::user()->esAdmin())
                @if($comprobante->estado->descripcion == "Pendiente")
                    <div class="form-group">
                        <div class="left col-md-10">
                            <button type="submit" class="btn btn-danger right" id="rechazar-comprobante" data-op="rechazar" data-comprobante-id="{{ $comprobante->id }}">
                                <i class="fa fa-btn fa-remove"></i>&nbsp;Rechazar
                            </button>
                        </div>

                        <div class="right">
                            <button type="submit" class="btn btn-success left" id="aceptar-comprobante" data-op="aceptar" data-comprobante-id="{{ $comprobante->id }}">
                                <i class="fa fa-btn fa-check"></i>&nbsp;Aprobar
                            </button>
                        </div>
                    </div>
                @else
                    <div class="left col-md-10">
                        <button type="submit" class="btn btn-warning right" id="rechazar-comprobante" data-op="pendiente" data-comprobante-id="{{ $comprobante->id }}">
                            <i class="fa fa-btn fa-stop"></i>&nbsp;Pendiente
                        </button>
                    </div>
                @endif
            @endif
        @endif
    </div>
@stop

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        $(document).ready(function() {
            // La operacion puede ser aprobar o desaprobar
            $('#aceptar-comprobante, #rechazar-comprobante').on('click', function(){
                var operacion = $(this).data('op'),
                        op_msg = operacion,
                        comprobante_id = $(this).data('comprobante-id');

                // Modifico el mensaje para poner en pendiente
                if (operacion == "pendiente")
                    op_msg = 'poner pendiente';

                if (confirm('Está seguro que desea ' + op_msg + ' el comprobante?')) {
                    url = '/admin/comprobantes/' + comprobante_id + '/' + operacion;

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: { 'id' : comprobante_id},
                        dataType: 'json',
                        success: function(data){
                            alert(data.msg);
                            window.location.href = '/admin/comprobantes'
                        }
                    });
                }
            });
        });
    </script>
@stop
