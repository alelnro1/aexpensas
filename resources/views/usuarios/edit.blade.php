@extends('layouts.app')

@section('site-name', 'Editar usuario')

@section('content')
    <div class="panel-heading">Usuario</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST"
              action="{{ url( $base_url . 'usuario-admin/' . $usuario->id) }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}

        <!-- Nombre -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="{{ $usuario->nombre }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ $usuario->email }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Admin  -->
            <div class="form-group{{ $errors->has('administrador_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Administrador</label>
                <div class="col-md-6">
                    <select class="form-control" name="administrador_id">
                        <option value=""></option>
                        @foreach($administradores as $administrador)
                                <option @if($usuario->administrador_id == $administrador->id) selected @endif value="{{$administrador->id}}">{{$administrador->nombre}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('administrador_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('administrador_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Contraseña -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Contraseña</label>

                <div class="col-md-6">
                    <input type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <!-- Confirmar Contraseña -->
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Confirmar Contraseña</label>

                <div class="col-md-6">
                    <input type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/usuarios/edit.js') }}"></script>
@stop