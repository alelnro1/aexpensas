@extends('layouts.app')

@section('site-name', 'Viendo a usuario')

@section('content')
    <div class="panel-heading">
        Usuario con nombre <b><i>{{ $usuario->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('usuario_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $usuario->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $usuario->email }}</td>
            </tr>


            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $usuario->created_at }}</td>
            </tr>
            <tr>
                <td><strong>Administración</strong></td>
                <td>{{ $usuario->Administrador->nombre }}</td>
            </tr>
        </table>

        <div>
             <a href="/usuarios/{{ $usuario->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
