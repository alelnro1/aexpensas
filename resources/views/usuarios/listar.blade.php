@extends('layouts.app')

@section('site-name', 'Listando usuarios')

@section('content')
    <div class="panel-heading">
        Usuarios @if(isset($texto_unidad_funcional)){{$texto_unidad_funcional}}@endif
        @if (Auth::user()->esSuperAdmin())
            <div style="float:right;">
                <a href="{{ url( $base_url . 'usuario-admin/create') }}" class="btn btn-default btn-xs">Nuevo
                    Usuario Admin</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('usuario_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('usuario_eliminado') }}
                </div>
            @endif
            @if(Session::has('usuario_creado'))
                <div class="alert alert-success">
                    {{ Session::get('usuario_creado') }}
                </div>
            @endif

            @if (count($usuarios) > 0)
                <table class="table table-striped task-table" id="usuarios" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        @if(!Auth::user()->esSuperAdmin())
                            <th>Teléfono</th>
                            <th>
                                Unidades Funcionales
                                <table>
                                    <tr>
                                        <th>Código - Piso - Dpto</th>
                                    </tr>
                                </table>
                            </th>
                        @endif
                        @if (Auth::user()->esSuperAdmin())
                            <th>Administración</th>
                            <th></th>
                        @endif
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($usuarios as $usuario)
                        <tr>
                            <td>{{ $usuario->nombre }}</td>
                            <td>{{ $usuario->email }}</td>

                            @if(!Auth::user()->esSuperAdmin())
                                <td>{{ $usuario->telefono }}</td>
                                <td>
                                    <table>
                                        <tbody>
                                        @foreach($usuario->UnidadesFuncionales as $unidad_funcional)
                                            <tr>
                                                <td>
                                                    <a href="{{ url( $base_url . 'unidades-funcionales/' . $unidad_funcional->id) }}">
                                                        {{ $unidad_funcional->codigo }}
                                                    </a> -
                                                </td>
                                                <td>{{ $unidad_funcional->piso }} -</td>
                                                <td>{{ $unidad_funcional->dpto }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            @endif

                            @if (Auth::user()->esSuperAdmin())
                                <td>
                                    {{$usuario->Administrador->nombre}}
                                </td>
                                <td>

                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'usuario-admin/' . $usuario->id) }}">Ver</a>
                                    |
                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'usuario-admin/' . $usuario->id) }}/edit">Editar</a>
                                    |
                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'usuario-admin/' . $usuario->id) }}"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Esta seguro que desea eliminar al usuario: {{ $usuario->email }}?">
                                        Eliminar
                                    </a>

                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay usuarios
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/usuarios/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/usuarios/delete-link.js') }}"></script>

@stop