@extends('layouts.app')

@section('site-name', 'Listando pedidos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Pedidos

        <div style="float:right;">
            <a class="btn btn-xs btn-default"  href="{{ url( $base_url . 'pedidos/create') }}">Nuevo Pedido</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('pedido_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_eliminado') }}
            </div>
        @endif

        @if(Session::has('pedido_creado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_creado') }}
            </div>
        @endif

        @if(Session::has('pedido_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_actualizado') }}
            </div>
        @endif

        @if (Auth::user()->esPropietario())
            <fieldset>
                <legend>Mis Pedidos</legend>

                @if (count($mis_pedidos) > 0)
                    <table class="table table-striped task-table" id="pedidos" width="100%">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>fecha_completa</th>
                            <th>Título</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($mis_pedidos as $pedido)
                            <tr>
                                <td>{{ $pedido->created_at }}</td>
                                <td>{{ $pedido->titulo }}</td>
                                <td>{{ $pedido->EstadoPedido->descripcion }}</td>
                                <td>{{ date('d/m/Y', strtotime($pedido->created_at)) }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}">Ver</a>
                                    @if ($pedido->EstadoPedido->descripcion == 'Pendiente')
                                        |
                                        <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}/edit">Editar</a>
                                        |
                                        <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}"
                                           data-method="delete"
                                           data-token="{{ csrf_token() }}"
                                           data-confirm="Eliminar pedido?">
                                            Eliminar
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No tiene pedidos realizados
                @endif
            </fieldset>

            <fieldset>
                <legend>Pedidos del Consorcio</legend>

                @if (count($pedidos_otras_ufs) > 0)
                    <table class="table table-striped task-table" id="otros_pedidos" width="100%">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>fecha_completa</th>
                            <th>Título</th>
                            <th>UF</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($pedidos_otras_ufs as $pedido)
                            <tr>
                                <td>{{ $pedido->created_at }}</td>
                                <td>{{ $pedido->titulo }}</td>
                                <td>{{ $pedido->UnidadFuncional->codigo }}|{{ $pedido->UnidadFuncional->piso }}-{{ $pedido->UnidadFuncional->dpto }}</td>
                                <td>{{ $pedido->EstadoPedido->descripcion }}</td>
                                <td>{{ date('d/m/Y', strtotime($pedido->created_at)) }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}">Ver</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No hay pedidos del consorcio
                @endif
            </fieldset>
        @elseif (Auth::user()->esAdmin())
            <fieldset>
                <legend>Pedidos</legend>

                @if (count($pedidos) > 0)
                    <table class="table table-striped task-table" id="otros_pedidos" width="100%">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>fecha_completa</th>
                            <th>Título</th>
                            <th>UF</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($pedidos as $pedido)
                            <tr>
                                <td>{{ $pedido->created_at }}</td>
                                <td>{{ $pedido->titulo }}</td>
                                <td>{{ $pedido->UnidadFuncional->codigo }}|{{ $pedido->UnidadFuncional->piso }}-{{ $pedido->UnidadFuncional->dpto }}</td>
                                <td>{{ $pedido->EstadoPedido->descripcion }}</td>
                                <td>{{ date('d/m/Y', strtotime($pedido->created_at)) }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}">Ver</a>
                                    @if ($pedido->EstadoPedido->descripcion != 'Finalizado')|
                                    <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}/edit">Editar</a>
                                    |
                                    <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/' . $pedido['id'])}}"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Eliminar pedido?">
                                        Eliminar
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No hay pedidos del consorcio
                @endif
            </fieldset>
        @endif


        {{--@if (count($pedidos) > 0)
            <table class="table table-striped task-table" id="pedidos" width="100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>fecha_completa</th>
                        <th>Título</th>
                        <th>Descripción</th>
                        <th>UF</th>
                        <th>Estado</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido->created_at }}</td>
                        <td>{{ $pedido->titulo }}</td>
                        <td>
                        @if(strlen( $pedido->descripcion) > 15){{substr ($pedido->descripcion, 0 , 15  ) . '...'}}
                        @else {{$pedido->descripcion}}
                        @endif
                        <td>{{ $pedido->UnidadFuncional->codigo }}|{{ $pedido->UnidadFuncional->piso }}-{{ $pedido->UnidadFuncional->dpto }}</td>
                        <td>
                            @foreach($estados_pedidos as $estado_pedido)
                                @if($pedido->estado_id  == $estado_pedido->id)

                                    {{
                                    $estado = $estado_pedido->descripcion,
                                    $estado_pedido->descripcion
                                    }}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($pedido->created_at)) }} {{ dump($pedido) }}
                            @foreach ($pedido->UnidadFuncional->Usuarios as $usuario)
                                @if ($usuario->id == Auth::user()->id)
                                    {{ dump($usuario) }}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/'.$pedido['id'])}}/edit">Editar</a>
                            @if($estado == 'Pendiente')|

                            <a class="btn btn-xs btn-default" href="{{url( $base_url . 'pedidos/'.$pedido['id'])}}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Eliminar pedido?">
                                Eliminar
                            </a>
                             @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay pedidos
        @endif--}}
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/pedidos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pedidos/delete-link.js') }}"></script>
@stop