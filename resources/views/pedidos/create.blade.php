@extends('layouts.app')

@section('site-name', 'Nuevo pedido')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Pedido</div>

    <div class="panel-body">
        <form action="{{ url( $base_url . 'pedidos') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Titulo -->
            <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Título</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="titulo" value="{{ old('titulo') }}">

                    @if ($errors->has('titulo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('titulo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Unidad Funcional  -->
            <div class="form-group{{ $errors->has('unidad_funcional_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Unidad Funcional</label>

                <div class="col-md-6">
                    <select class="form-control" name="unidad_funcional_id">
                        <option value="">Seleccionar Unidad Funcional...</option>
                        @foreach($unidades_funcionales as $unidad_funcional)
                            <option value="{{ $unidad_funcional->id }}">{{ $unidad_funcional->codigo .' | '.$unidad_funcional->piso .'-'.$unidad_funcional->dpto.' | '.$unidad_funcional->nombre .' '.$unidad_funcional->apellido  }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('unidad_funcional_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('unidad_funcional_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}" id="contenedor_archivo">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo" id="archivo">
                    <leyend>El archivo debe ser menor a 2MB</leyend>


                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/pedidos/create.js') }}"></script>
@stop