@extends('layouts.app')

@section('site-name', 'Editar pedido')

@section('content')
    <div class="panel-heading">Pedido</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{url($base_url . 'pedidos/'.  $pedido->id) }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}

        <!-- Titulo -->
            <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Título</label>

                <div class="col-md-6">
                    <input maxlength="20" type="text" class="form-control" name="titulo" value="{{ $pedido->titulo }}">

                    @if ($errors->has('titulo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('titulo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea  name="descripcion" rows="3" class="form-control col-md-6">{{ $pedido->descripcion  }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Unidad Funcional  -->
            <div class="form-group{{ $errors->has('unidad_funcional_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Unidad Funcional</label>

                <div class="col-md-6">
                    <select  class="form-control" name="unidad_funcional_id">
                        <option value="">Seleccionar Unidad Funcional...</option>
                        @foreach($unidades_funcionales as $unidad_funcional)
                            <option value="{{ $unidad_funcional->id }}" @if($pedido->unidad_funcional_id == $unidad_funcional->id) selected @endif>{{ $unidad_funcional->codigo .' | '.$unidad_funcional->piso .'-'.$unidad_funcional->dpto.' | '.$unidad_funcional->nombre .' '.$unidad_funcional->apellido  }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('unidad_funcional_id'))
                        <span class="help-block">
                                <strong>{{ $errors->first('unidad_funcional_id') }}</strong>
                            </span>
                    @endif
                </div>
            </div>


            @if(Auth::user()->esAdmin())
                <div class="form-group{{ $errors->has('estado_descripcion') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Estado</label>

                    <div class="col-md-6">
                        <select  class="form-control" name="estado_descripcion">
                            @foreach($estados_pedidos as $estado_pedido)
                                <option value="{{$estado_pedido->descripcion}}" @if($pedido->estado_id == $estado_pedido->id) selected @endif>{{ $estado_pedido->descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        @endif

        <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}" id="contenedor_archivo">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-4">
                    <input type="file" class="form-control" name="archivo" id="archivo">
                    <span>El archivo debe ser menor a 2MB</span>


                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>

                @if($pedido->archivo)
                    <div class="col-md-2">
                        <br>
                        <a href="/{{ $pedido->archivo }}" download>Ver Archivo Actual</a>
                    </div>
                @endif
            </div>

            <hr>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/pedidos/edit.js') }}"></script>
@stop