@extends('layouts.app')

@section('site-name', 'Viendo a pedido')

@section('content')
    <div class="panel-heading">
        Pedido <strong><i>{{ $pedido->titulo }}</i></strong>
    </div>

    <div class="panel-body">
        @if(Session::has('pedido_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 100%;">
            <tr>
                <td><strong>Título</strong></td>
                <td>{{ $pedido->titulo }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $pedido->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $pedido->EstadoPedido->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Unidad Funcional</strong></td>
                <td>{{ $pedido->UnidadFuncional->codigo }}|{{ $pedido->UnidadFuncional->piso }}-{{ $pedido->UnidadFuncional->dpto }}</td>
            </tr>

            @if($pedido->archivo)
                <tr>
                    <td><strong>Adjunto</strong></td>
                    <td><a href="/{{ $pedido->archivo }}" download>Ver</a></td>
                </tr>
            @endif

            <tr>
                <td><strong>Fecha Creación</strong></td>
                <td>{{ date("d/m/Y", strtotime($pedido->created_at)) }}</td>
            </tr>
        </table>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
