@extends('layouts.app')

@section('content')
    <div>
        <div class="row">
            <div class="visible-xs" style="margin-top:20%"></div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Bienvenido Al Consorcio {{session('CONSORCIO')->nombre}}</div>

                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                @if(session('CONSORCIO')->archivo != null)
                                    <img src="{{ session('CONSORCIO')->archivo}}" style="max-width:100%;"
                                         class="img-thumbnail" alt="">
                                @else
                                    <img src="/images/no_imagen_edificio.jpg" style="max-width:100%;"
                                         class="img-thumbnail" alt="">
                                @endif
                                <hr>
                                <strong>Cantidad De Unidades Funcionales:</strong>{{ ' '.count($unidadesFuncionales)}}<br>
                                @if (session('CONSORCIO')->email_encargado != "")
                                    <strong>Email del Encargado: {{ session('CONSORCIO')->email_encargado }}</strong><br>
                                @endif

                                @if (session('CONSORCIO')->tel_encargado != "")
                                    <strong>Teléfono del Encargado: {{ session('CONSORCIO')->tel_encargado }}</strong><br>
                                @endif


                                @if (count($encuestas_activas) > 0)
                                    <hr>
                                    <div style="font-weight: bold;">
                                        {!! csrf_field() !!}

                                        @foreach ($encuestas_activas as $encuesta)
                                            @if (count($unidades_funcionales_habilitadas) > 0)
                                                <div class="encuesta" data-id="{{ $encuesta->id }}"
                                                     style="text-align: left;">
                                                    <span>{{ $encuesta->pregunta }}</span>

                                                    <div class="respuestas" data-encuesta-id="{{ $encuesta->id }}">
                                                        @foreach ($encuesta->Respuestas as $respuesta)
                                                            <br><input type="radio" name="respuesta"
                                                                       value="{{ $respuesta->id }}">
                                                            &nbsp;{{ $respuesta->respuesta }}
                                                        @endforeach

                                                        <br>
                                                        <select name="unidad_funcional" id="unidad_funcional">
                                                            @foreach ($unidades_funcionales_habilitadas as $uf)
                                                                <option value="{{ $uf->id }}">{{ $uf->codigo }}
                                                                    |{{ $uf->piso }}°{{ $uf->dpto }}</option>
                                                            @endforeach
                                                        </select>
                                                        <button class="votar_encuesta"
                                                                data-encuesta-id="{{ $encuesta->id }}">Votar!
                                                        </button>
                                                    </div>
                                                </div>
                                            @else
                                                <div>{{ $encuesta->pregunta }}</div>
                                                <div><a href="{{ url('admin/encuestas/' . $encuesta->id) }}">Ver Resultados...</a></div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>

                            <div class="col-lg-8 "><br>
                                <div style="width: 100%">
                                    <iframe width="100%" height="400"
                                            src="http://www.mapsdirections.info/crear-un-mapa-de-google/map.php?width=100%&height=400&hl=en&q={{session('CONSORCIO')->domicilio}}+({{session('CONSORCIO')->nombre}})&ie=UTF8&t=h&z=19&iwloc=A&output=embed"
                                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a
                                                href="http://www.mapsdirections.info/crear-un-mapa-de-google/">Crear
                                            Mapa Google</a> by <a href="http://www.mapsdirections.info/"></a></iframe>
                                </div>
                                <br/>
                            </div>

                        </div>

                        @if (count($novedades) > 0)
                            <div class="row">
                                <div class="col-xs-12">


                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th><h3>Novedades</h3></th>
                                            </tr>
                                            @foreach ($novedades as $novedad)
                                                <tr>
                                                    <td>
                                                        <strong style="color:#068192">{{ $novedad->nombre }}</strong><br>
                                                        {{ $novedad->descripcion }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">

                            <div class="col-lg-7 col-xs-12">
                                <h3>Unidades Con Deuda</h3>
                                @if(count($unidades_funcionales_deudoras)>0)
                                    <div class="table-responsive">
                                        <table class="table">
                                            <?php $i = 1?>
                                            <tr>
                                                <th>Unidad Funcional</th>
                                                <th>Propietario</th>
                                                <th style="text-align: right">Deuda</th>
                                            </tr>

                                            @foreach($unidades_funcionales_deudoras as $unidad_funcional)
                                                <tr>
                                                    <td>{{$unidad_funcional->codigo .' | '. $unidad_funcional->piso.'-'.$unidad_funcional->dpto}}</td>
                                                    <td>{{ucfirst($unidad_funcional->nombre) . " ". ucfirst($unidad_funcional->apellido)}}</td>
                                                    <td style="text-align: right">{{'$ ' . number_format($unidad_funcional->deuda,2)}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                @else
                                    <strong>No existen unidades funcionales con deuda</strong>
                                @endif
                            </div>

                            <div class="col-lg-5 col-xs-12">
                                @if (isset($grafico_totales_por_tipo))
                                    <div id="grafico-totales"></div>

                                    {!! $grafico_totales_por_tipo->render('PieChart', 'TotalesPorTipo', 'grafico-totales') !!}
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            @if(count($sums)>0)

                                <div class="col-lg-8">

                                    <div class="table-responsive">
                                        <table class="table">
                                            <?php $i = 1?>
                                            <tr>
                                                <th><h3>Espacios Comunes</h3></th>
                                                <th></th>
                                                <th></th>
                                            </tr>

                                            @foreach($sums as $sum)
                                                <tr>
                                                    <td class="">
                                                        <h4 class=" visible-xs">{{ucfirst($sum->nombre)}}</h4>
                                                        <img src="
                                                    @if($sum->archivo != null)
                                                        {{url($sum->archivo )}}
                                                        @else
                                                                '/images/no_imagen.jpg'
                                                                @endif
                                                                " style="max-height:100px;" class="img-thumbnail"
                                                             alt="">
                                                    </td>

                                                    <td class="hidden-xs">{{ucfirst($sum->nombre)}}</td>
                                                    <td>
                                                        @if(count($sum->Turnos) > 0)
                                                            <a class="btn btn-xs btn-default"
                                                               href="{{ url($base_url . 'sums/' . $sum['id'] . '/reservar') }}">Reservar</a>
                                                            <a class="btn btn-xs btn-default"
                                                               href="{{ url($base_url . 'sums/' . $sum['id']) }}">Ver
                                                                Reservas</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                            @endif
                            <div class="col-lg-4">

                                @if(count($liquidaciones)>0)
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th><h3>Ultimas Expensas</h3></th>
                                            </tr>

                                            @foreach($liquidaciones as $liquidacion)
                                                <tr>
                                                    <td>
                                                        <a href="{{$liquidacion->pdf}}" target="_blank">
                                                            {{ucfirst($liquidacion->nombre_periodo)}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                @else
                                    <span>Aún no hay expensas</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $('.votar_encuesta').on('click', function (e) {
            if (confirm('Confirma el voto?')) {
                var encuesta_id = $(this).data('encuesta-id'),
                        respuesta_id = $(this).closest('.respuestas').find('input[name=respuesta]:checked').val(),
                        uf_id = $(this).parent().find('select#unidad_funcional option:selected').val();

                if (respuesta_id != undefined) {
                    $.ajax({
                        url: 'admin/votar',
                        data: {
                            '_token': $('input[name=_token]').val(),
                            'encuesta_id': encuesta_id,
                            'respuesta_id': respuesta_id,
                            'uf_id': uf_id
                        },
                        type: 'POST',
                        dataType: 'json',
                        success: function (data) {
                            alert(data.respuesta);

                            if (data.success == true) {
                                window.location.href = 'admin';
                            }
                        }
                    })
                } else {
                    alert('Seleccione una opción');
                }
            } else {
                e.preventDefault();
            }
        });
    </script>
@stop