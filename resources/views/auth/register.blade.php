@extends('front-end.app')

@section('content')
    <div style="clear:both;"></div>

    <section id="main" class="container 75%">
        <header style="margin-top:260px">
            <h2>Regístrese</h2>
        </header>
        @if(Session::has('unidad_rechazada'))
            <div style="color:red; font-size:14px; font-family: 'Arial Black';text-align: center;">
                {{ Session::get('unidad_rechazada') }}
            </div>
        @endif
        <div class="box">
            <form class="form-horizontal" role="form" method="POST" action="/usuarios/store">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} " class="row uniform">

                    <div class="12u">
                        <span><strong>Nombre Completo</strong></span>
                        <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" placeholder="Nombre" >

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"class="row uniform">

                    <div class="12u">
                        <span><strong>Email</strong></span>
                        <input id="email" type="email" class="form-control" name="email"
                               @if (isset($email) && !empty($email))
                                       value="{{ $email }}"
                               @else
                                       value="{{ old('email') }}"
                               @endif
                               placeholder="Email" >

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" class="row uniform">

                    <div class="12u">
                        <span><strong>Contraseña</strong></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" >

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" class="row uniform">

                    <div class="12u">
                        <span><strong>Repita la Contraseña</strong></span>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }} " class="row uniform">

                    <div class="12u">
                        <span><strong>Teléfono</strong></span>
                        <input id="telefono" type="text" class="form-control" name="telefono" value="{{ old('telefono') }}" placeholder="Teléfono" >

                        @if ($errors->has('telefono'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('codigo_asociacion') ? ' has-error' : '' }} " class="row uniform">

                    <div class="12u">
                        <span><strong>Código de Asociación</strong></span>
                        <input id="codigo_asociacion" type="text" class="form-control" name="codigo_asociacion"
                               @if (isset($codigo) && !empty($codigo))
                                    value="{{ $codigo }}"
                               @else
                                    value="{{ old('codigo_asociacion') }}"
                               @endif
                               placeholder="Código Unidad Funcional" >

                        @if ($errors->has('codigo_asociacion'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('codigo_asociacion') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <br>

                <div class="row uniform">
                    <div class="12u">
                        <ul class="actions align-center">
                            <li><input type="submit" value="Registrar" /></li>
                        </ul>
                    </div>
                </div>

            </form>
        </div>
        </div>

    </section>

    @include('front-end.footer-contacto')
@stop