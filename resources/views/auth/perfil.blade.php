@extends('layouts.app')

@section('site-name', 'Mi Perfil')

@section('content')
    <div class="panel-heading">
        Mi Perfil
    </div>

    <div class="panel-body">
        @if(Session::has('perfil_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('perfil_actualizado') }}
            </div>
        @endif

        @if($usuario->archivo != "")
            <div class="form-group text-center">
                <img src="/{{ $usuario->archivo }}" width="250">
            </div>
        @endif

        <fieldset>

            <legend>Datos</legend>
                <table class="table table-striped task-table">
                    <tr>
                        <td><strong>Nombre</strong></td>
                        <td>{{ $usuario->nombre }}</td>
                    </tr>

                    <tr>
                        <td><strong>Apellido</strong></td>
                        <td>{{ $usuario->apellido }}</td>
                    </tr>

                    <tr>
                        <td><strong>Email</strong></td>
                        <td>{{ $usuario->email }}</td>
                    </tr>

                    <tr>
                        <td><strong>Telefono</strong></td>
                        <td>{{ $usuario->telefono }}</td>
                    </tr>

                    <tr>
                        <td><strong>Fecha de alta</strong></td>
                        <td>{{ date("d/m/Y", strtotime($usuario->created_at)) }}</td>
                    </tr>
                </table>
        </fieldset>

        <fieldset>
            @if(Auth::user()->esPropietario())
                <legend>Unidades Funcionales</legend>
                <table class="table table-striped task-table">
                    <tr>
                        <th>Unidad Funcional</th>
                        <th>Propietario</th>
                        <th>Piso</th>
                        <th>Dpto</th>
                    </tr>

                    @foreach($usuario->UnidadesFuncionales as $unidad_funcional)
                        <tr>
                            <td>{{ $unidad_funcional->codigo }}</td>
                            <td>{{ $unidad_funcional->nombre }} {{ $unidad_funcional->apellido }}</td>
                            <td>{{ $unidad_funcional->piso }}</td>
                            <td>{{ $unidad_funcional->dpto }}</td>
                        </tr>
                    @endforeach
                </table>
            @elseif(Auth::user()->esAdmin())
                <legend>Consorcios</legend>
                <table class="table table-striped task-table">
                    <tr>
                        <th>Nombre</th>
                        <th>Domicilio</th>
                        <th></th>
                    </tr>

                    @foreach($usuario->administrador->consorcios as $consorcio)
                        <tr>
                            <td>{{ $consorcio->nombre }}</td>
                            <td>{{ $consorcio->domicilio }}</td>
                            <td><a class="btn btn-xs btn-default" href="{{ url( $base_url . 'consorcios/' . $consorcio->id) }}">Ver</a></td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </fieldset>
    </div>
@stop

