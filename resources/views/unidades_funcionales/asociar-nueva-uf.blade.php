@extends('layouts.app')

@section('site-name', 'Asociar Unidad Funcional')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Asociar Unidad Funcional</div>

    <div class="panel-body">
        @if(Session::has('uf_asociada'))
            <div class="alert alert-success">
                {{ Session::get('uf_asociada') }}
            </div>
        @endif

        <form action="{{ url( $base_url . 'asociar-nueva-uf') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Codigo de Asociación -->
            <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                <label class="col-md-5 control-label">Código de Asociación de la Unidad Funcional</label>

                <div class="col-md-7">
                    <input type="text" class="form-control" name="codigo" value="{{ old('codigo') }}">

                    @if ($errors->has('codigo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Asociar
                    </button>
                </div>
            </div>
        </form>

    </div>
@stop