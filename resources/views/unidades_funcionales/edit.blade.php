@extends('layouts.app')

@section('site-name', 'Editar unidadfuncional')

@section('content')
    <div class="panel-heading">Unidad Funcional</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url( $base_url . 'unidades-funcionales/' . $unidad_funcional->id) }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}
        <!-- Codigo -->
            <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Unidad Funcional</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="codigo" value="{{ $unidad_funcional->codigo }}">

                    @if ($errors->has('codigo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Piso -->
            <div class="form-group{{ $errors->has('piso') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Piso</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="piso" value="{{ $unidad_funcional->piso }}">

                    @if ($errors->has('piso'))
                        <span class="help-block">
                            <strong>{{ $errors->first('piso') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Dpto -->
            <div class="form-group{{ $errors->has('dpto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Dpto</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="dpto" value="{{ $unidad_funcional->dpto }}">

                    @if ($errors->has('dpto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dpto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Nombre  -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $unidad_funcional->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Apellido -->
            <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Apellido</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="apellido" value="{{ $unidad_funcional->apellido }}">

                    @if ($errors->has('apellido'))
                        <span class="help-block">
                            <strong>{{ $errors->first('apellido') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Telefono -->
            <div class="form-group{{ $errors->has('dpto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ $unidad_funcional->telefono }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ $unidad_funcional->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Metros Cubiertos -->
            <div class="form-group{{ $errors->has('mtrs_cubiertos') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Metros Cubiertos</label>

                <div class="col-md-6">
                    <input type="number" class="form-control" name="mtrs_cubiertos" value="{{ $unidad_funcional->mtrs_cubiertos }}">

                    @if ($errors->has('mtrs_cubiertos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mtrs_cubiertos') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Metros Descubiertos -->
            <div class="form-group{{ $errors->has('mtrs_descubiertos') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Metros Descubiertos</label>

                <div class="col-md-6">
                    <input type="number" class="form-control" name="mtrs_descubiertos" value="{{ $unidad_funcional->mtrs_descubiertos }}">

                    @if ($errors->has('mtrs_descubiertos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mtrs_descubiertos') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- porcentaje a -->
            <div class="form-group{{ $errors->has('porcentaje_a') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">% A</label>

                <div class="col-md-6">
                    <input type="number" min="0" max="100" step="0.01" class="form-control" name="porcentaje_a" value="{{ $unidad_funcional->porcentaje_a}}">

                    @if ($errors->has('porcentaje_a'))
                        <span class="help-block">
                            <strong>{{ $errors->first('porcentaje_a') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- porcentaje b -->
            <div class="form-group{{ $errors->has('porcentaje_b') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">% B</label>

                <div class="col-md-6">
                    <input type="number" min="0" max="100" step="0.01" class="form-control" name="porcentaje_b" value="{{ $unidad_funcional->porcentaje_b}}">

                    @if ($errors->has('porcentaje_b'))
                        <span class="help-block">
                        <strong>{{ $errors->first('porcentaje_b') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        @if ($unidad_funcional->Consorcio->cochera_complementaria)
            <!-- Cochera -->
                <div class="form-group{{ $errors->has('cochera') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Cochera</label>
                    <div class="col-md-6">
                        <select class="form-control" name="cochera">

                            @if($unidad_funcional->cochera == 1)
                                <option selected value="1">Si</option>
                                <option value="0">No</option>
                            @else
                                <option value="1">Si</option>
                                <option selected value="0">No</option>
                            @endif

                        </select>
                        @if ($errors->has('cochera'))
                            <span class="help-block">
                            <strong>{{ $errors->first('cochera') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            @endif

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('unidades_funcionales') }}"></script>
@stop