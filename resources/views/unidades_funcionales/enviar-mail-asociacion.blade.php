@extends('layouts.app')

@section('site-name', 'Asociar Unidad Funcional')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Enviar Mail Asociativo para <strong><i><u>UF {{ $uf->codigo }}|{{ $uf->piso }}-{{ $uf->dpto }}</u></i></strong>
    </div>

    <div class="panel-body">
        <form action="{{ url( $base_url . 'enviar-mail-asociacion/' . $id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Codigo de Asociación -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label">Email</label>

                <div class="col-md-7">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Enviar código
                    </button>
                </div>
            </div>
        </form>

    </div>
@stop