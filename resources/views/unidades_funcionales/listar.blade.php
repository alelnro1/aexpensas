@extends('layouts.app')

@section('site-name', 'Unidades Funcionales')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Unidades Funcionales

        <div style="float:right;">
            @if(Auth::user()->esAdmin())
                <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'unidades-funcionales/create') }}">Nueva
                    Unidad Funcional</a>
            @elseif (Auth::user()->esPropietario())
                <a class="btn btn-xs btn-default" href="{{ url( $base_url . 'nueva-unidad-funcional') }}">Nueva Unidad
                    Funcional</a>
            @endif
        </div>
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('unidadfuncional_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('unidadfuncional_eliminado') }}
                </div>
            @endif

            @if(Session::has('unidadfuncional_creado'))
                <div class="alert alert-success">
                    {{ Session::get('unidadfuncional_creado') }}
                </div>
            @endif

            @if(Session::has('unidadfuncional_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('unidadfuncional_actualizado') }}
                </div>
            @endif

            @if(Session::has('email_asociativo_enviado'))
                <div class="alert alert-success">
                    {{ Session::get('email_asociativo_enviado') }}
                </div>
            @endif

            @if(Session::has('total_porcentaje_a_' . $CONSORCIO))
                <div class="alert alert-danger">
                    La suma de los porcentajes de las UF debe ser 100%. Actualmente son: <br>
                    A: {{ number_format(Session::get('total_porcentaje_a_' . $CONSORCIO), 2) }}%<br>
                    B: {{ number_format(Session::get('total_porcentaje_b_' . $CONSORCIO), 2) }}%
                </div>
            @endif


            @if (count($unidades_funcionales) > 0)
                <table class="table table-striped task-table display responsive" id="unidadesfuncionales" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>codigo</th>
                        <th>Unidad Funcional</th>
                        <th>Propietario</th>
                        <th>% A</th>
                        <th>% B</th>
                        <th>Código Asociación</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($unidades_funcionales as $unidad_funcional)
                        <tr>
                            <td>{{ $unidad_funcional->codigo }}</td>
                            <td>
                                {{ $unidad_funcional->codigo }} |
                                {{ $unidad_funcional->piso }} - {{ $unidad_funcional->dpto }}
                            </td>
                            <td>{{ $unidad_funcional->nombre ." ". $unidad_funcional->apellido}}</td>
                            <td class="text-right">{{ number_format($unidad_funcional->porcentaje_a, 2) }}%</td>
                            <td class="text-right">{{ number_format($unidad_funcional->porcentaje_b, 2) }}%</td>
                            <td>{{ $unidad_funcional->codigo_asociacion }}</td>
                            <td>
                                <a class="btn btn-xs btn-default"
                                   href="{{ url( $base_url . 'unidades-funcionales/' . $unidad_funcional['id']) }}">Ver</a>
                                |
                                <a class="btn btn-xs btn-default"
                                   href="{{ url( $base_url . 'movimientos/' . $unidad_funcional['id']) }}">Movimientos</a>
                                |
                                @if(Auth::user()->esAdmin())
                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'unidades-funcionales/' . $unidad_funcional['id']) }}"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Esta seguro que desea eliminar a consorcio con codigo: {{ $unidad_funcional->codigo }}?">
                                        Eliminar
                                    </a>
                                @elseif(Auth::user()->esPropietario())
                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'desasociar-unidad-funcional/' . $unidad_funcional['id']) }}"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Esta seguro que desea desasociar a consorcio con codigo: {{ $unidad_funcional->codigo }}?">
                                        Desasociar
                                    </a>
                                @endif

                                @if(Auth::user()->esAdmin())

                                    |

                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'unidades-funcionales/' . $unidad_funcional->id . '/edit') }}">Editar</a>

                                    |

                                    <a class="btn btn-xs btn-default"
                                       href="{{ url( $base_url . 'listado_usuarios_segun_uf/' . $unidad_funcional->id . '/' . $unidad_funcional->codigo . ' | '
                            .$unidad_funcional->piso . '-' . $unidad_funcional->dpto ) }}">Usuarios</a>
                                    |
                                    <a href="{{ url($base_url . 'enviar-mail-asociacion/' . $unidad_funcional->id) }}" class="btn btn-xs btn-default">
                                        Enviar Mail
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay unidades funcionales
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/unidades_funcionales/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/unidades_funcionales/delete-link.js') }}"></script>
@stop