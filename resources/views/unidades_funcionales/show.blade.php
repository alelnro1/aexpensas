@extends('layouts.app')

@section('site-name', 'Viendo a unidadfuncional')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <style>
        table {
            font-size: 11px !important;
        }
    </style>
@stop

@section('content')
    <div class="panel-heading">
        Unidad funcional con Codigo <b><i>{{ $unidad_funcional->codigo }}</i></b>
    </div>

    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('unidadfuncional_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('unidadfuncional_actualizado') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <fieldset>
                        <legend>Datos de UF</legend>

                        <table class="table table-striped">
                            <tr>
                                <td><strong>Unidad Funcional</strong></td>
                                <td>{{ $unidad_funcional->codigo }}</td>
                            </tr>

                            <tr>
                                <td><strong>Descripción</strong></td>
                                <td>{{ $unidad_funcional->descripcion }}</td>
                            </tr>
                            <tr>
                                <td><strong>Piso</strong></td>
                                <td>{{ $unidad_funcional->piso }}</td>
                            </tr>

                            <tr>
                                <td><strong>Dpto</strong></td>
                                <td>{{ $unidad_funcional->dpto }}</td>
                            </tr>

                            <tr>
                                <td><strong>Código Asociación Usuario </strong></td>
                                <td>{{ $unidad_funcional->codigo_asociacion }}</td>
                            </tr>

                            @if ($unidad_funcional->Consorcio->cochera_complementaria)
                                <tr>
                                    <td><strong>Tiene Cochera</strong></td>
                                    <td>
                                        @if ($unidad_funcional->cochera == true)
                                            Sí
                                        @else
                                            No
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </fieldset>
                </div>

                <div class="col-lg-6 col-xs-12">
                    <fieldset>
                        <legend>Saldos</legend>

                        <table class="table table-striped task-table">
                            <tr>
                                <td><strong>Término</strong></td>
                                <td>${{ number_format($unidad_funcional->termino, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Deuda</strong></td>
                                <td>${{ number_format($unidad_funcional->deuda, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Interés</strong></td>
                                <td>${{ number_format($unidad_funcional->interes, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Gastos Particulares</strong></td>
                                <td>${{ number_format($unidad_funcional->gastos_particulares, 2) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Futuros</strong></td>
                                <td>${{ number_format($unidad_funcional->futuros, 2) }}</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <fieldset>
                        <legend>Contacto Principal</legend>

                        <table class="table table-striped">
                            <tr>
                                <td><strong>Nombre y Apellido</strong></td>
                                <td>{{ $unidad_funcional->nombre }} {{ $unidad_funcional->apellido }}</td>
                            </tr>

                            <tr>
                                <td><strong>Teléfono</strong></td>
                                <td>{{ $unidad_funcional->telefono }}</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>

                <div class="col-lg-4 col-xs-12">
                    <fieldset>
                        <legend>Porcentajes</legend>

                        <table class="table table-striped task-table">
                            <tr>
                                <td><strong>% A</strong></td>
                                <td>{{ number_format($unidad_funcional->porcentaje_a, 2) }}%</td>
                            </tr>

                            <tr>
                                <td><strong>% B</strong></td>
                                <td>{{ number_format($unidad_funcional->porcentaje_b, 2) }}%</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>

                <div class="col-lg-4 col-xs-12">
                    <fieldset>
                        <legend>Cobertura</legend>

                        <table class="table table-striped task-table" >
                            <tr>
                                <td><strong>Metros Cubiertos</strong></td>
                                <td>{{$unidad_funcional->mtrs_cubiertos }}</td>
                            </tr>

                            <tr>
                                <td><strong>Metros Descubiertos</strong></td>
                                <td>{{ $unidad_funcional->mtrs_descubiertos }}</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
            <hr>

            <div class="col-xs-12">
                @include('movimientos.listado', ['movimientos' => $movimientos])
            </div>

            @if (Auth::user()->esAdmin())
                <div>
                    <a class="btn btn-xs btn-default" href="/admin/unidades-funcionales/{{ $unidad_funcional->id }}/edit">Editar</a>
                </div>
            @endif
        </div>
    </div>
@stop

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#movimientos').DataTable({
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: -1 },
                    {
                        "targets": [ 0 ],
                        "visible": false
                    }
                ],
                order: [[0, 'desc']],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ movimientos filtrados",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ movimientos",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ movimientos)",
                }
            });
        });
    </script>
@stop