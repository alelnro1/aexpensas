@extends('layouts.app')

@section('site-name', 'Listando mailings')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Mails

        <div style="float:right;">
            <a href="{{ url('admin/mails/create') }}" class="btn btn-default btn-xs">Nuevo Mail</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('mailing_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('mailing_eliminado') }}
            </div>
        @endif

        @if(Session::has('mailing_creado'))
            <div class="alert alert-success">
                {{ Session::get('mailing_creado') }}
            </div>
        @endif

        @if (count($mails) > 0)
            <table class="table table-striped task-table" id="mailings">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Asunto</th>
                    <th>Enviado</th>
                    <th>Fecha de Creado</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($mails as $mail)
                    <tr>
                        <td>{{ $mail->asunto }}</td>
                        <td>
                            @if($mail->enviado)
                                Enviado
                            @else
                                No enviado
                            @endif
                        </td>
                        <td>{{ date('d/m/Y', strtotime($mail->created_at)) }}</td>

                        <td>
                            <a href="{{ url('mails/' . $mail['id']) }}">Ver</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay mails
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/mailings/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/mailings/delete-link.js') }}"></script>
@stop