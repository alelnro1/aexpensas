@extends('layouts.app')

@section('site-name', 'Nuevo mailing')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('css/file-upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('css/file-upload/jquery.fileupload-ui.css') }}">
    <style>
        .bar {
            height: 18px;
            background: green;
        }
    </style>
@stop

@section('content')
    <div class="panel-heading">Nuevo Mail</div>

    <div class="panel-body">
        <form action="{{ url('admin/mails') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Asunto -->
            <div class="form-group{{ $errors->has('asunto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Asunto</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="asunto" value="{{ old('asunto') }}" required>

                    @if ($errors->has('asunto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asunto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Cuerpo -->
            <div class="form-group {{ $errors->has('cuerpo') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Cuerpo</label>

                <div class="col-md-6">
                    <textarea name="cuerpo" rows="3" class="form-control col-md-6" required>{{ old('cuerpo') }}</textarea>

                    @if ($errors->has('cuerpo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuerpo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('destinatarios') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Destinatarios</label>

                <div class="col-md-6">
                    <select name="destinatarios[]" id="destinatarios" multiple="multiple" required>
                        @foreach ($unidades_funcionales as $uf)
                            <option value="{{ $uf->id }}"
                                    @if (old('destinatarios'))
                                    @foreach (old('destinatarios') as $old_dest)
                                    @if ($old_dest == $uf->id) selected @endif
                                    @endforeach
                                    @endif
                            >{{ $uf->codigo }}|{{ $uf->piso }}-{{ $uf->dpto }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('destinatarios'))
                        <span class="help-block">
                            <strong>{{ $errors->first('destinatarios') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Adjuntos -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Adjuntos</label>

                <div class="col-md-6">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Agregar adjunto...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="adjuntos[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Aca se guardan temporalmente las urls de los adjuntos --}}
            <input type="hidden" name="urls_archivos" id="urls_archivos" value="">

            {{-- Aca se dice si es borrador o no --}}
            <input type="hidden" name="borrador" id="borrador" value="">

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary" id="enviar">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        &nbsp;Enviar Email
                    </button>

                    <button type="submit" class="btn btn-primary" id="guardar">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        &nbsp;Guardar como Borrador
                    </button>
                </div>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="/js/gastos/multiselect.js"></script>
    <script src="{{ asset('js/file-upload/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/file-upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/file-upload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/file-upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/mails/subir-archivos.js') }}"></script>


    <!--script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}"></--script-->
    <script type="text/javascript" src="{{ asset('/js/mails/create.js') }}"></script>
    <script>
        $('#destinatarios').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Buscar...',
            allSelectedText: 'Todos seleccionados...',
            numberDisplayed: 1,

            nonSelectedText: 'Seleccione los destinatarios...',
            selectAllText: 'Seleccionar todos',
            maxHeight: 220
        });

        $('#enviar').on('click', function(e) {
            e.preventDefault();

            $('#borrador').val('0');

            $('form').submit();
        });

        $('#guardar').on('click', function(e) {
            e.preventDefault();

            $('#borrador').val('1');

            $('form').submit();
        });

        $(function () {
            $('#destinatarios').multiselect();
        });
    </script>
@stop