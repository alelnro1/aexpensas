<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<table>
    <tr>
        <th>Fecha</th>
        <th>Cuota</th>
        <th>Importe</th>
        <th>Rubro</th>
        <th>Descrip</th>
        <th>Tipo Gasto</th>
        <th>Tipo Expensa</th>
        <th>Proveedor</th>
    </tr>

    @foreach ($gastos as $gasto)
        @foreach ($gasto->Cuotas as $cuota)
            <tr>
                <td>{{ date("d/m/Y", strtotime($cuota->fecha)) }}</td>
                <td>{{ $cuota->descripcion }}</td>
                <td>{{ $cuota->importe }}</td>
                <td>{{ $gasto->Rubro->descripcion }}</td>
                <td>{{ $gasto->descripcion }}</td>
                <td>{{ $gasto->TipoGasto->descripcion }}</td>
                <td>{{ $gasto->TipoExpensa->descripcion }}</td>

                @if ($gasto->Proveedor)
                    <td>{{ $gasto->Proveedor->nombre_razon_social }}</td>
                @endif
            </tr>
        @endforeach
    @endforeach
</table>

</body>
</html>