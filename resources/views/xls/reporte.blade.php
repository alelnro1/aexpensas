<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table>
    <thead>
    <tr>
        <th></th>
        @foreach($liquidaciones as $liquidacion)
            <th>{{ $liquidacion->nombre_periodo }}</th>
        @endforeach
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Saldo Inicial</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->saldo_anterior, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>Ingreso</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->ingresos_totales, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td colspan="3"><strong>Egresos</strong></td>
    </tr>

    <tr>
        <td>A</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->egresos_tipo_a, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>B</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->egresos_tipo_b, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>C</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->egresos_tipo_c, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>Particular</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->egresos_tipo_particular, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>Cuota Extra</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->egresos_cuota_extra_asignacion, 2) }}</td>
        @endforeach
    </tr>


    <tr>
        <td>Total</td>
        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->total_egresos, 2) }}</td>
        @endforeach
    </tr>

    <tr>
        <td>Saldo</td>

        @foreach ($liquidaciones as $liquidacion)
            <td>{{ number_format($liquidacion->saldo_estado_financiero, 2) }}</td>
        @endforeach
    </tr>
    </tbody>
</table>

</body>
</html>