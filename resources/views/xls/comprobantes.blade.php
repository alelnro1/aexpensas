<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<table>
    <tr>
        <td>Numerador</td>
        <td>Cod Op</td>
        <td>UF</td>
        <td>Descrip</td>
        <td>Importe</td>
        <td>Medio de Pago</td>
        <td>Estado</td>
        <td>Fecha</td>
    </tr>
    @foreach ($comprobantes as $comprobante)
        <tr>
            <td>{{ $comprobante->numerador }}</td>
            <td>{{ $comprobante->codigo_operacion }}</td>
            <td>
                {{ $comprobante->UnidadFuncional->codigo }} |
                {{ $comprobante->UnidadFuncional->piso }} - {{ $comprobante->UnidadFuncional->dpto }}
            </td>
            <td>{{ $comprobante->descripcion }}</td>
            <td>{{ $comprobante->importe }}</td>
            <td>{{ $comprobante->MedioDePago->descripcion }}</td>
            <td>{{ $comprobante->estado->descripcion }}</td>
            <td>{{ date('d/m/Y', strtotime($comprobante->fecha_transaccion)) }}</td>
        </tr>
    @endforeach

</table>

</body>
</html>