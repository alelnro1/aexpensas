@extends('layouts.app')

@section('site-name', 'Listando empleados')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Empleados

        <div style="float:right;">
            <a href=" {{ url( $base_url . 'empleados/create')}}" class="btn btn-xs btn-default" >Nuevo Empleado</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('empleado_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('empleado_eliminado') }}
            </div>
        @endif

        @if(Session::has('empleado_creado'))
            <div class="alert alert-success">
                {{ Session::get('empleado_creado') }}
            </div>
        @endif

        @if(Session::has('empleado_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('empleado_actualizado') }}
            </div>
        @endif

        @if (count($empleados) > 0)
            <table class="table table-striped task-table" id="empleados" style="width:100%">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Fecha Inicio</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($empleados as $empleado)
                    <tr>
                        <td>{{ $empleado->nombre }}</td>
                        <td>{{ $empleado->cargo }}</td>
                        <td>{{ date('Y/m/d', strtotime($empleado->fecha)) }}</td>
                        <td>{{ $empleado->email }}</td>
                        <td>{{ $empleado->telefono }}</td>

                        <td>
                            <a href=" {{ url( $base_url . 'empleados/' . $empleado['id']) }}"
                               class="btn btn-default btn-sm">Ver</a>

                            <a href="{{ url( $base_url . 'empleados/' . $empleado['id'] .'/edit')}}"
                               class="btn btn-default btn-sm">Editar</a>
                            <a href="{{ url( $base_url . 'empleados/baja/' . $empleado['id'] )}}"
                               class="btn btn-default btn-sm">
                                @if($empleado->baja)
                                    Alta
                                @else
                                    Baja
                                @endif
                            </a>

                            <a href="{{ url( $base_url .'empleados/'. $empleado['id']) }}"
                               class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a empleado con nombre {{ $empleado->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay empleados
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/empleados/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/empleados/delete-link.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
