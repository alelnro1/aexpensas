@extends('layouts.app')

@section('site-name', 'Viendo a empleado')

@section('content')
    <div class="panel-heading">
        Empleado con nombre <b><i>{{ $empleado->nombre }}</i></b>
    </div>

    <div class="panel-body">

        @if(isset($empleado->archivo) && $empleado->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $empleado->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <table class="table table-striped task-table" style="margin-bottom: 20px;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $empleado->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $empleado->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Legajo</strong></td>
                <td>{{ $empleado->legajo }}</td>
            </tr>
            <tr>
                <td><strong>Cuil</strong></td>
                <td>{{ $empleado->cuil }}</td>
            </tr>
            <tr>
                <td><strong>Sector</strong></td>
                <td>{{ $empleado->sector }}</td>
            </tr>
            <tr>
                <td><strong>Cargo</strong></td>
                <td>{{ $empleado->cargo }}</td>
            </tr>
            <tr>
                <td><strong>Categoria</strong></td>
                <td>{{ $empleado->categoria }}</td>
            </tr>
            <tr>
                <td><strong>Fecha Ingreso</strong></td>
                <td>{{ date('Y/m/d', strtotime($empleado->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $empleado->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $empleado->email }}</td>
            </tr>

            <tr>
                <td><strong>Teléfono</strong></td>
                <td>{{ $empleado->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Fecha De Ingreso</strong></td>
                <td>{{ $empleado->fecha }}</td>
            </tr>
        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/admin/empleados/{{ $empleado->id }}/edit" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
