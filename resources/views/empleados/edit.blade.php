@extends('layouts.app')

@section('site-name', 'Editar empleado')

@section('content')
    <div class="panel-heading">Empleado</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url($base_url .'empleados/' . $empleado->id) }}"
              enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}

        <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $empleado->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3"
                              class="form-control col-md-6">{{ $empleado->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Legajo -->
            <div class="form-group{{ $errors->has('legajo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Legajo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="legajo" value="{{ $empleado->legajo }}"  placeholder="Escriba el legajo">

                    @if ($errors->has('legajo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('legajo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Sector -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Sector</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="sector" value="{{ $empleado->sector }}">

                    @if ($errors->has('sector'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sector') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Cuil -->
            <div class="form-group{{ $errors->has('cuil') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cuil</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuil" value="{{ $empleado->cuil }}">

                    @if ($errors->has('cuil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuil') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Cargo -->
            <div class="form-group{{ $errors->has('cargo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cargo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cargo" value="{{ $empleado->cargo }}">

                    @if ($errors->has('cargo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cargo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Categoria -->
            <div class="form-group{{ $errors->has('categoria') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Categoria</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="categoria" value="{{ $empleado->categoria }}">

                    @if ($errors->has('categoria'))
                        <span class="help-block">
                            <strong>{{ $errors->first('categoria') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ $empleado->email }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono -->
            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ $empleado->telefono }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Fecha -->
            <div class="form-group{{ $errors->has('fecha') ? ' has-error' : '' }}">
                <label for="fecha" class="control-label col-md-4">Fecha Ingreso</label>

                <div class="col-md-6">
                    <div class="input-group date" id="datetimepicker1">
                        <input type="text" class="form-control" name="fecha"
                               value="{{ date("Y/m/d", strtotime($empleado->fecha)) }}" autocomplete="off" readonly/>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <br>
            </div>

            <!-- Domicilio -->
            <div class="form-group{{ $errors->has('domicilio') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio" value="{{ $empleado->domicilio }}"
                           id="domicilio" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAjTpj9h5ANX5iTQIKxkAhI-zcoPxl8GtY"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/empleados/edit.js') }}"></script>
@stop
