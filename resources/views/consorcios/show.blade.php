@extends('layouts.app')

@section('site-name', 'Viendo a consorcio')

@section('content')
    <div class="panel-heading">
        Consorcio con nombre <b><i>{{ $consorcio->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('consorcio_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('consorcio_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" >
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $consorcio->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $consorcio->email }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $consorcio->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Teléfono del Encargado</strong></td>
                <td>{{ $consorcio->tel_encargado }}</td>
            </tr>

            <tr>
                <td><strong>Email del Encargado</strong></td>
                <td>{{ $consorcio->email_encargado }}</td>
            </tr>

            <tr>
                <td><strong>Redondeo a UF</strong></td>
                <td>
                    @if($consorcio->redondeo == 0)
                        No
                    @else
                        Si
                    @endif
                </td>
            </tr>

            <tr>
                <td><strong>Tiene Cochera Complementaria</strong></td>
                <td>
                    @if($consorcio->cochera_complementaria == 0)
                        No
                    @else
                        Si
                    @endif
                </td>
            </tr>

            <tr>
                <td><strong>Expensa fija</strong></td>
                <td>@if($consorcio->expensa_fija == 1) Si @else No @endif </td>
            </tr>
            @if($consorcio->expensa_fija == 1)
                <tr>
                    <td><strong>Monto Fijo A</strong></td>
                    <td>{{ $consorcio->monto_fijo_a }}</td>
                </tr>
                <tr>
                    <td><strong>Monto Fijo B</strong></td>
                    <td>{{ $consorcio->monto_fijo_b }}</td>
                </tr>
                <tr>
                    <td><strong>Monto Fijo C</strong></td>
                    <td>{{ $consorcio->monto_fijo_c }}</td>
                </tr>
            @endif
            <tr>
                <td><strong>Dia Primer Vencimiento</strong></td>
                <td>{{ $consorcio->dia_vto_1 }}</td>
            </tr>
            {{--<tr>
                <td><strong>Dia Segundo Vencimiento</strong></td>
                <td>{{ $consorcio->dia_vto_2 }}</td>
            </tr>--}}
            <tr>
                <td><strong>Interés Primer Vencimiento</strong></td>
                <td>{{ number_format($consorcio->interes_vto_1, 2) }}%</td>
            </tr>
            {{--<tr>
                <td><strong>Interés Segundo Vencimiento</strong></td>
                <td>{{ number_format($consorcio->interes_vto_2, 2) }}%</td>
            </tr>--}}

            <tr>
                <td><strong>Clave SUTERH</strong></td>
                <td>{{ $consorcio->suterh }}</td>
            </tr>

            <tr>
                <td><strong>Código de Pago Electrónico</strong></td>
                <td>{{ $consorcio->cod_pago_electronico }}</td>
            </tr>

            <tr>
                <td><strong>Número de Cuenta</strong></td>
                <td>{{ $consorcio->numero_de_cuenta }}</td>
            </tr>
            <tr>
                <td><strong>CBU</strong></td>
                <td>{{ $consorcio->cbu }}</td>
            </tr>
            <tr>
                <td><strong>CUIT</strong></td>
                <td>{{ $consorcio->cuit }}</td>
            </tr>
            <tr>
                <td><strong>Tipo</strong></td>
                <td>{{ $consorcio->tipo }}</td>
            </tr>
            <tr>
                <td><strong>Banco</strong></td>
                <td>{{ $consorcio->banco }}</td>
            </tr>
            <tr>
                <td><strong>Sucursal</strong></td>
                <td>{{ $consorcio->sucursal }}</td>
            </tr>

            <tr>
                <td><strong>Titular</strong></td>
                <td>{{ $consorcio->titular }}</td>
            </tr>


            <tr>
                <td><strong>Firma</strong></td>
                <td>
                    @if($consorcio->firma != "")
                        <img src="/{{ $consorcio->firma }}" width="70" alt="Firma">
                    @else
                        Sin subir
                    @endif
                </td>
            </tr>

            <tr>
                <td><strong>Nota Expensas</strong></td>
                <td>{!!html_entity_decode($consorcio->nota_expensas)!!}</td>
            </tr>

            @if($consorcio->archivo != "")
                <tr>
                    <td><strong>Archivo</strong></td>
                    <td><img src="/{{ $consorcio->archivo }}" width="70" /></td>
                </tr>
            @endif
        </table>

        <td>
            <a class="btn btn-xs btn-default" href="/admin/consorcios/{{ $consorcio->id }}/edit">Editar</a>
        </td>

    </div>
@stop