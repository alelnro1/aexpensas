@extends('layouts.app')

@section('site-name', 'Editar consorcio')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/editor.css') }}">
@stop

@section('content')
    <div class="panel-heading">Consorcio</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/admin/consorcios/{{ $consorcio->id }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {!! csrf_field() !!}
        @if(Auth::user()->esSuperAdmin())
            <!-- Precio -->
            <div class="form-group{{ $errors->has('precioXuf') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Precio Por Uf</label>

                <div class="col-md-6">
                    <input type="number" step="0.01" class="form-control" name="precioXuf" value="{{ $consorcio->precioXuf }}">

                    @if ($errors->has('precioXuf'))
                        <span class="help-block">
                            <strong>{{ $errors->first('precioXuf') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        @endif 
        <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $consorcio->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ $consorcio->email }}" autocomplete="off">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

        @if($consorcio->archivo != "")

            <!-- Archivo -->
                <div class="form-group">
                    <label for="foto" class="control-label col-md-4">Archivo Actual</label>
                    <div class="col-md-6">
                        <img src="/{{ $consorcio->archivo }}" height="100" />
                    </div>
                </div>
        @endif
        <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Nuevo Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <!-- Domicilio -->
            <div class="form-group{{ $errors->has('domicilio') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio" value="{{ $consorcio->domicilio }}" id="domicilio" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <!-- Número de Cuenta y tipo-->
            <!-- Número de Cuenta -->
            <div class="form-group{{ $errors->has('numero_de_cuenta') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Número de Cuenta</label>

                <div class="col-md-4">
                    <input type="text" class="form-control" name="numero_de_cuenta" value="{{ $consorcio->numero_de_cuenta }}">

                    @if ($errors->has('numero_de_cuenta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('numero_de_cuenta') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="tipo">
                        CA$" y "CC$
                        <option @if('CA$' == $consorcio->tipo ) selected @endif value="CA$">CA$</option>
                        <option @if('CC$' == $consorcio->tipo ) selected @endif value="CC$">CC$</option>
                    </select>
                    @if ($errors->has('tipo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- CBU -->
            <div class="form-group{{ $errors->has('cbu') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">CBU</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cbu" id="cbu" value="{{ $consorcio->cbu }}">

                    @if ($errors->has('cbu'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cbu') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- cuit -->
            <div class="form-group{{ $errors->has('cuit') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">CUIT</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit" id="cuit" value="{{ $consorcio->cuit }}">

                    @if ($errors->has('cuit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Banco -->
            <div class="form-group{{ $errors->has('banco') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Banco</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="banco" value="{{ $consorcio->banco }}">

                    @if ($errors->has('banco'))
                        <span class="help-block">
                            <strong>{{ $errors->first('banco') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- sucursal -->
            <div class="form-group{{ $errors->has('sucursal') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Sucursal</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="sucursal" value="{{ $consorcio->sucursal }}">

                    @if ($errors->has('sucursal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sucursal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Titular -->
            <div class="form-group{{ $errors->has('titular') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Titular</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="titular" value="{{ $consorcio->titular }}">

                    @if ($errors->has('titular'))
                        <span class="help-block">
                            <strong>{{ $errors->first('titular') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Redondeo a UF -->
            <div class="form-group{{ $errors->has('redondeo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Redondeo a UF</label>
                <div class="col-md-6">
                    <select class="form-control" name="redondeo">

                        @if($consorcio->redondeo == 1)
                            <option selected value="1">Si</option>
                            <option  value="0">No</option>
                        @else
                            <option  value="1">Si</option>
                            <option selected value="0">No</option>
                        @endif

                    </select>
                    @if ($errors->has('redondeo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('redondeo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Redondeo a 0 -->
            <div class="form-group{{ $errors->has('redondeo_0') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Redondeo a 0</label>
                <div class="col-md-6">
                    <select class="form-control" name="redondeo_0">

                        @if($consorcio->redondeo_0 == 1)
                            <option selected value="1">Si</option>
                            <option  value="0">No</option>
                        @else
                            <option  value="1">Si</option>
                            <option selected value="0">No</option>
                        @endif

                    </select>
                    @if ($errors->has('redondeo_0'))
                        <span class="help-block">
                            <strong>{{ $errors->first('redondeo_0') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Cochera -->
            <div class="form-group{{ $errors->has('cochera_complementaria') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cochera Complementaria</label>
                <div class="col-md-6">
                    <select class="form-control" name="cochera_complementaria">
                        @if($consorcio->cochera_complementaria == 1)
                            <option selected value="1">Si</option>
                            <option value="0">No</option>
                        @else
                            <option value="1">Si</option>
                            <option selected value="0">No</option>
                        @endif

                    </select>
                    @if ($errors->has('cochera_complementaria'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cochera_complementaria') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- dia_vto_1  -->
            <div class="form-group{{ $errors->has('dia_vto_1') ? ' has-error' : '' }}" >
                <label class="col-md-4 control-label">Fecha Primer Vencimiento</label>

                <div class="col-md-6">
                    <input type="number" class="form-control" name="dia_vto_1" value="{{$consorcio->dia_vto_1}}" >
                    @if ($errors->has('dia_vto_1'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dia_vto_1') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- dia_vto_2  -->
        {{--<div class="form-group{{ $errors->has('dia_vto_2') ? ' has-error' : '' }}" >
            <label class="col-md-4 control-label">Fecha Segundo Vencimiento</label>

            <div class="col-md-6">
                <input type="number" class="form-control" name="dia_vto_2" value="{{$consorcio->dia_vto_2}}">
                @if ($errors->has('dia_vto_2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dia_vto_2') }}</strong>
                    </span>
                @endif
            </div>
        </div>--}}

        <!-- Interes Primer Vencimiento  -->
            <div class="form-group{{ $errors->has('interes_vto_1') ? ' has-error' : '' }}" >
                <label class="col-md-4 control-label">Interés Primer Vencimiento (%)</label>

                <div class="col-md-6">
                    <input type="number" class="form-control" name="interes_vto_1" value="{{$consorcio->interes_vto_1}}">
                    @if ($errors->has('interes_vto_1'))
                        <span class="help-block">
                            <strong>{{ $errors->first('interes_vto_1') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- interes_vto_2  -->
        {{--<div class="form-group{{ $errors->has('interes_vto_2') ? ' has-error' : '' }}" >
            <label class="col-md-4 control-label">Interés Segundo Vencimiento (%)</label>

            <div class="col-md-6">
                <input type="number" class="form-control" name="interes_vto_2" value="{{$consorcio->interes_vto_2}}">
                @if ($errors->has('interes_vto_2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('interes_vto_2') }}</strong>
                    </span>
                @endif
            </div>
        </div>--}}

        <!-- clave suterh  -->
            <div class="form-group{{ $errors->has('suterh') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Clave SUTERH</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="suterh" value="{{$consorcio->suterh}}">
                    @if ($errors->has('suterh'))
                        <span class="help-block">
                        <strong>{{ $errors->first('suterh') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email_encargado') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email Encargado</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="email_encargado" value="{{$consorcio->email_encargado}}">
                    @if ($errors->has('email_encargado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email_encargado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tel_encargado') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono Encargado</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="tel_encargado" value="{{$consorcio->tel_encargado}}">
                    @if ($errors->has('tel_encargado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tel_encargado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Codigo pago electronico  -->
            <div class="form-group{{ $errors->has('cod_pago_electronico') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Código de Pago Electrónico</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cod_pago_electronico" value="{{$consorcio->cod_pago_electronico}}">
                    @if ($errors->has('cod_pago_electronico'))
                        <span class="help-block">
                        <strong>{{ $errors->first('cod_pago_electronico') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <!-- Expensa Fija -->
            <div class="form-group{{ $errors->has('expensa_fija') ? ' has-error' : '' }}" id="contenedor_expensa_fija">
                <label class="col-md-4 control-label">Expensa fija</label>

                <div class="col-md-6">
                    <select class="form-control" name="expensa_fija" id="expensa_fija">
                        <option value="0" @if(0 == $consorcio->expensa_fija) selected @endif >No</option>
                        <option value="1" @if(1 == $consorcio->expensa_fija) selected @endif >Si</option>

                    </select>
                    @if ($errors->has('expensa_fija'))
                        <span class="help-block">
                            <strong>{{ $errors->first('expensa_fija') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div hidden id="contenedor_monto_fijo">
                <!-- Monto Fijo a -->
                <div  class="form-group{{ $errors->has('monto_fijo_a') ? ' has-error' : '' }}" >
                    <label class="col-md-4 control-label">Monto Fijo A</label>

                    <div class="col-md-6">
                        <input type="number" class="form-control" name="monto_fijo_a" value="{{$consorcio->monto_fijo_a}}">
                        @if ($errors->has('monto_fijo_a'))
                            <span class="help-block">
                                <strong>{{ $errors->first('monto_fijo_a') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- Monto Fijo b -->
                <div  class="form-group{{ $errors->has('monto_fijo_b') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Monto Fijo B</label>

                    <div class="col-md-6">
                        <input type="number" class="form-control" name="monto_fijo_b" value="{{$consorcio->monto_fijo_b}}">
                        @if ($errors->has('monto_fijo_b'))
                            <span class="help-block">
                                <strong>{{ $errors->first('monto_fijo_b') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- Monto Fijo c-->
                <div  class="form-group{{ $errors->has('monto_fijo_c') ? ' has-error' : '' }}" >
                    <label class="col-md-4 control-label">Monto Fijo C</label>

                    <div class="col-md-6">
                        <input type="number" class="form-control" name="monto_fijo_c" value="{{$consorcio->monto_fijo_c}}">
                        @if ($errors->has('monto_fijo_c'))
                            <span class="help-block">
                                <strong>{{ $errors->first('monto_fijo_c') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Aplica Interes-->
                <div class="form-group{{ $errors->has('aplica_interes') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label"></label>

                    <div class="col-md-6">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="aplica_interes" value="">
                                Aplica Interés
                            </label>
                        </div>

                        @if ($errors->has('aplica_interes'))
                            <span class="help-block">
                                <strong>{{ $errors->first('aplica_interes') }}</strong>
                            </span>
                        @endif

                        <div class="help-block">
                            Tilde si quiere "eliminar" el interés al calcular los prorrateos
                        </div>
                    </div>
                </div>
            </div>

            <!-- Firma -->
            <div class="form-group {{ $errors->has('firma') ? ' has-error' : '' }}">
                <label for="firma" class="control-label col-md-4">Firma</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="firma" id="firma">

                    @if ($errors->has('firma'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firma') }}</strong>
                        </span>
                    @endif
                    <br>
                    @if ($consorcio->firma != "")
                        <img src="/{{ $consorcio->firma }}" width="75" alt="">
                    @endif
                </div>
            </div>

            <!-- Nota Expensas  -->
            <div class="form-group{{ $errors->has('interes_vnota_expensasto_1') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nota para Expensas</label>

                <div class="col-md-6">
                    <textarea class="form-control jqte-test" name="nota_expensas" placeholder="Ingrese la nota que aparecerá abajo de la expensa">{{ $consorcio->nota_expensas }}</textarea>

                    @if ($errors->has('nota_expensas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nota_expensas') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript"
            src="{{ asset('js/plugins/jquery.mask.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/consorcios/edit.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/editor.js') }}"></script>
    <script>
        $('.jqte-test').jqte();

        // settings of status
        var jqteStatus = true;
        $(".status").click(function()
        {
            jqteStatus = jqteStatus ? false : true;
            $('.jqte-test').jqte({"status" : jqteStatus})
        });
    </script>
@stop