@extends('layouts.app')

@section('site-name', 'Listando consorcios')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Consorcios

        <div style="float:right;">
            <a class="btn btn-xs btn-default" href="/admin/consorcios/create">Nuevo Consorcio</a>
        </div>
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('consorcio_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('consorcio_eliminado') }}
                </div>
            @endif

            @if(Session::has('consorcio_creado'))
                <div class="alert alert-success">
                    {{ Session::get('consorcio_creado') }}
                </div>
            @endif
            @if(Session::has('consorcio_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('consorcio_actualizado') }}
                </div>
            @endif

            @if(Session::has('consorcio_tiene_uf'))
                <div class="alert alert-danger">
                    {{ Session::get('consorcio_tiene_uf') }}
                </div>
            @endif

            @if (count($consorcios) > 0)
                <table class="table table-striped task-table" id="consorcios" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Domicilio</th>
                        <th>Imagen</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($consorcios as $consorcio)
                        <tr>
                            <td>
                                <div>
                                    @if(strlen( $consorcio->nombre) > 15){{substr ($consorcio->nombre, 0 , 15  )}}...
                                    @else {{$consorcio->nombre}}
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div>
                                    @if(strlen( $consorcio->domicilio) > 20){{substr ($consorcio->domicilio, 0 , 20  )}}...
                                    @else {{$consorcio->domicilio}}
                                    @endif
                                </div>
                            <td>
                                @if($consorcio->archivo !="")
                                    <img src="/{{ $consorcio->archivo }}" height="25px" />
                                @endif
                            </td>

                            <td>
                                <a class="btn btn-xs btn-default" href="/admin/consorcios/{{ $consorcio['id'] }}">Ver</a>
                                |
                                <a class="btn btn-xs btn-default" href="/admin/consorcios/{{ $consorcio['id'] }}"
                                   data-method="delete"
                                   data-token="{{ csrf_token() }}"
                                   data-confirm="Esta seguro que desea eliminar a consorcio con nombre {{ $consorcio->nombre }}?">
                                    Eliminar
                                </a>
                                |
                                <a class="btn btn-xs btn-default" href="/admin/consorcios/{{ $consorcio->id }}/edit">Editar</a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay consorcios
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/consorcios/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/consorcios/delete-link.js') }}"></script>
@stop