<fieldset>
    <legend>Movimientos</legend>

    <table class="table table-striped table-hover display responsive" id="movimientos" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Acción</th>
            <th>Liquidación</th>
            <th>Comprobante</th>
            <th>Monto</th>
            <th>Saldo Actual</th>
            <th>Fecha</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($movimientos as $movimiento)
            <tr>
                <td>{{ $movimiento->updated_at }}</td>
                <td>{{ $movimiento->accion }}</td>
                <td>
                    @if ($movimiento->Liquidacion)
                        <a href="/{{ $movimiento->Liquidacion->pdf }}" download>
                            {{ $movimiento->Liquidacion->nombre_periodo }}
                        </a>
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if ($movimiento->Comprobante)
                        <a href="{{ url('/admin/comprobantes/' . $movimiento->Comprobante->id) }}">
                            Ver
                        </a>
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if ($movimiento->Comprobante)
                        ${{ number_format($movimiento->Comprobante->importe, 2) }}
                    @endif
                </td>
                <td>
                    ${{ number_format($movimiento->saldo_nuevo, 2) }}</td>
                <td>{{ date("d/m/Y", strtotime($movimiento->created_at)) }}</td>
            </tr>

            {{-- */
            $i=0;
            /* --}}
        @endforeach
        </tbody>
    </table>
</fieldset>