@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Mis Movimientos
    </div>

    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @include('movimientos.listado')
        </div>
    </div>
@stop

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#movimientos').DataTable({
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: -1 },
                    {
                        "targets": [ 0 ],
                        "visible": false
                    }
                ],
                order: [[0, 'desc']],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ movimientos filtrados",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ movimientos",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ movimientos)",
                }
            });
        });
    </script>
@stop