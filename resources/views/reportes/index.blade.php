@extends('layouts.app')
@section('site-name', 'Viendo a proveedor')

@section('styles')
    <link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Reportes de Balance
    </div>

    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if ($errors->any())
                <div class="alert alert-danger">
                    Verifique que todos los campos estén completos y que la diferencia entre
                    fechas sea como <strong>máximo</strong> de <strong>1 año</strong>
                </div>
            @endif

            <form action="{{ url($base_url . 'reportes') }}" method="POST" id="form-reporte" class="hidden">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-lg-3  col-md-3 col-sm-3 ">
                        <label>Desde</label>
                        <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            <input type="text" class="form-control" name="fecha_desde" id="fecha_desde"
                                   @if (old('fecha_desde'))
                                   value="{{ old('fecha_desde') }}"
                                   @elseif (isset($fecha_desde))
                                   value="{{ $fecha_desde }}"
                                   @else
                                   value="{{ date("Y/m/d", strtotime("-1 year")) }}"
                                   @endif
                                   autocomplete="off"/>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-3 col-sm-3 ">
                        <label>Hasta</label>
                        <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            <input type="text" class="form-control" name="fecha_hasta" id="fecha_hasta"
                                   @if (old('fecha_hasta'))
                                   value="{{ old('fecha_hasta') }}"
                                   @elseif (isset($fecha_hasta))
                                   value="{{ $fecha_hasta }}"
                                   @else
                                   value="{{ date("Y/m/d", strtotime("now")) }}"
                                   @endif
                                   autocomplete="off"/>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-3 col-sm-3">
                        <label>Rubros</label>

                        <div>
                            <select name="rubros[]" id="rubros" multiple="multiple">
                                @foreach ($rubros as $rubro)
                                    <option value="{{ $rubro->id }}"
                                            @if (is_array(old('rubros')) && in_array($rubro->id, old('rubros')))
                                            selected
                                            @elseif (isset($rubros_elegidos) && in_array($rubro->id, $rubros_elegidos))
                                            selected
                                            @elseif (!is_array(old('rubros')) && !isset($rubros_elegidos))
                                            selected
                                            @endif>
                                        {{ $rubro->descripcion }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-3 col-sm-3   checkbox">
                        <label>
                            <p>&nbsp;</p>
                            <input type="checkbox" name="ver_ingresos"
                                   @if (old('ver_ingresos') && old('ver_ingresos') == "on") checked
                                   @elseif (isset($ver_ingresos_elegido) && $ver_ingresos_elegido == "on") checked
                                   @elseif(!old('ver_ingresos') && !isset($ver_ingresos_elegido)) checked
                                    @endif
                            >
                            <b>Ver Ingresos</b>
                        </label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3 col-sm-3  col-md-3">
                        <label>Tipos Expensas</label>
                        <div>
                            <select name="tipos_expensas[]" id="tipos_expensas" multiple="multiple">
                                @foreach ($tipos_expensas as $tipo_expensa)
                                    <option value="{{ $tipo_expensa->id }}"
                                            @if (is_array(old('tipos_expensas')) && in_array($tipo_expensa->id, old('tipos_expensas')))
                                            selected
                                            @elseif (isset($tipos_expensa_elegidos) && in_array($tipo_expensa->id, $tipos_expensa_elegidos))
                                            selected
                                            @elseif (!is_array(old('tipo_expensas')) && !isset($tipos_expensa_elegidos))
                                            selected
                                            @endif>
                                        {{ $tipo_expensa->descripcion }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3  col-md-3">
                        <label>Tipos Gastos</label>

                        <div>
                            <select name="tipos_gastos[]" id="tipos_gastos" multiple="multiple">
                                @foreach ($tipos_gastos as $tipo_gasto)
                                    <option value="{{ $tipo_gasto->id }}"
                                            @if (is_array(old('tipos_gastos')) && in_array($tipo_gasto->id, old('tipos_gastos')))
                                            selected
                                            @elseif (isset($tipos_gastos_elegidos) && in_array($tipo_gasto->id, $tipos_gastos_elegidos))
                                            selected
                                            @elseif (!is_array(old('tipos_gastos')) && !isset($tipos_gastos_elegidos))
                                            selected
                                            @endif
                                    >{{ $tipo_gasto->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3  col-md-3">
                        <label>Proveedor</label>

                        <div>
                            <select name="proveedores[]" id="proveedores" multiple="multiple">
                                @foreach ($proveedores as $proveedor)
                                    <option value="{{ $proveedor->id }}"
                                            @if (is_array(old('proveedores')) && in_array($proveedor->id, old('proveedores')))
                                            selected
                                            @elseif (isset($proveedores_elegidos) && in_array($proveedor->id, $proveedores_elegidos))
                                            selected
                                            @elseif (!is_array(old('proveedores')) && !isset($proveedores_elegidos))
                                            selected
                                            @endif
                                    >{{ $proveedor->nombre_razon_social }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3  col-md-3">
                        <label>&nbsp;</label>
                        <div>
                            <input type="submit" class="btn btn-default" value="Generar">
                        </div>
                    </div>
                </div>

            </form>
            <div style="clear:both;"></div>

            @if (isset($lava))
                    {!! $lava->render('ColumnChart', 'Finances', 'perf_div', ['width' => '100%', 'height' => '100%']) !!}
                <div>
                    <a href="{{ url($base_url . 'reportes/exportar') }}">Exportar</a>
                </div>
            @endif
            <div id="perf_div"></div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('js/gastos/multiselect.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#rubros, #tipos_expensas, #tipos_gastos, #proveedores').multiselect({
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Buscar...',
                allSelectedText: 'Todos seleccionados...',
                numberDisplayed: 0,

                nonSelectedText: 'Seleccione...',
                maxHeight: 220,
                nSelectedText: 'seleccionados'
            });

            $('#fecha_desde').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy/mm/dd'
            });

            $('#fecha_hasta').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy/mm/dd'
            });

            $('#form-reporte').removeClass('hidden');
        });
    </script>
@stop