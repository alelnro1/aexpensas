@extends('layouts.app')

@section('content')
<div class="panel-heading">Nuevo rol </div>
<div class="panel-body">
    <form class="form-horizontal" role="form" method="POST" action="/roles/create">
        {!! csrf_field() !!}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Nombre</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Crear
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
