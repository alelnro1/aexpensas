@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Roles | <a href="{{ url('/roles/nuevo') }}">Nuevo</a> </div>
                <div class="panel-body">
                	@if (count($roles) > 0)
	                	<table class="table table-striped task-table">
		                	<!-- Table Headings -->
		                	<thead>
		                		<th>ID Rol</th>
		                		<th>Nombre</th>
		                	</thead>

		                	<tbody>
		                		@foreach ($roles as $rol)
		                			<tr>
		                				<td class="table-text">
		                					<div>{{ $rol->id }}</div>
		                				</td>
		                				<td>
		                					<div>{{ $rol->nombre }}</div>
		                				</td>
		                				<td>
		                					<a href="{{ url('roles/editar/' . $rol->id) }}">Editar</a>
		                				</td>
		                			</tr>
		                		@endforeach
		                	</tbody>
		                </table>
		            @else
		            	NO HAY ROLES
		            @endif
            </div>
        </div>
    </div>
</div>
@endsection
