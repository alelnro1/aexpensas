@extends('layouts.app')

@section('site-name', 'Solicitud Nueva Funcionalidad')

@section('content')
    <div class="panel-heading">Solicitud Nueva Funcionalidad</div>

    <div class="panel-body">
        @if(Session::has('mejora_solicitada'))
            <div class="alert alert-success">
                Muchas gracias por su solicitud
            </div>
        @endif

        <div class="help-block">
            En AExpensas estamos buscando la mejora continuamente, deseamos que este producto
            siga siendo el mejor del mercado, para eso te proponemos que si considerás que podés ayudar pidiéndonos
            una funcionliadad que no está en el sistema, nos la expliques lo mejor posible así podemos analizarla y
            desarrollarla. Desde ya, te agradecemos muchísimo tu colaboración.
        </div>

        <form action="{{ url( $base_url . 'mejora-continua') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Mail -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Titulo -->
            <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Título</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" required>

                    @if ($errors->has('titulo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('titulo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Funcionalidad Deseada </label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6" required>{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Solicitar!
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop
