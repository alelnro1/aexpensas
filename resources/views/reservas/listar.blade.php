@extends('layouts.app')

@section('site-name', 'Listando reservas')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Reservas
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('reserva_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('reserva_eliminado') }}
                </div>
            @endif

            @if(Session::has('reserva_creado'))
                <div class="alert alert-success">
                    {{ Session::get('reserva_creado') }}
                </div>
            @endif

            @if (count($reservas) > 0)
                <table class="table table-striped task-table" id="reservas" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Sum</th>
                        <th>UF</th>
                        <th>Turno</th>
                        <th>Estado</th>
                        <th>Fecha Solicitud</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($reservas as $reserva)
                        <tr>
                            <td>{{ $reserva->Sum->nombre }}</td>
                            <td>{{ $reserva->UnidadFuncional->codigo }} | {{ $reserva->UnidadFuncional->piso }}°{{ $reserva->UnidadFuncional->dpto }}</td>
                            <td>De {{ $reserva->Turno->hora_inicio }} a {{ $reserva->Turno->hora_fin }}</td>
                            <td>{{ $reserva->EstadoReserva->descripcion }}</td>
                            <td>{{ date('d/m/Y H:i', strtotime($reserva->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay reservas
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/reservas/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/reservas/delete-link.js') }}"></script>
@stop