@extends('layouts.app')

@section('site-name', 'Viendo a reserva')

@section('content')
    <div class="panel-heading">
        Reserva de Sum <b>{{ $reserva->Sum->descripcion }}</b>
    </div>

    <div class="panel-body">
        @if(Session::has('reserva_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('reserva_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 100%;">
            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $reserva->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $reserva->EstadoReserva->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Solicitada</strong></td>
                <td>{{ date('Y/m/d', strtotime($reserva->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Turno Solicitado</strong></td>
                <td>De {{ $reserva->Turno->hora_inicio }} a {{ $reserva->Turno->hora_fin }}</td>
            </tr>
        </table>

        @if (
            Auth::user()->esAdmin() ||
            (Auth::user()->tieneUnidadFuncional($reserva->UnidadFuncional->id) && $reserva->EstadoReserva->descripcion == "Pendiente")
            )
            <div>
                <a href="{{ url($base_url . 'reservas/' . $reserva->id . '/edit')  }}">Editar</a>
            </div>
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
