@extends('layouts.app')

@section('site-name', 'Nuevo reserva')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nueva Reserva</div>

    <div class="panel-body">
        <form action="{{ url($base_url . 'sums/' . $sum->id . '/reservar') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Espacio Común</label>

                <div class="col-md-6">
                    <span>{{ $sum->nombre }}</span>
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Fecha -->
            <div class="form-group{{ $errors->has('fecha') ? ' has-error' : '' }}">
                <label for="fecha" class="control-label col-md-4">Fecha</label>

                <div class="col-md-6">
                    <div class="input-group date" id="datetimepicker1">
                        <input type="text" class="form-control" name="fecha" value="{{ old('fecha') }}" autocomplete="off" readonly />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>

                    @if ($errors->has('fecha'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fecha') }}</strong>
                        </span>
                    @endif
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <!-- Turnos -->
            <div class="form-group{{ $errors->has('turno_id') ? ' has-error' : '' }}">
                <label for="turno_id" class="control-label col-md-4">Turno</label>

                <div class="col-md-6">
                    <select name="turno_id" class="form-control">
                        <option value="">Elija</option>

                        @foreach ($sum->turnos as $turno)
                            <option value="{{ $turno->id }}" @if (old('turno_id') == $turno->id) selected @endif>De {{ $turno->hora_inicio }} a {{ $turno->hora_fin }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('turno_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('turno_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <!-- Unidades Funcionales -->
            <div class="form-group{{ $errors->has('unidad_funcional_id') ? ' has-error' : '' }}">
                <label for="unidad_funcional_id" class="control-label col-md-4">Unidad Funcional</label>

                <div class="col-md-6">
                    <select name="unidad_funcional_id" class="form-control">
                        <option value="">Elija</option>

                        @foreach($unidades_funcionales as $unidad_funcional)
                            <option value="{{ $unidad_funcional->id }}" @if (old('unidad_funcional_id') == $unidad_funcional->id) selected @endif
                            >{{ $unidad_funcional->codigo .' | '.$unidad_funcional->piso .'-'.$unidad_funcional->dpto.' | '.$unidad_funcional->nombre .' '.$unidad_funcional->apellido  }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('unidad_funcional_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('unidad_funcional_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <hr>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i> Solicitar Reserva
                    </button>
                </div>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/reservas/create.js') }}"></script>
@stop