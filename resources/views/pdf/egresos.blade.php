@foreach($rubros as $rubro)
    <div class="text-center background-categoria padding-row break" style="max-height: 18px; padding:0px; margin:0px;">
        <strong>{{ strtoupper($rubro->descripcion) }}</strong>
    </div>
    <fieldset style="border: 0 none; margin: 0; min-width: 0; padding: 0; font-size: 12px;">
        <div style="margin-left: 20px;">
            @foreach($rubro->RubrosHijos as $key => $rubro_hijo)
                @if ($rubro_hijo->descripcion == "Detalle De Sueldos Y Cargas Sociales")
                    @include('pdf.sueldos')
                @else
                    <span>{{ $key+1 }}- {{ $rubro_hijo->descripcion }}</span>
                    <table class="table table-striped task-table"
                           style="margin-bottom: 20px; max-width: 99%; width: 99%;">
                        @if(count($rubro_hijo->Gastos) <= 0)
                            <tr>
                                <td style="text-align:center; max-height: 1px; padding:0px; margin:0px;">No hay gastos
                                    de este tipo
                                </td>
                            </tr>
                        @else
                            <tr>
                                <th class="col-xs-6" style="max-height: 1px; padding:0px; margin:0px;">Descripción</th>
                                <th class="col-xs-1" style="max-height: 1px; padding:0px; margin:0px;">Tipo Expensa</th>
                                <th class="col-xs-2 text-right" style="max-height: 1px; padding:0px; margin:0px;">
                                    Importe
                                </th>
                                <th class="col-xs-3 text-right" style="max-height: 1px; padding:0px 10px; margin:0px;">
                                    Total
                                </th>
                            </tr>
                            @foreach($rubro_hijo->Gastos as $gasto)
                                @foreach($gasto->Cuotas as $cuota)
                                    <tr>
                                        <td class="col-xs-6" style="max-height: 1px; padding:0px 5px; margin:0px;">
                                            {{ $gasto->Proveedor->nombre_razon_social }} -
                                            {{ $gasto->descripcion }}.
                                            @if ($gasto->totalCuotas($gasto->id) > 1)
                                                Cuota: {{ $cuota->descripcion }}.
                                            @endif
                                            @if ($gasto->nro_factura != "")
                                                Factura #{{ $gasto->nro_factura }}
                                            @endif
                                        </td>
                                        <td class="col-xs-1" style="max-height: 1px; padding:0px 5px; margin:0px;">
                                            {{ $gasto->TipoExpensa->descripcion }}
                                        </td>
                                        <td class="text-right col-xs-2"
                                            style="max-height: 1px; padding:0px 5px; margin:0px;">
                                            @if ($gasto->TipoExpensa->descripcion == "Cochera" || $gasto->TipoExpensa->descripcion == "Particular")
                                                ${{ number_format($cuota->importe * $cuota->dameCantidadDeUFsConEseGasto(), 2) }}
                                            @else
                                                ${{ number_format($cuota->importe, 2) }}
                                            @endif
                                        </td>
                                        <td class="col-xs-2" style="max-height: 1px; padding:0px 5px; margin:0px;"></td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <td class="col-xs-6"></td>
                                <td class="col-xs-2"></td>
                                <td class="col-xs-2" style="max-height: 1px; padding:0px 5px; margin:0px;">Subtotal</td>
                                <td class="text-right col-xs-2" style="max-height: 1px; padding:0px 10px; margin:0px;">
                                    ${{ number_format($rubro_hijo->total, 2) }}
                                </td>
                            </tr>
                        @endif
                    </table>
                @endif
            @endforeach
        </div>
        <div class="background-categoria" style="float:right; margin-right: 20px; padding: 5px;">
            Total {{ $rubro->descripcion }}:

            {{-- Muestro el porcentaje que tiene ese rubro contra el total --}}
            @if ($rubro->total == 0)
                0.00%
            @else
                {{ number_format(($rubro->total / $total_gastos) * 100, 2) }}% &nbsp;&nbsp;
            @endif

            ${{ number_format($rubro->total, 2) }}
        </div>
    </fieldset>
@endforeach
