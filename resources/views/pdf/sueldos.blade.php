<span>{{ $key+1 }}- {{ $rubro_hijo->descripcion }}</span><br><br>

@if(count($rubro_hijo->Gastos) <= 0)
    <table class="table table-striped task-table"
           style="margin-bottom: 20px; max-width: 99%; width: 99%;">
        <tr>
            <td style="text-align:center; max-height: 1px; padding:0px; margin:0px;">No hay gastos
                de este tipo
            </td>
        </tr>
    </table>
@else
    @foreach($rubro_hijo->Gastos as $gasto)
        <table class="table table-striped task-table"
               style="margin-bottom: 20px; max-width: 99%; width: 99%;">

            <tr>
                <th class="col-xs-6" style="max-height: 1px; padding:0px; margin:0px;">Descripción</th>

                <th class="col-xs-4 text-right" style="max-height: 1px; padding:0px 10px; margin:0px;">
                    Total
                </th>
            </tr>
            <div class="col-xs-6">
                Empleado: {{ $gasto->Empleado->nombre }}<br>
                Cargo: {{ $gasto->Empleado->cargo }}<br>
            </div>

            <div class="col-xs-6">
                CUIL: {{ $gasto->Empleado->cuil }}<br>
                Categoría: {{ $gasto->Empleado->categoria }}<br>
            </div>
            <div class="col-xs-12">
                &nbsp;
                <br>
            </div>
            @foreach($gasto->DetallesSueldos as $detalle)
                <tr>
                    <td style="max-height: 1px; padding:0px; margin:0px;">
                        {{ $detalle->campo }}.
                    </td>
                    @if($detalle->valor > 0)
                        <td class="text-left"
                            style="max-height: 1px; padding:0px; margin:0px;">
                            ${{ number_format($detalle->valor, 2) }}
                        </td>
                    @else
                        <td class="text-right"
                            style="max-height: 1px; padding:0px; margin:0px;">
                            ${{ number_format($detalle->valor, 2) }}
                        </td>
                    @endif

                </tr>
            @endforeach
            <tr>
                <td class="col-xs-2" style="max-height: 1px; padding:0px 5px; margin:0px;">Subtotal</td>
                <td class="text-right col-xs-2" style="max-height: 1px; padding:0px 10px; margin:0px;">
                    ${{ number_format($gasto->Cuotas->first()->importe, 2) }}
                </td>
            </tr>
        </table>
    @endforeach

@endif
