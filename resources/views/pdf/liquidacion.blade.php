<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        .page-break {
            page-break-after: always;
        }

        .background-categoria {
            background: #d9edf7;
            padding-bottom: 10px;
        }

        .padding-row {
            margin-bottom: 10px;
            padding:8px 0px;
        }

        tr, td, th, tbody, thead, tfoot, legend, div.break, h3 {
            page-break-inside: avoid !important;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .alturas {
            max-height: 1px; padding:0px 5px; margin:0px;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
</head>
<body>

<div class="text-center background-categoria" style="max-height: 20px; padding:0px; margin:0px;">
    <strong>AEXPENSAS&nbsp;</strong>

    Liquidación de Mes: {{ $nombre_periodo }}
</div>


<div class="row padding-row">
    <div class="col-xs-4">
        <strong>ADMINISTRACIÓN</strong> <br>

        Nombre: {{ $consorcio->Administrador->nombre }}<br>
        Domicilio: {{ $consorcio->Administrador->domicilio }}<br>
        Mail: {{ $consorcio->Administrador->email }}<br>
        CUIT: {{ $consorcio->Administrador->cuit }}<br>
        Inscripción R.P.A: {{ $consorcio->Administrador->inscripcion_rpa }}<br>
    </div>

    <div class="col-xs-4">
        <br><br><br>
        Te: {{ $consorcio->Administrador->telefono }}<br>
        Situación Fiscal: {{ $consorcio->Administrador->situacion_fiscal }}<br>
    </div>

    <div class="col-xs-4">
        <strong>CONSORCIO</strong> <br>
        Nombre: {{ $consorcio->nombre }}<br> <br>
        CUIT: {{ $consorcio->cuit }} <br>
        CLAVE SUTERH: {{ $consorcio->suterh }} <br>
    </div>
</div>

{{-- GASTOS --}}
<div class="row padding-row" style="padding:0px; margin:0px;">
    @include('pdf.egresos', ['rubros' => $rubros, 'total_gastos' => $total_gastos])
</div>

{{--<div class="page-break"></div>--}}

{{-- ESTADO FINANCIERO --}}
<div class="row padding-row text-center background-categoria padding-row" style="max-height: 18px; padding:0px; margin:0px;">
        ESTADO FINANCIERO
</div>

@include('liquidaciones.estado-financiero-verificar', [
    'ultima_liquidacion' => $ultima_liquidacion,
    'fondo_operativo' => $fondo_operativo,
    'categorias' => $categorias_financieras
])

{{-- PROVEEDORES --}}
<div>
    <span style="font-size: 14px; font-weight: bold;">REFERENCIA DE PROVEEDORES UTILIZADOS</span>
</div>

@include('pdf.proveedores', ['pdf.proveedores' => $proveedores])

<div>
    {{-- PRORRATEO --}}
    @include('pdf.prorrateos', ['prorrateos' => $prorrateos])
</div>

<div>
    <span style="font-size: 14px; font-weight: bold;">FORMAS DE PAGO: DEPÓSITO O TRANSFERENCIA</span>
</div>

<div>
    Día Primer Vto: {{ $consorcio->dia_vto_1 }} {{-- - Día Segundo Vto: {{ $consorcio->dia_vto_2 }} --}}
</div>

<div class="col-xs-4">
    Titular: {{ $consorcio->titular }}<br>

    CBU: {{ $consorcio->cbu }}<br>
</div>

<div class="col-xs-4">
    Banco: {{ $consorcio->banco }}<br>

    Sucursal: {{ $consorcio->sucursal }}<br>
</div>

<div class="col-xs-4">
    N° de Cuenta: {{ $consorcio->numero_de_cuenta }}<br>

    Código Pago Electrónico: {{ $consorcio->cod_pago_electronico }}<br>
</div>

<div style="clear:both;"></div>

@if ($consorcio->nota_expensas != "")
    <div>
        <strong>Notas</strong>

        <div>
            {!!html_entity_decode($consorcio->nota_expensas)!!}
        </div>
    </div>
@endif

</body>
</html>
