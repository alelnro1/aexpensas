<div class="col-lg-5" style="border: 2px solid #1b6d85;">
    <div class="row" style="border-bottom: 2px solid #1b6d85; padding: 10px;">
            <span style="color: #1b6d85;">
                <strong>
                    COMPROBANTE DE PAGO #{{ $comprobante->numerador }}<br>
                    PROPIEDAD HORIZONTAL Ley N° 13.512 <br><br>
                </strong>
            </span>

        <strong>ADMINISTRACIÓN</strong> <br>
        Nombre: {{ $administrador->nombre }} <br>
        Domicilio: {{ $administrador->domicilio }} <br>
        CUIT: {{ $administrador->cuit }}<br>

        <div class="text-right">RPA: {{ $administrador->inscripcion_rpa }}</div>
    </div>

    <div class="row" style="border-bottom: 2px solid #1b6d85; padding: 10px;">
        <strong>CONSORCIO</strong> <br>
        Propietario: {{ $comprobante->UnidadFuncional->nombre }} {{ $comprobante->UnidadFuncional->apellido }} <br>
        Dirección: {{ $consorcio->domicilio }} <br>
        <div>
            <div class="text-left" style="width: 50%; float: left">
                Dpto: {{ $comprobante->UnidadFuncional->piso }}-{{ $comprobante->UnidadFuncional->dpto }}
            </div>

            <div class="text-right">
                U.F.: {{ $comprobante->UnidadFuncional->codigo }}
            </div>
        </div>
    </div>

    <div class="row" style="padding: 10px;">
        <br>

        <strong>
            Recibí conforme la suma de ${{ number_format($comprobante->importe, 2) }}
            el dia {{ date("d/m/Y", strtotime($comprobante->fecha_transaccion)) }}
        </strong> <br><br>

        <div class="col-xs-5" style="margin-left:-15px;">
            <span class="text-left">
                <small>
                    Forma de Pago:
                    {{ $comprobante->MedioDePago->descripcion }}
                </small>

                <br><br>

                <small>
                    <i>Talón para la administración</i>
                </small>
            </span>
        </div>
    </div>
</div>