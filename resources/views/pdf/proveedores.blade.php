<table class="table table-bordered table-hover" style="font-size: 10px;">
    @if(count($proveedores) >= 1)
        @foreach ($proveedores as $proveedor)
            {{ $proveedor->nombre_razon_social }} (Tél. {{ $proveedor->telefono }}) CUIT ({{ $proveedor->cuit_cuil }})
            @if(end($proveedores) !== $proveedor)
            |
            @endif
        @endforeach
    @else
        No se han utilizado proveedores
    @endif
</table>