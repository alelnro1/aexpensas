<div style="page-break-inside: avoid !important">
    <div class="text-center background-categoria padding-row break">
        ESTADO DE CUENTAS Y PRORRATEOS DE GASTOS
    </div>

    <fieldset style="border: 0 none;margin: 0;min-width: 0;padding: 0; font-size: 10px;">
        <table class="table table-responsive table-bordered"
               style="margin-bottom: 20px; max-width: 100%; width: 100%;">
            <thead>
            <tr>
                <th>UF</th>
                <th>Propietario</th>
                <th>%</th>
                <th>Saldo&nbsp;Ant</th>
                <th>Pagos</th>
                <th>Deuda</th>
                <th>Interés</th>
                <th>Exp&nbsp;A</th>
                <th>Exp&nbsp;B</th>
                <th>Cuota&nbsp;Extra</th>
                {{--<th>Monto&nbsp;Exp</th>--}}
                <th>Part</th>

                @if (session('CONSORCIO_TIENE_COCHERA'))
                    <th>Cochera</th>
                @endif
                <th>Total</th>
            </tr>
            </thead>

            <tbody>
            @foreach ($prorrateos as $prorrateo)
                <tr>
                    <td style="width: 20px; max-height: 1px; padding: 0px; margin: 0px;">{{ $prorrateo->codigo }}|{{ $prorrateo->piso }}-{{ $prorrateo->dpto }}</td>
                    <td style="max-height: 1px; padding: 0px; margin: 0px;">{{ $prorrateo->nombre }} {{ $prorrateo->apellido }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">{{ number_format($prorrateo->porcentaje_a, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->saldo_anterior, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->monto_abonado, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->deuda - $prorrateo->futuros, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->intereses, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->expensas_tipo_a, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->expensas_tipo_b, 2) }}</td>
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->cuota_extra, 2) }}</td>
                    {{--<td class="text-right">${{ number_format($prorrateo->monto_expensas, 2) }}</td>--}}
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->expensas_tipo_particular, 2) }}</td>

                    @if (session('CONSORCIO_TIENE_COCHERA'))
                        <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->cochera, 2) }}</td>

                        <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->saldo_a_pagar + $prorrateo->cochera, 2) }}</td>
                    @else
                        <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($prorrateo->saldo_a_pagar, 2) }}</td>
                    @endif
                </tr>
            @endforeach
            {{-- Fila de Totales --}}
            <tr class="background-categoria">
                <td colspan="2" class="text-center" style="max-height: 1px; padding: 0px; margin: 0px;">TOTAL</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">{{ number_format($total_prorrateos['porc_a'], 2) }}%</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['saldo_ant'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['monto_abonado'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['deuda'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['intereses'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['exp_a'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['exp_b'], 2) }}</td>
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['cuota_extra'], 2) }}</td>
                {{--<td class="text-right">${{ number_format($total_prorrateos['monto_exp'], 2) }}</td>--}}
                <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['exp_part'], 2) }}</td>

                @if (session('CONSORCIO_TIENE_COCHERA'))
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['cochera'], 2) }}</td>

                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['saldo_a_pagar'] + $total_prorrateos['cochera'], 2) }}</td>
                @else
                    <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($total_prorrateos['saldo_a_pagar'], 2) }}</td>
                @endif
            </tr>
            </tbody>
        </table>
    </fieldset>
</div>