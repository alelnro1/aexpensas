@extends('layouts.app')

@section('site-name', 'Nuevo sum')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Espacio Común</div>

    <div class="panel-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Hubo errores al tratar de crear el Espacio Común. Verifique los datos ingresados</strong>
            </div>
        @endif

        <form action="/admin/sums" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label">Nombre</label>

                <div class="col-md-7">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-3">Descripción</label>

                <div class="col-md-7">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-3">Archivo</label>

                <div class="col-md-7">
                    <input type="file" class="form-control" name="archivo" id="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group {{ $errors->has('requiere_reserva') ? ' has-error' : '' }}">
                <label for="requiere_reserva" class="control-label col-md-3">Requiere Reserva</label>

                <div class="col-md-7">
                    <input type="checkbox" name="requiere_reserva"
                           @if (old('requiere_reserva') == "on") checked @endif
                           id="requiere_reserva">
                </div>
            </div>

            <!-- Turnos -->
            <div class="form-group{{ $errors->has('turnos') ? ' has-error' : '' }}" id="turnos-div" style="display: none;">
                <label for="" class="control-label col-xs-3">Turnos</label>

                <div class="row col-xs-9">
                    <div class="col-xs-7" id="listado-turnos">
                        <div class="col-xs-12 turno">
                            <div class="col-xs-5">
                                <select name="horas_inicio[]" class="form-control">
                                    <option value="">Hora Inicio</option>

                                    @for ($i=0; $i <= 23; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="col-xs-5">
                                <select name="horas_fin[]" class="form-control">
                                    <option value="">Hora Fin</option>

                                    @for ($i=1; $i <= 24; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="col-xs-1">
                                <a href="#" class="btn btn-danger" id="eliminar-turno">-</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-1">
                        <a href="#" class="btn btn-success" id="nuevo-turno">Agregar Turno</a>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('costo') ? ' has-error' : '' }}" style="display:none" id="costo">
                <label class="col-md-3 control-label">Costo</label>

                <div class="col-md-7">
                    <div class="input-group col-xs-3">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" aria-label="Costo" name="costo" value="{{ old('costo') }}">
                    </div>

                    @if ($errors->has('costo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('costo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <hr>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-5">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Crear
                    </button>
                </div>
            </div>
        </form>


    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/sums/create.js') }}"></script>
    <script type="x-template" id="nueva-fila-turno">
        <div class="col-xs-12 turno">
            <br>
            <div class="col-xs-5">
                <select name="horas_inicio[]" class="form-control">
                    <option value="">Hora Inicio</option>

                    @for ($i=0; $i <= 24; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>

            <div class="col-xs-5">
                <select name="horas_fin[]" class="form-control">
                    <option value="">Hora Fin</option>

                    @for ($i=0; $i <= 24; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>

            <div class="col-xs-1">
                <button class="btn btn-danger" id="eliminar-turno">-</button>
            </div>
        </div>
    </script>

    <script type="text/javascript" src="{{ asset('/js/sums/turnos.js') }}"></script>
@stop