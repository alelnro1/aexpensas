@extends('layouts.app')

@section('site-name', 'Viendo a Espacio Compartido')

@section('content')
    <div class="panel-heading">
        Espacio Compartido <b><i>{{ $sum->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('sum_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('sum_actualizado') }}
            </div>
        @endif

        @if($sum->archivo != "")
            <div class="text-center">
                <img src="/{{ $sum->archivo }}" style="max-width:250px;" />
                <br><br>
            </div>
        @endif

        <table class="table table-striped task-table">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $sum->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td style="max-width:150px; overflow:hidden;"><div>{{ $sum->descripcion }}</div></td>
            </tr>

            @if (number_format($sum->costo, 0) > 0)
                <tr>
                    <td><strong>Costo</strong></td>
                    <td>${{ number_format($sum->costo, 2) }}</td>
                </tr>
            @endif
        </table>

        @if (count($sum->Turnos) > 0)
            <fieldset>
                <legend>Turnos</legend>

                <table class="table table-striped task-table">
                    @foreach ($sum->Turnos as $turno)
                        <tr>
                            <td>De {{ $turno->hora_inicio }} a {{ $turno->hora_fin }}</td>
                        </tr>
                    @endforeach
                </table>
            </fieldset>

            <fieldset>
                <legend>Reservas</legend>

                @if(Session::has('reserva_aceptada'))
                    <div class="alert alert-success">
                        {{ Session::get('reserva_aceptada') }}
                    </div>
                @endif

                @if(Session::has('reserva_rechazada'))
                    <div class="alert alert-success">
                        {{ Session::get('reserva_rechazada') }}
                    </div>
                @endif

                @if(Session::has('reserva_eliminada'))
                    <div class="alert alert-success">
                        {{ Session::get('reserva_eliminada') }}
                    </div>
                @endif

                @if (count($sum->Reservas) > 0)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Unidad Funcional</th>
                            <th>Descripción</th>
                            <th>Turno</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($sum->Reservas as $reserva)
                            <tr>
                                <td>
                                    {{ $reserva->UnidadFuncional->codigo }} |
                                    {{ $reserva->UnidadFuncional->piso }} -
                                    {{ $reserva->UnidadFuncional->dpto }}
                                </td>
                                <td>{{ $reserva->descripcion }}</td>
                                <td>De {{ $reserva->Turno->hora_inicio }} a {{ $reserva->Turno->hora_fin }}</td>
                                <td>{{ date("d-m-Y", strtotime($reserva->fecha)) }}</td>
                                <td>{{ $reserva->EstadoReserva->descripcion }}</td>
                                <td>
                                    @if (Auth::user()->esAdmin())
                                        <a href="{{ url( $base_url . 'reservas/' . $reserva->id . '/aceptar') }}" class="btn btn-success btn-xs">
                                            Aceptar
                                        </a>

                                        <a href="{{ url( $base_url . 'reservas/' . $reserva->id . '/rechazar') }}" class="btn btn-danger btn-xs">
                                            Rechazar
                                        </a>
                                    @elseif(Auth::user()->esPropietario() && Auth::user()->tieneUnidadFuncional($reserva->unidad_funcional_id))
                                        <a href="{{ url( $base_url . 'reservas/' . $reserva->id . '/eliminar') }}" class="btn btn-danger btn-xs eliminar">
                                            Eliminar
                                        </a>
                                    @endif

                                    <a href="{{ url( $base_url . 'reservas/' . $reserva->id) }}" class="btn btn-default btn-xs">
                                        Ver
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    No hay reservas
                @endif
            </fieldset>
        @endif


        @if (!Auth::user()->esPropietario())
            <td>
                <a class="btn btn-xs btn-default" href="/admin/sums/{{ $sum->id }}/edit">Editar</a>
            </td>
        @endif
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.eliminar').on('click', function(e) {
                if (!confirm('Realmente desea eliminar la reserva?')) {
                    e.preventDefault();
                }
            });
        });
    </script>

@endsection
