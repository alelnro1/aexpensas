@extends('layouts.app')

@section('site-name', 'Listando sums')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Espacios Comunes

        <div style="float:right;">
            <a href="{{ ('sums/ver-reservas') }}" class="btn btn-xs btn-default">Ver todas las Reservas</a>
            @if (!Auth::user()->esPropietario())
                <a class="btn btn-xs btn-default" href="/admin/sums/create">Nuevo Espacio Común</a>
            @endif
        </div>

    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('sum_eliminado'))
                <div class="alert alert-success">
                    {{ Session::get('sum_eliminado') }}
                </div>
            @endif

            @if(Session::has('sum_creado'))
                <div class="alert alert-success">
                    {{ Session::get('sum_creado') }}
                </div>
            @endif

            @if(Session::has('sum_actualizado'))
                <div class="alert alert-success">
                    {{ Session::get('sum_actualizado') }}
                </div>
            @endif

            @if(Session::has('reserva_creada'))
                <div class="alert alert-success">
                    {{ Session::get('reserva_creada') }}
                </div>
            @endif



            @if (count($sums) > 0)
                <table class="table table-striped task-table" id="sums" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($sums as $sum)
                        <tr>
                            <td>
                                <div>
                                    @if(strlen( $sum->nombre) > 15){{substr ($sum->nombre, 0 , 15  )}}...
                                    @else {{$sum->nombre}}
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div>
                                    @if(strlen( $sum->descripcion) > 15){{substr ($sum->descripcion, 0 , 15  )}}...
                                    @else {{$sum->descripcion}}
                                    @endif
                                </div>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href="/admin/sums/{{ $sum['id'] }}">Ver</a>
                                @if(count($sum->Turnos) > 0)
                                    |
                                    <a class="btn btn-xs btn-default" href="{{ url($base_url . 'sums/' . $sum['id'] . '/reservar') }}">Reservar</a>
                                @endif

                                @if (!Auth::user()->esPropietario())
                                    |
                                    <a class="btn btn-xs btn-default" href="/admin/sums/{{ $sum['id'] }}"
                                       data-method="delete"
                                       data-token="{{ csrf_token() }}"
                                       data-confirm="Esta seguro que desea eliminar a consorcio con nombre {{ $sum->nombre }}?">
                                        Eliminar
                                    </a>
                                    |
                                    <a class="btn btn-xs btn-default" href="/admin/sums/{{ $sum->id }}/edit">Editar</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay Espacios Comunes
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/sums/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sums/delete-link.js') }}"></script>
@stop


Usuarios
