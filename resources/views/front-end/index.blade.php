@extends('front-end.app')

@section('header')
    alt
@stop

@section('content')
    <section id="banner">
        <h2><img src="/images_template/blancotransparente.png" alt=""  style="height:100px;"></h2>

        <p>La Manera m&aacute;s F&aacute;cil de Administrar Consorcios.</p>
        <ul class="actions">
            <li><a href="/admin" class="button special">Ingresar</a></li>
            <li><a href="/register" class="button">Reg&iacute;strese</a></li>
        </ul>

    </section>

    <section id="main" class="container">
        <section class="box special">
            <header class="major">
                <h2>Tanto los Administradores como los Propietarios pueden llevar un control y una &aacute;gil gesti&oacute;n de todos los temas que involucran a la Administraci&oacute;n de un Consorcio</h2>
                <p>Por s&oacute;lo un peque&ntilde;o abono mensual<br> usted podr&aacute; acceder a todas las funcionalidad de <b>AExpensas</b></p>
            </header>
            <span class="image featured"><img src="images_template/pic01.jpg" alt="" /></span>
        </section>

        <section class="box special features">
            <div class="features-row">
                <section>
                    <span class="icon major fa-bolt accent2"></span>
                    <h3>Liquidaci&oacute;n de Expensas</h3>
                    <p>Desde AExpensas usted podrá ver todas las Liquidaciones de Expensas anteriores.
                        Recibirá por mail el aviso cada emisión de expensa en su correo electrónico.</p>
                </section>
                <section>
                    <span class="icon major fa-area-chart accent3"></span>
                    <h3>Declaraci&oacute;n de Pagos</h3>
                    <p>
                        Cada vez que usted realiza un pago, tiene la posibilidad de avisar que ha realizado
                        la transferencia desde AExpensas para que quede registrado dentro del sistema.
                    </p>
                </section>
            </div>
            <div class="features-row">
                <section>
                    <span class="icon major fa-cloud accent4"></span>
                    <h3>Gesti&oacute;n de Pedidos</h3>
                    <p>Si usted desea realizar un pedido a la administración, nada mejor que hacerlo por este medio;
                        de esta manera queda registrado la fecha y todo detalle relevanete.</p>
                </section>
                <section>
                    <span class="icon major fa-lock accent5"></span>
                    <h3>Estad&iacute;sticas de Ingresos y Egresos</h3>
                    <p>Con AExpensas el propietario puede ver con claridad todas las transacciones que se
                        realizan del consorcio y los detalles del mismo, tanto ingresos como egresos.</p>
                </section>
            </div>
        </section>

        <div class="row">
            <div class="6u 12u(narrower)">

                <section class="box special">
                    <span class="image featured"><img src="images_template/pic02.jpg" alt="" /></span>
                    <h3>Funcionalidades</h3>
                    <p>Reserva de SUMs. Pedidos. Declaración de Pagos. Mailing para notificaciones automáticas.
                        Estado de Cuenta de cada Unidad Funcional. Administración de Proveedores.</p>
                    {{--<ul class="actions">
                        <li><a href="#" class="button alt">Leer M&aacute;s</a></li>
                    </ul>--}}
                </section>

            </div>
            <div class="6u 12u(narrower)">

                <section class="box special">
                    <span class="image featured"><img src="images_template/pic03.jpg" alt="" /></span>
                    <h3>Servicios Extras</h3>
                    <p>Independientemente de realizar la gestión de las expensas y centralizar todo en un mismo sistema,
                        también podemos enviarle las expensas físicas (en papel) al domicilio.</p>
                    {{--<ul class="actions">
                        <li><a href="#" class="button alt">Leer M&aacute;s</a></li>
                    </ul>--}}
                </section>

            </div>
        </div>
    </section>

    @include('front-end.footer-contacto')
@stop