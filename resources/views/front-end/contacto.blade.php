@extends('front-end.app')

@section('content')
	<script>
		function onSubmit(token) {
			document.getElementById("contactForm").submit();
		}
	</script>
	
    <div style="clear:both;"></div>

    <section id="main" class="container 75%">
        <header style="margin-top:260px">
            <h2>Cont&aacute;ctenos</h2>
            <a href="https://www.instagram.com/aexpensas/"><img src="/images/icon_instagram.jpg" alt="Smiley face" height="42" width="42"></a>
            <a href="https://www.facebook.com/aexpensas/"><img src="/images/icon_face.png" alt="Smiley face" height="42" width="42"></a>
            <a href="https://play.google.com/store/apps/details?id=com.wAExpensas_3786976"><img src="/images/icon_android.jpg" alt="Smiley face" height="42" width="42"></a>

            <p>Si usted es administrador de consorcios, es parte del consejo de propietarios o quiere recomendarnos que realicemos alguna funcionalidad que sirva para la mejora continua de <b>AExpensas</b>, no deje de escribirnos y nos pondremos en contacto con usted a la brevedad.</p>
        </header>

        <div class="box">
            @if(Session::has('contacto_enviado'))
                <div style="color: green; font-weight: bold; text-align: center;">
                    Su contacto ha sido enviado satisfactoriamente!
                    <br><br>
                </div>
            @endif

            <form method="post" action="{{ url('contacto') }}" id="contactForm">
                {{ csrf_field() }}
                <div class="row uniform 50%">
                    <div class="6u 12u(mobilep) {{ $errors->has('nombre') ? ' has-error' : '' }}">
                        <input type="text" name="nombre" id="name" value="" placeholder="Nombre" />
                        @if ($errors->has('nombre'))
                            <span class="help-block" style="color: #dd0000 !important;">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="6u 12u(mobilep)">
                        <input type="email" name="email" id="email" value="" placeholder="Email" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="12u">
                        <input type="text" name="titulo" id="subject" value="" placeholder="T&iacute;tulo" />
                        @if ($errors->has('titulo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('titulo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="12u">
                        <textarea name="mensaje" id="message" placeholder="Mensaje" rows="6"></textarea>
                        @if ($errors->has('mensaje'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mensaje') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row uniform">
                    <div class="12u">
                        <ul class="actions align-center">
							<li>
								<button
									class="g-recaptcha form-control"
									data-sitekey="6LeEPRwUAAAAAPA7PlmSbqmJQ6JhQ75ovwJRd-q4"
									data-callback="onSubmit">
									Enviar
								</button>
							</li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </section>

	
	<script src='https://www.google.com/recaptcha/api.js'></script>
@stop