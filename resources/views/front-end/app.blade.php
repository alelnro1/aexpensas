<!DOCTYPE HTML>
<!--
	Alpha by HTML5 UP
	html5up.net | @ajlkn
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
    -->
<html>
<head>
    <title>AExpensas</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="/assets_template/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="/assets_template/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets_template/css/ie8.css"/><![endif]-->
</head>
<body class="landing">
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="@yield('header')">
        <h1><a href="/">AExpensas</a></h1>
        <nav id="nav">
            <ul>
                <li><a href="/">Inicio</a></li>
                {{--<li>
                    <a href="#" class="icon fa-angle-down">Características</a>
                    <ul>
                        <li><a href="generic.html">Funcionalidades</a></li>
                        <li><a href="generic.html">Tarifas</a></li>
                        <li><a href="generic.html">FAQs</a></li>
                        <li><a href="contact.html">Cont&aacute;ctenos</a></li>
                        <li>
                            <a href="#">Submenu</a>
                            <ul>
                                <li><a href="#">Option One</a></li>
                                <li><a href="#">Option Two</a></li>
                                <li><a href="#">Option Three</a></li>
                                <li><a href="#">Option Four</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>--}}
                <li><a href="/contacto" class="button">Cont&aacute;ctenos</a></li>
            </ul>
        </nav>
    </header>

    <!-- Main -->
@yield('content')


<!-- Footer -->
    <footer id="footer">
        <ul class="icons">
            <li><a href="https://www.instagram.com/aexpensas/"><img src="/images/icon_instagram.jpg" alt="Smiley face" height="42" width="42"></a></li>
            <li><a href="https://www.facebook.com/aexpensas/"><img src="/images/icon_face.png" alt="Smiley face" height="42" width="42"></a></li>
            <li><a href="https://play.google.com/store/apps/details?id=com.wAExpensas_3786976"><img src="/images/icon_android.jpg" alt="Smiley face" height="42" width="42"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; Todos los Derechos Rervados.</li><li>Desarrollado por: <a href="http://www.doublepoint.com.ar">Double Point</a>							</li>
        </ul>

    </footer>

</div>

<!-- Scripts -->
<script src="/assets_template/js/jquery.min.js"></script>
<script src="/assets_template/js/jquery.dropotron.min.js"></script>
<script src="/assets_template/js/jquery.scrollgress.min.js"></script>
<script src="/assets_template/js/skel.min.js"></script>
<script src="/assets_template/js/util.js"></script>
<!--[if lte IE 8]>
<script src="/assets_template/js/ie/respond.min.js"></script><![endif]-->
<script src="/assets_template/js/main.js"></script>

<script type="text/javascript">
    $('#contactar-admin').on('submit', function (e) {
        e.preventDefault();

        var email = $(this).find('#email').val();

        if (email.length < 5) {
            alert('Ingrese un email válido');
        } else {
            $.ajax({
                url: 'contactar-admin',
                type: 'POST',
                data: {
                    'email': email,
                    '_token': $('input[name="_token"]').val()
                },
                dataType: 'json',
                success: function (data) {
                    if (data.mail_invalido) {
                        alert('Ingrese un email invalido');
                    } else if (data.mail_enviado) {
                        alert('Gracias por contactarnos. A la brevedad nos pondremos en contacto');
                    }
                }
            });
        }
    })
</script>
</body>
</html>