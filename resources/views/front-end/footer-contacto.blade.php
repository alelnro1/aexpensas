<section id="cta">

    <h2>Soy Administrador, cont&aacute;ctenme</h2>
    <p>Nos pondremos en contacto con usted a la brevedad para acercarle un propuesta.</p>

    <form method="POST" action="{{ url('contactar-admin') }}" id="contactar-admin">
        {{ csrf_field() }}
        <div class="row uniform 50%">
            <div class="8u 12u(mobilep)">
                <input type="email" name="email" id="email" placeholder="Email" />
            </div>
            <div class="4u 12u(mobilep)">
                <input type="submit" value="Enviar" class="fit" />
            </div>
        </div>
    </form>

</section>