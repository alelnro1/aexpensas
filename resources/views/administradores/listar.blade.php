@extends('layouts.app')

@section('site-name', 'Listando administradores')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Administradores

        <div style="float:right;">
            <a href="{{ url( $base_url . 'administradores/create') }}" class="btn btn-default btn-xs">Nuevo
                Administrador</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('administrador_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('administrador_eliminado') }}
            </div>
        @endif
        @if(Session::has('administrador_no_eliminado'))
            <div class="alert alert-warning">
                {{ Session::get('administrador_no_eliminado') }}
            </div>
        @endif

        @if(Session::has('administrador_creado'))
            <div class="alert alert-success">
                {{ Session::get('administrador_creado') }}
            </div>
        @endif

        @if (count($administradores) > 0)
            <table class="table table-striped task-table" id="administradores">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Consorcios</th>
                    <th>Usuarios</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($administradores as $administrador)
                    <tr>
                        <td>{{ $administrador->nombre }}</td>
                        <td>
                            <ul class="list-unstyled">
                                <?php 
                                    $total      = 0;
                                    $total_pago = 0;
                                    $tiene_seteado_pagoXuf = 1;
                                ?>

                                @foreach($administrador->consorcios as $consorcio)
                                    <?php 
                                        $total = $total +  count($consorcio->UnidadesFuncionales);

                                    ?>
                                    @if( $consorcio->precioXuf != '')
                                    <?php 
                                        $total = $total +  count($consorcio->UnidadesFuncionales);
                                        $total_pago = $total_pago +  $consorcio->precioXuf * count($consorcio->UnidadesFuncionales); 
                                    ?>
                                    @else
                                    <?php 
                                        $tiene_seteado_pagoXuf = 0;
                                        
                                    ?>  
                                    @endif

                                    <li>
                                        <a href="{{ url( $base_url . 'consorcios/' . $consorcio->id) }}" class="btn">
                                            {{ $consorcio->nombre }} ({{count($consorcio->UnidadesFuncionales)}})
                                            @if($tiene_seteado_pagoXuf) ${{$consorcio->precioXuf * count($consorcio->UnidadesFuncionales)}}@endif
                                        </a>
                                    </li>
                                @endforeach
                                    <p>Total Unidades Funcionales:  {{$total}}  </p>
                                    @if($tiene_seteado_pagoXuf )
                                        <p>Pago: $ {{$total_pago}} </p>
                                    @endif

                            </ul>
                        </td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach($administrador->Usuarios as $usuario)
                                    <li>
                                        {{ $usuario->email }}
                                    </li>
                                @endforeach
                            </ul>
                        </td>

                        <td>
                            <a href="{{ url( $base_url . 'administradores/' . $administrador['id']) }}"
                               class="btn btn-default btn-xs">Ver</a>

                            <a href="{{ url( $base_url . 'administradores/' . $administrador['id'] . '/edit') }}"
                               class="btn btn-default btn-xs">Editar</a>

                            <a href="{{ url( $base_url . 'administradores/' . $administrador['id']) }}"
                               class="btn btn-default btn-xs"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a administrador con nombre {{ $administrador->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay administradores
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/administradores/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/administradores/delete-link.js') }}"></script>
@stop