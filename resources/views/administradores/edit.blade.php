@extends('layouts.app')

@section('site-name', 'Editar administrador')

@section('content')
    <div class="panel-heading">Administrador</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url( $base_url . 'administradores/' . $administrador->id) }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <!-- Nombre -->
                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="nombre" value="{{ $administrador->nombre}}">

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Email</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ $administrador->email}}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <!-- Telefono -->
                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Teléfono</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telefono" value="{{ $administrador->telefono}}">

                        @if ($errors->has('telefono'))
                            <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <!-- Cuit -->
                <div class="form-group{{ $errors->has('inscripcion_rpa') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Cuit</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="cuit" value="{{ $administrador->cuit}}">

                        @if ($errors->has('cuit'))
                            <span class="help-block">
                        <strong>{{ $errors->first('cuit') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                <!-- Inscripcion_rpa -->
                <div class="form-group{{ $errors->has('inscripcion_rpa') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Inscripción RPA</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="inscripcion_rpa" value="{{ $administrador->inscripcion_rpa}}">

                        @if ($errors->has('inscripcion_rpa'))
                            <span class="help-block">
                            <strong>{{ $errors->first('inscripcion_rpa') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <!-- Situacion Fiscal -->
                <div class="form-group{{ $errors->has('situacion_fiscal') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Situación Fiscal</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="situacion_fiscal" value="{{ $administrador->situacion_fiscal}}">

                        @if ($errors->has('situacion_fiscal'))
                            <span class="help-block">
                            <strong>{{ $errors->first('situacion_fiscal') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <!-- Archivo -->
                <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                    <label for="archivo" class="control-label col-md-4">Archivo</label>

                    <div class="col-md-6">
                        <input type="file" class="form-control" name="archivo">

                        @if ($errors->has('archivo'))
                            <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <!-- Domicilio -->
                <div class="form-group{{ $errors->has('domicilio') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Domicilio</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="domicilio" value="{{$administrador->domicilio}}" id="domicilio" autocomplete="off">

                        @if ($errors->has('domicilio'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/administradores/edit.js') }}"></script>
@stop