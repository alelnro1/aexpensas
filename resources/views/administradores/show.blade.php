@extends('layouts.app')

@section('site-name', 'Viendo a administrador')

@section('content')
    <div class="panel-heading">
        Administrador con nombre <b><i>{{ $administrador->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('administrador_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('administrador_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $administrador->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $administrador->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $administrador->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $administrador->telefono }}</td>
            </tr>
            <tr>
                <td><strong>Cuit</strong></td>
                <td>{{ $administrador->cuit }}</td>
            </tr>
            <tr>
                <td><strong>Inscripción RPA</strong></td>
                <td>{{ $administrador->inscripcion_rpa  }}</td>
            </tr>
            <tr>
                <td><strong>Situación Fiscal</strong></td>
                <td>{{ $administrador->situacion_fiscal }}</td>
            </tr>

            <tr>
                <td><strong>Consorcios</strong></td>
                <td>
                    <ul class="list-unstyled">
                        @foreach($administrador->consorcios as $consorcio)
                            <li>
                                <a href="{{ url( $base_url . 'consorcios/' . $consorcio->id) }}" class="btn">
                                    {{ $consorcio->nombre }}  ({{count($consorcio->UnidadesFuncionales)}})
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            <tr>
                <td><strong>Usuarios</strong></td>
                <td>
                    <ul class="list-unstyled">
                        @foreach($administrador->Usuarios as $usuario)
                            <li>
                                {{ $usuario->email }}
                            </li>
                        @endforeach
                    </ul>
                </td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $administrador->archivo }}" height="250"/></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $administrador->created_at }}</td>
            </tr>
        </table>

        <div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                    <a class="btn btn-primary" href="{{ url( $base_url . 'administradores/' . $administrador->id) }}/edit">Editar</a>
            </div>
            <div class="col-md-6">
                <a href="#" class="btn btn-primary" onclick="window.history.go(-1); return false;">Volver</a>
            </div>
        </div>
    </div>
@stop
