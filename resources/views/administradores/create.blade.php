@extends('layouts.app')

@section('site-name', 'Nuevo Administrador')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo Administrador</div>

    <div class="panel-body">
        <form action="{{ url( $base_url . 'administradores') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono -->
            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ old('telefono') }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Cuit -->
            <div class="form-group{{ $errors->has('inscripcion_rpa') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cuit</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit" value="{{ old('cuit') }}">

                    @if ($errors->has('cuit'))
                        <span class="help-block">
                        <strong>{{ $errors->first('cuit') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

                <!-- Inscripcion_rpa -->
            <div class="form-group{{ $errors->has('inscripcion_rpa') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Inscripción RPA</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="inscripcion_rpa" value="{{ old('inscripcion_rpa') }}">

                    @if ($errors->has('inscripcion_rpa'))
                        <span class="help-block">
                            <strong>{{ $errors->first('inscripcion_rpa') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

                <!-- Situacion Fiscal -->
            <div class="form-group{{ $errors->has('situacion_fiscal') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Situación Fiscal</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="situacion_fiscal" value="{{ old('situacion_fiscal') }}">

                    @if ($errors->has('situacion_fiscal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('situacion_fiscal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Domicilio -->
            <div class="form-group{{ $errors->has('domicilio') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio" value="{{ old('domicilio') }}" id="domicilio" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/administradores/create.js') }}"></script>
@stop