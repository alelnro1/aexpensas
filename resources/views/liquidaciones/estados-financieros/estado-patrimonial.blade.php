<div class="break">
    <legend style="font-size: 14px; font-weight: bold; margin-bottom: 0px;">Estado Patrimonial</legend>

    <span style="font-size: 10px;">(Conf. art. 10 inc. c Ley N° 941)</span>

    <table class="table table-bordered" style="margin-bottom: -1px; font-size: 10px;">
        <tr class="info">
            <td style="max-height: 1px; padding:0px; margin:0px;">Saldo de Disponibilidades al cierre</td>

            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($saldo_estado_financiero, 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Expensas y otros conceptos a cobrar</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($saldo_estado_patrimonial, 2) }}</td>
        </tr>

        <tr class="info">
            <td style="max-height: 1px; padding:0px; margin:0px;">Patrimonio Neto al cierre</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($saldo_estado_financiero + $saldo_estado_patrimonial, 2) }}</td>
        </tr>
    </table>
</div>
