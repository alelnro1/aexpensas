<div class="break">
    <legend style="font-size: 14px; font-weight: bold; margin-bottom: 0px;">Fondo Operativo</legend>

    <table class="table table-bordered table-hover" id="estados_financieros" style="font-size: 10px;">
        <tr class="info">
            <td style="max-height: 1px; padding:0px; margin:0px;">SALDO INICIAL</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">
                @if($ultima_liquidacion)
                    ${{ number_format($ultima_liquidacion->saldo_estado_financiero, 2) }}
                @else
                    $0.00
                @endif
            </td>
        </tr>
        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Ingreso por pago de expensas en término</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($fondo_operativo['ingresos_terminos'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Ingreso por pago de expensas adeudadas</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($fondo_operativo['ingresos_adeudados'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Ingreso por pago de expensas particulares</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($fondo_operativo['ingresos_particulares'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Ingreso por pago de expensas de intereses</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($fondo_operativo['ingresos_intereses'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Ingreso por pago de expensas futuros</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($fondo_operativo['ingresos_futuros'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Egresos por gastos del mes</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">-${{ number_format($fondo_operativo['egresos_generales'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Egresos por gastos particulares</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">-${{ number_format($fondo_operativo['egresos_particulares'], 2) }}</td>
        </tr>

        <tr>
            <td style="max-height: 1px; padding:0px; margin:0px;">Egresos por Asignación de Cuota Extra</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">-${{ number_format($fondo_operativo['egresos_asignacion_cuota_extra'], 2) }}</td>
        </tr>

        <tr class="info">
            <td style="max-height: 1px; padding:0px; margin:0px;">Saldo al Cierre</td>
            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">
                ${{ number_format($saldo_estado_financiero, 2) }}
            </td>
        </tr>
    </table>

</div>
