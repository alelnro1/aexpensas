@foreach ($categorias as $categoria)
    @if ($categoria->nombre != "Datos de Juicios") @continue @endif

    <div>
        <div class="break">
            <legend style="font-size: 14px; font-weight: bold; margin-bottom: 0px;">{{ $categoria->nombre }}</legend>

            {{ $categoria->descripcion_ley }}
        </div>

        @if (count($categoria->EstadosFinancieros) > 0)
            {{-- */$count=0;/* --}}
            <table class="table table-bordered table-hover" id="estados_financieros" style="font-size: 10px;">
                @foreach ($categoria->EstadosFinancieros as $estado_financiero)
                    {{-- */$count++;/* --}}
                    <tr>
                        <td style="max-height: 1px; padding: 0px; margin: 0px;">{{ $estado_financiero->nombre }}</td>
                        <td class="text-right" style="max-height: 1px; padding: 0px; margin: 0px;">${{ number_format($estado_financiero->monto, 2) }}</td>
                    </tr>
                @endforeach
            </table>

            @if (!$count)
                <table class="table table-bordered table-hover" style="font-size: 10px;">
                    <tr>
                        <td style="max-height: 1px; padding: 0px; margin: 0px; font-size: 10px;">SIN MOVIMIENTOS</td>
                        <td style="max-height: 1px; padding: 0px; margin: 0px;"></td>
                    </tr>
                </table>
            @endif
        @else
            <table class="table table-bordered table-hover">
                <tr>
                    <td style="max-height: 1px; padding: 0px; margin: 0px; font-size: 10px;">SIN MOVIMIENTOS</td>
                    <td style="max-height: 1px; padding: 0px; margin: 0px;"></td>
                </tr>
            </table>
        @endif
    </div>
@endforeach