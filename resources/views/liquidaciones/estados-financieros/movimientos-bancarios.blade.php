@foreach ($categorias as $categoria)
    @if ($categoria->nombre != "Resumen de Movimientos Bancarios") @continue @endif

    <div>
        <div class="break">
            <legend style="font-size: 14px; font-weight: bold; margin-bottom: 0px;">Resumen de Movimientos Bancarios</legend>
            <span style="font-size: 10px;">(Conf. art. 10 inc. c Ley N° 941)</span>
        </div>

        <table class="table table-bordered" style="margin-bottom: -1px; font-size: 10px;">
            <tr class="info">
                <td style="max-height: 1px; padding:0px; margin:0px;">Saldo Inicial</td>

                <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($saldo_anterior_movimientos_bancarios, 2) }}</td>
            </tr>
        </table>

        @if (count($categoria->EstadosFinancieros) > 0)
            {{-- */$count=0;/* --}}
            <table class="table table-bordered table-hover" id="estados_financieros" style="font-size: 10px;">
                @foreach ($categoria->EstadosFinancieros as $estado_financiero)
                        {{-- */$count++;/* --}}
                        <tr>
                            <td style="max-height: 1px; padding:0px; margin:0px;">{{ $estado_financiero->nombre }}</td>
                            <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($estado_financiero->monto, 2) }}</td>
                        </tr>
                @endforeach

                <tr class="info">
                    <td style="max-height: 1px; padding:0px; margin:0px;">Saldo al Cierre</td>
                    <td class="text-right" style="max-height: 1px; padding:0px; margin:0px;">${{ number_format($saldo_movimientos_bancarios, 2) }}</td>
                </tr>
            </table>

            @if (!$count)
                <table class="table table-bordered table-hover">
                    <tr>
                        <td style="max-height: 1px; padding:0px; margin:0px; font-size: 10px;">SIN MOVIMIENTOS</td>
                        <td style="max-height: 1px; padding:0px; margin:0px;"></td>
                    </tr>
                </table>
            @endif
        @else
            <table class="table table-bordered table-hover">
                <tr>
                    <td style="max-height: 1px; padding:0px; margin:0px; font-size: 10px;">SIN MOVIMIENTOS</td>
                    <td style="max-height: 1px; padding:0px; margin:0px;"></td>
                </tr>
            </table>
        @endif
    </div>
@endforeach