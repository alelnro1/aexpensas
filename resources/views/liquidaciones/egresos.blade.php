<div id="accordion">
    @foreach($rubros as $rubro)
        <h3>{{ $rubro->descripcion }}</h3>
        <fieldset>
            <div style="margin-left: 20px;">
                @foreach($rubro->RubrosHijos as $key => $rubro_hijo)
                    <span>{{ $key+1 }}- {{ $rubro_hijo->descripcion }}</span>

                    <table class="table table-striped task-table" id="gastos">
                        @if(count($rubro_hijo->Gastos) <= 0)
                            <tr>
                                <td style="text-align:right;">$0.00</td>
                            </tr>
                        @else
                            <tr>
                                <th class="col-xs-4">Descripción</th>
                                <th>Tipo Expensa</th>
                                <th class="text-right">Importe</th>
                                <th class="text-right">Total</th>
                            </tr>
                            @foreach($rubro_hijo->Gastos as $gasto)
                                @foreach($gasto->Cuotas as $cuota)

                                    <tr>
                                        <td class="col-xs-4">
                                            {{ $gasto->descripcion }}.
                                            @if ($gasto->totalCuotas($gasto->id) > 1)
                                                Cuota: {{ $cuota->descripcion }}
                                            @endif
                                        </td>
                                        <td>{{ $gasto->TipoExpensa->descripcion }}</td>
                                        <td class="text-right">
                                            @if ($gasto->TipoExpensa->descripcion == "Cochera" || $gasto->TipoExpensa->descripcion == "Particular")
                                                ${{ number_format($cuota->importe * $cuota->dameCantidadDeUFsConEseGasto(), 2) }}
                                            @else
                                                ${{ number_format($cuota->importe, 2) }}
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Subtotal</td>
                                <td class="text-right">
                                    ${{ number_format($rubro_hijo->total, 2) }}
                                </td>
                            </tr>
                        @endif
                    </table>
                @endforeach
            </div>
            <div style="float:right;">
                <span>Total {{ $rubro->descripcion }}: ${{ number_format($rubro->total, 2) }}</span>
            </div>
        </fieldset>
    @endforeach
</div>