<tr class="datos-item">
    <td>
        <select name="categoria_financiera_id[]" class="form-control categoria_financiera_id" data-id="categoria_financiera_id">
            <option value="">Categoría</option>

            @foreach ($categorias as $categoria)
                <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
            @endforeach
        </select>
    </td>

    <td class="subcategorias">
        <select name="subcategoria_financiera_id[]" class="form-control subcategoria_financiera_id" data-id="subcategoria_financiera_id">
            <option value="">Subcategoría</option>
        </select>
    </td>

    <td>
        <input type="text" name="nombre[]" class="form-control nombre" data-id="nombre">
    </td>

    <td>
        <div class="input-group">
            <span class="input-group-addon">$</span>
            <input type="text" data-id="monto" name="monto[]" class="form-control monto" autocomplete="off" id="monto_{{ mt_rand(0, 9) }}">
        </div>
        <label for="monto"></label>
    </td>

    <td data-id="" style="min-width: 100px;">
        <button class="btn btn-danger eliminar-item">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </button>

        <button class="btn btn-success guardar-item">
            <i class="fa fa-check" aria-hidden="true"></i>
        </button>
    </td>
</tr>
