<div id="accordion">
    @if(Session::has('total_porcentaje_a_' . $CONSORCIO))
        <div class="alert alert-danger">
            La suma de los porcentajes de las UF debe ser 100%. Actualmente son: <br>
            A: {{ number_format(Session::get('total_porcentaje_a_' . $CONSORCIO), 2) }}%<br>
            B: {{ number_format(Session::get('total_porcentaje_b_' . $CONSORCIO), 2) }}%
            <br><br>
            No podrá seguir con la Expensa hasta que no corrija los porcentajes.
        </div>
    @endif

    <h3>Estado de Cuenta y Prorrateo</h3>

    <fieldset>
        <table class="table table-responsive table-bordered" style="width: 100%;">
            <thead>
            <tr>
                <th>UF</th>
                <th>Propietario</th>
                <th>%</th>
                <th>Saldo&nbsp;Ant</th>
                <th>Pagos</th>
                <th>Deuda</th>
                <th>Interés</th>
                <th>Exp&nbsp;A</th>
                <th>Exp&nbsp;B</th>
                <th>Cuota Extra</th>
                {{--<th>Monto&nbsp;Exp</th>--}}
                <th>Part</th>
                @if (session('CONSORCIO_TIENE_COCHERA'))
                    <th>Cochera</th>
                @endif
                <th>Total</th>
            </tr>
            </thead>

            <tbody style="font-size: 12px;">
            @foreach ($prorrateos as $prorrateo)
                <tr>
                    <td>{{ $prorrateo->codigo }}|{{ $prorrateo->piso }}-{{ $prorrateo->dpto }}</td>
                    <td>{{ $prorrateo->nombre }} {{ $prorrateo->apellido }}</td>
                    <td>{{ number_format($prorrateo->porcentaje_a, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->saldo_anterior, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->monto_abonado, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->deuda, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->intereses, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->expensas_tipo_a, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->expensas_tipo_b, 2) }}</td>
                    <td class="text-right">${{ number_format($prorrateo->cuota_extra, 2) }}</td>
                    {{--<td class="text-right">${{ number_format($prorrateo->monto_expensas, 2) }}</td>--}}
                    <td class="text-right">${{ number_format($prorrateo->expensas_tipo_particular, 2) }}</td>

                    @if (session('CONSORCIO_TIENE_COCHERA'))
                        <td class="text-right">${{ number_format($prorrateo->cochera, 2) }}</td>

                        <td class="text-right">${{ number_format($prorrateo->saldo_a_pagar + $prorrateo->cochera, 2) }}</td>
                    @else
                        <td class="text-right">${{ number_format($prorrateo->saldo_a_pagar, 2) }}</td>
                    @endif
                </tr>
            @endforeach
            <tr class="background-categoria" style="">
                <td colspan="2" class="text-center" style="background-color: gray; color: white">TOTAL</td>
                <td class="text-right">{{ number_format($total_prorrateos['porc_a'], 2) }}%</td>
                <td class="text-right">${{ number_format($total_prorrateos['saldo_ant'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['monto_abonado'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['deuda'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['intereses'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['exp_a'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['exp_b'], 2) }}</td>
                <td class="text-right">${{ number_format($total_prorrateos['cuota_extra'], 2) }}</td>
                {{--<td class="text-right">${{ number_format($total_prorrateos['monto_exp'], 2) }}</td>--}}
                <td class="text-right">${{ number_format($total_prorrateos['exp_part'], 2) }}</td>

                @if (session('CONSORCIO_TIENE_COCHERA'))
                    <td class="text-right">${{ number_format($total_prorrateos['cochera'], 2) }}</td>

                    <td class="text-right">${{ number_format($total_prorrateos['saldo_a_pagar'] + $total_prorrateos['cochera'], 2) }}</td>
                @else
                    <td class="text-right">${{ number_format($total_prorrateos['saldo_a_pagar'], 2) }}</td>
                @endif
            </tr>
            </tbody>
        </table>
    </fieldset>
</div>