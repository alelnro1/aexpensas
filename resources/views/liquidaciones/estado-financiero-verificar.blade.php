<div id="accordion">
    {{ csrf_field() }}

    <div id="estados_financieros">
        @include('liquidaciones.estados-financieros.fondo-operativo', [
            'ultima_liquidacion' => $ultima_liquidacion,
            'fondo_operativo' => $fondo_operativo,
            'saldo_estado_financiero' => $saldo_estado_financiero
        ])

        @include('liquidaciones.estados-financieros.movimientos-bancarios', [
            'saldo_anterior_movimientos_bancarios' => $saldo_anterior_movimientos_bancarios,
            'saldo_movimientos_bancarios' => $saldo_movimientos_bancarios
        ])

        @include('liquidaciones.estados-financieros.estado-patrimonial', [

        ])

        @include('liquidaciones.estados-financieros.datos-de-juicios')
    </div>
</div>