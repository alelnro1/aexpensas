@extends('layouts.app')

@section('site-name', 'Liquidación')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="{{ asset('css/wizard.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nueva Liquidación</div>

    <div class="panel-body">
        @include('loader')

        <div id="body-content">

            <span>Seleccione una fecha límite: </span>
            <div class="input-group date col-xs-6" id="datetimepicker1">
                <input type="text"
                       class="form-control"
                       name="fecha"
                       id="fecha"
                       data-fecha-ultima-liquidacion="@if($fecha_ultima_liq != ""){{ date('d/m/Y', strtotime($fecha_ultima_liq . "+ 1 day")) }}@endif"
                       value="@if(isset($fecha)) {{ date('d/m/Y', strtotime($fecha)) }} @endif"
                       autocomplete="off"
                       readonly />
                <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
            </div>

            <button class="btn btn-default" id="aplicar-fecha-limite">Aplicar</button>

            <br><br>

            <div id="contenido">
                {{ csrf_field() }}
                @if(isset($error_falta_fecha) && $error_falta_fecha == true)
                    <div class="alert-danger">Debe seleccionar una fecha</div>
                @else
                    <div data-wizard-init>
                        <ul class="steps">
                            <li data-step="1">Egresos</li>
                            <li data-step="2">Prorrateo</li>
                            @if (!Session::has('total_porcentaje_a_' . $CONSORCIO))
                                <li data-step="3">Estado Financiero</li>
                                <li data-step="4">Verificar Estado Financiero</li>
                                <li data-step="5">Confirmar</li>
                            @endif
                        </ul>
                        <div class="steps-content">
                            <div data-step="1">
                                @include('liquidaciones.egresos')
                            </div>

                            <div data-step="2">
                                @include('liquidaciones.prorrateo')
                            </div>

                            @if (!Session::has('total_porcentaje_a_' . $CONSORCIO))
                                <div data-step="3">
                                    @include('liquidaciones.cargar-estado-financiero')
                                </div>

                                <div data-step="4">
                                    <h3 class="text-center">Estado Financiero</h3>
                                    @include('liquidaciones.estado-financiero-verificar')
                                </div>

                                <div data-step="5">
                                    <div class="col-xs-4 left">
                                        <input type="text" name="nombre_periodo" class="form-control"
                                               placeholder="Escriba el nombre del período"
                                               id="nombre-periodo" value="{{ old('nombre_periodo') }}">
                                    </div>

                                    <div class="col-xs-3 left">
                                        <button class="btn btn-default" id="generar-pdf">Ver PDF</button>
                                    </div>

                                    <br><br>

                                    <div id="pdf-link" style="margin-left: 15px;"></div>

                                    <div class="text-right" id="div-confirmar-expensa">
                                        <button class="btn btn-success" id="confirmar-expensa">Confirmar Expensa</button>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @endif
                    </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                $('body').on('click', '#generar-pdf', function() {
                    var fecha = $('#fecha').val(),
                            nombre_periodo = $('#nombre-periodo').val();

                    if (fecha == "" || nombre_periodo == "") {
                        alert('La fecha y el nombre del período deben estar completos');
                    } else {
                        $('#pdf-link').empty().html(
                                '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>'
                        );

                        $.ajax({
                            url: 'generar-pdf',
                            type: 'POST',
                            data: {
                                'nombre_periodo' : nombre_periodo,
                                '_token': $('input[name="_token"]').val()
                            },
                            dataType: 'json',
                            success: function (data) {
                                $('#pdf-link').empty().append(
                                        "<a href='../../" + data.link + "' download>Click aquí</a> para descargar el PDF generado"
                                );
                            }
                        });
                    }
                });
            </script>
            <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
            <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
            <script src="{{ asset('js/wizard.js') }}"></script>

            <script type="x-template" id="nueva-fila-categoria-financiera">
                @if(!isset($error_falta_fecha) || $error_falta_fecha != true)
                    @include('liquidaciones.nueva-fila-categoria-financiera')
                @endif
            </script>


            <script type="text/javascript">
                $(document).ready(function() {
                    $("#accordion").accordion({
                        active: false,
                        collapsible: true,
                        heightStyle: "content"
                    });

                    $('#datetimepicker1').datepicker({
                        todayHighlight: true,
                        //endDate: '+0d',
                        autoclose: true,
                        format: 'dd/mm/yyyy',
                        startDate: $('#fecha').data('fecha-ultima-liquidacion')
                    });

                    function nuevaFila() {
                        var fila = $('#nueva-fila-categoria-financiera').html();

                        $('#estado-financiero tbody').last().append(fila);
                    }

                    function cargarNuevosEstadosFinancieros() {
                        $.ajax({
                            url: '/admin/liquidaciones/actualizar-estado-financiero',
                            dataType: 'json',
                            success: function(data) {
                                $('#estados_financieros').empty().html(data.listado);
                            }
                        })
                    }

                    nuevaFila();

                    $('#agregar-item').on('click', function(e){
                        e.preventDefault();

                        // Cargo una fila nueva
                        nuevaFila();
                    });

                    $('body').on('click', '.guardar-item', function() {
                        var item = $(this);

                        if (valoresCompletosYMontoNumerico(item)) {
                            // Guardo la fila
                            guardarFila(item);
                        } else {
                            alert('Complete el nombre y la categoría. El monto debe ser numérico. Ej: 9.99');
                        }
                    }).on('click', '.eliminar-item', function() {
                        if (confirm('Eliminar?')) {
                            var fila = $(this);

                            // Borro la fila actual
                            fila.parents('tr:first').remove();

                            // Mando a borrar la fila de la BD
                            eliminarFila(fila);

                            // Cuento cuantas filas me quedaron
                            var cantidad_de_filas = $('#estado-financiero tbody tr').length;

                            // Si no hay ninguna fila => cargo una nueva
                            if (cantidad_de_filas < 1) {
                                nuevaFila();
                            }
                        }

                    });

                    function valoresCompletosYMontoNumerico(item) {
                        var select = "", input = "", numerico = false;

                        item.parent().parent().find('td').each(function () {
                            $(this).find('select.categoria_financiera_id').each(function() {
                                if ($(this).val() != "")
                                    select = $(this).val();
                            });
                        });

                        item.parent().parent().find('td').each(function () {
                            $(this).find('input.nombre').each(function() {
                                if ($(this).val() != "")
                                    input = $(this).val();
                            });

                            $(this).find('input.monto').each(function() {
                                var valor = $(this).val();

                                if (!$.isNumeric(valor)){
                                    numerico = true;
                                }
                            });
                        });

                        return !(select == "" || input == "" || numerico);
                    }

                    function guardarFila(item) {
                        var fila = armarFilaDeEstadoFinanciero(item);

                        $.ajax({
                            url: '/admin/liquidaciones/guardar-items-temporales',
                            type: 'POST',
                            data: {
                                'temp_fila': fila,
                                '_token': $('input[name="_token"]').val()
                            },
                            dataType: 'json',
                            success: function(data) {
                                // Grabo en el item el id que nos devolvio php
                                item.parent().attr('data-id', data.estado_id);

                                if (data.estado_id){
                                    alert('Item guardado');
                                }

                                // Refresco los estados financieros
                                cargarNuevosEstadosFinancieros();
                            }
                        });
                    }

                    function eliminarFila(item) {
                        var fila = armarFilaDeEstadoFinanciero(item);

                        $.ajax({
                            url: '/admin/liquidaciones/eliminar-item-estado-financiero',
                            type: 'POST',
                            data: {
                                'temp_fila': fila,
                                '_token': $('input[name="_token"]').val()
                            },
                            dataType: 'json',
                            success: function(data) {
                                cargarNuevosEstadosFinancieros();
                            }
                        });
                    }

                    function armarFilaDeEstadoFinanciero(item) {
                        var id = item.parent().attr('data-id'),
                                temp_fila = {};

                        temp_fila['item_id'] = id;

                        item.parent().parent().find('td').each(function() {
                            $(this).find('input').each(function() {
                                var id = $(this).attr('data-id'),
                                        contenido_input = $(this).val();

                                temp_fila[id] = contenido_input;
                            });

                            // Busco el select
                            $(this).find('select').each(function() {
                                var id = $(this).attr('data-id');

                                temp_fila[id] = $(this).val();
                            });
                        });

                        return temp_fila;
                    }

                    // Cargo las subcategorias
                    $('body')
                            .on('change', '.categoria_financiera_id', function() {
                                var categoria = $(this);

                                $.ajax({
                                    url: 'cargar-subcategorias/' + categoria.val(),
                                    dataType: 'json',
                                    success: function (data) {
                                        // Borro las opciones que estaban antes y cargo las nuevas
                                        categoria.parent().parent().find('td.subcategorias').each(function () {
                                            $(this).find('select').each(function () {
                                                var select = $(this);
                                                // Borro las subcategorias anteriores
                                                select.find('option').remove();

                                                // Cargo las nuevas subcategorias
                                                $.each(data.subcategorias, function (subcategoria_id) {
                                                    var subcategoria = data.subcategorias[subcategoria_id];

                                                    var id = subcategoria.id,
                                                            nombre = subcategoria.nombre;

                                                    select.append(
                                                            "<option value='" + id + "'>" + nombre + "</option>"
                                                    );
                                                });
                                            });
                                        });
                                    }
                                })
                            })
                            .on('click', '#aplicar-fecha-limite', function() {
                                if ($('#fecha').val() == "") {
                                    alert('Seleccione una fecha')
                                } else {
                                    $.ajax({
                                        url: 'aplicar-fecha-limite',
                                        type: 'POST',
                                        data: {
                                            'fecha': $('#fecha').val(),
                                            '_token': $('input[name="_token"]').val()
                                        },
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data.success == true) {
                                                window.location.href = '/admin/liquidaciones/nueva';
                                            }
                                        }
                                    });
                                }
                            })
                            .on('click', '#confirmar-expensa', function(e) {
                                if (confirm('¿Está seguro que desea confirmar la expensa?')) {
                                    var fecha = $('#fecha').val(),
                                            nombre_periodo = $('#nombre-periodo').val();

                                    if (fecha == "" || nombre_periodo == "") {
                                        alert('La fecha y el nombre del período deben estar completos');
                                    } else {
                                        $('#div-confirmar-expensa').empty().html(
                                            '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>'
                                        );

                                        $.ajax({
                                            url: 'confirmar-expensa',
                                            type: 'POST',
                                            dateType: 'json',
                                            data:
                                            {
                                                'fecha' : fecha,
                                                'nombre_periodo' : nombre_periodo,
                                                '_token' : $('input[name="_token"]').val()
                                            },
                                            success: function(data) {
                                                window.location.href = '/admin/liquidaciones';
                                            }
                                        });
                                    }
                                }
                            });
                });
            </script>
@stop