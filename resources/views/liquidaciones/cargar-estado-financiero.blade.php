<div id="accordion">
    {{ csrf_field() }}

    <table id="estado-financiero" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Categoría</th>
                <th>Subcategoría</th>
                <th>Nombre</th>
                <th>Monto</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($estados_financieros as $item)
                @include('liquidaciones.fila-con-datos', ['item' => $item])
            @endforeach
        </tbody>
    </table>

    <div class="text-right margin-bottom">
        <a href="#" class="btn btn-success" id="agregar-item">+</a>
    </div>
</div>