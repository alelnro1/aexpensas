@extends('layouts.app')

@section('site-name', 'Listando liquidaciones')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Liquidaciones

        @if (Auth::user()->esAdmin())
            <div style="float:right;">
                <a class="btn btn-xs btn-default hidden-xs  "  href="{{ url( $base_url . 'liquidaciones/nueva') }}">Nueva Liquidación</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @include('loader')

        <div id="body-content">
            @if(Session::has('liquidacion_creada'))
                <div class="alert alert-success">
                    Expensa generada
                </div>
            @endif

            @if (count($liquidaciones) > 0)
                <table class="table table-striped task-table display responsive" id="liquidaciones" width="100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th></th>
                        <th>Período</th>
                        <th>Fecha</th>
                        <th>PDF</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($liquidaciones as $liquidacion)
                        <tr>
                            <td>{{ $liquidacion->created_at }}</td>
                            <td>{{ $liquidacion->nombre_periodo }}</td>
                            <td>{{ date("d-m-Y", strtotime($liquidacion->fecha_fin)) }}</td>
                            <td>
                                <a href="{{ url($liquidacion->pdf) }}" target="_blank" class="btn btn-sm btn-default">Ver PDF</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                No hay liquidaciones
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#liquidaciones').DataTable({
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: -1 },
                    {
                        "targets": [ 0 ],
                        "visible": false
                    }
                ],
                order: [[0, 'desc']],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ liquidaciones filtradas",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ liquidaciones",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ liquidaciones)",
                }
            });
        });
    </script>
@stop