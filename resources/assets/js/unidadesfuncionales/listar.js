$(document).ready(function() {
    oTable = $('#unidades_funcionales').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ unidades_funcionales filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ unidades_funcionales",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ unidades_funcionales)",
        }
    });
});