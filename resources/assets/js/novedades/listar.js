$(document).ready(function() {
    oTable = $('#novedades').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ novedades filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ novedades",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ novedades)",
        }
    });
});