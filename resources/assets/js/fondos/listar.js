$(document).ready(function() {
    oTable = $('#fondos').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ fondos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ fondos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ fondos)",
        }
    });
});