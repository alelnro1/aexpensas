<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosPedidosTable extends Migration {

	public function up()
	{
		Schema::create('estados_pedidos', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('estados_pedidos');
	}
}