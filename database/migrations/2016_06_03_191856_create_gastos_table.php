<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGastosTable extends Migration {

	public function up()
	{
		Schema::create('gastos', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('proveedor_id')->unsigned();
			$table->integer('consorcio_id')->unsigned();
			$table->integer('rubro_id')->unsigned();
			$table->integer('fondo_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('gastos');
	}
}