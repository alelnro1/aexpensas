<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('consorcios', function(Blueprint $table) {
			$table->foreign('administrador_id')->references('id')->on('administradores')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('novedades', function(Blueprint $table) {
			$table->foreign('administrador_id')->references('id')->on('administradores')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('consorcios_novedades', function(Blueprint $table) {
			$table->foreign('novedad_id')->references('id')->on('novedades')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('consorcios_novedades', function(Blueprint $table) {
			$table->foreign('consorcio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->foreign('proveedor_id')->references('id')->on('proveedores')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->foreign('consorcio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->foreign('rubro_id')->references('id')->on('rubros')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->foreign('fondo_id')->references('id')->on('fondos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('proveedores', function(Blueprint $table) {
			$table->foreign('administrador_id')->references('id')->on('administradores')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('rubros', function(Blueprint $table) {
			$table->foreign('rubro_id')->references('id')->on('rubros')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('rol_id')->references('id')->on('roles')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('unidades_funcionales', function(Blueprint $table) {
			$table->foreign('consorcio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('mailing', function(Blueprint $table) {
			$table->foreign('unidad_funcional_id')->references('id')->on('unidades_funcionales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('comprobantes', function(Blueprint $table) {
			$table->foreign('unidad_funcional_id')->references('id')->on('unidades_funcionales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('comprobantes', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados_comprobantes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('fondos', function(Blueprint $table) {
			$table->foreign('edificio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('fondos', function(Blueprint $table) {
			$table->foreign('consorcio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('items_fondos', function(Blueprint $table) {
			$table->foreign('fondo_id')->references('id')->on('fondos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('unidad_funcional_usuario', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('unidad_funcional_usuario', function(Blueprint $table) {
			$table->foreign('unidad_funcional_id')->references('id')->on('unidades_funcionales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pedidos', function(Blueprint $table) {
			$table->foreign('unidad_funcional_id')->references('id')->on('unidades_funcionales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pedidos', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados_pedidos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('unidad_funcional_id')->references('id')->on('unidades_funcionales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('sum_id')->references('id')->on('sums')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados_reservas')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('sums', function(Blueprint $table) {
			$table->foreign('consorcio_id')->references('id')->on('consorcios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('consorcios', function(Blueprint $table) {
			$table->dropForeign('consorcios_administrador_id_foreign');
		});
		Schema::table('novedades', function(Blueprint $table) {
			$table->dropForeign('novedades_administrador_id_foreign');
		});
		Schema::table('consorcios_novedades', function(Blueprint $table) {
			$table->dropForeign('consorcios_novedades_novedad_id_foreign');
		});
		Schema::table('consorcios_novedades', function(Blueprint $table) {
			$table->dropForeign('consorcios_novedades_consorcio_id_foreign');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->dropForeign('gastos_proveedor_id_foreign');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->dropForeign('gastos_consorcio_id_foreign');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->dropForeign('gastos_rubro_id_foreign');
		});
		Schema::table('gastos', function(Blueprint $table) {
			$table->dropForeign('gastos_fondo_id_foreign');
		});
		Schema::table('proveedores', function(Blueprint $table) {
			$table->dropForeign('proveedores_administrador_id_foreign');
		});
		Schema::table('rubros', function(Blueprint $table) {
			$table->dropForeign('rubros_rubro_id_foreign');
		});
		Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_rol_id_foreign');
		});
		Schema::table('unidades_funcionales', function(Blueprint $table) {
			$table->dropForeign('unidades_funcionales_consorcio_id_foreign');
		});
		Schema::table('mailing', function(Blueprint $table) {
			$table->dropForeign('mailing_unidad_funcional_id_foreign');
		});
		Schema::table('comprobantes', function(Blueprint $table) {
			$table->dropForeign('comprobantes_unidad_funcional_id_foreign');
		});
		Schema::table('comprobantes', function(Blueprint $table) {
			$table->dropForeign('comprobantes_estado_id_foreign');
		});
		Schema::table('fondos', function(Blueprint $table) {
			$table->dropForeign('fondos_edificio_id_foreign');
		});
		Schema::table('fondos', function(Blueprint $table) {
			$table->dropForeign('fondos_consorcio_id_foreign');
		});
		Schema::table('items_fondos', function(Blueprint $table) {
			$table->dropForeign('items_fondos_fondo_id_foreign');
		});
		Schema::table('unidad_funcional_usuario', function(Blueprint $table) {
			$table->dropForeign('unidad_funcional_usuario_usuario_id_foreign');
		});
		Schema::table('unidad_funcional_usuario', function(Blueprint $table) {
			$table->dropForeign('unidad_funcional_usuario_unidad_funcional_id_foreign');
		});
		Schema::table('pedidos', function(Blueprint $table) {
			$table->dropForeign('pedidos_unidad_funcional_id_foreign');
		});
		Schema::table('pedidos', function(Blueprint $table) {
			$table->dropForeign('pedidos_estado_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_unidad_funcional_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_sum_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_estado_id_foreign');
		});
		Schema::table('sums', function(Blueprint $table) {
			$table->dropForeign('sums_consorcio_id_foreign');
		});
	}
}