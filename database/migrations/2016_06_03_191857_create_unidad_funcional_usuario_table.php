<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnidadFuncionalUsuarioTable extends Migration {

	public function up()
	{
		Schema::create('unidad_funcional_usuario', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('usuario_id')->unsigned();
			$table->integer('unidad_funcional_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('unidad_funcional_usuario');
	}
}