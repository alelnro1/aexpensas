<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsorciosTable extends Migration {

	public function up()
	{
		Schema::create('consorcios', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('administrador_id')->unsigned();
			$table->string('domicilio', 255);
			$table->string('cantidad_unidades_funcionales', 255);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('consorcios');
	}
}