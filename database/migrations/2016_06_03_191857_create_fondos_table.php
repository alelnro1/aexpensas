<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFondosTable extends Migration {

	public function up()
	{
		Schema::create('fondos', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->integer('edificio_id')->unsigned();
			$table->integer('consorcio_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('fondos');
	}
}