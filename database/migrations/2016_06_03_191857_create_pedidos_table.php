<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table) {
			$table->softDeletes();
			$table->integer('unidad_funcional_id')->unsigned();
			$table->integer('estado_id')->unsigned();
			$table->increments('id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('pedidos');
	}
}