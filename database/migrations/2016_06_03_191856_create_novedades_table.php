<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNovedadesTable extends Migration {

	public function up()
	{
		Schema::create('novedades', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('administrador_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('novedades');
	}
}