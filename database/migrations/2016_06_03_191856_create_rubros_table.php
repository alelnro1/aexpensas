<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRubrosTable extends Migration {

	public function up()
	{
		Schema::create('rubros', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('rubro_id')->unsigned();
			$table->text('descripcion');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('rubros');
	}
}