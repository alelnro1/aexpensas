<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('email', 255);
			$table->integer('rol_id')->unsigned();
			$table->softDeletes();
			$table->string('foto', 255);
			$table->string('firma_digital', 255);
			$table->timestamps();
			$table->string('dni', 255);
			$table->string('direccion', 255);
			$table->rememberToken('rememberToken');
			$table->string('nombre');
			$table->text('descripcion');
			$table->string('password', 60);
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}