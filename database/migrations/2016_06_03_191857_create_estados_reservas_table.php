<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosReservasTable extends Migration {

	public function up()
	{
		Schema::create('estados_reservas', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('estados_reservas');
	}
}