<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnidadesFuncionalesTable extends Migration {

	public function up()
	{
		Schema::create('unidades_funcionales', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('consorcio_id')->unsigned();
			$table->string('numero', 255);
			$table->integer('piso');
			$table->softDeletes();
			$table->integer('cantidad_cocheras');
			$table->integer('cantidad_bauleras');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('unidades_funcionales');
	}
}