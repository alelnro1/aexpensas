<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsorciosNovedadesTable extends Migration {

	public function up()
	{
		Schema::create('consorcios_novedades', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('novedad_id')->unsigned();
			$table->integer('consorcio_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('consorcios_novedades');
	}
}