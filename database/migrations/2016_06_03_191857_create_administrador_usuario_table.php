<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdministradorUsuarioTable extends Migration {

	public function up()
	{
		Schema::create('administrador_usuario', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('administrador_usuario');
	}
}