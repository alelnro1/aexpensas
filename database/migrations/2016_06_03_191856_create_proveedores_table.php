<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProveedoresTable extends Migration {

	public function up()
	{
		Schema::create('proveedores', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('administrador_id')->unsigned();
			$table->softDeletes();
			$table->string('cuit', 255);
			$table->string('email', 255);
			$table->string('telefono', 255);
			$table->string('foto', 255);
			$table->string('razon_social');
			$table->string('direccion', 255);
			$table->string('nombre', 255);
			$table->string('apellido', 255);
			$table->string('cuil', 255);
			$table->string('dni', 255);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('proveedores');
	}
}