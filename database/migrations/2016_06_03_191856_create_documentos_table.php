<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentosTable extends Migration {

	public function up()
	{
		Schema::create('documentos', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->string('archivo', 255);
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('documentos');
	}
}