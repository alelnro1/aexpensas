<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsFondosTable extends Migration {

	public function up()
	{
		Schema::create('items_fondos', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->integer('fondo_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('items_fondos');
	}
}