<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailingTable extends Migration {

	public function up()
	{
		Schema::create('mailing', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('unidad_funcional_id')->unsigned();
			$table->string('cuerpo', 255);
			$table->string('asunto', 255);
			$table->string('destinatario', 255);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('mailing');
	}
}