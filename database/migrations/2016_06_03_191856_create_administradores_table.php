<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdministradoresTable extends Migration {

	public function up()
	{
		Schema::create('administradores', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('administradores');
	}
}