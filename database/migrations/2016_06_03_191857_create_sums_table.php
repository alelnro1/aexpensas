<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSumsTable extends Migration {

	public function up()
	{
		Schema::create('sums', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->integer('consorcio_id')->unsigned();
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sums');
	}
}