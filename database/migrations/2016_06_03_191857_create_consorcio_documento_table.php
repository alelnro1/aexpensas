<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsorcioDocumentoTable extends Migration {

	public function up()
	{
		Schema::create('consorcio_documento', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('consorcio_documento');
	}
}