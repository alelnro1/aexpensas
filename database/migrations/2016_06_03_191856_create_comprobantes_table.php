<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComprobantesTable extends Migration {

	public function up()
	{
		Schema::create('comprobantes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('unidad_funcional_id')->unsigned();
			$table->integer('estado_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('comprobantes');
	}
}