<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservasTable extends Migration {

	public function up()
	{
		Schema::create('reservas', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->integer('unidad_funcional_id')->unsigned();
			$table->integer('sum_id')->unsigned();
			$table->integer('estado_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('reservas');
	}
}