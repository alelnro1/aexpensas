<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('legajo');
            $table->string('cuil');
            $table->string('cargo');
            $table->string('categoria');
            $table->text('descripcion');
            $table->string('archivo');
            $table->string('domicilio');
            $table->string('email');
            $table->string('telefono');
            $table->integer('consorcio_id')->unsigned();
            $table->dateTime('fecha');
            $table->timestamps();
            $table->softDeletes();
    
        });
    
        Schema::table('empleados', function($table) {
            $table->foreign('consorcio_id')->references('id')->on('consorcios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleados', function(Blueprint $table) {
            $table->dropForeign('empleados_FOREIGN_ID_foreign');
        });
        
        Schema::drop('empleados');
    }
}
