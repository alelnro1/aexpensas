<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosComprobantesTable extends Migration {

	public function up()
	{
		Schema::create('estados_comprobantes', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->text('descripcion');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('estados_comprobantes');
	}
}